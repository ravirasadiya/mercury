$(function () {
	'use strict';
	
	// settings
	var url = '/admin/import/upload',
		uploadButton = $('<button/>').addClass('btn btn-primary btn-block').prop('disabled', true).text('Processing...').on('click', function(){
				var $this = $(this),
					data = $this.data();

				$this.off('click').text('Abort').on('click', function(){
					$this.remove();
					data.abort();
				});
				data.submit().always(function(){
					$this.remove();
				});
			});

	$('#fileupload').fileupload({
		url: url,
		dataType: 'json',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(txt|xls|xlsx|csv|jpeg|jpg|gif)$/i,
		maxFileSize: 5000000, // 5 MB
	})
	.on('fileuploadadd', function (e, data) {
		data.context = $('<div class="row"></div>').appendTo('#files');

		$.each(data.files, function(index, file){
			$( $('<div class="col-md-8 lh-34"></div>').text(file.name) ).appendTo(data.context);
			
			if(!index){
				$( $('<div class="col-md-4"></div>').html( uploadButton.clone(true).data(data) ) ).appendTo(data.context);
			}
			//node.appendTo(data.context);
		});
	}).on('fileuploadprocessalways', function (e, data) {
		var index = data.index,
			file = data.files[index],
			node = $(data.context.children()[index]);
		
		if (file.preview) {
			//node.prepend('<br>').prepend(file.preview);
		}
		if (file.error) {
			node.append('<br>').append($('<span class="text-danger"/>').text(file.error));
		}
		if (index + 1 === data.files.length) {
			data.context.find('button').text('Upload').prop('disabled', !!data.files.error);
		}
	}).on('fileuploadprogressall', function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress .progress-bar').css('width', progress + '%');
	}).on('fileuploaddone', function (e, data) {
		/*$.each(data.result.files, function (index, file) {
			if (file.url) {
				var link = $('<a>').attr('target', '_blank').prop('href', file.url);
				$(data.context.children()[index]).wrap(link);
			} else if (file.error) {
				var error = $('<span class="text-danger"/>').text(file.error);
				$(data.context.children()[index]).append('<br>').append(error);
			}
		});*/

		location.reload();
	}).on('fileuploadfail', function (e, data) {
		$.each(data.files, function (index) {
			var error = $('<span class="text-danger"/>').text('File upload failed.');
			$(data.context.children()[index]).append('<br>').append(error);
		});
	}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});