

// docready
$(document).ready(function(){
	$body = $("body");
	$body.addClass("loading");

	// defaults
	ajaxErrorMsg = '<div class="alert alert-danger" role="alert">Oops! There was an error processing the request.</div>';

	// tooltips
	$('[data-toggle="tooltip"]').tooltip();
	$('.datatable-class').DataTable();

	$('[data-filter-list]').filterList({
		isToggledWithClassNames: false,
		isFilteredOnKeyup: true,
		filterListAttribute: 'data-filter-list',
		selectorItem: 'label',
		classNameHidden: 'is-hidden',
		classNameShown: 'is-shown'
	});

	// validation
	$('form.validate').validationEngine();
	$('form [class*="required"]').closest('.form-group').find('label:not(:empty)').append(' <i>*</i>');

	// datepicker
	$('input.is-date, input[name*="date"]:visible').not('input[type="checkbox"], input[type="file"]').datepicker({
		format: "yyyy-mm-dd",
		weekStart: 1,
		clearBtn: true,
		orientation: "right auto",
		todayHighlight: true,
		autoclose: true
	});
	$('input.is-date, input[name*="date"]:visible').not('input[type="checkbox"], input[type="file"]').wrap('<div class="input-group"></div>').after('<span class="input-group-addon"><i class="fa fa-calendar"></i></span>');

	// select search
	$('select.is-searchable').select2({
		allowClear: true,
		placeholder: "Select..."
	});

	// confirm submit
	$('body').on('submit', '[data-confirmsubmit]', function(e){
		var frm = $(this),
			msg = frm.data('confirmsubmit');

		if(!frm.hasClass('can-submit')){
			e.preventDefault();

			// confirm
			eModal.confirm(msg).then(function(){
					frm.addClass('can-submit');
					frm.trigger('submit');
				});
		}
	});

	// cancel
	$('body').on('click', '.btn.is-cancel-btn', function(e){
		e.preventDefault();

		history.go(-1);
	});

	// click
	$('[data-onload]').each(function(i, item){
		var el = $(this),
			action = el.data('onload');

		el.trigger(action);
	});

	// get reload
	$('body').on('click', '[data-getreload]', function(e){
		var el = $(this);

		e.preventDefault();

		$.get(el.data('getreload'), function(data){
			// reload
			if(data){
				window.location.replace(data);
			}else{
				location.reload();
			}
		});
	});

	// confirm
	$('body').on('click', '.confirm-del', function(e){
		var el = $(this);

		e.preventDefault();

		eModal
			.confirm('Are you sure you want to delete this?', 'Are You Sure?')
			.then(function(){
				location.href = el.attr('href');
			}, function(){
				return;
			});
	});

	// e-modals
	$('[data-emodal]').bind('click', function(e){
		var el = $(this);

		e.preventDefault();

		eModal.ajax(el.attr('href'), el.data('emodal'));
	});

	// ckeditors
	$('textarea.wysiwyg-editor.admin').ckeditor({
		toolbar: 'Admin'
	});
	$('textarea.wysiwyg-editor.basic').ckeditor({
		toolbar: 'Basic'
	});
	$('textarea.wysiwyg-editor.spartan').ckeditor({
		toolbar: 'Spartan'
	});




	/* -------------------------------------------------------------
	 * CUSTOM SCRIPTS
	 * -------------------------------------------------------------
	 */

	// load
	$('form').on('change keyup', '[data-loadpackagelevels]', function(){
		var sel = $(this),
			packageid = sel.val(),
			packagebundleid = sel.data('loadpackagelevels'),
			url = '/admin/packagelevels/json_getpackagelevels/' + packageid + '/' + packagebundleid,
			plCBoxCont = sel.closest('form').find('.packagelevel-cbox-cont');

		// clear
		plCBoxCont.html('');

		// get
		$.get(url, function(data){
			var html = '';

			// loop
			$.each(data, function(i, row){
				var checked = (row.packagebundlelevelid > 0) ? 'checked="checked"' : '';

				html += '<label><input type="checkbox" name="plform[' + row.packagelevelid + ']" value="' + row.packagebundlelevelid + '" ' + checked + ' /> ' + row.title + '</label><br />';

				// check
				if((i+1) == data.length){
					plCBoxCont.html(html);
				}
			});
		}, 'json').fail(function() {
			//
		});
	});
	$('select[data-loadpackagelevels]').trigger('change');

	// toggle lesson content
	$('form').on('change keyup', 'select.lesson-slotnum', function(){
		var sel = $(this),
			frm = sel.closest('form');

		frm.find('.lescon-type').hide();

		if(sel.val() < 5){
			frm.find('.lescon-type').not('.lescon-type.drillog, .lescon-type.rating').show();
		}else{
			frm.find('.lescon-type.drillog, .lescon-type.rating').show();
		}
	});
	$('select.lesson-slotnum').trigger('change');

	// toggle slideshow package
	$('form').on('change keyup', 'select.slideshow-type', function(){
		var sel = $(this),
			frm = sel.closest('form');

		frm.find('select[name="form[packageid]"]').closest('.form-group').hide();

		if(sel.val() == 'package'){
			frm.find('select[name="form[packageid]"]').closest('.form-group').show();
		}
	});
	$('select.slideshow-type').trigger('change');

	// save redirect
	$('body').on('click', '.comp-save-redirect-ctn > a', function(e){
		var el = $(this),
			frm = el.closest('form');

		e.preventDefault();

		frm.find('input[name="redirecturi"]').val(el.attr('href'));

		frm.trigger('submit');
	});

	// update
	$('body').on('change keyup', 'select[name="competencygroupid"]', function(e){
		var el = $(this),
			frm = el.closest('form'),
			parSel = frm.find('select[name="form[competencyparameterid]"]'),
			uri = '/admin/competencies/json_get_competency_parameters/' + el.val();

		e.preventDefault();

		// check
		if(el.val() == ''){
			return false;
		}

		// clean
		parSel.html('');

		// get
		$.get(uri, function(data){
			// loop
			$.each(data.rs, function(i, item){
				parSel.append('<option value="' + i + '">' + item + '</option>')
			});
		}, 'json').fail(function(){
			console.log('Get group parameter error.');
		});
		//$('select[name="competencygroupid"]').trigger('change');
	});

	// confirm team change
	$('body').on('change keyup', 'select[data-confirmteamchangeid]', function(e){
		var sel = $(this),
			val = sel.val(),
			teamID = sel.data('confirmteamchangeid');

		// check
		if(teamID && teamID != val){
			// confirm
			eModal.confirm('You are about to assign this plan to a new team. Are you sure?', 'Change Alert').then(function(){
				sel.data('confirmteamchangeid', '');
				//sel.val(teamID);
			}, function(){
				sel.val(teamID);
			});
		}
	});


	// directors
    $('body').on('change keyup', 'form[name="reportform"] select[name*="arr"]', function(){
		var sel = $(this),
			frm = sel.closest('form'),
			mdSel = frm.find('select[name*="mdarr"]'),
			groupSel = frm.find('select[name*="grouparr"]'),
			smSel = frm.find('select[name*="smarr"]'),
			teamSel = frm.find('select[name*="teamarr"]'),
			spSel = frm.find('select[name*="sparr"]'),
			params = frm.serialize();

		// clean
		mdSel.html('').multiselect('rebuild');
		groupSel.html('').multiselect('rebuild');
		smSel.html('').multiselect('rebuild');
		teamSel.html('').multiselect('rebuild');
		spSel.html('').multiselect('rebuild');

		// get
		$.get('/admin/reports/json_get_report_filter_selects', params, function(data) {
			// directors
			fillRebuildSel(mdSel, data.mduserrs, data.formarr.mdarr);
			// groups
			fillRebuildSel(groupSel, data.grouprs, data.formarr.grouparr);
			// sales managers
			fillRebuildSel(smSel, data.smuserrs, data.formarr.smarr);
			// teams
			fillRebuildSel(teamSel, data.teamrs, data.formarr.teamarr);
			// sales personal
			fillRebuildSel(spSel, data.spuserrs, data.formarr.sparr);
		}, 'json').fail(function() {
			console.log('Error getting report filter selects.');
		});
	});

	var gid = $( "#group_listing" );
	var gid2 = $("#selected_group");
	var competencygrouprs_count = $("#competencygrouprs_count").val();
	var height_one = gid.height( parseInt(competencygrouprs_count*27) );
	var height_two = gid2.height( parseInt(competencygrouprs_count*27) );
	// if(height_one>height_two){
	// 	$( "#selected_group" ).height(height_one);
	// } else{
	// 	$("#group_listing").height(height_two);
	// }



	/* group selection for development suggestion details */
	$("#selected_group .selected-group").sort(function(a, b) {
		return parseInt($(a).attr('data-maincounter')) - parseInt($(b).attr('data-maincounter'));
	}).each(function() {
		var elem = $(this);
		elem.remove();
		$(elem).appendTo("#selected_group");
	});
	
	$("#groupparameters .selected-group").sort(function(a, b) {
		return parseInt($(a).attr('data-maincounter')) - parseInt($(b).attr('data-maincounter'));
	}).each(function() {
		var elem = $(this);
		elem.remove();
		$(elem).appendTo("#selected_group");
	});

	$("#mainparameters .main-parameters").sort(function(a, b) {
		return parseInt($(a).attr('data-parameterordercount')) - parseInt($(b).attr('data-parameterordercount'));
	}).each(function() {
		var elem = $(this);
		elem.remove();
		$(elem).appendTo("#mainparameters");
	});

	$('#selected_group').children('.selected-group').each(function( index ) {
		var ii_id = $( this ).attr('data-sid');
		$("[data-id="+ii_id+"]").html(parseInt(index)+1+' - ');
	});

	$("#group_listing,#selected_group").sortable({
		refreshPositions :true,
		connectWith:"#group_listing,#selected_group",
		start: function (event, ui) {

			ui.helper.css({ 'top': $(window).scrollTop() + 'px' });
			// ui.helper[0].css({ 'top': $(window).scrollTop() + 'px' });
	    },
	    stop: function (event, ui) {
	    	if(event.target.id == ui.item.parent().get(0).id){
	    		// console.log("No change");

	    		if($("#selected_group")){
					var new_grp_list = $("#selected_group").children();
					$(new_grp_list).each(function(){
						var companycompetencyplanrefid = $(this).attr('data-companycompetencyplanrefid');
						var gsid = $(this).attr('data-sid');
						var sortorder = $("span[data-id="+gsid+"]").html();
						/* call ajax */
						$.ajax({
							url: '/admin/companycompetencyplans/parameters_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
							type: 'GET',
							data: {},
							success:function(data){
								$body.removeClass("loading");
								if(data=="success"){
								} else{
									alert("Something went wrong please try again");
								}
							},error:function(error){
								alert("Something went wrong please try again");
							}
						});
					});
				}
	    	} else{
	    		// remvoe
				if(event.target.id=='selected_group'){
					var companycompetencyplanrefid = ui.item.attr('data-companycompetencyplanrefid');
					
					$.ajax({
						url: '/admin/companycompetencyplans/group_update_ajax_delete/'+companycompetencyplanrefid,
						type: 'GET',
						async: true,
						cache: false,
						data: {},
						success:function(res){
							console.log("success");
							console.log(res);
							if($("#selected_group")){
								var new_grp_list = $("#selected_group").children();
								$(new_grp_list).each(function(){
									var companycompetencyplanrefid = $(this).attr('data-companycompetencyplanrefid');
									var gsid = $(this).attr('data-sid');
									var sortorder = $("span[data-id="+gsid+"]").html();
									/* call ajax */
									$.ajax({
										url: '/admin/companycompetencyplans/parameters_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
										type: 'GET',
										data: {},
										success:function(data){
											$body.removeClass("loading");
											if(data=="success"){
											} else{
												alert("Something went wrong please try again");
											}
										},error:function(error){
											alert("Something went wrong please try again");
										}
									});
								});
							}
						}, error:function(error){
							console.log("error");	
							console.log(error);	
						}
					});
		    	}

		    	// add
		    	if(event.target.id=='group_listing'){
					var companycompetencyplanid = $("#companycompetencyplanid").val();
					var groups = ui.item.attr('data-groupid');
					var companycompetencyplanrefid = ui.item.attr('data-companycompetencyplanrefid');
					
					$.ajax({
						url: '/admin/companycompetencyplans/group_update_ajax_add/'+groups+'/'+companycompetencyplanrefid+'/'+companycompetencyplanid,
						type: 'GET',
						async: true,
						cache: false,
						data: {},
						success:function(res){
							console.log("success");
							console.log(res);
							ui.item.attr('data-companycompetencyplanrefid',res);
							if($("#selected_group")){
								var new_grp_list = $("#selected_group").children();
								$(new_grp_list).each(function(){
									var companycompetencyplanrefid = $(this).attr('data-companycompetencyplanrefid');
									var gsid = $(this).attr('data-sid');
									var sortorder = $("span[data-id="+gsid+"]").html();
									/* call ajax */
									$.ajax({
										url: '/admin/companycompetencyplans/parameters_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
										type: 'GET',
										data: {},
										success:function(data){
											$body.removeClass("loading");
											if(data=="success"){
											} else{
												alert("Something went wrong please try again");
											}
										},error:function(error){
											alert("Something went wrong please try again");
										}
									});
								});
							}
						}, error:function(error){
							console.log("error");	
							console.log(error);	
						}
					});
		    	}
	    	}
        },
        update:function(event, ui){
        	$body.addClass("loading");

        	$('#group_listing').children('.selected-group').each(function( index ) {
	    		var ii_id = $( this ).attr('data-sid');	    		
	    		var groupid = $( this ).attr('data-groupid');
	    		$("[data-id="+ii_id+"]").html('');
	    		$("[data-oldgroupid="+groupid+"]").html(groupid+'. ');
			});
	    	$('#selected_group').children('.selected-group').each(function( index ) {
	    		var ii_id = $( this ).attr('data-sid');
	    		var groupid = $( this ).attr('data-groupid');
	    		$("[data-id="+ii_id+"]").html(parseInt(index)+1+' - ');
	    		$("[data-oldgroupid="+groupid+"]").html('('+groupid+').');
			});
	    	
	    	$("#group_listing .selected-group").sort(function(a, b) {
			  return parseInt($(a).attr('data-groupid')) - parseInt($(b).attr('data-groupid'));
			}).each(function() {
				var elem = $(this);
				elem.remove();
				$(elem).appendTo("#group_listing");
			});

	    	$body.removeClass("loading");
        },
        sort: function (event, ui) {
            ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
        },
        create:function(event, ui){
        	// console.log(ui);
        	// ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
        }
	});

	$("#group_listing,#selected_group").disableSelection();

	/* parameters box size set on page load time */
	$('.main-parameters').each(function() {
		var sid = $( this ).attr('data-sid');
		var height_one = $(".parameters_listing_first_"+sid).height();
		var height_two = $(".parameters_listing_second_"+sid).height();
		if(height_one>height_two){
			$(".parameters_listing_second_"+sid).height(height_one);
		} else{
			$(".parameters_listing_first_"+sid).height(height_two);
		}
	});

	/* set every parameters box as sortable */
	$('.main-parameters').each(function() {
		var sid = $( this ).attr('data-sid');

		var pid = $(".parameters_listing_first_"+sid);
		var pid2 = $(".parameters_listing_second_"+sid);
		var parameters_count = $(".parameters_listing_first_"+sid+"_count").val();
		if(parameters_count==0){
			parameters_count = 1;
		}
		var height_one = pid.height( parseInt(parameters_count*27) );
		var height_two = pid2.height( parseInt(parameters_count*27) );

		$(".parameters_listing_second_"+sid+" .selected-group-"+sid).sort(function(a, b) {
		  return parseInt($(a).attr('data-maincounter')) - parseInt($(b).attr('data-maincounter'));
		}).each(function() {
			var elem = $(this);
			elem.remove();
			$(elem).appendTo(".parameters_listing_second_"+sid);
		});

		$(".parameters_listing_second_"+sid).children('.selected-group-'+sid).each(function( index ) {
    		var competencyparameterid = $( this ).attr('data-competencyparameterid');
    		var companycompetencyplanrefid = $(this).attr("data-companycompetencyplanrefid");
    		var counter = $( this ).attr('data-counter');
    		var sid = $( this ).attr('data-sid');
    		// var oldparahtml = $("span[data-competencyparameterid="+companycompetencyplanrefid+"]").html();
    		// $("span[data-competencyparameterid="+competencyparameterid+"]").html(oldparahtml);
    		var mainid = $("span[data-mainid="+competencyparameterid+"]").html(counter+'.');
    		$("span[data-subid="+competencyparameterid+"]").attr('data-pcounter',parseInt(index)+1);
    		// $("span[data-subid="+competencyparameterid+"]").html(parseInt(index)+1+" ("+oldparahtml+")"+" - ");
		});

		$(".parameters_listing_first_"+sid+",.parameters_listing_second_"+sid).sortable({
			refreshPositions :true,
			connectWith:".parameters_listing_first_"+sid+",.parameters_listing_second_"+sid,
			start: function (event, ui) {
	            // ui.item.toggleClass("highlight");
		    },
		    stop: function (event, ui) {
		    	console.log("stop");
		    	if($(event.target).hasClass('parameters_listing_first_'+sid)==true && $(ui.item.parent().get(0)).hasClass('parameters_listing_first_'+sid)){
		    		console.log("No change");
		    	} else if($(event.target).hasClass('parameters_listing_second_'+sid)==true && $(ui.item.parent().get(0)).hasClass('parameters_listing_second_'+sid)){
		    		console.log("No change second");
		    		if($(".parameters_listing_second_"+sid)){
						var new_para_list = $(".parameters_listing_second_"+sid).children();
						$(new_para_list).each(function(){
							var companycompetencyplanrefid = $(this).attr('data-companycompetencyplanrefid');
							var cpid = $(this).attr('data-competencyparameterid');
							var sortorder = $("span[data-subid="+cpid+"]").attr('data-pcounter');
							/* call ajax */
							$.ajax({
								url: '/admin/companycompetencyplans/parameters_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
								type: 'GET',
								data: {},
								success:function(data){
									$body.removeClass("loading");
									if(data=="success"){
									} else{
										alert("Something went wrong please try again");
									}
								},error:function(error){
									alert("Something went wrong please try again");
								}
							});
						});
					}
		    	} else{
					$body.addClass("loading");

					/* add */
					if($(this).hasClass('parameters_listing_first_'+sid)){
						var companycompetencyplanid = $("#companycompetencyplanid").val();
						var competencygroupid = ui.item.attr('data-competencygroupid');
						var competencyparameterid = ui.item.attr('data-competencyparameterid');
						var new_sid = ui.item.attr('data-sid');
						var sortorder = $("span[data-masterid="+new_sid+"]").html();
						$.ajax({
							url: '/admin/companycompetencyplans/parameters_update_ajax_add/'+competencygroupid+'/'+competencyparameterid+'/'+companycompetencyplanid+'/'+sortorder,
							type: 'GET',
							data: {},
							async:true,
							success:function(res){
								console.log("success");
								$("[data-competencyparameterid="+competencyparameterid+"]").attr('data-companycompetencyplanrefid',res);
								if($(".parameters_listing_second_"+sid)){
									var new_para_list = $(".parameters_listing_second_"+sid).children();
									$(new_para_list).each(function(){
										var companycompetencyplanrefid = $(this).attr('data-companycompetencyplanrefid');
										var cpid = $(this).attr('data-competencyparameterid');
										var sortorder = $("span[data-subid="+cpid+"]").attr('data-pcounter');
										/* call ajax */
										$.ajax({
											url: '/admin/companycompetencyplans/parameters_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
											type: 'GET',
											data: {},
											success:function(data){
												$body.removeClass("loading");
												if(data=="success"){
												} else{
													alert("Something went wrong please try again");
												}
											},error:function(error){
												alert("Something went wrong please try again");
											}
										});
									});
								}
							}, error:function(error){
								console.log("error");	
								console.log(error);	
							}
						});
					}

					/* remove */
					if($(this).hasClass('parameters_listing_second_'+sid)){

						var companycompetencyplanrefid = ui.item.attr('data-companycompetencyplanrefid');
					
						$.ajax({
							url: '/admin/companycompetencyplans/parameters_update_ajax_delete/'+companycompetencyplanrefid,
							type: 'GET',
							data: {},
							async:true,
							success:function(res){
								console.log("success");
								console.log(res);
								if($("#selected_group")){
									var new_grp_list = $("#selected_group").children();
									$(new_grp_list).each(function(){
										var companycompetencyplanrefid = $(this).attr('data-companycompetencyplanrefid');
										var gsid = $(this).attr('data-sid');
										var sortorder = $("span[data-id="+gsid+"]").html();
										/* call ajax */
										$.ajax({
											url: '/admin/companycompetencyplans/parameters_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
											type: 'GET',
											data: {},
											success:function(data){
												$body.removeClass("loading");
												if(data=="success"){
												} else{
													alert("Something went wrong please try again");
												}
											},error:function(error){
												alert("Something went wrong please try again");
											}
										});
									});
								}
							}, error:function(error){
								console.log("error");	
								console.log(error);	
							}
						});
					}

					$body.removeClass("loading");
				}
	        },
	        update:function(event, ui){
	        	console.log("update");
	        	$(".parameters_listing_first_"+sid).children('.selected-group-'+sid).each(function( index ) {
		    		var sid = $( this ).attr('data-sid');
		    		// var oldparahtml = $("span[data-competencyparameterid="+companycompetencyplanrefid+"]").html();
		    		var competencyparameterid = $( this ).attr('data-competencyparameterid');
		    		var competencygroupid = $( this ).attr('data-competencygroupid');
		    		$("span[data-masterid="+sid+"]").html('');
		    		$("span[data-competencyparameterid="+competencyparameterid+"]").html(competencygroupid+'.'+competencyparameterid+'. ');
		    		$("span[data-subid="+competencyparameterid+"]").html('');
				});

				$(".parameters_listing_second_"+sid).children('.selected-group-'+sid).each(function( index ) {
		    		var competencyparameterid = $( this ).attr('data-competencyparameterid');
		    		// var companycompetencyplanrefid = $( this ).attr('data-companycompetencyplanrefid');
		    		var counter = $( this ).attr('data-counter');
		    		var sid = $( this ).attr('data-sid');
		    		var competencygroupid = $( this ).attr('data-competencygroupid');
		    		var mainid = $("span[data-mainid="+competencyparameterid+"]").html(counter+'.');

		    		$("span[data-competencyparameterid="+competencyparameterid+"]").html('');
		    		$("span[data-subid="+competencyparameterid+"]").attr('data-pcounter',parseInt(index)+1);
		    		$("span[data-subid="+competencyparameterid+"]").html(parseInt(index)+1+" - ("+competencygroupid+"."+competencyparameterid+") ");
				});

				// $(".parameters_listing_second_"+sid+" .selected-group-"+sid).sort(function(a, b) {
				//   return parseInt($(a).attr('data-competencyparameterid')) - parseInt($(b).attr('data-competencyparameterid'));
				// }).each(function() {
				// 	var elem = $(this);
				// 	elem.remove();
				// 	$(elem).appendTo(".parameters_listing_second_"+sid);
				// });
	        },
	        sort: function (event, ui) {
	            ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
	        },
		});

		$(".parameters_listing_first_"+sid+",.parameters_listing_second_"+sid).disableSelection();
	});

	$(".selected-suggestion").each(function(index, el) {
		var suggestionboxgroupparaid = $(el).attr('data-suggestionboxgroupparaid');
		$(".selected-suggestion-"+suggestionboxgroupparaid).children().sort(function(a, b) {
			return parseInt($(a).attr('data-sortablenewsuggestioncount')) - parseInt($(b).attr('data-sortablenewsuggestioncount'));
		}).each(function(index, el) {
			var elem = $(this);
			elem.remove();
			$(elem).appendTo(".selected-suggestion-"+suggestionboxgroupparaid);	
		});

		$(".selected-suggestion-"+suggestionboxgroupparaid).each(function( index ) {
			var suggcompetencysuggestionid = $( this ).attr('data-suggcompetencysuggestionid');
			var spansuggestionidobj = $( this ).find('span[data-suggcompetencysuggestionid='+suggcompetencysuggestionid+']').get(0);
			var suggestionboxgroupidnew = $( this ).attr('data-suggestionboxgroupidnew');
			var suggestionboxparaidnew = $( this ).attr('data-suggestionboxparaidnew');
			// var suggestionboxgroupparaid =$( this ).attr('data-suggestionboxgroupparaid');
			// var mainsuggestionboxobj = $( this ).children('[data-sugboxuniqueid='+suggestionboxgroupparaid+']').get(0);
			// update group id
			$(this).children().each(function(index, el) {
				var newsid = $(el).attr('data-sid');
				var labelobj =  $(el).children('label').get(0);
				var spansugggroupid = $(labelobj).find('span[data-spansugggroupid]').get(0);
				var spansuggparameterid = $(labelobj).find('span[data-spansuggparameterid]').get(0);
				var suggcompetencysuggestionid = $(labelobj).find('span[data-suggcompetencysuggestionid]').get(0);
				$(spansugggroupid).html(suggestionboxgroupidnew+'.');
				$(spansuggparameterid).html(suggestionboxparaidnew+'.');
				$(suggcompetencysuggestionid).attr('data-newsuggestioncount',(parseInt(index)+1));
				$(suggcompetencysuggestionid).attr('data-suggnumberfinder',newsid);
				$(suggcompetencysuggestionid).html((parseInt(index)+1));
			});
		});
	});
	// data-sortablenewsuggestioncount
	/* suggestion  */
	$('.main-suggestions').each(function() {	
		var suggestiongrouporder = $(this).attr('data-suggestiongrouporder');
		var suggestiongroupid = $(this).attr('data-suggestiongroupid');		

		$(".select-suggestion-"+suggestiongroupid).each(function(index, el) {
			var subsuggestions = $(this).attr('data-subsuggestions');
			var gpsuggestionid = suggestiongrouporder+subsuggestions;
			var parasuggestionscount = $(".parasuggestionscount_"+gpsuggestionid+"_count").val();

			if(parasuggestionscount==0){
				parasuggestionscount = 1;
			}
			var newheight = parseInt(parasuggestionscount*29);
			$("[data-suggestionboxgroupparaid="+gpsuggestionid+"]").height(newheight);

			/* suggestion sorting methods */
			$(".available-suggestion-"+gpsuggestionid+",.selected-suggestion-"+gpsuggestionid).sortable({
				refreshPositions :true,
				connectWith:".available-suggestion-"+gpsuggestionid+",.selected-suggestion-"+gpsuggestionid,
				start: function (event, ui) {
		            // ui.item.toggleClass("highlight");
			    },
			    stop: function (event, ui) {
			    	console.log("stop suggestion");
			    	if($(event.target).hasClass("available-suggestion-"+gpsuggestionid)==true && $(ui.item.parent().get(0)).hasClass("available-suggestion-"+gpsuggestionid)){
		    			console.log("No change");
	    			} else if($(event.target).hasClass("selected-suggestion-"+gpsuggestionid)==true && $(ui.item.parent().get(0)).hasClass("selected-suggestion-"+gpsuggestionid)){
	    				console.log("change position");

	    				if($('.selected-suggestion-'+gpsuggestionid)){
							var new_sugg_list = $('.selected-suggestion-'+gpsuggestionid).children();
							console.log("dfgdfgfdg");
							console.log(new_sugg_list);
							$(new_sugg_list).each(function(){
								var companycompetencyplanrefid = $(this).attr('data-suggcompanycompetencyplanref');
								var newsid = $(this).attr('data-sid');
								var cpid = $(this).attr('data-suggcompetencyparameterid');
								var sortorder = $("span[data-suggnumberfinder="+newsid+"]").attr('data-newsuggestioncount');
								/* call ajax */
								$.ajax({
									url: '/admin/companycompetencyplans/suggestion_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
									type: 'GET',
									data: {},
									success:function(data){
										$body.removeClass("loading");
										if(data=="success"){
										} else{
											alert("Something went wrong please try again");
										}
									},error:function(error){
										alert("Something went wrong please try again");
									}
								});
							});
						}
	    			} else{
	    				$body.addClass("loading");
							console.log("Add/Remove suggestions");

							// add in suggestions
							if($(this).hasClass('available-suggestion-'+gpsuggestionid)){
								console.log("Add suggestion");
								var companycompetencyplanid = $("#companycompetencyplanid").val();
								var competencygroupid = ui.item.attr('data-suggcompetencygroupid');
								var competencyparameterid = ui.item.attr('data-suggcompetencyparameterid');
								var competencysuggestionid = ui.item.attr('data-suggcompetencysuggestionid');
								var sugboxuniqueid = ui.item.attr('data-sugboxuniqueid');
								var newsuggestionobj = $(ui.item).find('span[data-newsuggestioncount]').get(0);
								var newsuggestioncount = $(newsuggestionobj).attr('data-newsuggestioncount');
								// companygroupparamsuggrefid
								// sortorder
								$.ajax({
									url: '/admin/companycompetencyplans/suggestion_update_ajax_add/'+competencygroupid+'/'+competencyparameterid+'/'+competencysuggestionid+'/'+companycompetencyplanid+'/'+newsuggestioncount,
									type: 'GET',
									data: {},
									async:true,
									success:function(data){
										console.log("data");
										var selectobj = $('.selected-suggestion-'+sugboxuniqueid).get(0);
										var selectsuggestion = $(selectobj).find('[data-suggcompetencysuggestionid='+competencysuggestionid+']').get(0);										
										$(selectsuggestion).attr('data-suggcompanycompetencyplanref',data);

										if($('.selected-suggestion-'+sugboxuniqueid)){
											var new_sugg_list = $('.selected-suggestion-'+sugboxuniqueid).children();
											$(new_sugg_list).each(function(){
												var companycompetencyplanrefid = $(this).attr('data-suggcompanycompetencyplanref');
												var newsid = $(this).attr('data-sid');
												var cpid = $(this).attr('data-suggcompetencyparameterid');
												var sortorder = $("span[data-suggnumberfinder="+newsid+"]").attr('data-newsuggestioncount');
												/* call ajax */
												$.ajax({
													url: '/admin/companycompetencyplans/suggestion_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
													type: 'GET',
													data: {},
													success:function(data){
														$body.removeClass("loading");
														if(data=="success"){
														} else{
															alert("Something went wrong please try again");
														}
													},error:function(error){
														alert("Something went wrong please try again");
													}
												});
											});
										}

									},error:function(error){
										console.log("error");
										console.log(error);
									}
								});
							}
							// remove from suggestions
							if($(this).hasClass('selected-suggestion-'+gpsuggestionid)){

								var companycompetencyplanrefid = ui.item.attr('data-suggcompanycompetencyplanref');
					
								$.ajax({
									url: '/admin/companycompetencyplans/suggestion_update_ajax_delete/'+companycompetencyplanrefid,
									type: 'GET',
									data: {},
									async:true,
									success:function(res){
										console.log("success");
										console.log(res);
										if($('.selected-suggestion-'+gpsuggestionid)){
											var new_sugg_list = $('.selected-suggestion-'+gpsuggestionid).children();
											$(new_sugg_list).each(function(){
												var companycompetencyplanrefid = $(this).attr('data-suggcompanycompetencyplanref');
												var newsid = $(this).attr('data-sid');
												var cpid = $(this).attr('data-suggcompetencyparameterid');
												var sortorder = $("span[data-suggnumberfinder="+newsid+"]").attr('data-newsuggestioncount');
												/* call ajax */
												$.ajax({
													url: '/admin/companycompetencyplans/suggestion_sort_ajax/'+companycompetencyplanrefid+'/'+sortorder,
													type: 'GET',
													data: {},
													success:function(data){
														$body.removeClass("loading");
														if(data=="success"){
														} else{
															alert("Something went wrong please try again");
														}
													},error:function(error){
														alert("Something went wrong please try again");
													}
												});
											});
										}
									}, error:function(error){
										console.log("error");	
										console.log(error);	
									}
								});
							}
	    				$body.removeClass("loading");
	    			}
			    },
			    update:function(event, ui){
		        	console.log("suggestion update event");
		        	$(".available-suggestion-"+gpsuggestionid).each(function( index ) {
		        		var suggcompetencysuggestionid = ui.item.attr('data-suggcompetencysuggestionid');
		        		var spansuggestionidobj = ui.item.find('span[data-suggcompetencysuggestionid='+suggcompetencysuggestionid+']').get(0);
		        		var spangroupid = $(spansuggestionidobj).attr('data-suggcompetencygroupid');
		        		var spanparameterid = $(spansuggestionidobj).attr('data-suggcompetencyparameterid');
		        		var spansuggestionid = $(spansuggestionidobj).attr('data-suggcompetencysuggestionid');
		        		var spangroupidobj = ui.item.find('span[data-spansugggroupid]').get(0);
		        		var spanparameteridobj = ui.item.find('span[data-spansuggparameterid]').get(0);
		        		var oldsuggparagroupidobj = ui.item.find('span[data-oldsuggparagroupid]').get(0);

		        		$(spangroupidobj).html(spangroupid+'.');
		        		$(spanparameteridobj).html(spanparameterid+'.');
		        		$(spansuggestionidobj).html(spansuggestionid);
		        		$(oldsuggparagroupidobj).html('');
					});

					$(".selected-suggestion-"+gpsuggestionid).each(function( index ) {
						var suggcompetencysuggestionid = ui.item.attr('data-suggcompetencysuggestionid');
						var spansuggestionidobj = ui.item.find('span[data-suggcompetencysuggestionid='+suggcompetencysuggestionid+']').get(0);
						var suggestionboxgroupidnew = $( this ).attr('data-suggestionboxgroupidnew');
						var suggestionboxparaidnew = $( this ).attr('data-suggestionboxparaidnew');
						// var suggestionboxgroupparaid =$( this ).attr('data-suggestionboxgroupparaid');
						// var mainsuggestionboxobj = $( this ).children('[data-sugboxuniqueid='+suggestionboxgroupparaid+']').get(0);
						// update group id
						$(this).children().each(function(index, el) {
							var newsid = $(el).attr('data-sid');
							var competencygroupid = $(el).attr('data-suggcompetencygroupid');
							var competencyparameterid = $(el).attr('data-suggcompetencyparameterid');
							var competencysuggestionid = $(el).attr('data-suggcompetencysuggestionid');

							var labelobj =  $(el).children('label').get(0);
							var spansugggroupid = $(labelobj).find('span[data-spansugggroupid]').get(0);
							var spansuggparameterid = $(labelobj).find('span[data-spansuggparameterid]').get(0);
							var suggcompetencysuggestionid = $(labelobj).find('span[data-suggcompetencysuggestionid]').get(0);
							var oldsuggparagroupidobj = $(labelobj).find('span[data-oldsuggparagroupid]').get(0);

							$(spansugggroupid).html(suggestionboxgroupidnew+'.');
							$(spansuggparameterid).html(suggestionboxparaidnew+'.');
							$(suggcompetencysuggestionid).attr('data-newsuggestioncount',(parseInt(index)+1));
							$(suggcompetencysuggestionid).attr('data-suggnumberfinder',newsid);
							$(suggcompetencysuggestionid).html((parseInt(index)+1));
							$(oldsuggparagroupidobj).html('('+competencygroupid+'.'+competencyparameterid+'.'+competencysuggestionid+')');
						});
					});
				},
				sort: function (event, ui) {
		            ui.helper.css({ 'top': ui.position.top + $(window).scrollTop() + 'px' });
		            ui.helper.css({ 'left': ui.position.left + $(window).scrollLeft() + 'px' });
		        },
			});
			$(".available-suggestion-"+gpsuggestionid+",.selected-suggestion-"+gpsuggestionid).disableSelection();
		});
	});

	$("body").on('change', '.fileuploader', function(event) {
		var fullPath = $(this).val();    
		var filename = fullPath.replace(/^.*[\\\/]/, '');
		var extension = fullPath.substr( (fullPath.lastIndexOf('.') +1) );
		var filename = filename.substr(0, filename.lastIndexOf('.')) || filename;

		$(".filename").val(filename);
		$(".fileextension").val(extension);

		event.preventDefault();
		/* Act on the event */
	});

	$body.removeClass("loading");
});

// fill select and rebuild
function fillRebuildSel(sel, data, selArr)
{
	// loop
	$.each(data, function(groupName, options) {
		var optGroup = $("<optgroup>", { label: groupName });

		optGroup.appendTo(sel);

		// loop
		$.each(options, function(id, val) {
			var selected = (selArr && selArr.includes(id)) ? true : false,
				option = $("<option>", { value: id, text: val, selected: selected });

			option.appendTo(optGroup);
		});
	});
	sel.multiselect('rebuild');
}

// onload
$(window).ready(function(){
	// check
	if($('.sort-cnt').length > 0){
		// loop
		$.each($('.sort-cnt'), function(i, item){
			var div = $(item);

			div.find(' > .well,  > div.checkbox').sort(function(a,b){
				return a.dataset.sid > b.dataset.sid
			}).appendTo(div);
		});
	}

	if($('form .trigger-onload-change').length > 0){
		$('form .trigger-onload-change').trigger('change');
	}
});

$("body").on('mouseenter click','.ui-sortable-handle',function(){
	
});