/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/*CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};*/


CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbar_Admin = [
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat' ] },
		{ name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
		{ name: 'links', items: [ 'Link', 'Unlink' ] },
		{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Youtube' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
		{ name: 'styles', items: [ 'Styles', 'Format' ] },
		{ name: 'clipboard', items: [ 'Undo', 'Redo' ] },
		{ name: 'document', items: [ 'Source' ] }
	];
	config.toolbar_Basic = [
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
		{ name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
		{ name: 'links', items: [ 'Link', 'Unlink' ] },
		{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'SpecialChar', 'Youtube' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
		{ name: 'styles', items: [ 'Styles', 'Format' ] },
		{ name: 'clipboard', items: [ 'Undo', 'Redo' ] }
	];
	config.toolbar_Spartan = [
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
		{ name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
		{ name: 'clipboard', items: [ 'Undo', 'Redo' ] }
	];
	

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';
	
	// disallowefd
	//config.disallowedContent = 'img{width,height}';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	//config.removeDialogTabs = 'image:advanced;link:advanced';
	
	// kcfinder
	kcPath = '/themes/admin/custom/js/kcfinder-3.12/';
	config.filebrowserBrowseUrl = kcPath + 'browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = kcPath + 'browse.php?opener=ckeditor&type=images';
	config.filebrowserFlashBrowseUrl = kcPath + 'browse.php?opener=ckeditor&type=flash';
	config.filebrowserUploadUrl = kcPath + 'upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl = kcPath + 'upload.php?opener=ckeditor&type=images';
	config.filebrowserFlashUploadUrl = kcPath + 'upload.php?opener=ckeditor&type=flash';
	
	// plugins	
	/*config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
	config.toolbar_Admin.push({ name: 'wiris', items : [ 'ckeditor_wiris_formulaEditor' ]});
	config.toolbar_Basic.push({ name: 'wiris', items : [ 'ckeditor_wiris_formulaEditor' ]});*/
	
	config.skin = 'office2013';
	
	
	//config.extraPlugins += ',base64image';
	config.extraPlugins += ',codemirror';
	config.extraPlugins += ',youtube';
	config.extraPlugins += ',autogrow';
	config.extraPlugins += ',justify';
	config.extraPlugins += ',liststyle';
	//config.extraPlugins += ',floating-tools';
	
	config.stylesSet = 'my_styles';

};


