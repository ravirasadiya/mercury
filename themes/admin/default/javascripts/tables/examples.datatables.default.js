/*
Name: 			Tables / Advanced - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.6.0
*/

(function($) {

	'use strict';

	var datatableInit = function() {

		$('#datatable-default').dataTable({
			"lengthMenu": [ [25, 50, 100, 150, 250, -1], [25, 50, 100, 150, 250, "All"] ],
			"pageLength": 150
		});

	};

	$(function() {
		datatableInit();
	});

}).apply(this, [jQuery]);
