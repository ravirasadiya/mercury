<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companygroupparamsuggref_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companygroupparamsuggrefid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cgpsr.*
			';
		$this->db->select($sql);
		$this->db->from('companygroupparamsuggref cgpsr');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($companygroupparamsuggrefid){
			$this->db->where('cgpsr.companygroupparamsuggrefid', $companygroupparamsuggrefid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cgpsr.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('cgpsr.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companygroupparamsuggrefid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}

	function get_new_groups_list($companygroupparamsuggrefid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cgpsr.*,
				ccpr.sortorder
			';
		$this->db->select($sql);
		$this->db->from('companygroupparamsuggref cgpsr');
		$this->db->join('companycompetencyplanref ccpr', 'ccpr.companygroupparamsuggrefid=cgpsr.companygroupparamsuggrefid', 'left');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($companygroupparamsuggrefid){
			$this->db->where('cgpsr.companygroupparamsuggrefid', $companygroupparamsuggrefid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cgpsr.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('cgpsr.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companygroupparamsuggrefid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}

	// save
	function save($arr, $companygroupparamsuggrefid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// set
		$rs = null;
		$arr['incnum'] = 1;

		// get last
		if(isset($arr['competencygroupid'])){
			// get
			$rs = $this->get(null, array('cgpsr.companyid' => $arr['companyid']), array('where-str' => 'cgpsr.competencygroupid IS NOT NULL', 'order' => array('incnum' => 'DESC')), true);
		}
		if(isset($arr['competencyparameterid'])){
			// get
			$rs = $this->get(null, array('cgpsr.companyid' => $arr['companyid'], 'cgpsr.competencygroupid' => $arr['competencygroupid']), array('where-str' => 'cgpsr.competencyparameterid IS NOT NULL', 'order' => array('incnum' => 'DESC')), true);
		}
		if(isset($arr['competencysuggestionid'])){
			// get
			$rs = $this->get(null, array('cgpsr.companyid' => $arr['companyid'], 'cgpsr.competencygroupid' => $arr['competencygroupid'], 'cgpsr.competencyparameterid' => $arr['competencyparameterid']), array('where-str' => 'cgpsr.competencysuggestionid IS NOT NULL', 'order' => array('incnum' => 'DESC')), true);
		}
		$arr['incnum'] = ($rs) ? $rs['incnum']+1 : $arr['incnum'];

		// insert/update
		if(!$companygroupparamsuggrefid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companygroupparamsuggref', $arr);
			$companygroupparamsuggrefid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companygroupparamsuggrefid', $companygroupparamsuggrefid);
			$this->db->update('companygroupparamsuggref', $arr);
		}

		return $companygroupparamsuggrefid;
	}


	// delete
	function delete($companygroupparamsuggrefid, $perm=false)
	{
		if($perm){
			$this->db->where('companygroupparamsuggrefid', $companygroupparamsuggrefid);
			$this->db->delete('companygroupparamsuggref');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companygroupparamsuggrefid);
		}
	}


	// get keyval groups
	public function get_keyval_groups($companyid, $retarr=array())
	{
		// get
		$rs = $this->get_new_groups_list(null, array('cgpsr.companyid' => $companyid), array('where-str' => '(cgpsr.competencyparameterid IS NULL AND cgpsr.competencysuggestionid IS NULL)'));
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($rs as $row){
			$retarr[$row['competencygroupid']] = $row['incnum'];
		}

		return $retarr;
	}


	// get keyval params
	public function get_keyval_params($companyid, $retarr=array())
	{
		// get
		$rs = $this->get_new_groups_list(null, array('cgpsr.companyid' => $companyid), array('where-str' => '(cgpsr.competencyparameterid IS NOT NULL AND cgpsr.competencysuggestionid IS NULL)'));

		// loop
		foreach($rs as $row){
			$retarr[$row['competencygroupid']][$row['competencyparameterid']] = $row['incnum'];
		}

		return $retarr;
	}


	// get keyval suggestions
	public function get_keyval_suggs($companyid, $retarr=array())
	{
		// get
		$rs = $this->get_new_groups_list(null, array('cgpsr.companyid' => $companyid), array('where-str' => '(cgpsr.competencysuggestionid IS NOT NULL)'));

		// loop
		foreach($rs as $row){
			$retarr[$row['competencygroupid']][$row['competencyparameterid']][$row['competencysuggestionid']] = $row['incnum'];
		}

		return $retarr;
	}
}

/* End of file companygroupparamsuggref_model.php */
/* Location: ./application/models/companygroupparamsuggref_model.php */
