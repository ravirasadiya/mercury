<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Usersessionratingitem_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($usersessionratingitemid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				usri.*,
				sri.sessionratingitemid,
				sri.title AS rating,
				cs.salespersonaluserid,
				CONCAT(u.firstname, " ", u.lastname) AS fullname
			';
		$this->db->select($sql);
		$this->db->from('usersessionratingitem usri');
		$this->db->join('user u', 'u.userid=usri.userid', 'left');
		$this->db->join('coachingsession cs', 'cs.coachingsessionid=usri.coachingsessionid', 'left');
		$this->db->join('companysessionratingref csrr', 'csrr.companysessionratingrefid=usri.companysessionratingrefid', 'left');
		$this->db->join('sessionratingitem sri', 'sri.sessionratingitemid=csrr.sessionratingitemid', 'left');
		// where
		if($usersessionratingitemid){
			$this->db->where('usri.usersessionratingitemid', $usersessionratingitemid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('usri.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('usri.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usersessionratingitemid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $usersessionratingitemid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$usersessionratingitemid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('usersessionratingitem', $arr);
			$usersessionratingitemid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('usersessionratingitemid', $usersessionratingitemid);
			$this->db->update('usersessionratingitem', $arr);
		}

		return $usersessionratingitemid;
	}


	// delete
	function delete($usersessionratingitemid, $perm=false)
	{
		if($perm){
			$this->db->where('usersessionratingitemid', $usersessionratingitemid);
			$this->db->delete('usersessionratingitem');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $usersessionratingitemid);
		}
	}
}

/* End of file usersessionratingitem_model.php */
/* Location: ./application/models/usersessionratingitem_model.php */
