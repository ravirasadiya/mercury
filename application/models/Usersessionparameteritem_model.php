<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Usersessionparameteritem_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($usersessionparameteritemid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				uspi.*,
				ccpr.competencygroupid,
				ccpr.competencyparameterid,
				ccpr.competencysuggestionid,
				(SELECT ROUND(AVG(uspiavg.value), 2)
					FROM usersessionparameteritem uspiavg
					LEFT JOIN companycompetencyplanref ccpravg ON ccpravg.companycompetencyplanrefid=uspiavg.companycompetencyplanrefid
					WHERE
						uspiavg.coachingsessionid=uspi.coachingsessionid AND ccpravg.competencygroupid=ccpr.competencygroupid
				) AS group_avg,
				(SELECT ROUND(SUM(uspiavg.value), 2)
					FROM usersessionparameteritem uspiavg
					LEFT JOIN companycompetencyplanref ccpravg ON ccpravg.companycompetencyplanrefid=uspiavg.companycompetencyplanrefid
					WHERE
						uspiavg.coachingsessionid=uspi.coachingsessionid AND ccpravg.competencygroupid=ccpr.competencygroupid
				) AS group_total,
				cg.title AS group,
				cp.title As parameter,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS group_index,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid=ccpr.competencyparameterid AND cgpsr.competencysuggestionid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS param_index
			';
		$this->db->select($sql);
		$this->db->from('usersessionparameteritem uspi');
		$this->db->join('coachingsession cs', 'cs.coachingsessionid=uspi.coachingsessionid', 'left');
		$this->db->join('user smu', 'smu.userid=cs.salesmanageruserid', 'left');
		$this->db->join('companycompetencyplanref ccpr', 'ccpr.companycompetencyplanrefid=uspi.companycompetencyplanrefid', 'left');
		$this->db->join('competencygroup cg', 'cg.competencygroupid=ccpr.competencygroupid', 'left');
		$this->db->join('competencyparameter cp', 'cp.competencyparameterid=ccpr.competencyparameterid', 'left');
		// where
		if($usersessionparameteritemid){
			$this->db->where('uspi.usersessionparameteritemid', $usersessionparameteritemid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('uspi.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		$this->db->order_by('group_index');
		$this->db->order_by('param_index');
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usersessionparameteritemid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $usersessionparameteritemid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$usersessionparameteritemid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('usersessionparameteritem', $arr);
			$usersessionparameteritemid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('usersessionparameteritemid', $usersessionparameteritemid);
			$this->db->update('usersessionparameteritem', $arr);
		}

		return $usersessionparameteritemid;
	}


	// delete
	function delete($usersessionparameteritemid, $perm=false)
	{
		if($perm){
			$this->db->where('usersessionparameteritemid', $usersessionparameteritemid);
			$this->db->delete('usersessionparameteritem');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $usersessionparameteritemid);
		}
	}


	// get key row array
	function get_keyrow_arr($where=null, $other=null, $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$retarr[$row['competencygroupid']]['avg'] = $row['group_avg'];
			$retarr[$row['competencygroupid']]['pararr'][$row['competencyparameterid']] = $row;
		}

		return $retarr;
	}
}

/* End of file usersessionparameteritem_model.php */
/* Location: ./application/models/usersessionparameteritem_model.php */
