<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activityusage_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($activityusageid=null,$where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
			au.*
		';	
		$this->db->select($sql);
		$this->db->from('activityusage au');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($activityusageid){
			$this->db->where('au.activityusageid', $activityusageid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		// $this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($activityusageid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
		
		return $rs;
	}

	// save
	function save($arr, $activityusageid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$activityusageid){
			// $arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('activityusage', $arr);
			$activityusageid = $this->db->insert_id();
		}else{
			// $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('activityusageid', $activityusageid);
			$this->db->update('activityusage', $arr);
		}

		return $activityusageid;
	}


	// delete
	function delete($activityusageid, $perm=false)
	{
		if($perm){
			$this->db->where('activityusageid', $activityusageid);
			$this->db->delete('activityusage');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $activityusageid);
		}
	}
}

/* End of file Activityusage_model.php */
/* Location: ./application/models/Activityusage_model.php */
