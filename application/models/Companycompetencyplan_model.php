<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companycompetencyplan_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companycompetencyplanid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				ccp.*,
				cut.companyid,
				cut.title As team
			';
		$this->db->select($sql);
		$this->db->from('companycompetencyplan ccp');
		$this->db->join('companyuserteam cut', 'cut.companyuserteamid=ccp.companyuserteamid', 'left');
		// where
		if($companycompetencyplanid){
			$this->db->where('ccp.companycompetencyplanid', $companycompetencyplanid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('ccp.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('ccp.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companycompetencyplanid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companycompetencyplanid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companycompetencyplanid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companycompetencyplan', $arr);
			$companycompetencyplanid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companycompetencyplanid', $companycompetencyplanid);
			$this->db->update('companycompetencyplan', $arr);
		}

		return $companycompetencyplanid;
	}


	// delete
	function delete($companycompetencyplanid, $perm=false)
	{
		if($perm){
			$this->db->where('companycompetencyplanid', $companycompetencyplanid);
			$this->db->delete('companycompetencyplan');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companycompetencyplanid);
		}
	}
}

/* End of file companycompetencyplan_model.php */
/* Location: ./application/models/companycompetencyplan_model.php */
