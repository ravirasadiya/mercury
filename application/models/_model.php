<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class [table]_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;
	
	// get/get all
	function get($[table]id=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				[char].*
			';
		$this->db->select($sql);
		$this->db->from('[table] [char]');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($[table]id){
			$this->db->where('[char].[table]id', $[table]id);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('[char].deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('[char].');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;
			
			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();
		
		// row/rows
		$rs = ($[table]id || $returnsingle) ? $rs->row_array() : $rs->result_array();
		
		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;  
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;  
		
		return $rs;
	}

	
	// save
	function save($arr, $[table]id=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);		
		
		// insert/update
		if(!$[table]id){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");
			
			$this->db->insert('[table]', $arr);
			$[table]id = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");
		
			$this->db->where('[table]id', $[table]id);
			$this->db->update('[table]', $arr);		
		}

		return $[table]id;
	}
	
	
	// delete
	function delete($[table]id, $perm=false)
	{
		if($perm){
			$this->db->where('[table]id', $[table]id);
			$this->db->delete('[table]');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $[table]id);
		}
	}
}

/* End of file [table]_model.php */
/* Location: ./application/models/[table]_model.php */