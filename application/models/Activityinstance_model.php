<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activityinstance_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($activityinstanceid=null,$where=null, $other=null, $returnsingle=false)
	{
		// sql		
		$sql = '
			ai.*,
		';
		$this->db->select($sql);
		$this->db->from('activityinstance ai');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($activityinstanceid){
			$this->db->where('ai.activityinstanceid', $activityinstanceid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		// $this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($activityinstanceid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
		
		return $rs;
	}


	// save
	function save($arr, $activityinstanceid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$activityinstanceid){
			// $arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('activityinstance', $arr);
			$activityinstanceid = $this->db->insert_id();
		}else{
			// $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('activityinstanceid', $activityinstanceid);
			$this->db->update('activityinstance', $arr);
		}

		return $activityinstanceid;
	}


	// delete
	function delete($activityinstanceid, $perm=false)
	{
		if($perm){
			$this->db->where('activityinstanceid', $activityinstanceid);
			$this->db->delete('activityinstance');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $activityinstanceid);
		}
	}
}

/* End of file Activityinstance_model.php */
/* Location: ./application/models/Activityinstance_model.php */
