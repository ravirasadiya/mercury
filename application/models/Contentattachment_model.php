<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contentattachment_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($contentattachmentid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				c.*,
				CONCAT("'.UPLOADS_DIR.'", c.filelocation) AS fulluploadsdir
			';
		$this->db->select($sql);
		$this->db->from('contentattachment c');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($contentattachmentid){
			$this->db->where('c.contentattachmentid', $contentattachmentid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		// $this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($contentattachmentid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
		
		return $rs;
	}


	// save
	function save($arr, $contentid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$contentid){
			// $arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('contentattachment', $arr);
			$contentid = $this->db->insert_id();
		}else{
			// $arr['updatedon'] = date("Y-m-d H:i:s");
			if(count($this->contentattachment_model->get('',array('contentid'=>$contentid)))){
				$this->db->where('contentid', $contentid);
				$this->db->update('contentattachment', $arr);
			} else{
				$this->db->insert('contentattachment', $arr);
				$contentid = $this->db->insert_id();	
			}
		}

		return $contentid;
	}


	// delete
	function delete($contentattachmentid, $perm=false)
	{
		if($perm){
			$this->db->where('contentattachmentid', $contentattachmentid);
			$this->db->delete('contentattachment');
		}else{
			// $this->save(array('deletedon' => date("Y-m-d H:i:s")), $companyid);
		}
	}
}

/* End of file contentattachment_model.php */
/* Location: ./application/models/contentattachment_model.php */
