<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Deadlinesetting_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($deadlinesettingid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				dl.*
			';
		$this->db->select($sql);
		$this->db->from('deadlinesetting dl');
		// where
		if($deadlinesettingid){
			$this->db->where('dl.deadlinesettingid', $deadlinesettingid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		// $this->db->where('dl.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		// $this->db->order_by('dl.value');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($deadlinesettingid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $deadlinesettingid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$deadlinesettingid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('deadlinesetting', $arr);
			$deadlinesettingid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('deadlinesettingid', $deadlinesettingid);
			$this->db->update('deadlinesetting', $arr);
		}

		return $deadlinesettingid;
	}


	// delete
	function delete($deadlinesettingid, $perm=false)
	{
		if($perm){
			$this->db->where('deadlinesettingid', $deadlinesettingid);
			$this->db->delete('deadlinesetting');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $deadlinesettingid);
		}
	}

}

/* End of file Deadlinesetting_model.php */
/* Location: ./application/models/Deadlinesetting_model.php */
