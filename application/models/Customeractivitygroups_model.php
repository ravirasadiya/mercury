<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Customeractivitygroups_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($customeractivitygroupid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				c.*,
			';
			
		$this->db->select($sql);
		$this->db->from('customeractivitygroups c');
		//$this->db->join('', ' ON ', 'left');

		if(defined('ADMIN_COMPANYID') && ADMIN_COMPANYID){
			$this->db->where('c.companyid',ADMIN_COMPANYID);
		}
		// where
		if($customeractivitygroupid){
			$this->db->where('c.customeractivitygroupid', $customeractivitygroupid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($customeractivitygroupid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
				
		return $rs;
	}


	// save
	function save($arr, $customeractivitygroupid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$customeractivitygroupid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('customeractivitygroups', $arr);
			$customeractivitygroupid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('customeractivitygroupid', $customeractivitygroupid);
			$this->db->update('customeractivitygroups', $arr);
		}

		return $customeractivitygroupid;
	}


	// delete
	function delete($customeractivitygroupid, $perm=false)
	{
		if($perm){
			$this->db->where('customeractivitygroupid', $customeractivitygroupid);
			$this->db->delete('customeractivitygroups');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $customeractivitygroupid);
		}
	}
}

/* End of file Customeractivitygroups_model.php */
/* Location: ./application/models/Customeractivitygroups_model.php */
