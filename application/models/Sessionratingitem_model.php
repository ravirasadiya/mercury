<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sessionratingitem_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($sessionratingitemid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				sri.*
			';
		$this->db->select($sql);
		$this->db->from('sessionratingitem sri');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($sessionratingitemid){
			$this->db->where('sri.sessionratingitemid', $sessionratingitemid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('sri.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('sri.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($sessionratingitemid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $sessionratingitemid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$sessionratingitemid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('sessionratingitem', $arr);
			$sessionratingitemid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('sessionratingitemid', $sessionratingitemid);
			$this->db->update('sessionratingitem', $arr);
		}

		return $sessionratingitemid;
	}


	// delete
	function delete($sessionratingitemid, $perm=false)
	{
		if($perm){
			$this->db->where('sessionratingitemid', $sessionratingitemid);
			$this->db->delete('sessionratingitem');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $sessionratingitemid);
		}
	}
}

/* End of file sessionratingitem_model.php */
/* Location: ./application/models/sessionratingitem_model.php */
