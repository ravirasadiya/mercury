<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Competencygroup_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($competencygroupid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cg.*,
				(SELECT COUNT(*) FROM competencyparameter cp WHERE cp.competencygroupid=cg.competencygroupid AND cp.deletedon IS NULL) AS numparams
			';
		if(defined('COMPANYID') && COMPANYID){
			$sql .= ', (SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid='.COMPANYID.' AND cgpsr.competencygroupid=cg.competencygroupid AND cgpsr.competencyparameterid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS group_index';
		}
		$this->db->select($sql, true);
		$this->db->from('competencygroup cg');
		// where
		if($competencygroupid){
			$this->db->where('cg.competencygroupid', $competencygroupid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cg.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('cg.title');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($competencygroupid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $competencygroupid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$competencygroupid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('competencygroup', $arr);
			$competencygroupid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('competencygroupid', $competencygroupid);
			$this->db->update('competencygroup', $arr);
		}

		return $competencygroupid;
	}


	// delete
	function delete($competencygroupid, $perm=false)
	{
		if($perm){
			$this->db->where('competencygroupid', $competencygroupid);
			$this->db->delete('competencygroup');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $competencygroupid);
		}
	}
}

/* End of file competencygroup_model.php */
/* Location: ./application/models/competencygroup_model.php */
