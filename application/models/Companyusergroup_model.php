<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companyusergroup_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companyusergroupid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cug.*,
				CONCAT(mdu.firstname, " ", mdu.lastname) AS director
			';
		$this->db->select($sql);
		$this->db->from('companyusergroup cug');
		$this->db->join('user mdu', 'mdu.userid=cug.userid', 'left');
		// where
		if($companyusergroupid){
			$this->db->where('cug.companyusergroupid', $companyusergroupid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cug.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('cug.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companyusergroupid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companyusergroupid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companyusergroupid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companyusergroup', $arr);
			$companyusergroupid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companyusergroupid', $companyusergroupid);
			$this->db->update('companyusergroup', $arr);
		}

		return $companyusergroupid;
	}


	// delete
	function delete($companyusergroupid, $perm=false)
	{
		if($perm){
			$this->db->where('companyusergroupid', $companyusergroupid);
			$this->db->delete('companyusergroup');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companyusergroupid);
		}
	}
}

/* End of file companyusergroup_model.php */
/* Location: ./application/models/companyusergroup_model.php */
