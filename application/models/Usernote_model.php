<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Usernote_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($usernoteid=null, $where=null, $other=null, $returnsingle=false)
	{
		$domain = $this->siteURL();
		// sql
		$sql = '
				un.*,
				cs.salesmanageruserid,
				cs.salespersonaluserid,
				CONCAT(smu.firstname, " ", smu.lastname) AS salesmanager,
				CONCAT(spu.firstname, " ", spu.lastname) AS salesperson,
				CONCAT(u.firstname, " ", u.lastname) AS createdby,
				CONCAT("'.$domain.CONTENT_DIR.'", ca.filelocation) as filelocation,
				cn.competencyparameterid
			';
		$this->db->select($sql);
		$this->db->from('usernote un');
		$this->db->join('coachingsession cs', 'cs.coachingsessionid=un.coachingsessionid', 'left');
		$this->db->join('user smu', 'smu.userid=cs.salesmanageruserid', 'left');
		$this->db->join('user spu', 'spu.userid=cs.salespersonaluserid', 'left');
		$this->db->join('user u', 'u.userid=un.userid', 'left');
		$this->db->join('content cn', 'cn.usernoteid=un.usernoteid', 'left');
		$this->db->join('contentattachment ca', 'ca.contentid=cn.contentid', 'left');

		// where
		if($usernoteid){
			$this->db->where('un.usernoteid', $usernoteid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('un.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('un.createdon', 'DESC');
		$this->db->group_by('un.usernoteid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usernoteid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}

	function get_usernote($usernoteid=null, $where=null, $other=null, $returnsingle=false)
	{
		$domain = $this->siteURL();
		// sql
		$sql = '
				un.*,
				CONCAT("'.$domain.CONTENT_DIR.'", ca.filelocation) as filelocation,
				cn.competencyparameterid
			';
		$this->db->select($sql);
		$this->db->from('usernote un');
		$this->db->join('content cn', 'cn.usernoteid=un.usernoteid', 'left');
		$this->db->join('contentattachment ca', 'ca.contentid=cn.contentid', 'left');

		// where
		if($usernoteid){
			$this->db->where('un.usernoteid', $usernoteid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('un.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('un.createdon', 'DESC');
		$this->db->group_by('un.usernoteid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usernoteid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $usernoteid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$usernoteid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('usernote', $arr);
			$usernoteid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('usernoteid', $usernoteid);
			$this->db->update('usernote', $arr);
		}

		return $usernoteid;
	}


	// delete
	function delete($usernoteid, $perm=false)
	{
		if($perm){
			$this->db->where('usernoteid', $usernoteid);
			$this->db->delete('usernote');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $usernoteid);
		}
	}
	function siteURL() {
	    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $domainName = $_SERVER['HTTP_HOST'];
	    return $protocol . $domainName;
	}
}

/* End of file usernote_model.php */
/* Location: ./application/models/usernote_model.php */
