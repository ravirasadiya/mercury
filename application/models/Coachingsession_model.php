<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Coachingsession_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($coachingsessionid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cs.*,
				@salesmanager := CONCAT(smu.firstname, " ", smu.lastname) AS salesmanager,
				@salesperson := CONCAT(spu.firstname, " ", spu.lastname) AS salesperson,
				@cancelledbyname := IF(cs.cancelledby = "sales-person", @salesperson, @salesmanager) AS cancelledbyname_1,
				@cancelledbyname := IF(cs.cancelledby = "customer", cs.customertitle, @cancelledbyname) AS cancelledbyname,
				sp_cut.userid AS smuserid,
				sp_cut.companyuserteamid,
				sp_cut.title AS spteam,
				sm_cug.userid AS mduserid,
				sm_cug.companyusergroupid,
				sm_cug.title AS smgroup,
				smu.companyid
			';
		$this->db->select($sql, false);
		$this->db->from('coachingsession cs');
		$this->db->join('user smu', 'smu.userid=cs.salesmanageruserid', 'left');
		$this->db->join('user spu', 'spu.userid=cs.salespersonaluserid', 'left');
		$this->db->join('companyuserteamref sp_cutr', 'sp_cutr.userid=cs.salespersonaluserid', 'left');
		$this->db->join('companyuserteam sp_cut', 'sp_cut.companyuserteamid=sp_cutr.companyuserteamid AND sp_cut.deletedon IS NULL', 'left');
		$this->db->join('companyusergroupref sm_cugr', 'sm_cugr.userid=cs.salesmanageruserid', 'left');
		$this->db->join('companyusergroup sm_cug', 'sm_cug.companyusergroupid=sm_cugr.companyusergroupid AND sm_cug.deletedon IS NULL', 'left');
		// where
		if($coachingsessionid){
			$this->db->where('cs.coachingsessionid', $coachingsessionid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cs.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('cs.date', 'ASC');
		$this->db->order_by('cs.time', 'ASC');
		$this->db->group_by('cs.coachingsessionid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($coachingsessionid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $coachingsessionid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$coachingsessionid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('coachingsession', $arr);
			$coachingsessionid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('coachingsessionid', $coachingsessionid);
			$this->db->update('coachingsession', $arr);
		}

		return $coachingsessionid;
	}


	// delete
	function delete($coachingsessionid, $perm=false)
	{
		if($perm){
			$this->db->where('coachingsessionid', $coachingsessionid);
			$this->db->delete('coachingsession');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $coachingsessionid);
		}
	}
}

/* End of file coachingsession_model.php */
/* Location: ./application/models/coachingsession_model.php */
