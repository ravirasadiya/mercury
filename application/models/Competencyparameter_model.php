<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Competencyparameter_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($competencyparameterid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cp.*,
				cg.competencygroupid,
				cg.title AS competencygroup,
				CONCAT(cg.competencygroupid, ".", cp.competencyparameterid) AS id,
				(SELECT COUNT(*) FROM competencysuggestion cs WHERE cs.competencyparameterid=cp.competencyparameterid AND cs.deletedon IS NULL) AS numsuggestions
			';
		$this->db->select($sql);
		$this->db->from('competencyparameter cp');
		$this->db->join('competencygroup cg', 'cg.competencygroupid=cp.competencygroupid', 'left');
		// where
		if($competencyparameterid){
			$this->db->where('cp.competencyparameterid', $competencyparameterid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cp.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('cp.title');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($competencyparameterid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $competencyparameterid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);
		
		// insert/update
		if(!$competencyparameterid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('competencyparameter', $arr);
			$competencyparameterid = $this->db->insert_id();
		}else{
			if(count($this->competencyparameter_model->get(null,array('competencyparameterid'=>$competencyparameterid))))
			{
				$arr['updatedon'] = date("Y-m-d H:i:s");

				$this->db->where('competencyparameterid', $competencyparameterid);
				$this->db->update('competencyparameter', $arr);
			} else{
				$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

				$this->db->insert('competencyparameter', $arr);
				$competencyparameterid = $this->db->insert_id();
			}
		}

		return $competencyparameterid;
	}


	// delete
	function delete($competencyparameterid, $perm=false)
	{
		if($perm){
			$this->db->where('competencyparameterid', $competencyparameterid);
			$this->db->delete('competencyparameter');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $competencyparameterid);
		}
	}


	/* get next parameters id */
	function get_next_id(){
		$sql = 'competencyparameterid';
		$this->db->select_max($sql);
		$this->db->from('competencyparameter');
		$rs = $this->db->get();
		$rs = $rs->row_array();
		return (int)$rs['competencyparameterid']+1;
	}
	
}

/* End of file competencyparameter_model.php */
/* Location: ./application/models/competencyparameter_model.php */
