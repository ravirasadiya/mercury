<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model {

	// sp menu stats
	function sp_menu_stats($userid)
	{
		// set
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;
		$statarr = array();
		//echo "$fromdate - $todate";

		// sessions
		$coachingsessionrs = $this->coachingsession_model->get(null, array('cs.salespersonaluserid' => $userid), array('where-str' => 'cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'"))'));
		//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

		// loop
		foreach($coachingsessionrs as $row){
			$coachingsessionid = $row['coachingsessionid'];
			$salespersonaluserid = $row['salespersonaluserid'];
			$arr = array();

			// sum
			$sql = 'SELECT SUM(uspi.value) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.';';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['sum'] = $rs['total'];
			//echo $rs['total'].'<br />';

			// count
			$sql = 'SELECT COUNT(*) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.' AND uspi.value IS NOT NULL;';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['num'] = $rs['total'];
			$arr['avg'] = ($arr['sum'] && $arr['num']) ? round($arr['sum']/$arr['num'], ROUND_DECIMAL) : 0;

			$statarr[$salespersonaluserid]['sessions'][$coachingsessionid] = $arr;
		}

		//echo '<pre>'; print_r($statarr[$salespersonaluserid]); echo '</pre>'; exit;

		// set
		$total = 0;
		$totalcount = 0;

		// calculate totals
		foreach($statarr as $userid => $sesrow){
			$usertotal = 0;
			$usercount = count($sesrow['sessions']);

			// loop
			foreach($sesrow['sessions'] as $coachingsessionid => $row){
				$usertotal += $row['avg'];
			}

			// update
			$total += $usertotal;
			$totalcount += $usercount;

			$statarr[$userid]['avg'] = ($usertotal && $usercount) ? round($usertotal/$usercount, ROUND_DECIMAL) : 0;
		}

		// update
		$statarr['avg'] = ($total && $totalcount) ? round($total/$totalcount, ROUND_DECIMAL) : 0;

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// sm menu stats
	function sm_menu_stats($userid)
	{
		// set
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;
		$statarr = array();

		// sessions
		$coachingsessionrs = $this->coachingsession_model->get(null, array('cs.salesmanageruserid' => $userid), array('where-str' => 'cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'"))'));
		//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

		// loop
		foreach($coachingsessionrs as $row){
			$coachingsessionid = $row['coachingsessionid'];
			$companyusergroupid = $row['companyusergroupid'];
			$smuserid = $row['smuserid'];
			$companyuserteamid = $row['companyuserteamid'];
			$salespersonaluserid = $row['salespersonaluserid'];
			$arr = array();

			// sum
			$sql = 'SELECT SUM(uspi.value) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.';';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['sum'] = $rs['total'];

			// count
			$sql = 'SELECT COUNT(*) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.' AND uspi.value IS NOT NULL;';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['num'] = $rs['total'];
			$arr['avg'] = ($arr['sum'] && $arr['num']) ? round($arr['sum']/$arr['num'], ROUND_DECIMAL) : 0;

			$statarr[$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['sessions'][$coachingsessionid] = $arr;
		}

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// set
		$total = 0;
		$totalcount = 0;

		// calculate totals
		foreach($statarr as $smuserid => $managerrow){
			$mantotal = 0;
			$mancount = 0;

			// teams
			foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
				$teamtotal = 0;
				$teamcount = 0;

				// sessions
				foreach($teamrow['users'] as $userid => $userrow){
					$usertotal = 0;
					$usercount = 0;

					// loop
					foreach($userrow['sessions'] as $coachingsessionid => $row){
						$usertotal += $row['sum'];
						$usercount += $row['num'];
					}

					// add
					$teamtotal += $usertotal;
					$teamcount += $usercount;

					$statarr[$smuserid]['teams'][$companyuserteamid]['users'][$userid]['avg'] = ($usertotal && $usercount) ? round($usertotal/$usercount, ROUND_DECIMAL) : 0;
				}

				// add
				$mantotal += $teamtotal;
				$mancount += $teamcount;

				$statarr[$smuserid]['teams'][$companyuserteamid]['avg'] = ($teamtotal && $teamcount) ? round($teamtotal/$teamcount, ROUND_DECIMAL) : 0;
			}

			// add
			$total += $mantotal;
			$totalcount += $mancount;

			$statarr[$smuserid]['avg'] = ($mantotal && $mancount) ? round($mantotal/$mancount, ROUND_DECIMAL) : 0;
		}

		//echo "$total && $totalcount";

		$statarr['avg'] = ($total && $totalcount) ? round($total/$totalcount, ROUND_DECIMAL) : 0;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;
		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// return
		return $statarr;
	}


	// md menu stats
	function md_menu_stats($userid)
	{
		// set
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;
		$statarr = array();

		// sessions
		$coachingsessionrs = $this->coachingsession_model->get(null, array('sm_cug.userid' => $userid), array('where-str' => 'cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'"))'));
		//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

		// loop
		foreach($coachingsessionrs as $row){
			$coachingsessionid = $row['coachingsessionid'];
			$companyusergroupid = $row['companyusergroupid'];
			$smuserid = $row['smuserid'];
			$companyuserteamid = $row['companyuserteamid'];
			$salespersonaluserid = $row['salespersonaluserid'];
			$arr = array();

			// sum
			$sql = 'SELECT SUM(uspi.value) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.';';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['sum'] = $rs['total'];

			// count
			$sql = 'SELECT COUNT(*) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.' AND uspi.value IS NOT NULL;';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['num'] = $rs['total'];
			$arr['avg'] = ($arr['sum'] && $arr['num']) ? round($arr['sum']/$arr['num'], ROUND_DECIMAL) : 0;

			$statarr[$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['sessions'][$coachingsessionid] = $arr;
		}

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// set
		$total = 0;
		$totalcount = 0;

		// calculate totals
		foreach($statarr as $companyusergroupid => $grouprow){
			$grouptotal = 0;
			$groupcount = 0;

			// managers
			foreach($grouprow['managers'] as $smuserid => $managerrow){
				$mantotal = 0;
				$mancount = 0;

				// teams
				foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
					$teamtotal = 0;
					$teamcount = 0;

					// sessions
					foreach($teamrow['users'] as $userid => $userrow){
						$usertotal = 0;
						$usercount = 0;

						// loop
						foreach($userrow['sessions'] as $coachingsessionid => $row){
							$usertotal += $row['sum'];
							$usercount += $row['num'];
						}

						// add
						$teamtotal += $usertotal;
						$teamcount += $usercount;

						$statarr[$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['avg'] = ($usertotal && $usercount) ? round($usertotal/$usercount, ROUND_DECIMAL) : 0;
					}

					// add
					$mantotal += $teamtotal;
					$mancount += $teamcount;

					$statarr[$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['avg'] = ($teamtotal && $teamcount) ? round($teamtotal/$teamcount, ROUND_DECIMAL) : 0;
				}

				// add
				$grouptotal += $mantotal;
				$groupcount += $mancount;

				$statarr[$companyusergroupid]['managers'][$smuserid]['avg'] = ($mantotal && $mancount) ? round($mantotal/$mancount, ROUND_DECIMAL) : 0;

			}

			// add
			$total += $grouptotal;
			$totalcount += $groupcount;

			$statarr[$companyusergroupid]['avg'] = ($grouptotal && $groupcount) ? round($grouptotal/$groupcount, ROUND_DECIMAL) : 0;
		}

		$statarr['avg'] = ($total && $totalcount) ? round($total/$totalcount, ROUND_DECIMAL) : 0;

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;
		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// return
		return $statarr;
	}


	// sp missed deadlines
	function sp_misseddeadlines_stats($userid)
	{
		// set
		//$todate = date("Y-m-d", strtotime(USER_TODATE." -1 day"));
		//$fromdate = date("Y-m-01", strtotime("$todate -3 months"));
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;
		$statarr = array();

		// get
		$where = array('cs.salespersonaluserid' => $userid);
		$other = array('where-str' => 'cs.completedon IS NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ( DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'") )');
		$notcompleted_coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);
		//echo '<pre>'; print_r($notcompleted_coachingsessionrs); echo '</pre>'; exit;
		$where = array('cs.salespersonaluserid' => $userid);
		$other = array('where-str' => '( cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL) AND ( DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<=DATE("'.$todate.'") )');
		$notcompleted_usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, $where, $other);
		//echo '<pre>'; print_r($notcompleted_usersessionsuggestionitemrs); echo '</pre>'; exit;

		// count
		$statarr['num-sessions'] = ($notcompleted_coachingsessionrs) ? count($notcompleted_coachingsessionrs) : 0;
		$statarr['numsuggestions'] = ($notcompleted_usersessionsuggestionitemrs) ? count($notcompleted_usersessionsuggestionitemrs) : 0;
		$statarr['total'] = ($statarr['num-sessions']+$statarr['numsuggestions']);

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// sm missed deadlines
	function sm_misseddeadlines_stats($userid)
	{
		// set
		//$todate = date("Y-m-d", strtotime(USER_TODATE." -1 day"));
		//$fromdate = date("Y-m-01", strtotime("$todate -3 months"));
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;
		$statarr = array();

		// get
		$where = array('cs.salesmanageruserid' => $userid);
		$other = array('where-str' => '( cs.completedon IS NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ( DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<DATE("'.$todate.'") ) )');
		$notcompleted_coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);
		//echo '<pre>'; print_r($notcompleted_coachingsessionrs); echo '</pre>'; exit;
		$where = array('cs.salesmanageruserid' => $userid);
		$other = array('where-str' => '( cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL) AND ( DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<DATE("'.$todate.'") )');
		$notcompleted_usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, $where, $other);
		//echo '<pre>'; print_r($notcompleted_usersessionsuggestionitemrs); echo '</pre>'; exit;

		// count
		$statarr['num-sessions'] = ($notcompleted_coachingsessionrs) ? count($notcompleted_coachingsessionrs) : 0;
		$statarr['numsuggestions'] = ($notcompleted_usersessionsuggestionitemrs) ? count($notcompleted_usersessionsuggestionitemrs) : 0;
		$statarr['total'] = ($statarr['num-sessions']+$statarr['numsuggestions']);

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// md missed deadlines
	function md_misseddeadlines_stats($userid)
	{
		// set
		//$todate = date("Y-m-d", strtotime(USER_TODATE." -1 day"));
		//$fromdate = date("Y-m-d", strtotime("$todate -3 months"));
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;
		$statarr = array();

		// get
		$companyusergrouprefrs = $this->companyusergroupref_model->get(null, array('cug.userid' => $userid));
		$companyusergrouprefrs = ($companyusergrouprefrs) ? rs_to_keyval_array($companyusergrouprefrs, 'companyusergrouprefid', 'userid') : array();
		$companyusergrouprefstr = (is_array($companyusergrouprefrs)) ? implode(',', array_values($companyusergrouprefrs)) : '';
		$where = null;
		$other = array('where-str' => '(cs.salesmanageruserid IN ('.$companyusergrouprefstr.') AND cs.completedon IS NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ( DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'") ) )');
		//echo '<pre>'; print_r($other); echo '</pre>'; exit;
		$notcompleted_coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);
		//echo count($notcompleted_coachingsessionrs); exit;
		//echo '<pre>'; print_r($notcompleted_coachingsessionrs); echo '</pre>'; exit;
		$where = null;
		$other = array('where-str' => '( cs.salesmanageruserid IN ('.$companyusergrouprefstr.') AND cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL ) AND ( DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<=DATE("'.$todate.'") )');
		//echo '<pre>'; print_r($other); echo '</pre>'; exit;
		$notcompleted_usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, $where, $other);
		//echo count($notcompleted_usersessionsuggestionitemrs); exit;
		//echo '<pre>'; print_r($notcompleted_usersessionsuggestionitemrs); echo '</pre>'; exit;

		// count
		$statarr['num-sessions'] = ($notcompleted_coachingsessionrs) ? count($notcompleted_coachingsessionrs) : 0;
		$statarr['numsuggestions'] = ($notcompleted_usersessionsuggestionitemrs) ? count($notcompleted_usersessionsuggestionitemrs) : 0;
		$statarr['total'] = ($statarr['num-sessions']+$statarr['numsuggestions']);

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// session stats
	function session_stats($form=array())
	{
		// set
		$companyid = (isset($form['companyid'])) ? $form['companyid'] : null;
		$companyidarr = (isset($form['companyarr'])) ? $form['companyarr'] : null;
		$fromdate = (isset($form['fromdate'])) ? $form['fromdate'] : USER_FROMDATE;
		$todate = (isset($form['todate'])) ? $form['todate'] : USER_TODATE;
		$createdbyuserid = (isset($form['createdbyuserid'])) ? $form['createdbyuserid'] : null;
		$createdon = (isset($form['createdon'])) ? $form['createdon'] : null;
		$statarr = array();

		// sessions
		$where = ($companyid) ? array('smu.companyid' => $companyid) : null;
		$other = array('where-str' => '(DATE(cs.date)>="'.$fromdate.'" AND DATE(cs.date)<="'.$todate.'")');
		// check
		if($companyidarr){
			$other['where-str'] .= ' AND smu.companyid IN ('.implode(',', $companyidarr).')';
		}
		$coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);
		//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

		//echo '<pre>'; print_r($form); echo '</pre>';

		// loop
		foreach($coachingsessionrs as $row){
			$companyid = $row['companyid'];
			$coachingsessionid = $row['coachingsessionid'];
			$companyusergroupid = $row['companyusergroupid'];
			$mduserid = $row['mduserid'];
			$smuserid = $row['smuserid'];
			$companyuserteamid = $row['companyuserteamid'];
			$salespersonaluserid = $row['salespersonaluserid'];
			$arr = array();

			// check
			/*if(isset($form['frequency']) && is_array($form['frequency']) && !in_array($frequencyid, $form['frequency'])){
				continue;
			}*/
			if(isset($form['companyarr']) && is_array($form['companyarr']) && !in_array($companyid, $form['companyarr'])){
				continue;
			}
			if(isset($form['mdarr']) && is_array($form['mdarr']) && !in_array($mduserid, $form['mdarr'])){
				continue;
			}
			if(isset($form['grouparr']) && is_array($form['grouparr']) && !in_array($companyusergroupid, $form['grouparr'])){
				continue;
			}
			if(isset($form['smarr']) && is_array($form['smarr']) && !in_array($smuserid, $form['smarr'])){
				continue;
			}
			if(isset($form['teamarr']) && is_array($form['teamarr']) && !in_array($companyuserteamid, $form['teamarr'])){
				continue;
			}
			if(isset($form['sparr']) && is_array($form['sparr']) && !in_array($salespersonaluserid, $form['sparr'])){
				continue;
			}
			//echo '<pre>'; print_r($row); echo '</pre>';

			// get company
			if(!isset($statarr[$companyid]['title'])){
				// get
				$companyrs = $this->company_model->get($companyid);

				$statarr[$companyid]['title'] = $companyrs['title'];
				$statarr[$companyid]['fromdate'] = $fromdate;
				$statarr[$companyid]['todate'] = $todate;
			}
			// get group
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['title'])){
				// get
				$companyusergrouprs = $this->companyusergroup_model->get($companyusergroupid);

				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['title'] = $companyusergrouprs['title'];
				$statarr[$companyid]['directors'][$mduserid]['director'] = $companyusergrouprs['director'];
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['title'])){
				// get
				$companyuserteamrs = $this->companyuserteam_model->get($companyuserteamid);

				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['title'] = $companyuserteamrs['title'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['manager'] = $companyuserteamrs['manager'];
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['title'])){
				// get
				$sp_userrs = $this->user_model->get($salespersonaluserid);

				// set
				$numreqsessions = user_session_interval($salespersonaluserid, $fromdate, $todate);

				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['title'] = $row['salesperson'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['name'] = $row['salesperson'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-required'] = $numreqsessions;
			}

			// sum
			$sql = 'SELECT SUM(uspi.value) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.';';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['sum'] = ($rs) ? $rs['total'] : 0;

			// count
			$sql = 'SELECT COUNT(*) AS total FROM usersessionparameteritem uspi WHERE uspi.coachingsessionid='.$coachingsessionid.' AND uspi.value IS NOT NULL;';
			$rs = $this->db->query($sql, false)->row_array();

			// set
			$arr['num'] = ($rs) ? $rs['total'] : 0;
			$arr['avg'] = ($arr['sum'] && $arr['num']) ? round($arr['sum']/$arr['num'], ROUND_DECIMAL) : 0;
			$sessiontype = ($row['completedon']) ? 'completed-sessions' : 'scheduled-sessions';
			$sessiontype = ($row['cancelledon']) ? 'cancelled-sessions' : $sessiontype;

			$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid][$sessiontype][$coachingsessionid] = $arr;
		}

		$origstatarr = $statarr;
		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// set
		$total = 0;
		$totalcount = 0;
		$totalcompleted = 0;
		$totalscheduled = 0;
		$totalrequired = 0;
		$totalcoached = 0;

		// calculate totals
		foreach($statarr as $companyid => $dirarr){
			$companytotal = 0;
			$companycount = 0;
			$companycompleted = 0;
			$companyscheduled = 0;
			$companyrequired = 0;
			$companycoached = 0;

			// directors
			foreach($dirarr['directors'] as $mduserid => $grouprow){
				$dirtotal = 0;
				$dircount = 0;
				$dircompleted = 0;
				$dirscheduled = 0;
				$dirrequired = 0;
				$dircoached = 0;

				// groups
				foreach($grouprow['groups'] as $companyusergroupid => $grouprow){
					$grouptotal = 0;
					$groupcount = 0;
					$groupcompleted = 0;
					$groupscheduled = 0;
					$grouprequired = 0;
					$groupcoached = 0;

					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
						$mantotal = 0;
						$mancount = 0;
						$mancompleted = 0;
						$manscheduled = 0;
						$manrequired = 0;
						$mancoached = 0;

						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
							$teamtotal = 0;
							$teamcount = 0;
							$teamcompleted = 0;
							$teamscheduled = 0;
							$teamrequired = 0;
							$teamcoached = 0;

							// sessions
							foreach($teamrow['users'] as $userid => $userrow){
								// check
								$completedsessionsarr = (isset($userrow['completed-sessions'])) ? $userrow['completed-sessions'] : array();
								$scheduledsessionsarr = (isset($userrow['scheduled-sessions'])) ? $userrow['scheduled-sessions'] : array();
								$cancelledsessionsarr = (isset($userrow['cancelled-sessions'])) ? $userrow['cancelled-sessions'] : array();
								$usertotal = 0;
								$usercount = 0;

								// loop
								$sesstotal = 0;
								$sesscount = count($completedsessionsarr);
								foreach($completedsessionsarr as $coachingsessionid => $sesrow){
									$sesstotal += $sesrow['avg'];
								}
								$avg = ($sesstotal && $sesscount) ? round($sesstotal/$sesscount, ROUND_DECIMAL) : 0;

								// set
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['num-completed'] = $numcompleted = count($completedsessionsarr);
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['num-scheduled'] = $numscheduled = count($scheduledsessionsarr);
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['num-cancelled'] = count($cancelledsessionsarr);
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['num-required'] = $numrequired = ($userrow['num-required']) ? $userrow['num-required'] : 0;

								// add
								$teamtotal += $avg;
								$teamcount += 1;
								$teamcompleted += $numcompleted;
								$teamscheduled += $numscheduled;
								$teamrequired += $numrequired;
								$teamcoached += 1;

								// calc
								$totalsessions = ($numcompleted+$numscheduled);
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['num-total'] = $totalsessions;

								// calc
								$percompleted = ($numcompleted && !$numscheduled) ? 100 : 0;
								$percompleted = ($numscheduled) ? round(($numcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
								$reqpercompleted = ($numrequired && $numcompleted) ? round(($numcompleted*100)/$numrequired, ROUND_DECIMAL) : 0;

								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['avg'] = $avg;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['percentage-completed'] = $percompleted;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['required-percentage-completed'] = $reqpercompleted;
							} // end-users

							// add
							$mantotal += $teamtotal;
							$mancount += $teamcount;
							$mancompleted += $teamcompleted;
							$manscheduled += $teamscheduled;
							$manrequired += $teamrequired;
							$mancoached += $teamcoached;

							// calc
							$totalsessions = ($teamcompleted+$teamscheduled);
							$percompleted = ($teamcompleted && !$teamscheduled) ? 100 : 0;
							$percompleted = ($teamscheduled) ? round(($teamcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
							$reqpercompleted = ($teamrequired && $teamcompleted) ? round(($teamcompleted*100)/$teamrequired, ROUND_DECIMAL) : 0;

							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['avg'] = ($teamtotal && $teamcount) ? round($teamtotal/$teamcount, ROUND_DECIMAL) : 0;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['percentage-completed'] = $percompleted;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['required-percentage-completed'] = $reqpercompleted;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-completed'] = $teamcompleted;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-scheduled'] = $teamscheduled;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-required'] = $teamrequired;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-total'] = ($teamcompleted+$teamscheduled);
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-coached'] = $teamcoached;
						}

						// add
						$grouptotal += $mantotal;
						$groupcount += $mancount;
						$groupcompleted += $mancompleted;
						$groupscheduled += $manscheduled;
						$grouprequired += $manrequired;
						$groupcoached += $mancoached;

						// calc
						$totalsessions = ($mancompleted+$manscheduled);
						$percompleted = ($mancompleted && !$manscheduled) ? 100 : 0;
						$percompleted = ($manscheduled) ? round(($mancompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
						$reqpercompleted = ($manrequired && $mancompleted) ? round(($mancompleted*100)/$manrequired, ROUND_DECIMAL) : 0;

						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['avg'] = ($mantotal && $mancount) ? round($mantotal/$mancount, ROUND_DECIMAL) : 0;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['percentage-completed'] = $percompleted;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['required-percentage-completed'] = $reqpercompleted;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-completed'] = $mancompleted;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-scheduled'] = $manscheduled;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-required'] = $manrequired;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-total'] = ($mancompleted+$manscheduled);
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-coached'] = $mancoached;
					}

					// add
					$dirtotal += $grouptotal;
					$dircount += $groupcount;
					$dircompleted += $groupcompleted;
					$dirscheduled += $groupscheduled;
					$dirrequired += $grouprequired;
					$dircoached += $groupcoached;

					// calc
					$totalsessions = ($groupcompleted+$groupscheduled);
					$percompleted = ($groupcompleted && !$groupscheduled) ? 100 : 0;
					$percompleted = ($groupscheduled) ? round(($groupcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
					$reqpercompleted = ($grouprequired && $groupcompleted) ? round(($groupcompleted*100)/$grouprequired, ROUND_DECIMAL) : 0;

					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['avg'] = ($grouptotal && $groupcount) ? round($grouptotal/$groupcount, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['percentage-completed'] = $percompleted;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['required-percentage-completed'] = $reqpercompleted;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-completed'] = $groupcompleted;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-scheduled'] = $groupscheduled;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-required'] = $grouprequired;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-total'] = ($groupcompleted+$groupscheduled);
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-coached'] = $groupcoached;
				} // end-group

				// add
				$companytotal += $dirtotal;
				$companycount += $dircount;
				$companycompleted += $dircompleted;
				$companyscheduled += $dirscheduled;
				$companyrequired += $dirrequired;
				$companycoached += $dircoached;

				// calc
				$totalsessions = ($dircompleted+$dirscheduled);
				$percompleted = ($dircompleted && !$dirscheduled) ? 100 : 0;
				$percompleted = ($dirscheduled) ? round(($dircompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
				$reqpercompleted = ($dirrequired && $dircompleted) ? round(($dircompleted*100)/$dirrequired, ROUND_DECIMAL) : 0;

				$statarr[$companyid]['directors'][$mduserid]['avg'] = ($dirtotal && $dircount) ? round($dirtotal/$dircount, ROUND_DECIMAL) : 0;
				$statarr[$companyid]['directors'][$mduserid]['percentage-completed'] = $percompleted;
				$statarr[$companyid]['directors'][$mduserid]['required-percentage-completed'] = $reqpercompleted;
				$statarr[$companyid]['directors'][$mduserid]['num-completed'] = $dircompleted;
				$statarr[$companyid]['directors'][$mduserid]['num-scheduled'] = $dirscheduled;
				$statarr[$companyid]['directors'][$mduserid]['num-required'] = $dirrequired;
				$statarr[$companyid]['directors'][$mduserid]['num-total'] = ($dircompleted+$dirscheduled);
				$statarr[$companyid]['directors'][$mduserid]['num-coached'] = $dircoached;
			} // end-directors

			// add
			$total += $companytotal;
			$totalcount += $companycount;
			$totalcompleted += $companycompleted;
			$totalscheduled += $companyscheduled;
			$totalrequired += $companyrequired;
			$totalcoached += $companycoached;

			// calc
			$totalsessions = ($companycompleted+$companyscheduled);
			$percompleted = ($companycompleted && !$companyscheduled) ? 100 : 0;
			$percompleted = ($companyscheduled) ? round(($companycompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
			$reqpercompleted = ($companyrequired && $companycompleted) ? round(($companycompleted*100)/$companyrequired, ROUND_DECIMAL) : 0;

			$statarr[$companyid]['avg'] = ($companytotal && $companycount) ? round($companytotal/$companycount, ROUND_DECIMAL) : 0;
			$statarr[$companyid]['percentage-completed'] = $percompleted;
			$statarr[$companyid]['required-percentage-completed'] = $reqpercompleted;
			$statarr[$companyid]['num-completed'] = $companycompleted;
			$statarr[$companyid]['num-scheduled'] = $companyscheduled;
			$statarr[$companyid]['num-required'] = $companyrequired;
			$statarr[$companyid]['num-total'] = ($companycompleted+$companyscheduled);
			$statarr[$companyid]['num-coached'] = $companycoached;

		} // end-company

		//$statarr['avg'] = ($total && $totalcount) ? round($total/$totalcount, ROUND_DECIMAL) : 0;

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;
		//echo '<pre>'; print_r($origstatarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// session competency stats
	function session_competency_stats($form=array())
	{
		// set
		$companyid = (isset($form['companyid'])) ? $form['companyid'] : null;
		$companyidarr = (isset($form['companyarr'])) ? $form['companyarr'] : null;
		$coachingsessionid = (isset($form['coachingsessionid'])) ? $form['coachingsessionid'] : null;
		$fromdate = (isset($form['fromdate'])) ? $form['fromdate'] : USER_FROMDATE;
		$todate = (isset($form['todate'])) ? $form['todate'] : USER_TODATE;
		$createdbyuserid = (isset($form['createdbyuserid'])) ? $form['createdbyuserid'] : null;
		$createdon = (isset($form['createdon'])) ? $form['createdon'] : null;
		$statarr = array();
		//echo "$fromdate - $todate"; exit;

		// sessions
		$where = ($companyid) ? array('smu.companyid' => $companyid) : null;
		$where = $coachingsessionid ? array_merge($where, array('cs.coachingsessionid' => $coachingsessionid)): $where;
		$other = array('where-str' => 'cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND (DATE(cs.date)>="'.$fromdate.'" AND DATE(cs.date)<="'.$todate.'")');
		// check
		if($companyidarr){
			$other['where-str'] .= ' AND smu.companyid IN ('.implode(',', $companyidarr).')';
		}
		$coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);
		//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

		// set
		$yearmontharr = array();
		$workdate = date("Y-m-15", strtotime($fromdate));
		$stopdate = date("Y-m-d", strtotime("last day of $todate"));

		// loop
		while($workdate <= $stopdate){
			$monthyear = date("Y-m", strtotime($workdate));
			$description = date("F y", strtotime($workdate));

			$yearmontharr[$monthyear] = $description;

			$workdate = date("Y-m-d", strtotime("$workdate +1 month"));
		}

		// loop
		foreach($coachingsessionrs as $row){
			$companyid = $row['companyid'];
			$coachingsessionid = $row['coachingsessionid'];
			$companyusergroupid = $row['companyusergroupid'];
			$mduserid = $row['mduserid'];
			$smuserid = $row['smuserid'];
			$companyuserteamid = $row['companyuserteamid'];
			$salespersonaluserid = $row['salespersonaluserid'];
			$sessdate = $row['date'];
			$sessyearmonth = date("Y-m", strtotime($sessdate));
			$sessyearmonthtitle = date("F Y", strtotime($sessdate));
			$arr = array();
			$incsess = 1;

			// check
			/*if(isset($form['frequency']) && is_array($form['frequency']) && !in_array($frequencyid, $form['frequency'])){
				continue;
			}*/
			if(isset($form['companyarr']) && is_array($form['companyarr']) && !in_array($companyid, $form['companyarr'])){
				continue;
			}
			if(isset($form['mdarr']) && is_array($form['mdarr']) && !in_array($mduserid, $form['mdarr'])){
				continue;
			}
			if(isset($form['grouparr']) && is_array($form['grouparr']) && !in_array($companyusergroupid, $form['grouparr'])){
				continue;
			}
			if(isset($form['smarr']) && is_array($form['smarr']) && !in_array($smuserid, $form['smarr'])){
				continue;
			}
			if(isset($form['teamarr']) && is_array($form['teamarr']) && !in_array($companyuserteamid, $form['teamarr'])){
				continue;
			}
			if(isset($form['sparr']) && is_array($form['sparr']) && !in_array($salespersonaluserid, $form['sparr'])){
				continue;
			}
			//echo '<pre>'; print_r($row); echo '</pre>';

			// get company
			if(!isset($statarr[$companyid]['title'])){
				// get
				$companyrs = $this->company_model->get($companyid);

				$statarr[$companyid]['title'] = $companyrs['title'];
				$statarr[$companyid]['fromdate'] = $fromdate;
				$statarr[$companyid]['todate'] = $todate;
				//$statarr[$companyid]['evaltimes'] = 0;
				//$statarr[$companyid]['evaltotal'] = 0;
				//$statarr[$companyid]['evalavg'] = 0;
				$statarr[$companyid]['sessionmonths'] = $yearmontharr;
			}
			/*if(!isset($statarr[$companyid]['sessionmonths'][$sessyearmonth])){
				$statarr[$companyid]['sessionmonths'][$sessyearmonth] = $sessyearmonthtitle;
			}*/
			// director
			if(!isset($statarr[$companyid]['directors'][$mduserid]['eval-months'][$sessyearmonth])){
				$statarr[$companyid]['directors'][$mduserid]['comparr'] = array();
				$statarr[$companyid]['directors'][$mduserid]['eval-months'][$sessyearmonth] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
			}
			// groups
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['title'])){
				// get
				$companyusergrouprs = $this->companyusergroup_model->get($companyusergroupid);

				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['title'] = $companyusergrouprs['title'];
				$statarr[$companyid]['directors'][$mduserid]['director'] = $companyusergrouprs['director'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-sessions'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['evaltimes'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['evaltotal'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['evalavg'] = 0;
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['eval-months'][$sessyearmonth])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['comparr'] = array();
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['eval-months'][$sessyearmonth] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
			}
			// manager
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['eval-months'][$sessyearmonth])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['comparr'] = array();
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['eval-months'][$sessyearmonth] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
			}
			$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-sessions']++;
			// teams
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['title'])){
				// get
				$companyuserteamrs = $this->companyuserteam_model->get($companyuserteamid);

				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['title'] = $companyuserteamrs['title'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['manager'] = $companyuserteamrs['manager'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['evaltimes'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['evaltotal'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['evalavg'] = 0;
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-sessions'])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-sessions'] = 0;
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['eval-months'][$sessyearmonth])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['comparr'] = array();
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['eval-months'][$sessyearmonth] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
			}
			$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-sessions']++;
			// user
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['name'])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['name'] = $row['salesperson'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['manager'] = $row['salesmanager'];
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-sessions'])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-sessions'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-completed'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-missed'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-planned'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-total'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltimes'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltotal'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evalavg'] = 0;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'] = array();
			}
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
			}
			$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-sessions']++;
			$user_numsessions = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-sessions'];

			// get params
			$usersessionparameteritemrs = $this->usersessionparameteritem_model->get(null, array('uspi.coachingsessionid' => $coachingsessionid));
			//echo '<pre>'; print_r($usersessionparameteritemrs); echo '</pre>'; exit;

			// check
			$arr = array();
			if(isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr'])){
				$arr = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr'];
			}

			// loop
			foreach($usersessionparameteritemrs as $uspirow){
				$competencygroupid = $uspirow['competencygroupid'];
				$competencyparameterid = $uspirow['competencyparameterid'];

				// check params
				$pararr = (isset($arr['groups'][$uspirow['competencygroupid']]['params'][$uspirow['competencyparameterid']])) ? $arr['groups'][$uspirow['competencygroupid']]['params'][$uspirow['competencyparameterid']] : array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0, 'num-suggs-completed' => 0, 'num-suggs-missed' => 0, 'num-suggs-planned' => 0, 'num-suggs-total' => 0, 'num-sessions' => 0, 'eval-times-per-session-avg' => 0);
				//$evalmonth_arr = (isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth]))

				// check
				$uspirow['value'] = ($uspirow['value']) ? $uspirow['value'] : 0;

				$evaltimes = $pararr['evaltimes'] += ($uspirow['value']) ? 1 : 0;
				$pararr['evaltotal'] += ($uspirow['value']) ? $uspirow['value'] : 0;
				$pararr['evalavg'] = ($pararr['evaltimes'] && $pararr['evaltotal']) ? round(($pararr['evaltotal']/$pararr['evaltimes']), 2, ROUND_DECIMAL) : 0;
				$pararr['num-sessions'] = $user_numsessions;
				$pararr['eval-times-per-session-avg'] = ($evaltimes && $user_numsessions) ? round(($evaltimes/$user_numsessions)*100, ROUND_DECIMAL) : 0;
				//echo $coachingsessionid.'-'.$pararr['evaltotal'].'<br />';

				// get
				$where = array('cs.coachingsessionid' => $coachingsessionid, 'cs.salespersonaluserid' => $salespersonaluserid, 'ccpr.competencygroupid' => $uspirow['competencygroupid'], 'ccpr.competencyparameterid' => $uspirow['competencyparameterid']);
				$other = array('order' => array('ussi.dueon' => 'asc', 'ussi.createdon' => 'asc'));
				$usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, $where, $other);
				//echo "-------------------------- [$salespersonaluserid] --------------------------------- <br />";
				//echo implode(', ', $where).'<br />';
				//echo '<pre>'; print_r($usersessionsuggestionitemrs); echo '</pre>';

				// add suggestions
				foreach($usersessionsuggestionitemrs as $suggrow){
					$status = '';
					if($suggrow['completedon']){
						$status = 'Completed';
						$pararr['num-suggs-completed'] += 1;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-completed'] += 1;
					}
					if(!$suggrow['completedon'] && $suggrow['dueon'] < date("Y-m-d")){
						$status = 'Missed';
						$pararr['num-suggs-missed'] += 1;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-missed'] += 1;
					}
					if(!$suggrow['completedon'] && $suggrow['dueon'] >= date("Y-m-d")){
						$status = 'Planned';
						$pararr['num-suggs-planned'] += 1;
						//$pararr['num-suggs-total'] += 1;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-planned'] += 1;
					}
					//echo "[$companyusergroupid-$companyuserteamid-$salespersonaluserid][".$suggrow['competencysuggestionid']."]_";
					$duration = '';
					$pararr['num-suggs-total'] += 1;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-total'] += 1;

					// totals
					$completed = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-completed'];
					$missed = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-missed'];
					$planned = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-planned'];
					$total = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-total']; // = ($planned+$completed);
					$remaining = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-remaining'] = ($total-$completed);
					//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-perc-completed'] = ($completed && $total) ? round(($completed*100)/$total, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-perc-missed'] = ($missed && $total) ? round(($missed*100)/$total, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-perc-remaining'] = ($planned && $total) ? round(($planned*100)/$total, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['num-suggs-perc-completed'] = ($completed && $total) ? round(($completed*100)/$total, ROUND_DECIMAL) : 0;

					// check
					if($suggrow['completedon']){
						$createdon = new DateTime( date("Y-m-d", strtotime($row['completedon'])) );
						$completedon = new DateTime( date("Y-m-d", strtotime($suggrow['completedon'])) );
						$duration = $completedon->diff($createdon)->format("%a");
					}

					// append
					$pararr['suggs'][] = array(
						'id' => $suggrow['competencysuggestionid'],
						'createdon' => date("Y-m-d", strtotime($row['completedon'])),
						'dueon' => date("Y-m-d", strtotime($suggrow['dueon'])),
						'completedon' => ($suggrow['completedon']) ? date("Y-m-d", strtotime($suggrow['completedon'])) : '',
						'duration' => $duration,
						'status' => $status
					);
					//echo '<pre>'; print_r($pararr); echo '</pre>';
				} // end-foreach-user-suggestion

				//echo $uspirow['competencygroupid'].'--'.$uspirow['competencyparameterid'].'<br />';
				//echo '<pre>'; print_r($pararr); echo '</pre>';

				// update
				$arr['groups'][$uspirow['competencygroupid']]['params'][$uspirow['competencyparameterid']] = $pararr;

				// check group avg
				$avgarr = (isset($arr['groups'][$uspirow['competencygroupid']]['avg'])) ? $arr['groups'][$uspirow['competencygroupid']]['avg'] : array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0, 'num-sessions' => 0, 'eval-times-total' => 0, 'eval-times-per-session-avg-total' => 0, 'eval-times-per-session' => 0, 'num-param-sessions' => 0);

				//echo '<pre>'; print_r($arr['groups'][$uspirow['competencygroupid']]['params']); echo '</pre>'; exit;

				$avgarr['evaltimes'] += ($uspirow['value']) ? 1 : 0;
				$avgarr['evaltotal'] += $uspirow['value'];
				$avgarr['evalavg'] = ($avgarr['evaltimes'] && $avgarr['evaltotal']) ? round(($avgarr['evaltotal']/$avgarr['evaltimes']), 2, ROUND_DECIMAL) : 0;
				$avgarr['eval-times-per-session-avg-total'] = 0;
				$avgarr['num-sessions'] = $user_numsessions;
				$avgarr['num-params'] = count($arr['groups'][$uspirow['competencygroupid']]['params']);
				$avgarr['num-param-sessions'] += 1;
				//$avgarr['eval-times-per-session'] = ($avgarr['eval-times-per-session-avg-total'] && $avgarr['num-params']) ? round($avgarr['eval-times-per-session-avg-total']/$avgarr['num-params'], ROUND_DECIMAL) : 0;

				// update
				$arr['groups'][$uspirow['competencygroupid']]['avg'] = $avgarr;

				// per session
				/*if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid]['avg']['times-rated-per-session'])){
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid]['avg']['times-rated-per-session'] = 0;
				}
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid['avg']]['times-rated-per-session'] += $evaltimes;
				if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid]['params'][$competencyparameterid]['times-rated-per-session'])){
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid]['params'][$competencyparameterid]['times-rated-per-session'] = 0;
				}
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid]['params'][$competencyparameterid]['times-rated-per-session'] += $evaltimes;*/

				// check competency descriptions
				if(!isset($statarr[$companyid]['compdescarr'][$uspirow['competencygroupid']])){
					// set
					$statarr[$companyid]['compdescarr'][$uspirow['competencygroupid']]['row'] = array(
							'index' => $uspirow['group_index'],
							'title' => $uspirow['group'],
							'groupevaltimes' => 0, 'groupevaltotal' => 0, 'groupevalavg' => 0
						);
				}
				if(!isset($statarr[$companyid]['compdescarr'][$uspirow['competencygroupid']][$uspirow['competencyparameterid']])){
					$statarr[$companyid]['compdescarr'][$uspirow['competencygroupid']][$uspirow['competencyparameterid']]['row'] = array(
							'index' => $uspirow['param_index'],
							'title' => $uspirow['parameter'],
							'teamevaltimes' => 0, 'teamevaltotal' => 0, 'teamevalavg' => 0
						);
				}
				if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid])){
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0, 'num-sessions' => 0);
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
				}
				if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid])){
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid] = array('evaltimes' => 0, 'evaltotal' => 0, 'evalavg' => 0);
				}

				// add suggestions
				foreach($usersessionsuggestionitemrs as $suggrow){
					if(!isset($statarr[$companyid]['compdescarr'][$suggrow['competencygroupid']][$suggrow['competencyparameterid']][$suggrow['competencysuggestionid']])){
						$statarr[$companyid]['compdescarr'][$suggrow['competencygroupid']][$suggrow['competencyparameterid']][$suggrow['competencysuggestionid']]['row'] = array(
								'index' => $suggrow['sugg_index'],
								'title' => $suggrow['suggestion']
							);
					}
				}

				$incsess = 0;

				// user
				$evaltimes = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltimes'] += ($uspirow['value']) ? 1 : 0;
				$evaltotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltotal'] += $uspirow['value'];
				//echo "$coachingsessionid - $evaltimes - $evaltotal <br />";
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltimes'] = $evaltimes;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltotal'] = $evaltotal;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evalavg'] = ($evaltimes && $evaltotal) ? round(($evaltotal/$evaltimes), ROUND_DECIMAL) : 0;
				//echo $companyuserteamid.' - '.$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evalavg'].'<br />';

				// user months
				$evaltimes = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth]['evaltimes'] += ($uspirow['value']) ? 1 : 0;
				$evaltotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth]['evaltotal'] += $uspirow['value'];
				//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth]['evaltimes'] = $evaltimes;
				//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth]['evaltotal'] = $evaltotal;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth]['evalavg'] = ($evaltimes && $evaltotal) ? round(($evaltotal/$evaltimes), ROUND_DECIMAL) : 0;

				// user months - groups
				$evaltimes = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['evaltimes'] += ($uspirow['value']) ? 1 : 0;
				$evaltotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['evaltotal'] += $uspirow['value'];
				//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['evaltimes'] = $evaltimes;
				//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['evaltotal'] = $evaltotal;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['evalavg'] = ($evaltimes && $evaltotal) ? round(($evaltotal/$evaltimes), ROUND_DECIMAL) : 0;

				// user months - params
				$evaltimes = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid]['evaltimes'] += ($uspirow['value']) ? 1 : 0;
				$evaltotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid]['evaltotal'] += $uspirow['value'];
				//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid]['evaltimes'] = $evaltimes;
				//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid]['evaltotal'] = $evaltotal;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['eval-months'][$sessyearmonth][$competencygroupid]['params'][$competencyparameterid]['evalavg'] = ($evaltimes && $evaltotal) ? round(($evaltotal/$evaltimes), ROUND_DECIMAL) : 0;
			} // end-foreach-params

			// loop
			foreach($arr['groups'] as $gid => $garr){
				// loop
				foreach($garr['params'] as $pid => $parr){
					$eval_times_per_session_avg_total = $arr['groups'][$gid]['avg']['eval-times-per-session-avg-total'] += $parr['eval-times-per-session-avg'];
					$num_params = $arr['groups'][$gid]['avg']['num-params'];
					$arr['groups'][$gid]['avg']['eval-times-per-session-avg'] = ($eval_times_per_session_avg_total && $num_params) ? round($eval_times_per_session_avg_total/$num_params, ROUND_DECIMAL) : 0;
				}
			}

			$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['comparr'] = $arr;

			// user
			$evaltimes = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltimes'];
			$evaltotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$salespersonaluserid]['evaltotal'];
			//echo "cs: $coachingsessionid - tm: $companyuserteamid - sp: $salespersonaluserid - et: $evaltimes = tot: $evaltotal <br />";
		} // end-foreach-sessions

		$origstatarr = $statarr;
		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// set
		$total = 0;
		$totalcount = 0;
		$totalcompleted = 0;
		$totalscheduled = 0;
		$totalnumsuggscompleted = 0;
		$totalnumsuggsmissed = 0;
		$totalnumsuggsplanned = 0;
		$totalnumsuggstotal = 0;
		$totalevaltimes = 0;
		$totalnumsessions = 0;

		// calculate totals
		foreach($statarr as $companyid => $dirarr){
			$companytotal = 0;
			$companycount = 0;
			$companycompleted = 0;
			$companyscheduled = 0;
			$companynumsuggscompleted = 0;
			$companynumsuggsmissed = 0;
			$companynumsuggsplanned = 0;
			$companynumsuggstotal = 0;
			$companyevaltimes = 0;
			$companynumsessions = 0;

			// directors
			foreach($dirarr['directors'] as $mduserid => $grouprow){
				$dirtotal = 0;
				$dircount = 0;
				$dircompleted = 0;
				$dirscheduled = 0;
				$dirnumsuggscompleted = 0;
				$dirnumsuggsmissed = 0;
				$dirnumsuggsplanned = 0;
				$dirnumsuggstotal = 0;
				$direvaltimes = 0;
				$dirnumsessions = 0;

				// groups
				foreach($grouprow['groups'] as $companyusergroupid => $grouprow){
					$grouptotal = 0;
					$groupcount = 0;
					$groupcompleted = 0;
					$groupscheduled = 0;
					$groupnumsuggscompleted = 0;
					$groupnumsuggsmissed = 0;
					$groupnumsuggsplanned = 0;
					$groupnumsuggstotal = 0;
					$groupevaltimes = 0;
					$groupnumsessions = 0;

					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
						$mantotal = 0;
						$mancount = 0;
						$mancompleted = 0;
						$manscheduled = 0;
						$mannumsuggscompleted = 0;
						$mannumsuggsmissed = 0;
						$mannumsuggsplanned = 0;
						$mannumsuggstotal = 0;
						$manevaltimes = 0;
						$mannumsessions = 0;

						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
							$teamtotal = 0;
							$teamcount = 0;
							$teamcompleted = 0;
							$teamscheduled = 0;
							$teamnumsuggscompleted = 0;
							$teamnumsuggsmissed = 0;
							$teamnumsuggsplanned = 0;
							$teamnumsuggstotal = 0;
							$teamevaltimes = 0;
							$teamnumsessions = 0;

							// set
							$teamarr = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid];
							$allteamarr = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'];
							$smarr = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid];
							$allsmarr = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'];
							$grouparr = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid];
							$allgrouparr = $statarr[$companyid]['directors'][$mduserid]['groups'];
							$mdarr = $statarr[$companyid]['directors'][$mduserid];
							$allmdarr = $statarr[$companyid]['directors'];
							$comparr = $statarr[$companyid];

							// count
							$teamarr['num-subrows'] = count(array_filter(array_keys($teamarr['users']), 'is_numeric'));
							$allteamarr['num-subrows'] = count(array_filter(array_keys($teamarr), 'is_numeric'));
							$smarr['num-subrows'] = count(array_filter(array_keys($allteamarr), 'is_numeric'));
							$allsmarr['num-subrows'] = count(array_filter(array_keys($allsmarr), 'is_numeric'));
							$grouparr['num-subrows'] = count(array_filter(array_keys($grouparr), 'is_numeric'));
							$allgrouparr['num-subrows'] = count(array_filter(array_keys($grouparr), 'is_numeric'));
							$mdarr['num-subrows'] = count(array_filter(array_keys($allgrouparr), 'is_numeric'));
							$allmdarr['num-subrows'] = count(array_filter(array_keys($mdarr), 'is_numeric'));
							$comparr['num-subrows'] = count(array_filter(array_keys($allmdarr), 'is_numeric'));

							$varnames = array('teamarr', 'allteamarr', 'smarr', 'allsmarr', 'grouparr', 'allgrouparr', 'mdarr', 'allmdarr', 'comparr');

							// check init
							foreach($varnames as $var){
								if(!isset($$var) || !is_array($$var) || !count($$var)){
									echo $var; exit;
								}
							}

							// sessions
							foreach($teamrow['users'] as $userid => $userrow){
								// check
								$completedsessionsarr = (isset($userrow['completed-sessions'])) ? $userrow['completed-sessions'] : array();
								$scheduledsessionsarr = (isset($userrow['scheduled-sessions'])) ? $userrow['scheduled-sessions'] : array();
								$cancelledsessionsarr = (isset($userrow['cancelled-sessions'])) ? $userrow['cancelled-sessions'] : array();
								$usertotal = 0;
								$usercount = 0;

								//echo '<pre>'; print_r($userrow); echo '</pre>'; exit;

								// loop
								$sesstotal = 0;
								$sesscount = count($completedsessionsarr);
								foreach($completedsessionsarr as $coachingsessionid => $sesrow){
									$sesstotal += $sesrow['avg'];
								}
								$avg = ($sesstotal && $sesscount) ? round($sesstotal/$sesscount, ROUND_DECIMAL) : 0;

								// set
								$userrow['num-completed'] = $numcompleted = count($completedsessionsarr);
								$userrow['num-scheduled'] = $numscheduled = count($scheduledsessionsarr);
								$userrow['num-cancelled'] = count($cancelledsessionsarr);

								// calc
								$totalsessions = ($numcompleted+$numscheduled);
								$userrow['num-total'] = $totalsessions;

								// calc
								$percompleted = ($numcompleted && !$numscheduled) ? 100 : 0;
								$percompleted = ($numscheduled) ? round(($numcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

								$userrow['avg'] = $avg;
								$userrow['percentage-completed'] = $percompleted;

								// update
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid] = $userrow;
								//echo '<pre>'; print_r($userrow); echo '</pre>'; exit;

								/*if($companyuserteamid != 17){
									continue;
								}*/

								//echo '<pre>'; print_r($userrow); echo '</pre>'; exit;
								//echo '<hr />';

								// loop inc
								foreach($userrow as $key => $val){
									// check
									if(!is_array($val) && !in_array($key, array('name', 'manager'))){
										// loop
										foreach($varnames as $var){
											if($var == 'teamarr' && $key == 'num-sessions'){
												continue;
											}
											if(is_array($$var) && isset($$var[$key])){
												$$var[$key] = (is_numeric($$var[$key])) ? $$var[$key]+$val : $$var[$key].$val;
											}else{
												$$var[$key] = ($val ? $val : 0);
											}
										}
									}

									// check
									if($key == 'eval-months'){
										//echo '<pre>'; print_r($val); echo '</pre>'; exit;
										// loop
										foreach($varnames as $var){
											// loop
											foreach($val as $month => $montharr){
												//echo '<pre>'; print_r($val); echo '</pre>'; exit;
												// loop
												foreach($montharr as $mkey => $mval){
													//echo '<pre>'; print_r($montharr); echo '</pre>'; exit;
													// check
													if(is_array($mval)){
														// loop
														foreach($mval as $gkey => $gval){
															// check
															if(is_array($gval)){
																// loop
																foreach($gval as $pkey => $parr){
																	//echo "[$key][$month][$mkey][$gkey][$pkey]<br / >";
																	//echo '<pre>'; print_r($pval); echo '</pre>'; exit;
																	foreach($parr as $fkey => $fval){
																		$$var[$key][$month][$mkey][$gkey][$pkey][$fkey] = (isset($$var[$key][$month][$mkey][$gkey][$pkey][$fkey])) ? $$var[$key][$month][$mkey][$gkey][$pkey][$fkey]+$fval : $fval;
																		if($fkey == 'evalavg'){
																			$$var[$key][$month][$mkey][$gkey][$pkey][$fkey] = ($$var[$key][$month][$mkey][$gkey][$pkey]['evaltotal'] && $$var[$key][$month][$mkey][$gkey][$pkey]['evaltimes']) ? round(($$var[$key][$month][$mkey][$gkey][$pkey]['evaltotal']/$$var[$key][$month][$mkey][$gkey][$pkey]['evaltimes']), ROUND_DECIMAL) : 0;
																		}
																	}
																}
															}else{
																$$var[$key][$month][$mkey][$gkey] = (isset($$var[$key][$month][$mkey][$gkey])) ? $$var[$key][$month][$mkey][$gkey]+$gval : $gval;
															}
															if($gkey == 'evalavg'){
																$$var[$key][$month][$mkey][$gkey] = ($$var[$key][$month][$mkey]['evaltotal'] && $$var[$key][$month][$mkey]['evaltimes']) ? round(($$var[$key][$month][$mkey]['evaltotal']/$$var[$key][$month][$mkey]['evaltimes']), ROUND_DECIMAL) : 0;
															}
														}
													}else{
														$$var[$key][$month][$mkey] = (isset($$var[$key][$month][$mkey])) ? $$var[$key][$month][$mkey]+$mval : $mval;
													}
													if($mkey == 'evalavg'){
														$$var[$key][$month][$mkey] = ($$var[$key][$month][$mkey]['evaltotal'] && $$var[$key][$month][$mkey]['evaltimes']) ? round(($$var[$key][$month][$mkey]['evaltotal']/$$var[$key][$month][$mkey]['evaltimes']), ROUND_DECIMAL) : 0;
													}
												} // end-foreach
												$$var[$key][$month]['evalavg'] = ($$var[$key][$month]['evaltotal'] && $$var[$key][$month]['evaltimes']) ? round(($$var[$key][$month]['evaltotal']/$$var[$key][$month]['evaltimes']), ROUND_DECIMAL) : 0;
											} // end-foreach
										} // end-foreach
									} // end-if

									// check
									if($key == 'comparr'){
										// loop
										foreach($varnames as $var){
											//echo '<pre>'; print_r($$var); echo '</pre>'; exit;

											// loop
											foreach($val['groups'] as $gid => $garr){

												// loop
												foreach($garr['params'] as $pid => $parr){
													// loop
													foreach($parr as $pkey => $pval){
														if($pkey == 'suggs'){
															continue;
														}
														$$var['comparr']['groups'][$gid]['params'][$pid][$pkey] = (isset($$var['comparr']['groups'][$gid]['params'][$pid][$pkey])) ? $$var['comparr']['groups'][$gid]['params'][$pid][$pkey]+$pval : $pval;
													}
													$$var['comparr']['groups'][$gid]['params'][$pid]['evalavg'] = ($$var['comparr']['groups'][$gid]['params'][$pid]['evaltotal'] && $$var['comparr']['groups'][$gid]['params'][$pid]['evaltimes']) ? round($$var['comparr']['groups'][$gid]['params'][$pid]['evaltotal']/$$var['comparr']['groups'][$gid]['params'][$pid]['evaltimes'], ROUND_DECIMAL) : 0;
													//$$var['comparr']['groups'][$gid]['params'][$pid]['eval-times-per-session'] = ($$var['comparr']['groups'][$gid]['params'][$pid]['evaltotal'] && $$var['comparr']['groups'][$gid]['params'][$pid]['num-sessions']) ? round($$var['comparr']['groups'][$gid]['params'][$pid]['evaltotal']/$$var['comparr']['groups'][$gid]['params'][$pid]['num-sessions'], ROUND_DECIMAL) : 0;

													if(!isset($$var['num-sessions'])){
														//continue;
													}

													//$avg_sessions = ($$var['num-subrows'] && $$var['num-sessions']) ? round($$var['num-sessions']/$$var['num-subrows'], 2, ROUND_DECIMAL) : 0;
													/*if($var == 'smarr'){
														echo '<h4>::'.$pid.'</h4>';
														echo '<pre>'; print_r($parr); echo '</pre>';
													}*/

													if(!isset($$var['comparr']['groups'][$gid]['avg']['eval-times-debug'])){
														$$var['comparr']['groups'][$gid]['avg']['eval-times-debug'] = '';
													}

													$evaltimes = $$var['comparr']['groups'][$gid]['params'][$pid]['evaltimes'];
													$numsessions = $$var['comparr']['groups'][$gid]['params'][$pid]['num-sessions'];

													//$$var['comparr']['groups'][$gid]['avg']['eval-times-debug'] .= "[$pid-$numsessions]";
													$$var['comparr']['groups'][$gid]['params'][$pid]['eval-times-per-session'] = ($$var['num-subrows'] && $$var['comparr']['groups'][$gid]['params'][$pid]['evaltimes']) ? round(($$var['comparr']['groups'][$gid]['params'][$pid]['evaltimes']/$$var['num-subrows']), 2, ROUND_DECIMAL) : 0;
													//$$var['comparr']['groups'][$gid]['params'][$pid]['eval-times-per-session-avg'] = ($$var['comparr']['groups'][$gid]['params'][$pid]['eval-times-per-session'] && $avg_sessions) ? round(($$var['comparr']['groups'][$gid]['params'][$pid]['eval-times-per-session']/$avg_sessions)*100, ROUND_DECIMAL) : 0;
													$$var['comparr']['groups'][$gid]['params'][$pid]['eval-times-per-session-avg'] = ($evaltimes && $numsessions) ? round(($evaltimes/$numsessions)*100, ROUND_DECIMAL) : 0;
													//$$var['comparr']['groups'][$gid]['params'][$pid]['num-param-sessions'] = $numsessions;
												}
												// loop
												foreach($garr['avg'] as $avkey => $avval){
													$$var['comparr']['groups'][$gid]['avg'][$avkey] = (isset($$var['comparr']['groups'][$gid]['avg'][$avkey])) ? $$var['comparr']['groups'][$gid]['avg'][$avkey]+$avval : $avval;
												}
												$$var['comparr']['groups'][$gid]['avg']['evalavg'] = ($$var['comparr']['groups'][$gid]['avg']['evaltotal'] && $$var['comparr']['groups'][$gid]['avg']['evaltimes']) ? round($$var['comparr']['groups'][$gid]['avg']['evaltotal']/$$var['comparr']['groups'][$gid]['avg']['evaltimes'], ROUND_DECIMAL) : 0;
												$$var['comparr']['groups'][$gid]['avg']['eval-times-per-session'] = ($$var['comparr']['groups'][$gid]['avg']['evaltotal'] && $$var['comparr']['groups'][$gid]['avg']['num-sessions']) ? round($$var['comparr']['groups'][$gid]['avg']['evaltotal']/$$var['comparr']['groups'][$gid]['avg']['num-sessions'], ROUND_DECIMAL) : 0;

												if(!isset($$var['num-sessions'])){
													//continue;
												}

												//echo '<pre>'; print_r($$var['comparr']['groups'][$gid]); echo '</pre>'; exit;

												//$avg_sessions = ($$var['num-subrows'] && $$var['num-sessions']) ? round($$var['num-sessions']/$$var['num-subrows'], 2, ROUND_DECIMAL) : 0;
												//$num_params = (is_array($garr['params']) && count($garr['params'])) ? count($garr['params']) : 0;

												//$evaltimes = $$var['comparr']['groups'][$gid]['avg']['evaltimes'];
												$numsessions = $$var['comparr']['groups'][$gid]['avg']['num-sessions'];
												$numparamsessions = $$var['comparr']['groups'][$gid]['avg']['num-param-sessions'];
												$evaltimes = $$var['comparr']['groups'][$gid]['avg']['evaltimes'];

												$num_params = $$var['comparr']['groups'][$gid]['avg']['num-params'] = count($$var['comparr']['groups'][$gid]['params']);
												$$var['comparr']['groups'][$gid]['avg']['eval-times-per-session'] = ($num_params && $$var['comparr']['groups'][$gid]['avg']['evaltimes']) ? round(($$var['comparr']['groups'][$gid]['avg']['evaltimes']/$num_params), 2, ROUND_DECIMAL) : 0;
												//$$var['comparr']['groups'][$gid]['avg']['eval-times-per-session-avg'] = ($$var['comparr']['groups'][$gid]['avg']['eval-times-per-session'] && $avg_sessions && $num_params) ? round((($$var['comparr']['groups'][$gid]['avg']['eval-times-per-session']/$avg_sessions)*100)/$num_params, ROUND_DECIMAL) : 0;
												//$$var['comparr']['groups'][$gid]['avg']['eval-times-per-session-avg-debug'] = "$evaltimes && $numsessions";
												$$var['comparr']['groups'][$gid]['avg']['eval-times-per-session-avg'] = ($evaltimes && $numparamsessions) ? round(($evaltimes/$numparamsessions)*100, ROUND_DECIMAL) : 0;
											} // end-foreach
											//echo '<pre>'; print_r($$var['comparr']); echo '</pre>'; exit;

											$evaltimespersessionavg = 0;
											$grcount = count($$var['comparr']['groups']);
											/*if($var == 'comparr'){
												echo "$grcount <br />";
											}*/

											// loop
											foreach($$var['comparr']['groups'] as $grow){
												$evaltimespersessionavg += $grow['avg']['eval-times-per-session-avg'];

												/*if($var == 'comparr'){
													echo $grow['avg']['eval-times-per-session-avg'].'<br />';
												}*/
											}

											$$var['eval-times-per-session-avg'] = ($evaltimespersessionavg && $grcount) ? round(($evaltimespersessionavg/$grcount), ROUND_DECIMAL) : 0;
											//$$var['eval-times-per-session-avg'] = $evaltimespersessionavg;
										} // end-foreach
									} // end-if
								} // end-foreach-userrow

								//echo '<pre>'; print_r($userrow['comparr']); echo '</pre>';
								//echo '<hr />';
								//echo '<pre>'; print_r($smarr['comparr']); echo '</pre>';

								// update
								$userrow = $statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid];

								// loop inc
								foreach($userrow as $key => $val){
									// check
									if(!is_array($val) && !in_array($key, array('name', 'manager'))){
										// averages
										if($key == 'evalavg'){
											// loop
											foreach($varnames as $var){
												$evaltotal = $$var['evaltotal'];
												$evaltimes = $$var['evaltimes'];
												$$var[$key] = ($evaltotal && $evaltimes) ? round($evaltotal/$evaltimes, ROUND_DECIMAL) : 0;
											}
										}
										if($key == 'avg'){
											// loop
											foreach($varnames as $var){
												$numtotal = $$var['num-total'];
												$avg = $$var['avg'];
												$$var[$key] = ($numtotal && $avg) ? round($avg/$numtotal, ROUND_DECIMAL) : 0;
											}
										}
										if($key == 'percentage-completed'){
											// loop
											foreach($varnames as $var){
												$numscheduled = $$var['num-scheduled'];
												$numcompleted = $$var['num-completed'];
												$percompleted = ($numcompleted && !$numscheduled) ? 100 : 0;
												$percompleted = ($numscheduled) ? round(($numcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;
												$$var[$key] = $percompleted;
											}
										}
										if($key == 'num-suggs-perc-completed'){
											// loop
											foreach($varnames as $var){
												$numsuggstotal = $$var['num-suggs-total'];
												$numsuggscompleted = $$var['num-suggs-completed'];
												$$var[$key] = ($numsuggscompleted && $numsuggstotal) ? round(($numsuggscompleted*100)/$numsuggstotal, ROUND_DECIMAL) : 0;
											}
										}
									}
								} // end-foreach-userrow

								// set
								$statarr[$companyid] = $comparr;
								$statarr[$companyid]['directors'] = $allmdarr;
								$statarr[$companyid]['directors'][$mduserid] = $mdarr;
								$statarr[$companyid]['directors'][$mduserid]['groups'] = $allgrouparr;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid] = $grouparr;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'] = $allsmarr;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid] = $smarr;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'] = $allteamarr;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid] = $teamarr;
								//echo '<pre>'; print_r($teamarr); echo '</pre>';
								/*/echo '<pre>'; print_r($statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]); echo '</pre>'; exit;

								// add
								/*$teamtotal += $avg;
								$teamcount += 1;
								$teamcompleted += $numcompleted;
								$teamscheduled += $numscheduled;
								$teamnumsuggscompleted += $userrow['num-suggs-completed'];
								$teamnumsuggsmissed += $userrow['num-suggs-missed'];
								$teamnumsuggsplanned += $userrow['num-suggs-planned'];
								$teamnumsuggstotal += $userrow['num-suggs-total'];
								$teamevaltimes += $userrow['evalavg'];
								$teamnumsessions += $userrow['num-sessions'];

								// calc
								$totalsessions = ($numcompleted+$numscheduled);
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['num-total'] = $totalsessions;

								// calc
								$percompleted = ($numcompleted && !$numscheduled) ? 100 : 0;
								$percompleted = ($numscheduled) ? round(($numcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['avg'] = $avg;
								$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['percentage-completed'] = $percompleted;*/
							} // end-users

							// add
							$mantotal += $teamtotal;
							$mancount += $teamcount;
							$mancompleted += $teamcompleted;
							$manscheduled += $teamscheduled;
							$mannumsuggscompleted += $teamnumsuggscompleted;
							$mannumsuggsmissed += $teamnumsuggsmissed;
							$mannumsuggsplanned += $teamnumsuggsplanned;
							$mannumsuggstotal += $teamnumsuggstotal;
							$manevaltimes += $teamevaltimes;
							$mannumsessions += $teamnumsessions;

							// calc
							$totalsessions = ($teamcompleted+$teamscheduled);
							$percompleted = ($teamcompleted && !$teamscheduled) ? 100 : 0;
							$percompleted = ($teamscheduled) ? round(($teamcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

							/*$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['avg'] = ($teamtotal && $teamcount) ? round($teamtotal/$teamcount, ROUND_DECIMAL) : 0;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['percentage-completed'] = $percompleted;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-completed'] = $teamcompleted;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-scheduled'] = $teamscheduled;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-suggs-completed'] = $teamnumsuggscompleted;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-suggs-missed'] = $teamnumsuggsmissed;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-suggs-planned'] = $teamnumsuggsplanned;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-suggs-total'] = $teamnumsuggstotal;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-suggs-perc-completed'] = ($teamnumsuggscompleted && $teamnumsuggstotal) ? round(($teamnumsuggscompleted*100)/$teamnumsuggstotal, ROUND_DECIMAL) : 0;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['evaltimes'] = $teamevaltimes;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['num-sessions'] = $teamnumsessions;
							//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['eval-times-per-session'] = ($teamevaltimes && $teamnumsessions) ? round(($teamevaltimes*100)/$teamnumsessions, ROUND_DECIMAL) : 0;
							$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['eval-times-per-session'] = ($teamevaltimes && $teamnumsessions) ? round($teamevaltimes/$teamnumsessions, ROUND_DECIMAL) : 0;*/
						}

						// add
						$grouptotal += $mantotal;
						$groupcount += $mancount;
						$groupcompleted += $mancompleted;
						$groupscheduled += $manscheduled;
						$groupnumsuggscompleted += $mannumsuggscompleted;
						$groupnumsuggsmissed += $mannumsuggsmissed;
						$groupnumsuggsplanned += $mannumsuggsplanned;
						$groupnumsuggstotal += $mannumsuggstotal;
						$groupevaltimes += $manevaltimes;
						$groupnumsessions += $mannumsessions;

						// calc
						$totalsessions = ($mancompleted+$manscheduled);
						$percompleted = ($mancompleted && !$manscheduled) ? 100 : 0;
						$percompleted = ($manscheduled) ? round(($mancompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

						/*$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['avg'] = ($mantotal && $mancount) ? round($mantotal/$mancount, ROUND_DECIMAL) : 0;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['percentage-completed'] = $percompleted;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-completed'] = $mancompleted;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-scheduled'] = $manscheduled;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-suggs-completed'] = $mannumsuggscompleted;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-suggs-missed'] = $mannumsuggsmissed;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-suggs-planned'] = $mannumsuggsplanned;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-suggs-total'] = $mannumsuggstotal;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-suggs-perc-completed'] = ($mannumsuggscompleted && $mannumsuggstotal) ? round(($mannumsuggscompleted*100)/$mannumsuggstotal, ROUND_DECIMAL) : 0;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['evaltimes'] = $manevaltimes;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['num-sessions'] = $mannumsessions;
						//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['eval-times-per-session'] = ($manevaltimes && $mannumsessions) ? round(($manevaltimes*100)/$mannumsessions, ROUND_DECIMAL) : 0;
						$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['eval-times-per-session'] = ($manevaltimes && $mannumsessions) ? round($manevaltimes/$mannumsessions, ROUND_DECIMAL) : 0;*/
					}

					// add
					$dirtotal += $grouptotal;
					$dircount += $groupcount;
					$dircompleted += $groupcompleted;
					$dirscheduled += $groupscheduled;
					$dirnumsuggscompleted += $groupnumsuggscompleted;
					$dirnumsuggsmissed += $groupnumsuggsmissed;
					$dirnumsuggsplanned += $groupnumsuggsplanned;
					$dirnumsuggstotal += $groupnumsuggstotal;
					$direvaltimes += $groupevaltimes;
					$dirnumsessions += $groupnumsessions;

					// calc
					$totalsessions = ($groupcompleted+$groupscheduled);
					$percompleted = ($groupcompleted && !$groupscheduled) ? 100 : 0;
					$percompleted = ($groupscheduled) ? round(($groupcompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

					/*$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['avg'] = ($grouptotal && $groupcount) ? round($grouptotal/$groupcount, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['percentage-completed'] = $percompleted;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-completed'] = $groupcompleted;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-scheduled'] = $groupscheduled;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-suggs-completed'] = $groupnumsuggscompleted;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-suggs-missed'] = $groupnumsuggsmissed;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-suggs-planned'] = $groupnumsuggsplanned;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-suggs-total'] = $groupnumsuggstotal;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-suggs-perc-completed'] = ($groupnumsuggscompleted && $groupnumsuggstotal) ? round(($groupnumsuggscompleted*100)/$groupnumsuggstotal, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['evaltimes'] = $groupevaltimes;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['num-sessions'] = $groupnumsessions;
					//$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['eval-times-per-session'] = ($groupevaltimes && $groupnumsessions) ? round(($groupevaltimes*100)/$groupnumsessions, ROUND_DECIMAL) : 0;
					$statarr[$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['eval-times-per-session'] = ($groupevaltimes && $groupnumsessions) ? round($groupevaltimes/$groupnumsessions, ROUND_DECIMAL) : 0;*/
				} // end-group

				// add
				$companytotal += $dirtotal;
				$companycount += $dircount;
				$companycompleted += $dircompleted;
				$companyscheduled += $dirscheduled;
				$companynumsuggscompleted += $dirnumsuggscompleted;
				$companynumsuggsmissed += $dirnumsuggsmissed;
				$companynumsuggsplanned += $dirnumsuggsplanned;
				$companynumsuggstotal += $dirnumsuggstotal;
				$companyevaltimes += $direvaltimes;
				$companynumsessions += $dirnumsessions;

				// calc
				$totalsessions = ($dircompleted+$dirscheduled);
				$percompleted = ($dircompleted && !$dirscheduled) ? 100 : 0;
				$percompleted = ($dirscheduled) ? round(($dircompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

				/*$statarr[$companyid]['directors'][$mduserid]['avg'] = ($dirtotal && $dircount) ? round($dirtotal/$dircount, ROUND_DECIMAL) : 0;
				$statarr[$companyid]['directors'][$mduserid]['percentage-completed'] = $percompleted;
				$statarr[$companyid]['directors'][$mduserid]['num-completed'] = $dircompleted;
				$statarr[$companyid]['directors'][$mduserid]['num-scheduled'] = $dirscheduled;
				$statarr[$companyid]['directors'][$mduserid]['num-suggs-completed'] = $dirnumsuggscompleted;
				$statarr[$companyid]['directors'][$mduserid]['num-suggs-missed'] = $dirnumsuggsmissed;
				$statarr[$companyid]['directors'][$mduserid]['num-suggs-planned'] = $dirnumsuggsplanned;
				$statarr[$companyid]['directors'][$mduserid]['num-suggs-total'] = $dirnumsuggstotal;
				$statarr[$companyid]['directors'][$mduserid]['num-suggs-perc-completed'] = ($dirnumsuggscompleted && $dirnumsuggstotal) ? round(($dirnumsuggscompleted*100)/$dirnumsuggstotal, ROUND_DECIMAL) : 0;
				$statarr[$companyid]['directors'][$mduserid]['evaltimes'] = $direvaltimes;
				$statarr[$companyid]['directors'][$mduserid]['num-sessions'] = $dirnumsessions;
				//$statarr[$companyid]['directors'][$mduserid]['eval-times-per-session'] = ($direvaltimes && $dirnumsessions) ? round(($direvaltimes*100)/$dirnumsessions, ROUND_DECIMAL) : 0;
				$statarr[$companyid]['directors'][$mduserid]['eval-times-per-session'] = ($direvaltimes && $dirnumsessions) ? round($direvaltimes/$dirnumsessions, ROUND_DECIMAL) : 0;*/
			} // end-directors

			// add
			$total += $companytotal;
			$totalcount += $companycount;
			$totalcompleted += $companycompleted;
			$totalscheduled += $companyscheduled;
			$totalnumsuggscompleted += $companynumsuggscompleted;
			$totalnumsuggsmissed += $companynumsuggsmissed;
			$totalnumsuggsplanned += $companynumsuggsplanned;
			$totalnumsuggstotal += $companynumsuggstotal;

			// calc
			$totalsessions = ($companycompleted+$companyscheduled);
			$percompleted = ($companycompleted && !$companyscheduled) ? 100 : 0;
			$percompleted = ($companyscheduled) ? round(($companycompleted*100)/$totalsessions, ROUND_DECIMAL) : $percompleted;

			/*$statarr[$companyid]['avg'] = ($companytotal && $companycount) ? round($companytotal/$companycount, ROUND_DECIMAL) : 0;
			$statarr[$companyid]['percentage-completed'] = $percompleted;
			$statarr[$companyid]['num-completed'] = $companycompleted;
			$statarr[$companyid]['num-scheduled'] = $companyscheduled;
			$statarr[$companyid]['num-suggs-completed'] = $companynumsuggscompleted;
			$statarr[$companyid]['num-suggs-missed'] = $companynumsuggsmissed;
			$statarr[$companyid]['num-suggs-planned'] = $companynumsuggsplanned;
			$statarr[$companyid]['num-suggs-total'] = $companynumsuggstotal;
			$statarr[$companyid]['num-suggs-perc-completed'] = ($companynumsuggscompleted && $companynumsuggstotal) ? round(($companynumsuggscompleted*100)/$companynumsuggstotal, ROUND_DECIMAL) : 0;
			$statarr[$companyid]['evaltimes'] = $companyevaltimes;
			$statarr[$companyid]['num-sessions'] = $companynumsessions;
			//$statarr[$companyid]['eval-times-per-session'] = ($companyevaltimes && $companynumsessions) ? round(($companyevaltimes*100)/$companynumsessions, ROUND_DECIMAL) : 0;
			$statarr[$companyid]['eval-times-per-session'] = ($companyevaltimes && $companynumsessions) ? round($companyevaltimes/$companynumsessions, ROUND_DECIMAL) : 0;*/

		} // end-company

		//$statarr['avg'] = ($total && $totalcount) ? round($total/$totalcount, ROUND_DECIMAL) : 0;

		//echo '<hr />';
		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;
		//echo '<pre>'; print_r($origstatarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// session stats
	function single_session_stats($coachingsessionid)
	{
		// set
		$statarr = array();

		// get
		$coachingsessionrs = $this->coachingsession_model->get(null, array('cs.coachingsessionid' => $coachingsessionid));
		//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

		// loop
		foreach($coachingsessionrs as $row){
			$coachingsessionid = $row['coachingsessionid'];
			$companyusergroupid = $row['companyusergroupid'];
			$mduserid = $row['mduserid'];
			$smuserid = $row['smuserid'];
			$companyuserteamid = $row['companyuserteamid'];
			$salespersonaluserid = $row['salespersonaluserid'];
			$arr = array();

			// check
			/*if(isset($form['frequency']) && is_array($form['frequency']) && !in_array($frequencyid, $form['frequency'])){
				continue;
			}
			if(isset($form['companyarr']) && is_array($form['companyarr']) && !in_array($companyid, $form['companyarr'])){
				continue;
			}
			if(isset($form['mdarr']) && is_array($form['mdarr']) && !in_array($mduserid, $form['mdarr'])){
				continue;
			}
			if(isset($form['grouparr']) && is_array($form['grouparr']) && !in_array($companyusergroupid, $form['grouparr'])){
				continue;
			}
			if(isset($form['smarr']) && is_array($form['smarr']) && !in_array($smuserid, $form['smarr'])){
				continue;
			}
			if(isset($form['teamarr']) && is_array($form['teamarr']) && !in_array($companyuserteamid, $form['teamarr'])){
				continue;
			}
			if(isset($form['sparr']) && is_array($form['sparr']) && !in_array($salespersonaluserid, $form['sparr'])){
				continue;
			}*/

			// get
			$usersessionparameteritemrs = $this->usersessionparameteritem_model->get(null, array('uspi.coachingsessionid' => $coachingsessionid));
			//echo '<pre>'; print_r($usersessionparameteritemrs); echo '</pre>'; exit;

			// loop
			foreach($usersessionparameteritemrs as $parrow){
				// group
				if(!isset($statarr[$coachingsessionid]['groups'][$parrow['competencygroupid']]['row'])){
					$statarr[$coachingsessionid]['groups'][$parrow['competencygroupid']]['row'] = array(
							'index' => $parrow['group_index'],
							'title' => $parrow['group'],
							'total' => 0,
							'totalcount' => 0,
							'avg' => 0
						);
				}
				// param
				if(!isset($statarr[$coachingsessionid]['groups'][$parrow['competencygroupid']]['params'][$parrow['competencyparameterid']]['row'])){
					$statarr[$coachingsessionid]['groups'][$parrow['competencygroupid']]['params'][$parrow['competencyparameterid']]['row'] = array(
							'index' => $parrow['param_index'],
							'title' => $parrow['parameter'],
							'val' => $parrow['value']
						);
				}

				// get
				$usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, array('ussi.coachingsessionid' => $coachingsessionid, 'cp.competencyparameterid' => $parrow['competencyparameterid']));
				//echo '<pre>'; print_r($usersessionsuggestionitemrs); echo '</pre>'; exit;

				// loop
				foreach($usersessionsuggestionitemrs as $sugrow){
					// suggestion
					if(!isset($statarr[$coachingsessionid]['groups'][$parrow['competencygroupid']]['params'][$parrow['competencyparameterid']]['suggs'][$sugrow['competencysuggestionid']]['row'])){
						$statarr[$coachingsessionid]['groups'][$parrow['competencygroupid']]['params'][$parrow['competencyparameterid']]['suggs'][$sugrow['competencysuggestionid']]['row'] = array(
								'index' => $sugrow['sugg_index'],
								'title' => $sugrow['suggestion'],
								'dueon' => $sugrow['dueon'],
								'completedon' => $sugrow['completedon']
							);
					}
				}

			}
		} // end-foreach

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// sessions
		foreach($statarr as $coachingsessionid => $sessrow){
			$sesstotal = 0;
			$sesscount = 0;

			// groups
			foreach($sessrow['groups'] as $competencygroupid => $grouprow){
				$grouptotal = 0;
				$groupcount = 0;

				// params
				foreach($grouprow['params'] as $competencyparameterid => $paramrow){
					$grouptotal += $paramrow['row']['val'];
					$groupcount += ($paramrow['row']['val']) ? 1 : 0;
				} // end-params

				$sesstotal += $grouptotal;
				$sesscount += $groupcount;

				// calc
				$avg = ($grouptotal && $groupcount) ? round($grouptotal/$groupcount, ROUND_DECIMAL) : 0;

				// update
				$statarr[$coachingsessionid]['groups'][$competencygroupid]['row']['total'] = $grouptotal;
				$statarr[$coachingsessionid]['groups'][$competencygroupid]['row']['totalcount'] = $groupcount;
				$statarr[$coachingsessionid]['groups'][$competencygroupid]['row']['avg'] = $avg;
			} // end-groups

			// calc
			$avg = ($sesstotal && $sesscount) ? round($sesstotal/$sesscount, ROUND_DECIMAL) : 0;

			// update
			$statarr[$coachingsessionid]['row']['total'] = $sesstotal;
			$statarr[$coachingsessionid]['row']['totalcount'] = $sesscount;
			$statarr[$coachingsessionid]['row']['avg'] = $avg;
		} // end-sessions

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// team-member evaluations
	function session_evaluations_stats($form=array())
	{
		// set
		$companyid = (isset($form['companyid'])) ? $form['companyid'] : null;
		$companyidarr = (isset($form['companyarr'])) ? $form['companyarr'] : null;
		$coachingsessionid = (isset($form['coachingsessionid'])) ? $form['coachingsessionid'] : null;
		$fromdate = (isset($form['fromdate'])) ? $form['fromdate'] : USER_FROMDATE;
		$todate = (isset($form['todate'])) ? $form['todate'] : USER_TODATE;
		$createdbyuserid = (isset($form['createdbyuserid'])) ? $form['createdbyuserid'] : null;
		$createdon = (isset($form['createdon'])) ? $form['createdon'] : null;
		$statarr = array();

		// sessions
		$where = ($companyid) ? array('cut.companyid' => $companyid) : null;
		$other = null;
		// check
		if($companyidarr){
			$other['where-str'] = 'cut.companyid IN ('.implode(',', $companyidarr).')';
			$other['order'] = array('director' => 'ASC', 'group' => 'ASC', 'manager' => 'ASC');
		}
		$sm_userrs = $this->companyuserteam_model->get(null, $where, $other);
		//echo '<pre>'; print_r($sm_userrs); echo '</pre>'; exit;

		// loop
		foreach($sm_userrs as $row){
			$companyid = $row['companyid'];
			$smuserid = $row['userid'];
			$manager = $row['manager'];
			$teamid = $row['companyuserteamid'];
			$team = $row['title'];
			$mduserid = $row['mduserid'];
			$director = $row['director'];
			$groupid = $row['companyusergroupid'];
			$group = $row['group'];

			// get
			$companysessionratingrefrs = $this->companysessionratingref_model->get(null, array('csr.companyid' => $companyid));
			//echo '<pre>'; print_r($companysessionratingrefrs); echo '</pre>'; exit;

			// check
			/*if(isset($form['frequency']) && is_array($form['frequency']) && !in_array($frequencyid, $form['frequency'])){
				continue;
			}*/
			if(isset($form['companyarr']) && is_array($form['companyarr']) && !in_array($companyid, $form['companyarr'])){
				continue;
			}
			if(isset($form['mdarr']) && is_array($form['mdarr']) && !in_array($mduserid, $form['mdarr'])){
				continue;
			}
			if(isset($form['grouparr']) && is_array($form['grouparr']) && !in_array($groupid, $form['grouparr'])){
				continue;
			}
			if(isset($form['smarr']) && is_array($form['smarr']) && !in_array($smuserid, $form['smarr'])){
				continue;
			}
			if(isset($form['teamarr']) && is_array($form['teamarr']) && !in_array($teamid, $form['teamarr'])){
				continue;
			}

			// set
			$ratarr = array();

			// companyid
			if(!isset($statarr[$companyid])){
				// get
				$companyrs = $this->company_model->get($companyid);
				$companysessionratingrs = $this->companysessionrating_model->get(null, array('csr.companyid' => $companyid), null, true);
				$companysessionratingrefrs = $this->companysessionratingref_model->get(null, array('csrf.companysessionratingid' => $companysessionratingrs['companysessionratingid']));

				$statarr[$companyid]['title'] = $companyrs['title'];
				$statarr[$companyid]['fromdate'] = $fromdate;
				$statarr[$companyid]['todate'] = $todate;
				$statarr[$companyid]['ratings'] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				$statarr[$companyid]['ratingdescarr'] = ($companysessionratingrefrs) ? rs_to_keyval_array($companysessionratingrefrs, 'sessionratingitemid', 'rating') : array();
			}
			// director
			if(!isset($statarr[$companyid]['directors'][$mduserid])){
				$statarr[$companyid]['directors'][$mduserid]['name'] = $director;
				$statarr[$companyid]['directors'][$mduserid]['ratings'] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);

				$dirratarr = array();
			}
			// groups
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['title'] = $group;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['ratings'] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);;

				$groupratarr = array();
			}
			// managers
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['name'] = $manager;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['ratings'] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);

				$manratarr = array();
			}
			// teams
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['title'] = $team;
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['ratings'] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
			}

			// check
			$other = null;
			if(isset($form['sparr']) && is_array($form['sparr']) && count($form['sparr'])){
				$other = array('where-str' => '(cutr.userid IN ('.implode(',', $form['sparr']).'))');
			}

			// get
			$where = array('cutr.companyuserteamid' => $teamid);
			$companyuserteamrefarr = $this->companyuserteamref_model->get_keyval_arr($where, $other);
			$teamuseridstr = (count($companyuserteamrefarr)) ? implode(',', array_values($companyuserteamrefarr)) : null;

			// <get></get>
			$other = array('where-str' => '(cs.salespersonaluserid IN ('.$teamuseridstr.'))');
			$usersessionratingitemrs = ($companyuserteamrefarr) ? $this->usersessionratingitem_model->get(null, null, $other) : array();
			//echo '<pre>'; print_r($usersessionratingitemrs); echo '</pre>';

			// loop
			foreach($usersessionratingitemrs as $row){
				$userid = $row['userid'];
				$sessionratingitemid = $row['sessionratingitemid'];

				// check
				if(!isset($ratarr[$row['sessionratingitemid']])){
					$ratarr[$row['sessionratingitemid']] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				}
				if(!isset($manratarr[$row['sessionratingitemid']])){
					$manratarr[$row['sessionratingitemid']] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				}
				if(!isset($groupratarr[$row['sessionratingitemid']])){
					$groupratarr[$row['sessionratingitemid']] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				}
				if(!isset($dirratarr[$row['sessionratingitemid']])){
					$dirratarr[$row['sessionratingitemid']] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				}
				// user
				if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid])){
					$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['name'] = $row['fullname'];
					$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['ratings'] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				}
				if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['rating-rows'][$sessionratingitemid])){
					$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['rating-rows'][$sessionratingitemid] = array('num-rated' => 0, 'num-total' => 0, 'num-avg' => 0);
				}

				$numrated = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['rating-rows'][$sessionratingitemid]['num-rated'] += ($row['value']) ? 1 : 0;;
				$numtotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['rating-rows'][$sessionratingitemid]['num-total'] += $row['value'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['rating-rows'][$sessionratingitemid]['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['ratings']['num-rated'] += ($row['value']) ? 1 : 0;;
				$numtotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['ratings']['num-total'] += $row['value'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersons'][$userid]['ratings']['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $ratarr[$row['sessionratingitemid']]['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $ratarr[$row['sessionratingitemid']]['num-total'] += $row['value'];
				$ratarr[$row['sessionratingitemid']]['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $manratarr[$row['sessionratingitemid']]['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $manratarr[$row['sessionratingitemid']]['num-total'] += $row['value'];
				$manratarr[$row['sessionratingitemid']]['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $groupratarr[$row['sessionratingitemid']]['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $groupratarr[$row['sessionratingitemid']]['num-total'] += $row['value'];
				$groupratarr[$row['sessionratingitemid']]['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $dirratarr[$row['sessionratingitemid']]['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $dirratarr[$row['sessionratingitemid']]['num-total'] += $row['value'];
				$dirratarr[$row['sessionratingitemid']]['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['ratings']['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['ratings']['num-total'] += $row['value'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['ratings']['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['ratings']['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['ratings']['num-total'] += $row['value'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['ratings']['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['ratings']['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['ratings']['num-total'] += $row['value'];
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['ratings']['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $statarr[$companyid]['directors'][$mduserid]['ratings']['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $statarr[$companyid]['directors'][$mduserid]['ratings']['num-total'] += $row['value'];
				$statarr[$companyid]['directors'][$mduserid]['ratings']['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;

				$numrated = $statarr[$companyid]['ratings']['num-rated'] += ($row['value']) ? 1 : 0;
				$numtotal = $statarr[$companyid]['ratings']['num-total'] += $row['value'];
				$statarr[$companyid]['ratings']['num-avg'] = ($numtotal && $numrated) ? round(($numtotal/$numrated), ROUND_DECIMAL) : 0;
			}

			// ratings
			$statarr[$companyid]['directors'][$mduserid]['rating-rows'] = $dirratarr;
			$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['rating-rows'] = $groupratarr;
			$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['rating-rows'] = $manratarr;
			$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['rating-rows'] = $ratarr;
		}

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}


	// company users
	function company_user_stats($form=array())
	{
		// set
		$companyid = (isset($form['companyid'])) ? $form['companyid'] : null;
		$companyidarr = (isset($form['companyarr'])) ? $form['companyarr'] : null;
		$coachingsessionid = (isset($form['coachingsessionid'])) ? $form['coachingsessionid'] : null;
		$fromdate = (isset($form['fromdate'])) ? $form['fromdate'] : USER_FROMDATE;
		$todate = (isset($form['todate'])) ? $form['todate'] : USER_TODATE;
		$createdbyuserid = (isset($form['createdbyuserid'])) ? $form['createdbyuserid'] : null;
		$createdon = (isset($form['createdon'])) ? $form['createdon'] : null;
		$statarr = array();

		// sessions
		$where = ($companyid) ? array('cut.companyid' => $companyid) : null;
		$other = null;
		// check
		if($companyidarr){
			$other['where-str'] = 'cut.companyid IN ('.implode(',', $companyidarr).')';
		}
		$sm_userrs = $this->companyuserteam_model->get(null, $where, $other);

		// loop
		foreach($sm_userrs as $row){
			$companyid = $row['companyid'];
			$smuserid = $row['userid'];
			$manager = $row['manager'];
			$teamid = $row['companyuserteamid'];
			$team = $row['title'];
			$mduserid = $row['mduserid'];
			$director = $row['director'];
			$groupid = $row['companyusergroupid'];
			$group = $row['group'];

			// check
			/*if(isset($form['frequency']) && is_array($form['frequency']) && !in_array($frequencyid, $form['frequency'])){
				continue;
			}*/
			if(isset($form['companyarr']) && is_array($form['companyarr']) && !in_array($companyid, $form['companyarr'])){
				continue;
			}
			if(isset($form['mdarr']) && is_array($form['mdarr']) && !in_array($mduserid, $form['mdarr'])){
				continue;
			}
			if(isset($form['grouparr']) && is_array($form['grouparr']) && !in_array($groupid, $form['grouparr'])){
				continue;
			}
			if(isset($form['smarr']) && is_array($form['smarr']) && !in_array($smuserid, $form['smarr'])){
				continue;
			}
			if(isset($form['teamarr']) && is_array($form['teamarr']) && !in_array($teamid, $form['teamarr'])){
				continue;
			}

			// companyid
			if(!isset($statarr[$companyid])){
				// get
				$companyrs = $this->company_model->get($companyid);
				$companysessionratingrs = $this->companysessionrating_model->get(null, array('csr.companyid' => $companyid), null, true);
				$companysessionratingrefrs = $this->companysessionratingref_model->get(null, array('csrf.companysessionratingid' => $companysessionratingrs['companysessionratingid']));

				$statarr[$companyid]['title'] = $companyrs['title'];
				$statarr[$companyid]['fromdate'] = $fromdate;
				$statarr[$companyid]['todate'] = $todate;
			}
			// director
			if(!isset($statarr[$companyid]['directors'][$mduserid])){
				$statarr[$companyid]['directors'][$mduserid]['name'] = $director;
			}
			// groups
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['title'] = $group;
			}
			// managers
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['name'] = $manager;
			}
			// teams
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['title'] = $team;
			}
			// salespersonal
			if(!isset($statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersonal'])){
				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersonal'] = array();
			}

			// get
			$companyuserteamrefrs = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $teamid));
			//$companyuserteamrefarr = rs_to_keyval_array($companyuserteamrefarr, 'userid', 'user');

			// loop
			foreach($companyuserteamrefrs as $row){
				// check
				if(isset($form['sparr']) && is_array($form['sparr']) && !in_array($row['userid'], $form['sparr'])){
					continue;
				}

				$statarr[$companyid]['directors'][$mduserid]['groups'][$groupid]['managers'][$smuserid]['teams'][$teamid]['salespersonal'][$row['userid']] = array('name' => $row['user']);
			}
		}

		//echo '<pre>'; print_r($statarr); echo '</pre>'; exit;

		// round
		$statarr = (is_array($statarr) && count($statarr)) ? round_values($statarr) : 0;

		// return
		return $statarr;
	}
}

/* End of file report_model.php */
/* Location: ./application/models/report_model.php */
