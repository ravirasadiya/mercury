<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Lookup_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($lookupid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				l.*,
				lp1.value AS parent1,
				IF(lp1.value, CONCAT(lp1.value, l.value), l.value) AS sorter
			';
		$this->db->select($sql);
		$this->db->from('lookup l');
		$this->db->join('lookup lp1', 'l.parentid=lp1.lookupid', 'left');
		// where
		if($lookupid){
			$this->db->where('l.lookupid', $lookupid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('l.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('l.value');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($lookupid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $lookupid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$lookupid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('lookup', $arr);
			$lookupid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('lookupid', $lookupid);
			$this->db->update('lookup', $arr);
		}

		return $lookupid;
	}


	// delete
	function delete($lookupid, $perm=false)
	{
		if($perm){
			$this->db->where('lookupid', $lookupid);
			$this->db->delete('lookup');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $lookupid);
		}
	}


	// get key=>val array
	function get_keyval_array($lookuptype, $retarr=array('' => '&nbsp;'), $defother=array(), $parentid=null, $char='')
	{
		// set
		$where = array('l.type' => $lookuptype);
		$other = $defother;

		// check
		if($parentid){
			$where['l.parentid'] = $parentid;
		}else{
			$other['where-str'] = 'l.parentid IS NULL';
		}

		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$haschildren = $this->get(null, array('l.type' => $lookuptype, 'l.parentid' => $row['lookupid']));

			$retarr[$row['lookupid']] = $char.ucwords($row['value']);

			if($haschildren){
				$retarr = $this->get_keyval_array($lookuptype, $retarr, $defother, $row['lookupid'], $char.' - ');
			}
		}

		return $retarr;
	}


	// get key=>val array
	function get_keyval_hier_array($lookuptype, $retarr=array(), $parentid=null)
	{
		// set
		$where = array('l.type' => $lookuptype);
		$other = null;

		// check
		if($parentid){
			$where['l.parentid'] = $parentid;
		}else{
			$other = array('where-str' => 'l.parentid IS NULL');
		}

		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$haschildren = $this->get(null, array('l.type' => $lookuptype, 'l.parentid' => $row['lookupid']));

			$retarr[$row['lookupid']]['title'] = ucwords($row['value']);

			$retarr[$row['lookupid']]['children'] = $this->get_keyval_hier_array($lookuptype, array(), $row['lookupid']);
		}

		return $retarr;
	}
}

/* End of file lookup_model.php */
/* Location: ./application/models/lookup_model.php */
