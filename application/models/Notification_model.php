<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($notificationid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				n.*,
				cut.title AS team
			';
		$this->db->select($sql);
		$this->db->from('notification n');
		$this->db->join('companyuserteam cut', 'cut.companyuserteamid=n.companyuserteamid', 'left');
		// where
		if($notificationid){
			$this->db->where('n.notificationid', $notificationid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('n.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('n.message');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($notificationid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $notificationid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$notificationid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('notification', $arr);
			$notificationid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('notificationid', $notificationid);
			$this->db->update('notification', $arr);
		}

		return $notificationid;
	}


	// delete
	function delete($notificationid, $perm=false)
	{
		if($perm){
			$this->db->where('notificationid', $notificationid);
			$this->db->delete('notification');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $notificationid);
		}
	}
}

/* End of file notification_model.php */
/* Location: ./application/models/notification_model.php */
