<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companyuserteamref_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companyuserteamrefid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cutr.*,
				cut.title AS team,
				cut.userid AS salesmanageruserid,
				CONCAT(u.firstname, " ", u.lastname) AS user
			';
		$this->db->select($sql);
		$this->db->from('companyuserteamref cutr');
		$this->db->join('companyuserteam cut', 'cut.companyuserteamid=cutr.companyuserteamid', 'left');
		$this->db->join('user u', 'u.userid=cutr.userid', 'left');
		// where
		if($companyuserteamrefid){
			$this->db->where('cutr.companyuserteamrefid', $companyuserteamrefid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cutr.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('cutr.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companyuserteamrefid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companyuserteamrefid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companyuserteamrefid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companyuserteamref', $arr);
			$companyuserteamrefid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companyuserteamrefid', $companyuserteamrefid);
			$this->db->update('companyuserteamref', $arr);
		}

		return $companyuserteamrefid;
	}


	// delete
	function delete($companyuserteamrefid, $perm=false)
	{
		if($perm){
			$this->db->where('companyuserteamrefid', $companyuserteamrefid);
			$this->db->delete('companyuserteamref');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companyuserteamrefid);
		}
	}


	// get key=>val array
	function get_keyval_arr($where=null, $other=null, $field='userid', $keyfield='companyuserteamrefid', $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$retarr[$row[$keyfield]] = $row[$field];
		}

		return $retarr;
	}


	// update
	function update($arr, $companyuserteamid)
	{
		// get
		$rs = $this->get_keyval_arr(array('cutr.companyuserteamid' => $companyuserteamid));

		// loop
		foreach($arr as $i => $userid){
			// check
			$companyuserteamrefid = ($i > 0) ? $i : null;

			// set
			$row = array();
			$row['companyuserteamid'] = $companyuserteamid;
			$row['userid'] = $userid;

			// save
			$this->save($row, $companyuserteamrefid);

			// check
			if(isset($rs[$companyuserteamrefid])){
				// unset
				unset($rs[$companyuserteamrefid]);
			}
		}

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($rs as $companyuserteamrefid => $val){
			// delete
			$this->delete($companyuserteamrefid);
		}
	}
}

/* End of file companyuserteamref_model.php */
/* Location: ./application/models/companyuserteamref_model.php */
