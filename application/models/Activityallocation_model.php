<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activityallocation_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($activityallocationid=null,$type=null,$where=null, $other=null, $returnsingle=false)
	{
		// sql
		if($type=='user'){
			$sql = '
				c.*,
				u.*,
				IF(u.isenabled, "Yes", "No") AS enabled,
			';	
		}
		if($type=='team'){
			$sql = '
				c.*,
				u.*,
				IF(u.isenabled, "Yes", "No") AS enabled,
				t.*,
				CONCAT(smu.firstname, " ", smu.lastname) AS manager,
				cug.companyusergroupid,
				cug.title AS group,
				mdu.userid AS mduserid,
				CONCAT(mdu.firstname, " ", mdu.lastname) AS director
			';
		}
		$this->db->select($sql);
		$this->db->from('activityallocation c');
		$this->db->join('companyuserteam t', 'c.companyuserteamid = t.companyuserteamid', 'left');
		$this->db->join('user u', 'c.userid=u.userid', 'left');
		$this->db->join('user smu', 'smu.userid=t.userid', 'left');
		$this->db->join('companyusergroupref cugr', 'cugr.userid=t.userid AND cugr.deletedon IS NULL', 'left');
		$this->db->join('companyusergroup cug', 'cug.companyusergroupid=cugr.companyusergroupid AND cug.deletedon IS NULL', 'left');
		$this->db->join('user mdu', 'mdu.userid=cug.userid', 'left');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($activityallocationid){
			$this->db->where('c.activityallocationid', $activityallocationid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		// $this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($activityallocationid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
		
		return $rs;
	}

	// get/get all
	function get_user($activityallocationid=null,$where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
			al.*
		';	
		
		$this->db->select($sql);
		$this->db->from('activityallocation al');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($activityallocationid){
			$this->db->where('c.activityallocationid', $activityallocationid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		// $this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($activityallocationid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
		
		return $rs;
	}


	// save
	function save($arr, $activityallocationid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$activityallocationid){
			// $arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('activityallocation', $arr);
			$activityallocationid = $this->db->insert_id();
		}else{
			// $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('activityallocationid', $activityallocationid);
			$this->db->update('activityallocation', $arr);
		}

		return $activityallocationid;
	}


	// delete
	function delete($activityallocationid, $perm=false)
	{
		if($perm){
			$this->db->where('activityallocationid', $activityallocationid);
			$this->db->delete('activityallocation');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $activityallocationid);
		}
	}
}

/* End of file Activityallocation_model.php */
/* Location: ./application/models/Activityallocation_model.php */
