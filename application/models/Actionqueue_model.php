<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ActionQueue_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($actionqueueid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				aq.*
			';
		$this->db->select($sql);
		$this->db->from('actionqueue aq');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($actionqueueid){
			$this->db->where('aq.actionqueueid', $actionqueueid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('aq.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('aq.actionqueueid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($actionqueueid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $actionqueueid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$actionqueueid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('actionqueue', $arr);
			$actionqueueid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('actionqueueid', $actionqueueid);
			$this->db->update('actionqueue', $arr);
		}

		return $actionqueueid;
	}


	// delete
	function delete($actionqueueid, $perm=false)
	{
		if($perm){
			$this->db->where('actionqueueid', $actionqueueid);
			$this->db->delete('actionqueue');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $actionqueueid);
		}
	}


	// inc_attempts
	function inc_attempts($actionqueueid)
	{
		// get
		$rs = $this->get($actionqueueid);

		// set
		$inc = ($rs['numattempts']) ? $rs['numattempts']+1 : 1;
		$arr = array('numattempts' => $inc);

		// check
		if($inc == 3 && !$rs['response']){
			$arr['response'] = 'Error [max-failed-attempts]';
		}

		// save
		$this->save($arr, $actionqueueid);
	}
}

/* End of file actionqueue_model.php */
/* Location: ./application/models/actionqueue_model.php */
