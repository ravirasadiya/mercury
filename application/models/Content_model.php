<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($contentid=null, $where=null, $other=null, $returnsingle=false)
	{
		$domain = $this->siteURL();
		// sql
		$sql = '
				c.*,ca.filelocation,ca.contentattachmentid,
				IF(c.filename IS NULL, c.filename, CONCAT("'.CONTENT_DIR.'", filename)) AS filepath,
				IF(ca.filelocation IS NULL, ca.filelocation, CONCAT("'.$domain.CONTENT_DIR.'", ca.filelocation)) AS filelocation
			';
		$this->db->select($sql);
		$this->db->from('content c');
		$this->db->join('contentattachment ca', 'ca.contentid=c.contentid', 'left');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($contentid){
			$this->db->where('c.contentid', $contentid);
		}
		if($where){
			$this->db->where(array_map('trim', $where));
		}
		$this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($contentid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}

	function get_company($contentid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		// $sql = '
		// 		c.*,ca.filelocation,ca.contentattachmentid,
		// 		IF(c.filename IS NULL, c.filename, CONCAT("'.CONTENT_DIR.'", filename)) AS filepath,
		// 		IF(ca.filelocation IS NULL, ca.filelocation, CONCAT("'.CONTENT_DIR.'", filelocation)) AS filelocation,
		// 		cp.title as parameter_title
		// 	';
		// $this->db->select($sql);
		// $this->db->from('content c');
		// $this->db->join('contentattachment ca', 'ca.contentid=c.contentid', 'left');
		// $this->db->join('competencyparameter cp', 'cp.competencyparameterid=c.competencyparameterid', 'left');
		// //$this->db->join('', ' ON ', 'left');
		// // where
		// if($contentid){
		// 	$this->db->where('c.contentid', $contentid);
		// }
		// if($where){
		// 	$this->db->where( array_map('trim', $where) );
		// }
		// $this->db->where('c.deletedon IS NULL');
		// if(isset($other['where-str'])){
		// 	$this->db->where($other['where-str']);
		// }
		// // order
		// if(isset($other['order'])){
		// 	// loop
		// 	foreach($other['order'] as $field => $dir){
		// 		$this->db->order_by($field, $dir);
		// 	}
		// }
		// //$this->db->order_by('c.');
		// // limit
		// if(isset($other['limit']) || isset($other['offset'])){
		// 	$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
		// 	$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

		// 	$this->db->limit($other['limit'], $other['offset']);
		// }

		$sql = 'SELECT DISTINCT 
			(content_tbl.competencyparameterid),
					(SELECT 
							count(content.competencyparameterid) 
					FROM 
						content 
					WHERE 
						content.competencyparameterid = content_tbl.competencyparameterid AND content.createdcompanyid IS NULL) AS content_count,
					(SELECT 
							count(content.competencyparameterid) 
					FROM 
						content 
					WHERE 
						content.competencyparameterid = content_tbl.competencyparameterid AND content.createdcompanyid IS NOT NULL) AS local_content_count,
					competencyparameter.title AS parameter_title

			FROM 
				content content_tbl
			INNER JOIN competencyparameter ON competencyparameter.competencyparameterid = content_tbl.competencyparameterid
			WHERE content_tbl.type = "content" AND content_tbl.islocked="1"
				';

		$rs = $this->db->query($sql);

		// row/rows
		$rs = ($contentid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $contentid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$contentid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('content', $arr);
			$contentid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('contentid', $contentid);
			$this->db->update('content', $arr);
		}

		return $contentid;
	}


	// delete
	function delete($contentid, $perm=false)
	{
		if($perm){
			$this->db->where('contentid', $contentid);
			$this->db->delete('content');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $contentid);
		}
	}
	function siteURL() {
	    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $domainName = $_SERVER['HTTP_HOST'];
	    return $protocol . $domainName;
	}
}

/* End of file content_model.php */
/* Location: ./application/models/content_model.php */
