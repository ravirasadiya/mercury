<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companyusergroupref_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companyusergrouprefid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cugr.*,
				cug.title AS group
			';
		$this->db->select($sql);
		$this->db->from('companyusergroupref cugr');
		$this->db->join('companyusergroup cug', 'cug.companyusergroupid=cugr.companyusergroupid', 'left');
		// where
		if($companyusergrouprefid){
			$this->db->where('cugr.companyusergrouprefid', $companyusergrouprefid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cugr.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('cug.title');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companyusergrouprefid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companyusergrouprefid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companyusergrouprefid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companyusergroupref', $arr);
			$companyusergrouprefid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companyusergrouprefid', $companyusergrouprefid);
			$this->db->update('companyusergroupref', $arr);
		}

		return $companyusergrouprefid;
	}


	// delete
	function delete($companyusergrouprefid, $perm=false)
	{
		if($perm){
			$this->db->where('companyusergrouprefid', $companyusergrouprefid);
			$this->db->delete('companyusergroupref');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companyusergrouprefid);
		}
	}


	// get key=>val array
	function get_keyval_arr($where=null, $other=null, $field='userid', $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$retarr[$row['companyusergrouprefid']] = $row[$field];
		}

		return $retarr;
	}


	// update
	function update($arr, $companyusergroupid)
	{
		// get
		$rs = $this->get_keyval_arr(array('cugr.companyusergroupid' => $companyusergroupid));

		// loop
		foreach($arr as $i => $userid){
			// check
			$companyusergrouprefid = ($i > 0) ? $i : null;

			// set
			$row = array();
			$row['companyusergroupid'] = $companyusergroupid;
			$row['userid'] = $userid;

			// save
			$this->save($row, $companyusergrouprefid);

			// check
			if(isset($rs[$companyusergrouprefid])){
				// unset
				unset($rs[$companyusergrouprefid]);
			}
		}

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($rs as $companyusergrouprefid => $val){
			// delete
			$this->delete($companyusergrouprefid);
		}
	}
}

/* End of file companyusergroupref_model.php */
/* Location: ./application/models/companyusergroupref_model.php */
