<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($userid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				u.*,
				CONCAT(u.firstname, " ", u.lastname) AS fullname,
				DATEDIFF(DATE(NOW()), DATE(u.lastloginon)) AS dayssincelastlogin,
				sp_cut.userid AS smuserid,
				sp_cut.title AS spteam,
				sp_cut.companyuserteamid,
				sm_cug.title AS smgroup,
				sm_cug.companyusergroupid,
				CONCAT(sm_dir.firstname, " ", sm_dir.lastname) AS director,
				CONCAT(sp_man.firstname, " ", sp_man.lastname) AS salesmanager,
				IF(u.isenabled, "Yes", "No") AS enabled,
				IF(u.onbehalfof, "Yes", "No") AS onbehalfed,
				c.isenabled companyisenabled,
				IF(c.isenabled, "Yes", "No") AS companyenabled,
				c.title AS company
			';
		$this->db->select($sql);
		$this->db->from('user u');
		$this->db->join('company c', 'c.companyid=u.companyid', 'left');
		$this->db->join('companyuserteamref sp_cutr', 'sp_cutr.userid=u.userid AND sp_cutr.deletedon IS NULL', 'left');
		$this->db->join('companyuserteam sp_cut', 'sp_cut.companyuserteamid=sp_cutr.companyuserteamid AND sp_cut.deletedon IS NULL', 'left');
		$this->db->join('user sp_man', 'sp_man.userid=sp_cut.userid', 'left');

		$this->db->join('companyuserteam sm_cut', 'sm_cut.userid=u.userid AND sm_cut.deletedon IS NULL', 'left');
		$this->db->join('companyusergroupref sm_cugr', 'sm_cugr.userid=u.userid AND sm_cugr.deletedon IS NULL', 'left');
		$this->db->join('companyusergroup sm_cug', 'sm_cug.companyusergroupid=sm_cugr.companyusergroupid', 'left');
		$this->db->join('user sm_dir', 'sm_dir.userid=sm_cug.userid', 'left');

		// where
		if($userid){
			$this->db->where('u.userid', $userid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('u.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('u.firstname');
		$this->db->order_by('u.lastname');
		$this->db->group_by('u.userid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($userid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $userid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$userid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('user', $arr);
			$userid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('userid', $userid);
			$this->db->update('user', $arr);
		}

		return $userid;
	}


	// delete
	function delete($userid, $perm=false)
	{
		if($perm){
			$this->db->where('userid', $userid);
			$this->db->delete('user');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $userid);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */
