<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companyuserteam_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companyuserteamid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cut.*,
				CONCAT(smu.firstname, " ", smu.lastname) AS manager,
				cug.companyusergroupid,
				cug.title AS group,
				mdu.userid AS mduserid,
				CONCAT(mdu.firstname, " ", mdu.lastname) AS director
			';
		$this->db->select($sql);
		$this->db->from('companyuserteam cut');
		$this->db->join('user smu', 'smu.userid=cut.userid', 'left');
		$this->db->join('companyusergroupref cugr', 'cugr.userid=cut.userid AND cugr.deletedon IS NULL', 'left');
		$this->db->join('companyusergroup cug', 'cug.companyusergroupid=cugr.companyusergroupid AND cug.deletedon IS NULL', 'left');
		$this->db->join('user mdu', 'mdu.userid=cug.userid', 'left');
		// where
		if($companyuserteamid){
			$this->db->where('cut.companyuserteamid', $companyuserteamid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cut.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('smu.firstname');
		$this->db->order_by('smu.lastname');
		$this->db->order_by('cut.title');
		$this->db->group_by('cut.companyuserteamid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companyuserteamid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companyuserteamid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companyuserteamid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companyuserteam', $arr);
			$companyuserteamid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companyuserteamid', $companyuserteamid);
			$this->db->update('companyuserteam', $arr);
		}

		return $companyuserteamid;
	}


	// delete
	function delete($companyuserteamid, $perm=false)
	{
		if($perm){
			$this->db->where('companyuserteamid', $companyuserteamid);
			$this->db->delete('companyuserteam');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companyuserteamid);
		}
	}


	// get manager team array
	function get_manager_team_array($companyid, $retarr=array())
	{
		// get
		$rs = $this->get(null, array('cut.companyid' => $companyid));

		// loop
		foreach($rs as $row){
			$retarr[$row['manager']][$row['companyuserteamid']] = $row['title'];
		}

		return $retarr;
	}
}

/* End of file companyuserteam_model.php */
/* Location: ./application/models/companyuserteam_model.php */
