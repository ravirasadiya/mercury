<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companyid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				c.*,
				IF(c.isenabled, "Yes", "No") AS enabled,
				CONCAT("'.UPLOADS_DIR.'", c.uploadsdir) AS fulluploadsdir
			';
		$this->db->select($sql);
		$this->db->from('company c');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($companyid){
			$this->db->where('c.companyid', $companyid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('c.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('c.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companyid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;
		
		return $rs;
	}


	// save
	function save($arr, $companyid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companyid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('company', $arr);
			$companyid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companyid', $companyid);
			$this->db->update('company', $arr);
		}

		return $companyid;
	}


	// delete
	function delete($companyid, $perm=false)
	{
		if($perm){
			$this->db->where('companyid', $companyid);
			$this->db->delete('company');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companyid);
		}
	}
}

/* End of file company_model.php */
/* Location: ./application/models/company_model.php */
