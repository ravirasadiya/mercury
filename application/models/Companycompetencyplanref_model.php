<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companycompetencyplanref_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companycompetencyplanrefid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				ccpr.*,
				umd.userid AS mduserid,
				CONCAT(umd.firstname, " ", umd.lastname) AS mdname,
				usm.userid AS smuserid,
				CONCAT(usm.firstname, " ", usm.lastname) AS smname,
				cug.companyusergroupid AS mdgroupid,
				cug.title AS mdgroup,
				cut.companyuserteamid AS smteamid,
				cut.title AS smteam,
				cg.title AS group,
				cg.description AS groupdescription,
				cp.title AS parameter,
				cp.description AS parameterdescription,
				cs.title AS suggestion,
				cgpsr.incnum
			';
		$this->db->select($sql);
		$this->db->from('companycompetencyplanref ccpr');
		$this->db->join('companycompetencyplan ccp', 'ccp.companycompetencyplanid=ccpr.companycompetencyplanid', 'left');
		$this->db->join('competencygroup cg', 'cg.competencygroupid=ccpr.competencygroupid', 'left');
		$this->db->join('competencyparameter cp', 'cp.competencyparameterid=ccpr.competencyparameterid', 'left');
		$this->db->join('competencysuggestion cs', 'cs.competencysuggestionid=ccpr.competencysuggestionid', 'left');
		$this->db->join('companygroupparamsuggref cgpsr', 'cgpsr.companygroupparamsuggrefid=ccpr.companygroupparamsuggrefid AND cgpsr.deletedon IS NULL', 'left');
		$this->db->join('companyuserteam cut', 'cut.companyuserteamid=ccp.companyuserteamid', 'left');
		$this->db->join('user usm', 'usm.userid=cut.userid', 'left');
		$this->db->join('companyusergroupref cugr', 'cugr.userid=cut.userid AND cugr.deletedon IS NULL', 'left');
		$this->db->join('companyusergroup cug', 'cug.companyusergroupid=cugr.companyusergroupid', 'left');
		$this->db->join('user umd', 'umd.userid=cug.userid', 'left');
		// where
		if($companycompetencyplanrefid){
			$this->db->where('ccpr.companycompetencyplanrefid', $companycompetencyplanrefid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('ccpr.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		// loop
		/*foreach(array('cug.title', 'cut.title', 'ccpr.competencygroupid', 'cg.title', 'ccpr.competencyparameterid', 'cp.title', 'ccpr.competencysuggestionid', 'cs.title') as $field){
			if(stristr($field, 'title')){
				$this->db->order_by("LENGTH($field)", 'ASC');
			}
			$this->db->order_by($field, 'ASC');
		}*/
		$this->db->order_by('cgpsr.incnum', 'ASC');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companycompetencyplanrefid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companycompetencyplanrefid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companycompetencyplanrefid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companycompetencyplanref', $arr);
			$companycompetencyplanrefid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companycompetencyplanrefid', $companycompetencyplanrefid);
			$this->db->update('companycompetencyplanref', $arr);
		}

		return $companycompetencyplanrefid;
	}


	// delete
	function delete($companycompetencyplanrefid, $perm=false)
	{
		if($perm){
			$this->db->where('companycompetencyplanrefid', $companycompetencyplanrefid);
			$this->db->delete('companycompetencyplanref');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companycompetencyplanrefid);
		}
	}


	// get key=>val arrtay
	function get_keyval_arr($where=null, $other=null, $field='companycompetencyplanrefid', $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$retarr[$row['companycompetencyplanrefid']] = $row[$field];
		}

		return $retarr;
	}


	// update - groups
	function update_groups($arr, $companycompetencyplanid)
	{
		// get
		$companycompetencyplanrs = $this->companycompetencyplan_model->get($companycompetencyplanid);
		$rs = $this->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL AND ccpr.competencysuggestionid IS NULL'));

		//echo '<pre>'; print_r($arr); echo '</pre>';
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($arr as $i => $competencygroupid){
			// check
			$companycompetencyplanrefid = ($i > 0) ? $i : null;

			// get
			$companygroupparamsuggrefrs = $this->companygroupparamsuggref_model->get(null, array('cgpsr.companyid' => $companycompetencyplanrs['companyid'], 'cgpsr.competencygroupid' => $competencygroupid), null, true);
			$companygroupparamsuggrefid = ($companygroupparamsuggrefrs) ? $companygroupparamsuggrefrs['companygroupparamsuggrefid'] : $this->companygroupparamsuggref_model->save(array('companyid' => $companycompetencyplanrs['companyid'], 'competencygroupid' => $competencygroupid));

			// set
			$row = array();
			$row['companycompetencyplanid'] = $companycompetencyplanid;
			$row['competencygroupid'] = $competencygroupid;
			$row['companygroupparamsuggrefid'] = $companygroupparamsuggrefid;

			// save
			$this->save($row, $companycompetencyplanrefid);

			// check
			if(isset($rs[$companycompetencyplanrefid])){
				// unset
				unset($rs[$companycompetencyplanrefid]);
			}
		}

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($rs as $companycompetencyplanrefid => $val){
			// delete
			$this->delete($companycompetencyplanrefid);
		}
	}

	// update - groups
	function update_groups_ajax_add($arr, $companycompetencyplanid,$sortorder=null)
	{
		// get
		$companycompetencyplanrs = $this->companycompetencyplan_model->get($companycompetencyplanid);
		$rs = $this->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL AND ccpr.competencysuggestionid IS NULL'));

		//echo '<pre>'; print_r($arr); echo '</pre>';
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($arr as $i => $competencygroupid){
			// check
			$companycompetencyplanrefid = ($i > 0) ? $i : null;

			// get
			$companygroupparamsuggrefrs = $this->companygroupparamsuggref_model->get(null, array('cgpsr.companyid' => $companycompetencyplanrs['companyid'], 'cgpsr.competencygroupid' => $competencygroupid), null, true);
			$companygroupparamsuggrefid = ($companygroupparamsuggrefrs) ? $companygroupparamsuggrefrs['companygroupparamsuggrefid'] : $this->companygroupparamsuggref_model->save(array('companyid' => $companycompetencyplanrs['companyid'], 'competencygroupid' => $competencygroupid));

			// set
			$row = array();
			$row['companycompetencyplanid'] = $companycompetencyplanid;
			$row['competencygroupid'] = $competencygroupid;
			$row['companygroupparamsuggrefid'] = $companygroupparamsuggrefid;
			$row['sortorder'] = $sortorder;

			// $companygroupparamsuggrefarr = array('incnum'=>$sortorder);
			// $this->companygroupparamsuggref_model->save($companygroupparamsuggrefarr,$companygroupparamsuggrefid);

			// save
			$companycompetencyplanrefid = $this->save($row, $companycompetencyplanrefid);
			// check
			if(isset($rs[$companycompetencyplanrefid])){
				// unset
				unset($rs[$companycompetencyplanrefid]);
			}
		}
		return $companycompetencyplanrefid;
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		// foreach($rs as $companycompetencyplanrefid => $val){
		// 	// delete
		// 	$this->delete($companycompetencyplanrefid);
		// }
	}

	function update_groups_ajax_remove($companycompetencyplanrefid)
	{
		$this->delete($companycompetencyplanrefid,true);
	}


	// update - parameters
	function update_parameters($arr, $companycompetencyplanid)
	{
		// get
		$companycompetencyplanrs = $this->companycompetencyplan_model->get($companycompetencyplanid);
		$rs = $this->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'));

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($arr as $competencygroupid => $parr){
			foreach($parr as $i => $competencyparameterid){
				// check
				$companycompetencyplanrefid = ($i > 0) ? $i : null;

				// get
				$companygroupparamsuggrefrs = $this->companygroupparamsuggref_model->get(null, array('cgpsr.companyid' => $companycompetencyplanrs['companyid'], 'cgpsr.competencygroupid' => $competencygroupid, 'cgpsr.competencyparameterid' => $competencyparameterid), null, true);
				$companygroupparamsuggrefid = ($companygroupparamsuggrefrs) ? $companygroupparamsuggrefrs['companygroupparamsuggrefid'] : $this->companygroupparamsuggref_model->save(array('companyid' => $companycompetencyplanrs['companyid'], 'competencygroupid' => $competencygroupid,'competencyparameterid' => $competencyparameterid));

				// set
				$row = array();
				$row['companycompetencyplanid'] = $companycompetencyplanid;
				$row['competencygroupid'] = $competencygroupid;
				$row['competencyparameterid'] = $competencyparameterid;
				$row['companygroupparamsuggrefid'] = $companygroupparamsuggrefid;

				// save
				$this->save($row, $companycompetencyplanrefid);

				// check
				if(isset($rs[$companycompetencyplanrefid])){
					// unset
					unset($rs[$companycompetencyplanrefid]);
				}
			}
		}

		// loop
		foreach($rs as $companycompetencyplanrefid => $val){
			// delete
			$this->delete($companycompetencyplanrefid);
		}
	}

	// update - parameters
	function update_parameters_ajax_add($arr, $companycompetencyplanid,$sortorder=null)
	{
		// get
		$companycompetencyplanrs = $this->companycompetencyplan_model->get($companycompetencyplanid);
		$rs = $this->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'));
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($arr as $competencygroupid => $parr){
			foreach($parr as $i => $competencyparameterid){
				// check
				$companycompetencyplanrefid = ($i > 0) ? $i : null;

				// get
				$companygroupparamsuggrefrs = $this->companygroupparamsuggref_model->get(null, array('cgpsr.companyid' => $companycompetencyplanrs['companyid'], 'cgpsr.competencygroupid' => $competencygroupid, 'cgpsr.competencyparameterid' => $competencyparameterid), null, true);
				$companygroupparamsuggrefid = ($companygroupparamsuggrefrs) ? $companygroupparamsuggrefrs['companygroupparamsuggrefid'] : $this->companygroupparamsuggref_model->save(array('companyid' => $companycompetencyplanrs['companyid'], 'competencygroupid' => $competencygroupid,'competencyparameterid' => $competencyparameterid));

				// set
				$row = array();
				$row['companycompetencyplanid'] = $companycompetencyplanid;
				$row['competencygroupid'] = $competencygroupid;
				$row['competencyparameterid'] = $competencyparameterid;
				$row['companygroupparamsuggrefid'] = $companygroupparamsuggrefid;
				$row['sortorder'] = $sortorder;

			// 	$companygroupparamsuggrefarr = array('incnum'=>$sortorder);
			// $this->companygroupparamsuggref_model->save($companygroupparamsuggrefarr,$companygroupparamsuggrefid);

				// save
				$companycompetencyplanrefid = $this->save($row, $companycompetencyplanrefid);

				// check
				if(isset($rs[$companycompetencyplanrefid])){
					// unset
					unset($rs[$companycompetencyplanrefid]);
				}
			}
		}

		return $companycompetencyplanrefid;
		// loop
		// foreach($rs as $companycompetencyplanrefid => $val){
		// 	// delete
		// 	$this->delete($companycompetencyplanrefid);
		// }
	}

	// update - parameters
	function update_parameters_ajax_delete($companycompetencyplanrefid)
	{
		$this->delete($companycompetencyplanrefid,true);
	}

	function update_suggestion_ajax_delete($companycompetencyplanrefid)
	{
		$this->delete($companycompetencyplanrefid,true);
	}

	// update - suggestions
	function update_suggestions($arr, $companycompetencyplanid)
	{
		// get
		$companycompetencyplanrs = $this->companycompetencyplan_model->get($companycompetencyplanid);
		$rs = $this->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL'));

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($arr as $competencygroupid => $parr){
			foreach($parr as $competencyparameterid => $sarr){
				foreach($sarr as $i => $competencysuggestionid){
					// check
					$companycompetencyplanrefid = ($i > 0) ? $i : null;

					// get
					$companygroupparamsuggrefrs = $this->companygroupparamsuggref_model->get(null, array('cgpsr.companyid' => $companycompetencyplanrs['companyid'], 'cgpsr.competencygroupid' => $competencygroupid, 'cgpsr.competencyparameterid' => $competencyparameterid, 'cgpsr.competencysuggestionid' => $competencysuggestionid), null, true);
					$companygroupparamsuggrefid = ($companygroupparamsuggrefrs) ? $companygroupparamsuggrefrs['companygroupparamsuggrefid'] : $this->companygroupparamsuggref_model->save(array('companyid' => $companycompetencyplanrs['companyid'], 'competencygroupid' => $competencygroupid, 'competencyparameterid' => $competencyparameterid, 'competencysuggestionid' => $competencysuggestionid));

					// set
					$row = array();
					$row['companycompetencyplanid'] = $companycompetencyplanid;
					$row['competencygroupid'] = $competencygroupid;
					$row['competencyparameterid'] = $competencyparameterid;
					$row['competencysuggestionid'] = $competencysuggestionid;
					$row['companygroupparamsuggrefid'] = $companygroupparamsuggrefid;

					// save
					$this->save($row, $companycompetencyplanrefid);

					// check
					if(isset($rs[$companycompetencyplanrefid])){
						// unset
						unset($rs[$companycompetencyplanrefid]);
					}
				}
			}
		}

		// loop
		foreach($rs as $companycompetencyplanrefid => $val){
			// delete
			$this->delete($companycompetencyplanrefid);
		}
	}

	// update - suggestions
	function update_suggestions_ajax_add($arr, $companycompetencyplanid,$sortorder=null)
	{
		// get
		$companycompetencyplanrs = $this->companycompetencyplan_model->get($companycompetencyplanid);
		$rs = $this->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL'));

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
		// loop
		foreach($arr as $competencygroupid => $parr){
			foreach($parr as $competencyparameterid => $sarr){
				foreach($sarr as $i => $competencysuggestionid){
					// check
					$companycompetencyplanrefid = ($i > 0) ? $i : null;

					// get
					$companygroupparamsuggrefrs = $this->companygroupparamsuggref_model->get(null, array('cgpsr.companyid' => $companycompetencyplanrs['companyid'], 'cgpsr.competencygroupid' => $competencygroupid, 'cgpsr.competencyparameterid' => $competencyparameterid, 'cgpsr.competencysuggestionid' => $competencysuggestionid), null, true);
					$companygroupparamsuggrefid = ($companygroupparamsuggrefrs) ? $companygroupparamsuggrefrs['companygroupparamsuggrefid'] : $this->companygroupparamsuggref_model->save(array('companyid' => $companycompetencyplanrs['companyid'], 'competencygroupid' => $competencygroupid, 'competencyparameterid' => $competencyparameterid, 'competencysuggestionid' => $competencysuggestionid));

					// set
					$row = array();
					$row['companycompetencyplanid'] = $companycompetencyplanid;
					$row['competencygroupid'] = $competencygroupid;
					$row['competencyparameterid'] = $competencyparameterid;
					$row['competencysuggestionid'] = $competencysuggestionid;
					$row['companygroupparamsuggrefid'] = $companygroupparamsuggrefid;
					$row['sortorder'] = $sortorder;

			// 		$companygroupparamsuggrefarr = array('incnum'=>$sortorder);
			// $this->companygroupparamsuggref_model->save($companygroupparamsuggrefarr,$companygroupparamsuggrefid);

					// save
					$companycompetencyplanrefid = $this->save($row, $companycompetencyplanrefid);

					// check
					if(isset($rs[$companycompetencyplanrefid])){
						// unset
						unset($rs[$companycompetencyplanrefid]);
					}
				}
			}
		}
		return $companycompetencyplanrefid;
		// loop
		// foreach($rs as $companycompetencyplanrefid => $val){
		// 	// delete
		// 	$this->delete($companycompetencyplanrefid);
		// }
	}


	// get hier ref
	function get_keyval_hier_arr($companycompetencyplanid, $retarr=array())
	{
		// get
		$rs = $this->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid),array('order'=>array('ccpr.sortorder'=>'ASC')));

		// loop - groups
		foreach($rs as $row){
			$companycompetencyplanrefid = $row['companycompetencyplanrefid'];
			$sortorder = $row['sortorder'];
			$companycompetencyplanid = $row['companycompetencyplanid'];
			$competencygroupid = $row['competencygroupid'];
			$competencyparameterid = $row['competencyparameterid'];
			$competencysuggestionid = $row['competencysuggestionid'];

			// check
			if($competencyparameterid || $competencysuggestionid){
				continue;
			}

			// set
			$retarr[$competencygroupid]['refid'] = $companycompetencyplanrefid;
			$retarr[$competencygroupid]['ordercount'] = $sortorder;
		}

		// loop - parameters
		foreach($rs as $row){
			$companycompetencyplanrefid = $row['companycompetencyplanrefid'];
			$sortorder = $row['sortorder'];

			$companycompetencyplanid = $row['companycompetencyplanid'];
			$competencygroupid = $row['competencygroupid'];
			$competencyparameterid = $row['competencyparameterid'];
			$competencysuggestionid = $row['competencysuggestionid'];

			// check
			if(!isset($retarr[$competencygroupid]) || !$competencyparameterid || $competencysuggestionid){
				continue;
			}
			// set
			$retarr[$competencygroupid]['parameters'][$competencyparameterid]['refid'] = $companycompetencyplanrefid;
			$retarr[$competencygroupid]['parameters'][$competencyparameterid]['ordercount'] = $sortorder;
		}

		// loop - suggestions
		foreach($rs as $row){
			$sortorder = $row['sortorder'];
			$companycompetencyplanrefid = $row['companycompetencyplanrefid'];
			$companycompetencyplanid = $row['companycompetencyplanid'];
			$competencygroupid = $row['competencygroupid'];
			$competencyparameterid = $row['competencyparameterid'];
			$competencysuggestionid = $row['competencysuggestionid'];

			// check
			if(!isset($retarr[$competencygroupid]['parameters'][$competencyparameterid]) || !$competencysuggestionid){
				continue;
			}

			// set
			$retarr[$competencygroupid]['parameters'][$competencyparameterid]['suggestions'][$competencysuggestionid]['refid'] = $companycompetencyplanrefid;
			$retarr[$competencygroupid]['parameters'][$competencyparameterid]['suggestions'][$competencysuggestionid]['ordercount'] = $sortorder;
		}
		return $retarr;
	}
}

/* End of file companycompetencyplanref_model.php */
/* Location: ./application/models/companycompetencyplanref_model.php */
