<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companysessionratingref_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($companysessionratingrefid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				csrf.*,
				sri.title AS rating
			';
		$this->db->select($sql);
		$this->db->from('companysessionratingref csrf');
		$this->db->join('companysessionrating csr', 'csr.companysessionratingid=csrf.companysessionratingid', 'left');
		$this->db->join('sessionratingitem sri', 'sri.sessionratingitemid=csrf.sessionratingitemid', 'left');
		// where
		if($companysessionratingrefid){
			$this->db->where('csrf.companysessionratingrefid', $companysessionratingrefid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('csrf.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('csrf.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($companysessionratingrefid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $companysessionratingrefid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$companysessionratingrefid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('companysessionratingref', $arr);
			$companysessionratingrefid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('companysessionratingrefid', $companysessionratingrefid);
			$this->db->update('companysessionratingref', $arr);
		}

		return $companysessionratingrefid;
	}


	// delete
	function delete($companysessionratingrefid, $perm=false)
	{
		if($perm){
			$this->db->where('companysessionratingrefid', $companysessionratingrefid);
			$this->db->delete('companysessionratingref');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $companysessionratingrefid);
		}
	}


	// get key=>val array
	function get_keyval_arr($where=null, $other=null, $field='sessionratingitemid', $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$retarr[$row['companysessionratingrefid']] = $row[$field];
		}

		return $retarr;
	}


	// update
	function update($arr, $companysessionratingid)
	{
		// get
		$rs = $this->get_keyval_arr(array('csrf.companysessionratingid' => $companysessionratingid));

		// loop
		foreach($arr as $i => $sessionratingitemid){
			// check
			$companysessionratingrefid = ($i > 0) ? $i : null;

			// set
			$row = array();
			$row['companysessionratingid'] = $companysessionratingid;
			$row['sessionratingitemid'] = $sessionratingitemid;

			// save
			$this->save($row, $companysessionratingrefid);

			// check
			if(isset($rs[$companysessionratingrefid])){
				// unset
				unset($rs[$companysessionratingrefid]);
			}
		}

		// loop
		foreach($rs as $companysessionratingrefid => $val){
			// delete
			$this->delete($companysessionratingrefid);
		}
	}
}

/* End of file companysessionratingref_model.php */
/* Location: ./application/models/companysessionratingref_model.php */
