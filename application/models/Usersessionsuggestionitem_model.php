<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Usersessionsuggestionitem_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($usersessionsuggestionitemid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				ussi.*,
				ccpr.competencygroupid,
				ccpr.competencyparameterid,
				ccpr.competencysuggestionid,
				cs.createdon AS sessiondate,
				cp.title AS parameter,
				csu.title AS suggestion,
				cs.customertitle,
				DATE(DATE_ADD(cs.createdon, INTERVAL 7 DAY)) AS duedate,
				smu.userid AS salesmanagerid,
				CONCAT(smu.firstname, " ", smu.lastname) AS salesmanager,
				spu.userid AS salespersonid,
				CONCAT(spu.firstname, " ", spu.lastname) AS salesperson,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS group_index,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid=ccpr.competencyparameterid AND cgpsr.competencysuggestionid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS param_index,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid=ccpr.competencyparameterid AND cgpsr.competencysuggestionid=ccpr.competencysuggestionid AND cgpsr.deletedon IS NULL LIMIT 1) AS sugg_index
			';
		$this->db->select($sql);
		$this->db->from('usersessionsuggestionitem ussi');
		$this->db->join('coachingsession cs', 'cs.coachingsessionid=ussi.coachingsessionid', 'left');
		$this->db->join('companycompetencyplanref ccpr', 'ccpr.companycompetencyplanrefid=ussi.companycompetencyplanrefid', 'left');
		$this->db->join('competencyparameter cp', 'cp.competencyparameterid=ccpr.competencyparameterid', 'left');
		$this->db->join('competencysuggestion csu', 'csu.competencysuggestionid=ccpr.competencysuggestionid', 'left');
		$this->db->join('user smu', 'smu.userid=cs.salesmanageruserid', 'left');
		$this->db->join('user spu', 'spu.userid=cs.salespersonaluserid', 'left');
		// where
		if($usersessionsuggestionitemid){
			$this->db->where('ussi.usersessionsuggestionitemid', $usersessionsuggestionitemid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('ussi.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		if(isset($other['having-str'])){
			$this->db->having($other['having-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('ussi.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usersessionsuggestionitemid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}

	function get_dev_plan($usersessionsuggestionitemid=null, $where=null, $other=null, $returnsingle=false,$mngrid=null)
	{
		// sql
		$sql = '
				ussi.*,
				ccpr.competencygroupid,
				ccpr.competencyparameterid,
				ccpr.competencysuggestionid,
				cs.createdon AS sessiondate,
				cp.title AS parameter,
				csu.title AS suggestion,
				cs.customertitle,
				DATE(DATE_ADD(cs.createdon, INTERVAL 7 DAY)) AS duedate,
				smu.userid AS salesmanagerid,
				CONCAT(smu.firstname, " ", smu.lastname) AS salesmanager,
				spu.userid AS salespersonid,
				CONCAT(spu.firstname, " ", spu.lastname) AS salesperson,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS group_index,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid=ccpr.competencyparameterid AND cgpsr.competencysuggestionid IS NULL AND cgpsr.deletedon IS NULL LIMIT 1) AS param_index,
				(SELECT incnum FROM companygroupparamsuggref cgpsr WHERE cgpsr.companyid=smu.companyid AND cgpsr.competencygroupid=ccpr.competencygroupid AND cgpsr.competencyparameterid=ccpr.competencyparameterid AND cgpsr.competencysuggestionid=ccpr.competencysuggestionid AND cgpsr.deletedon IS NULL LIMIT 1) AS sugg_index
			';
		$this->db->select($sql);
		$this->db->from('usersessionsuggestionitem ussi');
		$this->db->join('coachingsession cs', 'cs.coachingsessionid=ussi.coachingsessionid', 'left');
		$this->db->join('companycompetencyplanref ccpr', 'ccpr.companycompetencyplanrefid=ussi.companycompetencyplanrefid', 'left');
		$this->db->join('competencyparameter cp', 'cp.competencyparameterid=ccpr.competencyparameterid', 'left');
		$this->db->join('competencysuggestion csu', 'csu.competencysuggestionid=ccpr.competencysuggestionid', 'left');
		$this->db->join('user smu', 'smu.userid='.$mngrid, 'left');
		$this->db->join('user spu', 'spu.userid=cs.salespersonaluserid', 'left');
		// where
		if($usersessionsuggestionitemid){
			$this->db->where('ussi.usersessionsuggestionitemid', $usersessionsuggestionitemid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('ussi.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		if(isset($other['having-str'])){
			$this->db->having($other['having-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('ussi.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usersessionsuggestionitemid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $usersessionsuggestionitemid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$usersessionsuggestionitemid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('usersessionsuggestionitem', $arr);
			$usersessionsuggestionitemid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('usersessionsuggestionitemid', $usersessionsuggestionitemid);
			$this->db->update('usersessionsuggestionitem', $arr);
		}

		return $usersessionsuggestionitemid;
	}


	// delete
	function delete($usersessionsuggestionitemid, $perm=false)
	{
		if($perm){
			$this->db->where('usersessionsuggestionitemid', $usersessionsuggestionitemid);
			$this->db->delete('usersessionsuggestionitem');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $usersessionsuggestionitemid);
		}
	}
}

/* End of file usersessionsuggestionitem_model.php */
/* Location: ./application/models/usersessionsuggestionitem_model.php */
