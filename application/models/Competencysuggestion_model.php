<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Competencysuggestion_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($competencysuggestionid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				cs.*,
				cp.title AS competencyparameter,
				cg.competencygroupid,
				cg.title AS competencygroup,
				CONCAT(cg.competencygroupid, ".", cp.competencyparameterid, ".", cs.competencysuggestionid) AS id,
			';
		$this->db->select($sql);
		$this->db->from('competencysuggestion cs');
		$this->db->join('competencyparameter cp', 'cp.competencyparameterid=cs.competencyparameterid', 'left');
		$this->db->join('competencygroup cg', 'cg.competencygroupid=cp.competencygroupid', 'left');
		// where
		if($competencysuggestionid){
			$this->db->where('cs.competencysuggestionid', $competencysuggestionid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('cs.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		$this->db->order_by('cs.title');
		$this->db->group_by('cs.competencysuggestionid');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($competencysuggestionid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $competencysuggestionid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$competencysuggestionid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('competencysuggestion', $arr);
			$competencysuggestionid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('competencysuggestionid', $competencysuggestionid);
			$this->db->update('competencysuggestion', $arr);
		}

		return $competencysuggestionid;
	}


	// delete
	function delete($competencysuggestionid, $perm=false)
	{
		if($perm){
			$this->db->where('competencysuggestionid', $competencysuggestionid);
			$this->db->delete('competencysuggestion');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $competencysuggestionid);
		}
	}
}

/* End of file competencysuggestion_model.php */
/* Location: ./application/models/competencysuggestion_model.php */
