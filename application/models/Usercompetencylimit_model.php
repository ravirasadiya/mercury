<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Usercompetencylimit_model extends CI_Model {

	public $limit = 100;
	public $totalrows = 0;

	// get/get all
	function get($usercompetencylimitid=null, $where=null, $other=null, $returnsingle=false)
	{
		// sql
		$sql = '
				ucl.*
			';
		$this->db->select($sql);
		$this->db->from('usercompetencylimit ucl');
		//$this->db->join('', ' ON ', 'left');
		// where
		if($usercompetencylimitid){
			$this->db->where('ucl.usercompetencylimitid', $usercompetencylimitid);
		}
		if($where){
			$this->db->where( array_map('trim', $where) );
		}
		$this->db->where('ucl.deletedon IS NULL');
		if(isset($other['where-str'])){
			$this->db->where($other['where-str']);
		}
		// order
		if(isset($other['order'])){
			// loop
			foreach($other['order'] as $field => $dir){
				$this->db->order_by($field, $dir);
			}
		}
		//$this->db->order_by('ucl.');
		// limit
		if(isset($other['limit']) || isset($other['offset'])){
			$other['limit'] = (isset($other['limit'])) ? $other['limit'] : $this->limit;
			$other['offset'] = (isset($other['offset'])) ? $other['offset'] : 0;

			$this->db->limit($other['limit'], $other['offset']);
		}
		$rs = $this->db->get();

		// row/rows
		$rs = ($usercompetencylimitid || $returnsingle) ? $rs->row_array() : $rs->result_array();

		// set total rows
		$this->totalrows = ($rs && isset($rs[0])) ? count($rs) : $this->totalrows;
		$this->totalrows = ($rs && !isset($rs[0])) ? 1 : $this->totalrows;

		return $rs;
	}


	// save
	function save($arr, $usercompetencylimitid=null)
	{
		// clean
		$arr = array_map('trim_to_null', $arr);

		// insert/update
		if(!$usercompetencylimitid){
			$arr['createdon'] = $arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->insert('usercompetencylimit', $arr);
			$usercompetencylimitid = $this->db->insert_id();
		}else{
			$arr['updatedon'] = date("Y-m-d H:i:s");

			$this->db->where('usercompetencylimitid', $usercompetencylimitid);
			$this->db->update('usercompetencylimit', $arr);
		}

		return $usercompetencylimitid;
	}


	// delete
	function delete($usercompetencylimitid, $perm=false)
	{
		if($perm){
			$this->db->where('usercompetencylimitid', $usercompetencylimitid);
			$this->db->delete('usercompetencylimit');
		}else{
			$this->save(array('deletedon' => date("Y-m-d H:i:s")), $usercompetencylimitid);
		}
	}


	// get key=>val array
	function get_keyval_arr($where=null, $other=null, $field='userid', $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			$retarr[$row['usercompetencylimitid']] = $row[$field];
		}

		return $retarr;
	}


	// get hier key=>val array
	function get_hier_keyval_arr($where=null, $other=null, $field='value', $retarr=array())
	{
		// get
		$rs = $this->get(null, $where, $other);

		// loop
		foreach($rs as $row){
			if($row['competencyparameterid']){
				$retarr[$row['competencygroupid']][$row['competencyparameterid']][$field] = $row[$field];
			}else{
				$retarr[$row['competencygroupid']][$field] = $row[$field];
			}
		}

		return $retarr;
	}


	// update - groups
	function update($arr, $userid, $other, $field)
	{
		// get
		$rs = $this->get_hier_keyval_arr(array('ucl.userid' => $userid), $other, $field);
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($arr as $id => $val){
			$idarr = explode('|', $id.'|');
			$competencygroupid = $idarr[0];
			$competencyparameterid = $idarr[1];

			// check
			$usercompetencylimitid = ($competencyparameterid && isset($rs[$competencygroupid][$competencyparameterid])) ? $rs[$competencygroupid][$competencyparameterid][$field] : null;
			$usercompetencylimitid = (!$competencyparameterid && isset($rs[$competencygroupid])) ? $rs[$competencygroupid][$field] : $usercompetencylimitid;

			//echo '<pre>'; print_r($usercompetencylimitid); echo '</pre>'; exit;

			// set
			$row = array();
			if(count($idarr) > 1){
				$row['competencygroupid'] = $competencygroupid;
				$row['competencyparameterid'] = $competencyparameterid;
			}else{
				$row[$field] = $id;
			}
			$row['userid'] = $userid;
			$row['value'] = $val;

			// save
			$this->save($row, $usercompetencylimitid);

			// check
			if($usercompetencylimitid){
				// unset
				unset($rs[$usercompetencylimitid]);
			}
		}

		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($rs as $usercompetencylimitid => $val){
			// delete
			$this->delete($usercompetencylimitid);
		}
	}
}

/* End of file usercompetencylimit_model.php */
/* Location: ./application/models/usercompetencylimit_model.php */
