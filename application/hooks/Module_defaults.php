<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Moduledefaults {

	private $ci;

	public function __construct() {
		$this->ci =& get_instance();

		// load
		$this->ci->load->library('session');
	}

	// set defaults
	public function set_module_defaults()
	{
		// set module
		$module = $this->ci->router->fetch_module();

		//echo CI_VERSION;

		// set
		define('MODULE', $module);
		define('CONTROLLER', ($module) ? $this->ci->uri->segment(2) : $this->ci->router->fetch_class());

		// switch
		switch(MODULE){
			// admin
			case 'admin':
				// check
				if(!CONTROLLER){
					// redirect
					redirect(MODULE.'/login'); exit;
				}
				// set
				$loginloop = $this->ci->session->flashdata('LOGIN_LOOP');
				$userid = $this->ci->session->userdata('ADMIN_USERID');
				$companyid = $this->ci->session->userdata('ADMIN_COMPANYID');
				$fromdate = date("Y-m-01", strtotime("-1 year"));
				$todate = date("Y-m-d");

				// get
				$userrs = ($userid) ? $this->ci->user_model->get($userid) : null;
				$companyrs = ($companyid) ? $this->ci->company_model->get($companyid) : null;
				$companyrs = ($userid && !$companyid && $userrs['companyid']) ? $this->ci->company_model->get($userrs['companyid']) : $companyrs;

				// check
				if(!$userrs && CONTROLLER != 'login'){
					// check
					if($loginloop){
						echo 'Oops, login loop!'; exit;
					}

					// set check
					$this->ci->session->set_flashdata('LOGIN_LOOP', 1);

					// msg
					$this->ci->msg->add('Please log in first.');

					// redirect
					redirect(MODULE.'/login'); exit;
				}
				if($userrs && !$this->ci->uri->segment(3) && (!CONTROLLER || CONTROLLER == 'login')){
					// redirect
					redirect(MODULE.'/dashboard');
				}

				// get

				// user
				define('ADMIN_USERID', ($userrs) ? $userrs['userid'] : '');
				define('ADMIN_FULLNAME', ($userrs) ? $userrs['fullname'] : '');
				define('ADMIN_EMAIL', ($userrs) ? $userrs['email'] : '');
				define('ISDADMIN', ($userrs) ? 1 : 0);
				define('ADMIN_COMPANYID', ($companyrs) ? $companyrs['companyid'] : 0);
				define('ADMIN_COMPANYNAME', ($companyrs) ? $companyrs['title'] : '');
				define('USER_FROMDATE', $fromdate);
				define('USER_TODATE', $todate);
			break;

			// mobile
			case 'mobile':
				// set
				$userid = $this->ci->input->get('userid');
				$platform = $this->ci->input->get('platform');
				$platform = (stristr($platform, 'ios')) ? 'ios' : 'android';
				$fromdate = date("Y-m-01", strtotime("-1 year"));
				$todate = date("Y-m-d");

				// check
				//$userid = 182;
				$userrs = ($userid) ? $this->ci->user_model->get($userid) : $userid;

				// set
				$mduserid = null;
				$groupid = null;
				$smuserid = null;
				$teamid = null;

				// sales-person
				if($userrs && $userrs['type'] == 'sales-person'){
					// get
					$companyuserteamrefrs = $this->ci->companyuserteamref_model->get(null, array('cutr.userid' => $userrs['userid']), null, true);
					//echo '<pre>'; print_r($companyuserteamrefrs); echo '</pre>';
					$companyuserteamrs = ($companyuserteamrefrs) ? $this->ci->companyuserteam_model->get($companyuserteamrefrs['companyuserteamid']) : null;
					//echo '<pre>'; print_r($companyuserteamrs); echo '</pre>';

					$mduserid = ($companyuserteamrs) ? $companyuserteamrs['mduserid'] : null;
					$groupid = ($companyuserteamrs) ? $companyuserteamrs['companyusergroupid'] : null;
					$smuserid = ($companyuserteamrs) ? $companyuserteamrs['userid'] : null;
					$teamid = ($companyuserteamrs) ? $companyuserteamrs['companyuserteamid'] : null;
				}
				// manager
				if($userrs && $userrs['type'] == 'sales-manager'){
					$companyuserteamrs = $this->ci->companyuserteam_model->get(null, array('cut.userid' => $userrs['userid']), null, true);
					//echo '<pre>'; print_r($companyuserteamrs); echo '</pre>';

					$mduserid = ($companyuserteamrs) ? $companyuserteamrs['mduserid'] : null;
					$groupid = ($companyuserteamrs) ? $companyuserteamrs['companyusergroupid'] : null;
					$smuserid = ($companyuserteamrs) ? $companyuserteamrs['userid'] : null;
				}
				// director
				if($userrs && $userrs['type'] == 'managing-director'){
					$mduserid = $userrs['userid'];
				}

				$userid = ($userrs) ? $userid : null;
				$companyrs = ($userrs && $userrs['companyid']) ? $this->ci->company_model->get($userrs['companyid']) : null;
				$companyid = ($companyrs['companyid']) ? $companyrs['companyid'] : null;
				$usertype = ($userrs) ? $userrs['type'] : null;

				// check dates
				if($userrs && $userrs['disabledon'] && $userrs['disabledon']<$todate){
					$todate = date("Y-m-d", strtotime($userrs['disabledon']));
					$fromdate = date("Y-m-01", strtotime("$todate -1 year"));
				}

				// define
				define('USERID', $userid);
				define('COMPANYID', $companyrs['companyid']);
				define('MDUSERID', $mduserid);
				define('GROUPID', $groupid);
				define('SMUSERID', $smuserid);
				define('TEAMID', $teamid);
				define('PLATFORM', $platform);
				define('USER_FROMDATE', $fromdate);
				define('USER_TODATE', $todate);
				define('USERTYPE', $usertype);
			break;

			// public
			default:
				// check
				if(!in_array($this->ci->uri->segment(1), array('crons', 'tools', 'uploads', 'test', 'content'))){
					// redirect
					redirect('/admin/login');
				}
			break;
		}
	}

}

/* End of file module-defaults.php */
/* Location: ./application/hooks/module-defaults.php */
?>
