<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller {

	public function lastlogin()
	{
		// get
		$userrs = $this->user_model->get(null, array('u.type' => 'app'));

		// loop
		foreach($userrs as $row){
			// check
			if(!$row['lastloginon']){
				continue;
			}

			// check
			if(in_array($row['dayssincelastlogin'], array(7, 14))){
				// send e-mail
				$emarr = array(
						'to-email' => $ROW['email'],
						'subject' => ADMIN_TITLE.' Reminder',
						'msg' => $this->load->view('mobile/_email/user-weekly-reminder', array('userrs' => $userrs), true)
					);
				send_email($emarr);
			}
		}
	}


	// process action queue
	public function process_actionqueue()
	{
		// run
		$this->actionqueuelib->process();
	}


	// chedck notifications
	public function check_notifications()
	{
		// get
		$notificationrs = $this->notification_model->get();

		// set
		$teamarr = array();
		$msgarr = array();

		// loop
		foreach($notificationrs as $row){
			$companyuserteamid = $row['companyuserteamid'];

			// add
			$teamarr[$companyuserteamid]['notifications'][] = $row;

			// check - sm
			if(!isset($teamarr[$companyuserteamid]['smuseridarr'])){
				// get
				$rs = $this->companyuserteam_model->get($companyuserteamid);

				$teamarr[$companyuserteamid]['smuseridarr'] = ($rs) ? array($rs['userid']) : array();
			}

			// check - sp
			if(!isset($teamarr[$companyuserteamid]['spuseridarr'])){
				// get
				$rs = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $companyuserteamid));

				$teamarr[$companyuserteamid]['spuseridarr'] = ($rs) ? array_values(rs_to_keyval_array($rs, 'companyuserteamrefid', 'userid')) : array();
			}
		}

		// loop
		foreach($teamarr as $row){
			// set
			$smuseridarr = $row['smuseridarr'];
			$spuseridarr = $row['spuseridarr'];

			// check
			if(!count($smuseridarr) || !count($spuseridarr)){
				continue;
			}

			// loop
			foreach($row['notifications'] as $nrow){
				$days = ($nrow['interval'] == 'weeks') ? ($nrow['num']*7) : $nrow['num'];
				$searchdate = ($nrow['position'] == 'before') ? date("Y-m-d", strtotime("+$days days")) : date("Y-m-d", strtotime("-$days days"));

				//$nrow['message'] .= " $searchdate";
				//echo $nrow['type']." - ".date("Y-m-d")."-$days = $searchdate <br />";

				// switch
				switch($nrow['type']){
					case 'coaching-session':
						// get
						$where = array();
						$other = array('where-str' => '
								cs.cancelledon IS NULL AND
								cs.date=DATE("'.$searchdate.'") AND
								(cs.salesmanageruserid IN ('.implode(', ', $smuseridarr).') OR cs.salespersonaluserid IN ('.implode(', ', $smuseridarr).'))
							');
						$coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);

						// set
						$touseridarr = array();

						// loop
						foreach($coachingsessionrs as $srow){
							$touseridarr[] = ($nrow['recipient'] == 'sm') ? $srow['salesmanageruserid'] : $srow['salespersonaluserid'];
						}

						// check
						if(count($touseridarr)){
							$msgarr[] = array('msg' => $nrow['message'], 'touseridarr' => $touseridarr);
						}
					break;
					case 'coaching-session-rating':
						// get
						$where = array();
						$other = array('where-str' => '
								cs.completedon IS NOT NULL AND
								cs.cancelledon IS NULL AND
								cs.ratedon IS NULL AND
								cs.date=DATE("'.$searchdate.'") AND
								(cs.salesmanageruserid IN ('.implode(', ', $smuseridarr).') OR cs.salespersonaluserid IN ('.implode(', ', $smuseridarr).'))
							');
						$coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);

						// set
						$touseridarr = array();

						// loop
						foreach($coachingsessionrs as $srow){
							$userid = ($nrow['recipient'] == 'sm') ? $srow['salesmanageruserid'] : $srow['salespersonaluserid'];

							// check
							if(!in_array($userid, $touseridarr)){
								$touseridarr[] = $userid;
							}
						}

						// check
						if(count($touseridarr)){
							$msgarr[] = array('msg' => $nrow['message'], 'touseridarr' => $touseridarr);
						}
					break;
					case 'dev-plan-deadline':
						// get
						$where = array();
						$other = array('where-str' => '(
								cs.completedon IS NOT NULL AND
								cs.cancelledon IS NULL AND
								cs.deletedon IS NULL AND
								ussi.completedon IS NULL AND
								ussi.dueon=DATE("'.$searchdate.'")
							)');
						$usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, $where, $other);

						// set
						$touseridarr = array();

						// loop
						foreach($usersessionsuggestionitemrs as $srow){
							$userid = ($nrow['recipient'] == 'sm') ? $srow['salesmanagerid'] : $srow['salespersonid'];

							// check
							if(!in_array($userid, $touseridarr)){
								$touseridarr[] = $userid;
							}
						}

						// check
						if(count($touseridarr)){
							$msgarr[] = array('msg' => $nrow['message'], 'touseridarr' => $touseridarr);
						}
					break;
				}
			}
		}

		echo '<pre>'; print_r($msgarr); echo '</pre>';

		// send
		foreach($msgarr as $row){
			// get
			$tokenarr = get_token_array($row['touseridarr']);

			// send
			if(count($tokenarr)){
				// send
				onesignal_send_push_msg($row['msg'], $tokenarr);
			}
		}

		echo 'Done.'; exit;
	}

}

/* End of file crons.php */
/* Location: ./application/controllers/crons.php */
