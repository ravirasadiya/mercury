<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set limits
		set_time_limit(0);
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '2000M');
	}

	// onesignal
	public function onesignal()
	{
		// send
		onesignal_send_push_msg('new test message', array('39d159c7-cfd0-476c-8053-1f3d3541d4e5'));
	}


	// setup test session
	public function setup_test_sessions()
	{
		echo 'Already ran'; exit;

		// set
		$datearr = array('2018-04-05', '2018-05-05' , '2018-06-05', '2018-07-05', '2018-08-05');
		$sessarr = array();

		// get
		$companycompetencyplanrefrs = $this->companycompetencyplanref_model->get(null, array('cut.companyid' => 5));

		//echo '<pre>'; print_r($companycompetencyplanrefrs); echo '</pre>'; exit;

		// header
		$sessarr[] = array('sp-userid', 'sp', 'session', 'md-userid', 'md', 'md-groupid', 'md-group', 'sm-teamid', 'sm-team', 'sm-userid', 'sm', 'group-id', 'group', 'comp-id', 'competency', 'sugg-id', 'suggestion', 'duedate', 'done');

		// set
		$clmcount = count($sessarr[0]);
		$fillarr = array_fill(0, (count($sessarr[0])-1), null);
		$numrange = range(0, 100, 10);
		$numboolrange = array('0', '1', '1', '1', '0', '1', '0', '0', '0', '1', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '1', '0', '0', '1', '0', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '0', '1', '0', '0', '1', '0', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '1', '1', '0', '0', '0', '0', '1', '0', '1', '1', '0', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '1', '1', '0', '1', '1', '1', '1', '0', '1', '1', '0', '1', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0', '0', '1', '1', '0', '1', '1', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0', '0', '1', '0', '0', '1', '1', '0', '1', '0', '0', '1', '0', '1', '1', '0', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '1', '1');
		$sessidrefarr = array();

		// loop
		foreach($companycompetencyplanrefrs as $row){
			// set
			$sessarr[] = array(
					'',
					'',
					'',
					$row['mduserid'],
					$row['mdname'],
					$row['mdgroupid'],
					$row['mdgroup'],
					$row['smteamid'],
					$row['smteam'],
					$row['smuserid'],
					$row['smname'],
					(!$row['parameter']) ? $row['competencygroupid'] : null,
					(!$row['parameter']) ? $row['group'] : null,
					(!$row['suggestion']) ? $row['competencyparameterid'] : null,
					(!$row['suggestion']) ? $row['parameter'] : null,
					$row['competencysuggestionid'],
					$row['suggestion'],
					'',
					''
				);

			// check
			if(!$row['parameter']){
				// get
				$team_userrs = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $row['smteamid']));
				//echo '<pre>'; print_r($team_userrs); echo '</pre>'; exit;

				// loop users
				foreach($team_userrs as $tu_row){
					//$sessarr[] = array($tu_row['userid'], $tu_row['user']);

					foreach($datearr as $date){
						$insarr = array(
								'salesmanageruserid' => $row['smuserid'],
								'salespersonaluserid' => $tu_row['userid'],
								'customertitle' => 'Test Customer',
								'customerno' => '001',
								'date' => $date,
								'time' => '14:00',
								'beganon' => "$date 14:00:00",
								'completedon' => "$date 15:00:00",
								'ratingbeganon' => null,
								'ratedon' => null,
								'cancelledon' => null,
								'cancelledby' => null,
								'cancelledreason' => null,
							);
						if($date == '2018-05-05'){
							$insarr['cancelledon'] = "$date 10:00:00";
							$insarr['cancelledby'] = 'sales-manager';
							$insarr['cancelledreason'] = 'Cancelled to test.';
							$insarr['cancelledbyuserid'] = $row['smuserid'];
						}
						if($date == '2018-08-05'){
							$insarr['beganon'] = null;
							$insarr['completedon'] = null;
						}

						// check
						if(!isset($sessidrefarr[$tu_row['userid']][$date])){
							// save
							$coachingsessionid = $this->coachingsession_model->save($insarr);

							$sessidrefarr[$tu_row['userid']][$date] = $coachingsessionid;
							//echo '<pre>'; print_r($sessidrefarr); echo '</pre>'; exit;
						}
					}
				}
			}
			if($row['parameter'] && !$row['suggestion']){
				// get
				$team_userrs = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $row['smteamid']));
				//echo '<pre>'; print_r($team_userrs); echo '</pre>'; exit;

				// loop users
				foreach($team_userrs as $tu_row){
					$sessarr[] = array_slice(array_merge(array($tu_row['userid'], $tu_row['user']), $fillarr), 0, $clmcount);

					foreach($datearr as $date){
						$dat_arr = array_merge(array('', $sessidrefarr[$tu_row['userid']][$date], $date), $fillarr);
						if(!in_array($date, array('2018-05-05', '2018-08-05'))){
							shuffle($numrange);
							$dat_arr[14] = $value = $numrange[array_rand($numrange)];

							$sessarr[] = array_slice($dat_arr, 0, $clmcount);

							// set
							$insarr = array(
									'coachingsessionid' => $sessidrefarr[$tu_row['userid']][$date],
									'companycompetencyplanrefid' => $row['companycompetencyplanrefid'],
									'value' => $value,
								);

							// save
							$this->usersessionparameteritem_model->save($insarr);
						}
					}
				}
			}
			if($row['suggestion']){
				// get
				$team_userrs = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $row['smteamid']));
				//echo '<pre>'; print_r($team_userrs); echo '</pre>'; exit;

				// loop users
				foreach($team_userrs as $tu_row){
					$sessarr[] = array_slice(array_merge(array($tu_row['userid'], $tu_row['user']), $fillarr), 0, $clmcount);

					foreach($datearr as $date){
						$dat_arr = array_merge(array('', $sessidrefarr[$tu_row['userid']][$date], $date), $fillarr);
						if(!in_array($date, array('2018-05-05', '2018-08-05'))){
							shuffle($numboolrange);
							$dat_arr[17] = date("Y-m-d", strtotime("$date +7 days"));
							$dat_arr[18] = $value = $numboolrange[array_rand($numboolrange)];

							$sessarr[] = array_slice($dat_arr, 0, $clmcount);

							// set
							$insarr = array(
									'coachingsessionid' => $sessidrefarr[$tu_row['userid']][$date],
									'companycompetencyplanrefid' => $row['companycompetencyplanrefid'],
									'dueon' => date("Y-m-d", strtotime("$date +7 days")),
									'completedon' => ($value) ? date("Y-m-d", strtotime("$date +4 days")) : null,
								);

							// save
							$this->usersessionsuggestionitem_model->save($insarr);
						}
					}
				}
			}
		}

		// set
		$sheetarr = array('Stats' => $sessarr);

		// export
		$this->utils->save_excel($sheetarr, 'Stats-Test-'.date("Y-m-d").'.xls');

		echo array_to_table($sessarr); exit;

		//echo '<pre>'; print_r($sessarr); echo '</pre>'; exit;

		echo 'Done'; exit;
	}


	// setup test ratings
	public function setup_test_ratings()
	{
		echo 'Already ran'; exit;

		// set
		$ratarr = array();
		$numrange = range(0, 100, 10);

		// get
		$other = array('order' => array('smgroup' => 'ASC', 'spteam' => 'ASC', 'salesmanager' => 'ASC', 'salesperson' => 'ASC'));
		$coachingsessionrs = $this->coachingsession_model->get(null, array('smu.companyid' => 5), $other);
		$companysessionratingrefrs = $this->companysessionratingref_model->get(null, array('csr.companyid' => 5));
		//echo '<pre>'; print_r($companysessionratingrefrs); echo '</pre>'; exit;

		// set
		$ratarr[] = array('sm-userid', 'sm', 'sp-userid', 'sp', 'session-id', 'date', 'rating', 'value');

		// loop
		foreach($coachingsessionrs as $row){
			// check
			if($row['cancelledon'] || !$row['completedon']){
				continue;
			}
			//echo '<pre>'; print_r($row); echo '</pre>'; exit;
			//echo $row['coachingsessionid'].' => '.$row['date'].'<br />';

			$ratarr[] = array($row['salesmanageruserid'], $row['salesmanager'], $row['salespersonaluserid'], $row['salesperson'], $row['coachingsessionid'], $row['date']);

			// loop
			foreach($companysessionratingrefrs as $ratrow){
				shuffle($numrange);
				$value = $numrange[array_rand($numrange)];

				//echo '<pre>'; print_r($ratrow); echo '</pre>'; exit;

				$ratarr[] = array('', '', '', '', '', '', $ratrow['rating'], $value);

				// set
				$insarr = array(
						'coachingsessionid' => $row['coachingsessionid'],
						'companysessionratingrefid' => $ratrow['companysessionratingrefid'],
						'value' => $value,
						'userid' => $row['salespersonaluserid'],
					);

				// save
				$this->usersessionratingitem_model->save($insarr);
			}
		}

		//echo '<pre>'; print_r($ratarr); echo '</pre>'; exit;

		// set
		$sheetarr = array('Stats' => $ratarr);

		// export
		$this->utils->save_excel($sheetarr, 'Ratings-Test-'.date("Y-m-d").'.xls');

		echo array_to_table($ratarr); exit;

		//echo '<pre>'; print_r($sessarr); echo '</pre>'; exit;

		echo 'Done'; exit;
	}


	// test results
	public function check_session($coachingsessionid)
	{
		// queue
		$arr = array('coachingsessionid' => $coachingsessionid, 'type' => 'coaching-session-result', 'action' => 'download-report');
		$actionqueueid = $this->actionqueuelib->add($arr);

		// set
		$this->actionqueuelib->debug_view = true;

		// process
		$this->actionqueuelib->process($actionqueueid);
	}
}

/* End of file test.php */
/* Location: ./application/controllers/test.php */
