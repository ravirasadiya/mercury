<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->randomsuggestionarr = array(
				"See a movie at the drive-in",
				"Walk on the boardwalk and listen to the boards creak under your feet",
				"Blow bubbles",
				"Play tag, hopscotch, or one of your favorite childhood games",
				"Ride a roller coaster",
				"Play miniature golf",
				"Win a prize at the fair",
				"Catch fireflies at night",
				"Build a sandcastle at the beach",
				"Eat a whole lobster with your hands",
				"Pick berries and peaches at a farm",
				"Buy a Creamsicle from the neighborhood ice cream truck",
				"Roast marshmallows over a fire and make s’mores",
				"Make lemonade from scratch",
				"Eat corn on the cob",
				"Sip a sweating glass of iced tea",
				"Eat a slice of watermelon",
				"Buy fresh produce at the farmers’ market",
				"Mix up a pitcher of sangria",
				"Eat a soft-serve vanilla ice cream cone with rainbow sprinkles",
				"Dig your own clams",
				"Have a barbecue",
				"Nap in a hammock",
				"Have a picnic in the park",
				"Sit on a porch swing",
				"Stargaze while lying in the grass",
				"Watch the sun set from a beach",
				"Dangle your feet off a dock",
				"Bring a blanket and lie on the grass at an outdoor concert",
				"Pick wildflowers",
				"Swim in a lake",
				"Rent a bike",
				"Go fishing",
				"Go camping",
				"Play tennis",
				"Go for a hike",
				"Go kayaking or canoeing",
				"Toss a Frisbee",
				"Collect seashells at the beach",
				"Take a last-minute road trip",
				"See a summer blockbuster",
				"Read a trashy novel",
				"Walk barefoot in the grass",
				"Get caught in a summer rainstorm",
				"Sleep with the windows open",
				"Make a summer playlist",
				"Smell freshly-cut grass",
				"Feel the sun on your back",
				"Roll up your pant legs and go wading",
				"Go to a baseball game",
			);
	}


	public function index()
	{
		$data = '<a href="/tools/resetpackages" target="_blank">Reset Groups/Paramters</a><br />';


		echo $data; exit;
	}


	public function cleandb()
	{
		echo 'Disabled.'; exit;

		// truncate
		$this->db->truncate('actionqueue');
		$this->db->truncate('ci_sessions');
		$this->db->truncate('coachingsession');
		$this->db->truncate('company');
		$this->db->truncate('companycompetencyplan');
		$this->db->truncate('companycompetencyplanref');
		$this->db->truncate('companygroupparamsuggref');
		$this->db->truncate('companysessionrating');
		$this->db->truncate('companysessionratingref');
		$this->db->truncate('companyusergroup');
		$this->db->truncate('companyusergroupref');
		$this->db->truncate('companyuserteam');
		$this->db->truncate('companyuserteamref');
		$this->db->truncate('competencygroup');
		$this->db->truncate('competencyparameter');
		$this->db->truncate('competencysuggestion');
		//$this->db->truncate('content');
		//$this->db->truncate('lookup');
		$this->db->truncate('sessionratingitem');
		$this->db->truncate('user');
		$this->db->truncate('usercompetencylimit');
		$this->db->truncate('usercompetencyplanitem');
		$this->db->truncate('usernote');
		$this->db->truncate('usersessionparameteritem');
		$this->db->truncate('usersessionratingitem');
		$this->db->truncate('usersessionsuggestionitem');

		// add
		$insarr = array(
				'title' => 'Mr.',
				'firstname' => 'Mercuri',
				'lastname' => 'Admin',
				'mobile' => '',
				'officeno' => '',
				'email' => 'admin@domain.com',
				'password' => get_hash('pass'),
				'isenabled' => 1,
				'type' => 'admin',
			);
		$this->user_model->save($insarr);

		echo 'Done!'; exit;
	}


	public function reset_groups_params()
	{
		echo 'Disabled.'; exit;

		// set
		$grouparr = array();

		// truncate
		$this->db->truncate('competencygroup');
		$this->db->truncate('competencyparameter');
		$this->db->truncate('competencysuggestion');
		$this->db->truncate('sessionratingitem');

		// groups
		$grouparr[1] = array('title' => 'CONTEXT & FRAMEWORK');
		$grouparr[2] = array('title' => 'PLANNING');
		$grouparr[3] = array('title' => 'DEFINE THE NEEDS & GET ACCEPTANCE: OPENING THE VISIT');
		$grouparr[4] = array('title' => 'DEFINE THE NEEDS & GET ACCEPTANCE: QUESTIONING');
		$grouparr[5] = array('title' => 'PRESENT THE SOLUTION & GET ACCEPTANCE');
		$grouparr[6] = array('title' => 'HANDLE OBJECTIONS');
		$grouparr[7] = array('title' => 'CLOSE & NEXT STEPS');
		$grouparr[8] = array('title' => 'GENERAL');
		$grouparr[9] = array('title' => 'GROWING AND MAINTAINING CUSTOMER RELATIONSHIPS');

		// params
		$grouparr[1]['paramarr'] = array(
			array('title' => "Identifying longer term objectives for the customer", 'description' => ""),
			array('title' => "Setting SMART objectives for the visit", 'description' => ""),
			array('title' => "Preparing questions for the visit", 'description' => ""),
			array('title' => "Preparing required documentation and / or proofs for the visit", 'description' => ""),
			array('title' => "Anticipating customer reactions / behaviours", 'description' => ""),
		);
		$grouparr[2]['paramarr'] = array(
			array('title' => "Identifying longer term objectives for the customer", 'description' => "Long term plans e.g. as Grow, Defend, Increase Share, Brickwall, Cross/Up Sell, Pricing"),
			array('title' => "Setting SMART objectives for the visit", 'description' => "Primary, Secondary & Retreat Objectives in line with Long Term Objectives"),
			array('title' => "Preparing questions for the visit", 'description' => "What acceptances required to achieve objectives, what Questions to get get acceptances"),
			array('title' => "Preparing required documentation and / or proofs for the visit", 'description' => "What documentation, when and how it will be used"),
			array('title' => "Anticipating customer reactions / behaviours", 'description' => "What will cust. be happy/unhappy about, ask, demand? How will we influence/ respond. What did cust. ask/say previously?"),
		);
		$grouparr[3]['paramarr'] = array(
			array('title' => "Listening actively", 'description' => "Not interrupting, encouraging body language & words, ask to clarify, paraphrase"),
			array('title' => "1st impressions / greeting is positive- energetic- genuine", 'description' => "1st few minutes: happy/glad to see customer & the meeting. Face & voice inspiring. Sincere"),
			array('title' => "Establishing a good connection / building rapport with the customer ", 'description' => "Attention on cust., mirror cust., ask cust., not personal unless old contact, refer to previous good experiences"),
			array('title' => "Confirming meeting duration", 'description' => "Confirm with cust. either duration (i.e. 30 minutes) or end time (i.e. until 12h00)"),
			array('title' => "Presenting the agenda / discussion points", 'description' => "Printed or verbal statement of \"headings\" of meeting, stated as separate points, ask if cust. wants to add to agenda/ points"),
			array('title' => "Getting permission to take notes", 'description' => "Gets permission, must give reason/s for wanting to make notes"),
		);
		$grouparr[4]['paramarr'] = array(
			array('title' => "Asking a motivating question", 'description' => "Must motovate/inspire/good reason AND get permission"),
			array('title' => "Asking open questions to uncover business & personal needs", 'description' => "First Question or two must be very Open/ NOT Leading (short Open Questions)"),
			array('title' => "Applying the FOCA framework of questioning", 'description' => "Identify Hooks, FOCA structure followed for selected Hooks, keep to question structure"),
			array('title' => "Obtaining clear and convincing acceptances for each need", 'description' => "Acceptance/s of Needs (pains) and/or Needs for solution/s are clear"),
			array('title' => "Taking notes without interrupting", 'description' => "Notes structured, not breaking eye contact too much, refers to notes in meeting \"look at notes - \"you mentioned…\" please tell me..\""),
			array('title' => "Summarises well", 'description' => "Once all acceptances obtained, summarises everything agreed to and not yet discussed"),
			array('title' => "Asking sweeper question", 'description' => "After summary, ask sweeper question as OQ or CQ (what else/ anything else we should consider?)"),
			array('title' => "Identifying customer decision making unit", 'description' => "OQ to get roles, power, attitude of all relevant parties"),
		);
		$grouparr[5]['paramarr'] = array(
			array('title' => "Summarising the situation from the previous phase / meeting", 'description' => "After summary ask OLQ to check if still relevant (During our last meeting we… What has changed since then?)"),
			array('title' => "Displays strong projection, staying convincing and enthusiastic", 'description' => "Displays strong conviction during presentation of solution, up to the end"),
			array('title' => "Dealing with each need separately", 'description' => "Does not combine needs"),
			array('title' => "Following the sequence of need, product / service feature, benefit ", 'description' => "Follows full sequence with each need"),
			array('title' => "Providing proofs", 'description' => "Provides adequate proofs to establish credibility/ trust"),
			array('title' => "Obtaining clear acceptance on each solution", 'description' => "Each acceptance dealth with separately"),
			array('title' => "Summarising all needs / features / benefits and presenting investment", 'description' => "Summary completed before investment is presented"),
		);
		$grouparr[6]['paramarr'] = array(
			array('title' => "Right reaction towards objections", 'description' => "Does not respond without first asking"),
			array('title' => "Handling objections with a good methodology i.e. Empty and Lock", 'description' => "Must use a methodology"),
			array('title' => "Price handling  i.e. Compared to what?", 'description' => "Argumentation must be logical"),
			array('title' => "Value add argumentation", 'description' => "Checks each time with a new objection"),
		);
		$grouparr[7]['paramarr'] = array(
			array('title' => "Handling resistance with a good methodology i.e. 49% / 51%", 'description' => "Responds to buying signals"),
			array('title' => "Using trial close", 'description' => "Uses trial close from first buying signal, repeats if required"),
			array('title' => "Closing firmly", 'description' => "Closing clear and unambiguous"),
			array('title' => "Next steps", 'description' => "Confident on close, waits for customer to respond after close"),
		);
		$grouparr[8]['paramarr'] = array(
			array('title' => "Being in control of the visit yet not dominating", 'description' => ""),
			array('title' => "Communication", 'description' => ""),
			array('title' => "Professionalism", 'description' => ""),
			array('title' => "Building trust", 'description' => ""),
			array('title' => "Connecting with the customer", 'description' => ""),
		);
		$grouparr[9]['paramarr'] = array(
			array('title' => "Brickwalling", 'description' => ""),

		);


		//echo '<pre>'; print_r($grouparr); echo '</pre>'; exit;

		// loop
		foreach($grouparr as $grow){
			// save
			$competencygroupid = $this->competencygroup_model->save(array('title' => ucwords(strtolower($grow['title']))));

			// loop
			foreach($grow['paramarr'] as $prow){
				// save
				$competencyparameterid = $this->competencyparameter_model->save(array('competencygroupid' => $competencygroupid, 'title' => $prow['title'], 'description' => $prow['description']));

				// set
				$ransugarr = array_rand($this->randomsuggestionarr, 2);
				//echo '<pre>'; print_r($ransugarr); echo '</pre>'; exit;

				// save
				$this->competencysuggestion_model->save(array('competencyparameterid' => $competencyparameterid, 'title' => $this->randomsuggestionarr[$ransugarr[0]]));
				$this->competencysuggestion_model->save(array('competencyparameterid' => $competencyparameterid, 'title' => $this->randomsuggestionarr[$ransugarr[1]]));
			}
		}

		// session ratings
		$ratingsarr = array(
				'What impact did this coaching session have on your resources/knowledge/skill?',
				'How motivated/inspired do you feel after this coaching session?',
				'How well did your manager play their role as coach?',
				'During the post visit coaching session, how much of the total time did you speak?',
				'How accurately do you believe you were assessed?',
			);

		// loop
		foreach($ratingsarr as $title){
			// save
			$this->sessionratingitem_model->save(array('title' => $title));
		}

		echo 'Done.';

		exit;
	}
	
	// create db reletionships
	public function create_db_relationships()
	{
		// set
		$tblarr = array(
			'coachingsession', 'company', 'companycompetencyplan', 'companycompetencyplanref', 'companygroupparamsuggref', 'companysessionrating', 'companysessionratingref', 
			'companyusergroup', 'companyusergroupref', 'companyuserteam', 'companyuserteamref', 'competencygroup', 'competencyparameter', 'competencysuggestion', 
			'content', 'lookup', 'notification', 'sessionratingitem', 'user', 'usercompetencylimit', 'usercompetencyplanitem', 'usernote', 'usersessionparameteritem', 
			'usersessionratingitem', 'usersessionsuggestionitem', 
		);
		$logarr = array();

		// loop
		foreach($tblarr as $tbl){
			// get fields
			$fieldarr = $this->db->list_fields($tbl);

			// log
			//$logarr[] = '<h5>'.$tbl.'</h5>';
			//$logarr[] = 'ALTER TABLE '.$tbl.' ENGINE=InnoDB;';

			// loop
			foreach($fieldarr as $field){
				// loop
				foreach($tblarr as $id_tbl){
					// set
					$id = $id_tbl.'id';

					// check
					if(stristr($field, $id) && $tbl.'id' != $id){
						//$logarr[] = $tbl.' => '.$id_tbl.'/'.$id.'/'.$field;
						//$logarr[] = 'ALTER TABLE '.$tbl.' ADD CONSTRAINT fk_'.substr($field, 0, -2).'_id FOREIGN KEY ('.$field.') REFERENCES '.$id_tbl.'('.$id.');';
						$logarr[] = 'ALTER TABLE '.$tbl.' ADD FOREIGN KEY (`'.$field.'`) REFERENCES '.$id_tbl.'(`'.$id.'`);';
					}
				}
			} // end-foreach

			//$logarr[] = '<hr />';
		} // end-foreach

		// echo
		echo implode('<br />', $logarr);
	}
}

/* End of file tools.php */
/* Location: ./application/controllers/tools.php */
