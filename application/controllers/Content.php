<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	private $title = '';
	private $html = '';

	public function __construct()
	{
		parent::__construct();
	}


	public function index($type='privacy-policy')
	{
		// get
		$contentrs = $this->content_model->get(null, array('type' => $type), null, true);

		// check
		if($contentrs){
			// set
			$this->title = $contentrs['title'];

			// set
			$data = '';
			$data .= '<h2>'.$contentrs['title'].'</h2>';
			$data .= '<hr />';

			$data .= '<div class="row">';
				$data .= '<div class="col-md-12 text-justify">';

					$data .= $contentrs['text'];

				$data .= '</div>';
			$data .= '</div>';

			// set
			$this->html = $data;
		}

		// display
		$this->display();
	}


	// display
	public function display()
	{
		$data = '<!DOCTYPE html>';
		$data .= '<html lang="en">';
		$data .= '<head>';
			$data .= '<meta charset="utf-8">';
			$data .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$data .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
			$data .= '<title>'.$this->title.'</title>';

			$data .= '<!-- Bootstrap -->';
			$data .= '<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">';

			$data .= '<style>';
			$data .= '.container {';
				$data .= 'max-width: 700px;';
			$data .= '}';
			$data .= '.btn { text-align: left; }';
			$data .= '</style>';

			$data .= '<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->';
			$data .= '<!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->';
			$data .= '<!--[if lt IE 9]>';
				$data .= '<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>';
				$data .= '<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>';
			$data .= '<![endif]-->';
		$data .= '</head>';
		$data .= '<body>';

			$data .= '<div class="container">';

				$data .= $this->html;

			$data .= '</div>';

			$data .= '<!-- jQuery (necessary for Bootstrap\'s JavaScript plugins) -->';
			$data .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>';
			$data .= '<!-- Include all compiled plugins (below), or include individual files as needed -->';
			$data .= '<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>';
		$data .= '</body>';
		$data .= '</html>';

		echo $data; exit;
	}

}

/* End of file content.php */
/* Location: ./application/controllers/content.php */
