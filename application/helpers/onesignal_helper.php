<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use NNV\OneSignal\OneSignal;
use NNV\OneSignal\API\Notification;

function onesignal_send_push_msg($msg, $tokenarr=array(), $params=array())
{
	// set
	$authkey = ONESIGNAL_APPADMIN;
	$appid = ONESIGNAL_APPID;
	$apirestkey = ONESIGNAL_RESTAPIKEY;

	// check
	$title = (isset($params['title'])) ? $params['title'] : 'Mercuri';

	$oneSignal = new OneSignal($authkey, $appid, $apirestkey);
	$notification = new Notification($oneSignal);

	$notificationData = [
		'contents' => [
			'en' => $msg
		],
		'headings' => [
			'en' => $title
		],
		'template_id' => ONESIGNAL_DEFAULT_TEMPLATEID
	];
	if(count($tokenarr)){
		$notificationData['include_player_ids'] = $tokenarr;
		$notificationData['included_segments'] = array();
	}else{
		$notificationData['included_segments'] = ['All'];
	}
	//echo '<pre>'; print_r($notificationData); echo '</pre>';

	$res = $notification->create($notificationData);
	//echo '<pre>'; print_r($res); echo '</pre>'; exit;
}

/* End of file onesignal_helper.php */
/* Location: ./application/helpers/onesignal_helper.php */
?>
