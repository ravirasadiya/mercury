<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use ChartJs\ChartJS;

// get random color
function get_random_color($i=null)
{
    // set
    $colorarr = array('blue', 'orange', 'green', 'teal', 'maroon', 'aqua', 'olive', 'salmon', 'palegreen', 'coral', 'gold', 'darkseagreen', 'cyan', 'tan', 'deepskyblue', 'beige', 'cadetblue');

    // check
    $i = ($i) ? $i : rand(0, count($colorarr)-1);

    // set
    $color = (isset($colorarr[$i])) ? $colorarr[$i] : get_random_color();

    return $color;
}

// chart overview
function chart_overview($statsarr, $dataonly=false)
{
    // overview
    $dataarr = [
        'labels' => ['Sessions Scheduled', 'Sessions Held'],
        'datasets' => [[
            'data' => [($statsarr['num-scheduled']), $statsarr['num-completed']],
            'backgroundColor' => ['deepskyblue', 'orange']
        ]]
    ];
    $options = ['responsive' => true];
    $attributes = ['id' => 'total-summary', 'width' => 500, 'height' => 100];
    $chart = ($dataonly) ? $dataarr : new ChartJS('doughnut', $dataarr, $options, $attributes);

    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    return $chart;
}


// chart director percentages
function chart_director_percentages($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array(0 => array('label' => 'Directors', 'data' => array(), 'backgroundColor' => array()));

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        $labarr[] = $dirrow['director'];
        $dataarr[0]['data'][] = $dirrow['percentage-completed'];
        $dataarr[0]['backgroundColor'][] = 'deepskyblue';
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Director % Sessions Held',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-percentage-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart director sessions
function chart_director_sessions($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $dataarr[0] = array(
            'label' => 'Sessions Scheduled',
			'backgroundColor' => 'deepskyblue',
			'data' => array()
        );
    $dataarr[1] = array(
            'label' => 'Session Held',
			'backgroundColor' => 'orange',
			'data' => array()
        );

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        $labarr[] = $dirrow['director'];
        $dataarr[0]['data'][] = ($dirrow['num-scheduled']+$dirrow['num-completed']);
        $dataarr[1]['data'][] = ($dirrow['num-completed']);
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Director Sessions',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart manager percentages
function chart_manager_percentages($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array(0 => array('label' => 'Managers', 'data' => array(), 'backgroundColor' => array()));

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                $labarr[] = $managerrow['manager'];
                $dataarr[0]['data'][] = $managerrow['percentage-completed'];
                $dataarr[0]['backgroundColor'][] = 'deepskyblue';
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Manager % Sessions Held',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'manager-percentage-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart manager sessions
function chart_manager_sessions($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $dataarr[0] = array(
            'label' => 'Sessions Scheduled',
			'backgroundColor' => 'deepskyblue',
			'data' => array()
        );
    $dataarr[1] = array(
            'label' => 'Session Held',
			'backgroundColor' => 'orange',
			'data' => array()
        );

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                $labarr[] = $managerrow['manager'];
                $dataarr[0]['data'][] = ($managerrow['num-scheduled']+$managerrow['num-completed']);
                $dataarr[1]['data'][] = ($managerrow['num-completed']);
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Manager Sessions',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart teammember percentages
function chart_teammember_percentages($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array(0 => array('label' => 'Team-Members', 'data' => array(), 'backgroundColor' => array()));

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                // teams
                foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
                    // sessions
                    foreach($teamrow['users'] as $userid => $userrow){
                        $labarr[] = $userrow['name'];
                        $dataarr[0]['data'][] = $userrow['percentage-completed'];
                        $dataarr[0]['backgroundColor'][] = 'deepskyblue';
                    }
                }
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Team-Member % Sessions Held',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'manager-percentage-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart teammember sessions
function chart_teammember_sessions($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $dataarr[0] = array(
            'label' => 'Sessions Scheduled',
			'backgroundColor' => 'deepskyblue',
			'data' => array()
        );
    $dataarr[1] = array(
            'label' => 'Session Held',
			'backgroundColor' => 'orange',
			'data' => array()
        );

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                // teams
                foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
                    // sessions
                    foreach($teamrow['users'] as $userid => $userrow){
                        $labarr[] = $userrow['name'];
                        $dataarr[0]['data'][] = ($userrow['num-scheduled']+$userrow['num-completed']);
                        $dataarr[1]['data'][] = ($userrow['num-completed']);
                    }
                }
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Team-Member Sessions',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}



// chart adherence overview
function chart_adherence_overview($statsarr, $dataonly=false)
{
    // overview
    $dataarr = [
        'labels' => ['Required Sessions', 'Adherence'],
        'datasets' => [[
            'data' => [($statsarr['num-required']), $statsarr['num-completed']],
            'backgroundColor' => ['deepskyblue', 'orange']
        ]]
    ];
    $options = ['responsive' => true];
    $attributes = ['id' => 'total-summary', 'width' => 500, 'height' => 100];
    $chart = ($dataonly) ? $dataarr : new ChartJS('doughnut', $dataarr, $options, $attributes);

    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    return $chart;
}


// chart adherence director percentages
function chart_adherence_director_percentages($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array(0 => array('label' => 'Directors', 'data' => array(), 'backgroundColor' => array()));

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        $labarr[] = $dirrow['director'];
        $dataarr[0]['data'][] = $dirrow['required-percentage-completed'];
        $dataarr[0]['backgroundColor'][] = 'deepskyblue';
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Director % Adherence',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-percentage-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart adherence director sessions
function chart_adherence_director_sessions($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $dataarr[0] = array(
            'label' => 'Required Sessions',
			'backgroundColor' => 'deepskyblue',
			'data' => array()
        );
    $dataarr[1] = array(
            'label' => 'Session Held',
			'backgroundColor' => 'orange',
			'data' => array()
        );

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        $labarr[] = $dirrow['director'];
        $dataarr[0]['data'][] = ($dirrow['num-scheduled']+$dirrow['num-completed']);
        $dataarr[1]['data'][] = ($dirrow['num-completed']);
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Director Sessions',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart adherence manager percentages
function chart_adherence_manager_percentages($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array(0 => array('label' => 'Managers', 'data' => array(), 'backgroundColor' => array()));

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                $labarr[] = $managerrow['manager'];
                $dataarr[0]['data'][] = $managerrow['required-percentage-completed'];
                $dataarr[0]['backgroundColor'][] = 'deepskyblue';
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Manager % Adherence',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'manager-percentage-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart adherence manager sessions
function chart_adherence_manager_sessions($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $dataarr[0] = array(
            'label' => 'Required Sessions',
			'backgroundColor' => 'deepskyblue',
			'data' => array()
        );
    $dataarr[1] = array(
            'label' => 'Session Held',
			'backgroundColor' => 'orange',
			'data' => array()
        );

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                $labarr[] = $managerrow['manager'];
                $dataarr[0]['data'][] = ($managerrow['num-scheduled']+$managerrow['num-completed']);
                $dataarr[1]['data'][] = ($managerrow['num-completed']);
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Manager Sessions',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart adherence teammember percentages
function chart_adherence_teammember_percentages($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array(0 => array('label' => 'Team-Members', 'data' => array(), 'backgroundColor' => array()));

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                // teams
                foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
                    // sessions
                    foreach($teamrow['users'] as $userid => $userrow){
                        $labarr[] = $userrow['name'];
                        $dataarr[0]['data'][] = $userrow['required-percentage-completed'];
                        $dataarr[0]['backgroundColor'][] = 'deepskyblue';
                    }
                }
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Team-Member % Adherence',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'manager-percentage-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart adherence teammember sessions
function chart_adherence_teammember_sessions($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $dataarr[0] = array(
            'label' => 'Required Sessions',
			'backgroundColor' => 'deepskyblue',
			'data' => array()
        );
    $dataarr[1] = array(
            'label' => 'Session Held',
			'backgroundColor' => 'orange',
			'data' => array()
        );

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                // teams
                foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
                    // sessions
                    foreach($teamrow['users'] as $userid => $userrow){
                        $labarr[] = $userrow['name'];
                        $dataarr[0]['data'][] = ($userrow['num-scheduled']+$userrow['num-completed']);
                        $dataarr[1]['data'][] = ($userrow['num-completed']);
                    }
                }
            }
        }
    }
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Team-Member Sessions',
        'labels' => $labarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    return $chart;
}


// chart developments overview
function chart_developments_overview($statsarr, $dataonly=false)
{
    // check
    if(!$statsarr['num-suggs-completed'] && !$statsarr['num-suggs-missed'] && !$statsarr['num-suggs-planned']){
        $statsarr['num-suggs-planned'] = 0.1;
    }

    // overview
    $dataarr = [
        'labels' => ['Completed', 'Missed', 'Planned'],
        'datasets' => [[
            'data' => [$statsarr['num-suggs-completed'], $statsarr['num-suggs-missed'], $statsarr['num-suggs-planned']],
            'backgroundColor' => ['deepskyblue', 'orange']
        ]]
    ];
    $options = ['responsive' => true];
    $attributes = ['id' => 'total-summary', 'width' => 500, 'height' => 100];
    $chart = ($dataonly) ? $dataarr : new ChartJS('doughnut', $dataarr, $options, $attributes);

    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    return $chart;
}


// chart evaluation ratings
function chart_evaluation_ratings($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $legendarr = $statsarr['ratingdescarr'];
    $legendarr = $ratingdescarr = (!count($legendarr)) ? array(0 => 'No Ratings Selected') : $legendarr;

    //echo '<pre>'; print_r($statsarr['groups']); echo '</pre>'; exit;

    // directors
    foreach($statsarr['directors'] as $dirrow){
        // groups
        foreach($dirrow['groups'] as $grouprow){
            // managers
            foreach($grouprow['managers'] as $smuserid => $managerrow){
                $labarr[] = $managerrow['name'];

                // ratings
                foreach($ratingdescarr as $ratingid => $title){
					$dataarr[$ratingid][] = (isset($managerrow['rating-rows'][$ratingid])) ? $managerrow['rating-rows'][$ratingid]['num-avg']: 0;
                }
            }
        }
    }
    //echo '<pre>'; print_r($labarr); echo '</pre>';
    //echo '<pre>'; print_r($legendarr); echo '</pre>';
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => 'Team-Member Evaluation Ratings',
        'labels' => $labarr,
        'legends' => $legendarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'director-session-summary', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    //echo '<pre>'; print_r($chart); echo '</pre>'; exit;

    return $chart;
}


// chart competency groups
function chart_competency_groups($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $legendarr = array(0 => 'Team Member', 1 => 'Team');

    //echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

    //set
    $i = 0;

    // loop data
    foreach($statsarr['dates'] as $date => $row){
        $labarr[] = date("M y", strtotime("$date-01"));

        $dataarr[0][] = $row['user-avg'];
        $dataarr[1][] = $row['team-avg'];

        $i++;
    }

    //echo '<pre>'; print_r($labarr); echo '</pre>';
    //echo '<pre>'; print_r($legendarr); echo '</pre>';
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => $statsarr['title'],
        'labels' => $labarr,
        'legends' => $legendarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'competency-group', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    //echo '<pre>'; print_r($chart); echo '</pre>'; exit;

    return $chart;
}


// chart competency params
function chart_competency_params($statsarr, $dataonly=false)
{
    // set
    $labarr = array();
    $dataarr = array();
    $legendarr = array(0 => 'Team Member', 1 => 'Team');

    //echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

    //set
    $i = 0;

    // loop data
    foreach($statsarr['dates'] as $date => $row){
        $labarr[] = date("M y", strtotime("$date-01"));

        $dataarr[0][] = $row['user-avg'];
        $dataarr[1][] = $row['team-avg'];

        $i++;
    }

    //echo '<pre>'; print_r($labarr); echo '</pre>';
    //echo '<pre>'; print_r($legendarr); echo '</pre>';
    //echo '<pre>'; print_r($dataarr); echo '</pre>'; exit;

    // overview
    $dataarr = [
        'title' => $statsarr['title'],
        'labels' => $labarr,
        'legends' => $legendarr,
        'datasets' => $dataarr
    ];
    $options = ['responsive' => true, 'scales' => ['yAxes' => [['ticks' => ['beginAtZero' => true]]]]];
    $attributes = ['id' => 'competency-param', 'width' => 500, 'height' => 200];
    $chart = ($dataonly) ? $dataarr : new ChartJS('bar', $dataarr, $options, $attributes);

    //echo '<pre>'; print_r($chart); echo '</pre>'; exit;

    return $chart;
}


// get uuid
function get_uuid()
{
    // get ci
	$ci =& get_instance();

    // load
    $ci->load->library('Uuid');

    // generate
    $uuid = $ci->uuid->v4();

    // generate
    return $uuid;
}


// user session interval
function user_session_interval($userid, $fromdate=null, $todate=null)
{
    // get ci
	$ci =& get_instance();

    // check
    $fromdate = ($fromdate) ? $fromdate : date("Y-01-01");
    $todate = ($todate) ? $todate : date("Y-12-31");

    // get
    $userrs = $ci->user_model->get($userid);

    // check
    $numsessions = ($userrs['numsessions']) ? $userrs['numsessions'] : 1;
    $sessionfrequency = ($userrs['sessionfrequency']) ? $userrs['sessionfrequency'] : '1-month';

    // set
    $numreqsessions = 1;

    // switch
    switch($sessionfrequency){
        case '1-week':
        	$interval = 'ww';
            $frequency = 1;
        break;
        case '2-weeks':
        	$interval = 'ww';
            $frequency = 2;
        break;
        case '1-month':
        	$interval = 'm';
            $frequency = 1;
        break;
        case '2-months':
        	$interval = 'm';
            $frequency = 2;
        break;
        case '3-months':
        	$interval = 'm';
            $frequency = 3;
        break;
        case '4-months':
        	$interval = 'm';
            $frequency = 4;
        break;
        case '6-months':
        	$interval = 'm';
            $frequency = 6;
        break;
        case '12-months':
        	$interval = 'm';
            $frequency = 12;
        break;
    }

    // calc
    $int = datediff($interval, $fromdate, $todate);
    $diff = abs(strtotime($todate) - strtotime($fromdate));

    // calc
    $years = floor($diff / (365*60*60*24));
    $months = floor($diff / (30*60*60*24));
    $weeks = floor($diff / (60*60*24*7));
    $days = floor($diff / (60*60*24));

    //printf("%d years, %d months, %d weeks, %d days\n", $years, $months, $weeks, $days); exit;

    $int = ($interval == 'ww') ? $weeks : $months;
    //echo "$fromdate - $todate - $interval - $int - $frequency";
    //$numreqsessions = round(($int/$frequency)*$numsessions);
    $numreqsessions = round(($int/$frequency)*$numsessions);
    //echo $numreqsessions; exit;

    return $numreqsessions;

}

// get token array
function get_token_array($idarr, $retarr=array())
{
    // get ci
	$ci =& get_instance();

    // loop
    foreach($idarr as $userid){
        // get
        $rs = $ci->user_model->get($userid);

        // check
        if($rs['pushtoken']){
            $retarr[] = $rs['pushtoken'];
        }
    }

    return $retarr;
}

/* End of file custom_helper.php */
/* Location: ./application/helpers/custom_helper.php */
?>
