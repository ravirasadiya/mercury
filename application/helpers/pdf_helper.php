<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// create pdf
function create_pdf($html, $filepath, $download=false)
{
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->library('DOMPDFLib');

	// create
	$ci->dompdflib->create($html, $filepath, $download);

	return true;
}

/* End of file pdf_helper.php */
/* Location: ./application/helpers/pdf_helper.php */
?>
