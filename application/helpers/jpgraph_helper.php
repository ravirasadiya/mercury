<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// create bar chart
function create_jpg_bar_chart($arr=array(), $clrarr=null)
{
	// get ci
	$ci =& get_instance();

	// Example for use of JpGraph,
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph.php');
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph_bar.php');

	// We need some data
	$datay = $arr['datasets'][0]['data'];
	$datax = $arr['labels'];

	// Setup the graph.
	$graph = new Graph(700,400);
	$graph->clearTheme();
	$graph->img->SetMargin(50,50,50,150);
	$graph->SetScale("textlin");
	//$graph->SetMarginColor("lightblue:1.1");
	//$graph->SetShadow();
	$theme_class = new SoftyTheme;
	if($clrarr && is_array($clrarr)){
		$theme_class->colorarray = $clrarr;
	}
	$graph->SetTheme($theme_class);

	// Set up the title for the graph
	$graph->title->Set($arr['title']);
	$graph->title->SetMargin(0,0,0,50);
	//$graph->title->SetFont(FF_VERDANA,FS_BOLD,12);
	//$graph->title->SetColor("darkred");

	// Setup font for axis
	//$graph->xaxis->SetFont(FF_VERDANA,FS_NORMAL,10);
	//$graph->yaxis->SetFont(FF_VERDANA,FS_NORMAL,10);

	// Show 0 label on Y-axis (default is not to show)
	$graph->yscale->ticks->SupressZeroLabel(false);

	// Setup X-axis labels
	$graph->xaxis->SetTickLabels($datax);
	$graph->xaxis->SetLabelAngle(45);

	// Create the bar pot
	$bplot = new BarPlot($datay);
	$bplot->SetWidth(0.5);
	//$bplot->SetFillColor('orange');

	// Setup color for gradient fill style
	//$bplot->SetFillGradient("navy:0.9","navy:1.85",GRAD_LEFT_REFLECTION);

	// Set color for the frame of each bar
	//$bplot->SetColor("white");
	$graph->Add($bplot);

	// Finally send the graph to the browser
	$fileName = '.'.REPORTS_DIR.get_uuid().'.png';
	$graph->Stroke($fileName);

    //$graph->Stream($fileName);

	return $fileName;
}


// create grouped bar chart
function create_jpg_grouped_bar_chart($arr=array())
{
	// get ci
	$ci =& get_instance();

	// Example for use of JpGraph,
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph.php');
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph_bar.php');

	//$ydata = array(12,15,22,19,5);

	$graph = new Graph(700,400);
	$graph->clearTheme();
	$graph->img->SetMargin(50,50,50,150);
	$graph->SetScale("textlin");
	//$graph->SetShadow();
	$theme_class = new SoftyTheme;
	$graph->SetTheme($theme_class);

	$graph->title->Set($arr['title']);

	$bararr = array();

	foreach($arr['datasets'] as $row){
		$bplot = new BarPlot($row['data']);
		$bplot->SetLegend($row['label']);
		$bplot->SetWidth(0.5);
		$bararr[] = $bplot;
	}

	$gbar = new GroupbarPlot($bararr);

	$graph->Add($gbar);
	$graph->xaxis->SetTickLabels($arr['labels']);
	$graph->xaxis->SetLabelAngle(45);

	// Finally send the graph to the browser
	$fileName = '.'.REPORTS_DIR.get_uuid().'.png';
	$graph->Stroke($fileName);

    //$graph->Stream($fileName);

	return $fileName;
}


// create pie chart
function create_jpg_pie_chart($arr=array())
{
	// get ci
	$ci =& get_instance();

	// Example for use of JpGraph,
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph.php');
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph_pie.php');

	// Some data
	$data = $arr['datasets'][0]['data'];

	// Create the Pie Graph.
	$graph = new PieGraph(500,300);
	$graph->clearTheme();
	$graph->SetMargin(0,0,0,0);
	$graph->SetFrame(false);
	$theme_class = new SoftyTheme;
	$graph->SetTheme($theme_class);

	// Create
	$p1 = new PiePlot($data);
	$p1->SetLegends($arr['labels']);
	$graph->Add($p1);

	// Finally send the graph to the browser
	$filepath = '.'.REPORTS_DIR.get_uuid().'.png';
	$graph->Stroke($filepath);

	return $filepath;
}


// create line chart
function create_jpg_line_chart($arr=array(), $usetheme=false)
{
	// get ci
	$ci =& get_instance();

	// Example for use of JpGraph,
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph.php');
	require_once ('./application/libraries/jpGraph-4.2.1/jpgraph_line.php');

	// Some data
	$datay = array(28,19,18,23,12,11);
	$data2y = array(14,18,33,29,39,55);

	// A nice graph with anti-aliasing
	$graph = new Graph(800,500);
	$graph->clearTheme();
	$graph->img->SetMargin(75,150,75,150);
	//$graph->SetBackgroundImage("tiger_bkg.png",BGIMG_FILLFRAME);
	$graph->SetFrame(false);

	$graph->img->SetAntiAliasing();
	$graph->SetScale("textlin");
	//$graph->SetShadow();
	//$graph->title->Set("");

	// check theme
	if($usetheme){
		$graph->title->Set($arr['title']);
		$graph->img->SetMargin(75,55,40,0);
		$theme_class = new SoftyTheme;
		$graph->SetTheme($theme_class);
		$graph->img->SetAntiAliasing(false);
	}

	// Show 0 label on Y-axis (default is not to show)
	$graph->yscale->ticks->SupressZeroLabel(false);

	// Use built in font
	//$graph->title->SetFont(FF_FONT1,FS_BOLD);

	// Slightly adjust the legend from it's default position in the
	// top right corner.
	//$graph->legend->Pos(0.05,0.5,"right","center");

	//echo '<pre>'; print_r($arr); echo '</pre>'; exit;

	$graph->xaxis->SetTickLabels(array_values($arr['labels']));
	$graph->xaxis->SetLabelAngle(45);

	// set
	$check_total = 0;

	// loop
	foreach($arr['datasets'] as $row){
		$check_total += array_sum($row);
	}

	// check
	if($check_total == 0){
		// loop
		foreach($arr['datasets'] as $key => $row){
			$arr['datasets'][$key] = array_fill(0, count($row), 0);
		}
	}

	// loop
	foreach($arr['datasets'] as $id => $data){
		//$data = array(rand(0, 10), rand(0, 10), rand(0, 10));
		$plot = new LinePlot(array_values($data));
		$plot->SetLegend($arr['legends'][$id]);
		$graph->Add($plot);
		$plot->SetWeight(3);
	}

	// Create the first line
	/*$p1 = new LinePlot($datay);
	///$p1->mark->SetType(MARK_FILLEDCIRCLE);
	//$p1->mark->SetFillColor("red");
	//$p1->mark->SetWidth(4);
	//$p1->SetColor("blue");
	//$p1->SetCenter();
	$p1->SetLegend("Triumph Tiger -98");
	$graph->Add($p1);

	// ... and the second
	$p2 = new LinePlot($data2y);
	//$p2->mark->SetType(MARK_STAR);
	//$p2->mark->SetFillColor("red");
	//$p2->mark->SetWidth(4);
	//$p2->SetColor("red");
	//$p2->SetCenter();
	$p2->SetLegend("New tiger -99");
	$graph->Add($p2);*/

	// Finally send the graph to the browser
	$filepath = '.'.REPORTS_DIR.get_uuid().'.png';
	$graph->Stroke($filepath);

	return $filepath;
}


/* End of file jpgraph_helper.php */
/* Location: ./application/helpers/jpgraph_helper.php */
?>
