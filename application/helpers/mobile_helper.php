<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// echo json
function echo_json($retarr)
{
	// set default
	$defarr = config_item('default-mobile-response');

	// merge
	$retarr = array_merge($defarr, $retarr);

	// check
	if(USERID){
		$retarr['customcss'] = (isset($retarr['customcss'])) ? $retarr['customcss'] : '';
	}

	// check
	if(isset($retarr['view'])){
		// clean
		$retarr['view'] = preg_replace('/(?s)(<textarea.*?>.*?<\/textarea>)|(\r|\n|\t| {2,})/', '$1', $retarr['view']);

		// retrieve
		preg_match('/<ons-toolbar.*?>.+?ons-toolbar.*?>/', $retarr['view'], $matcharr);
		$retarr['toolbar'] = (isset($matcharr[0])) ? $matcharr[0] : null;

		// clean toolbar
		$retarr['view'] = preg_replace('/<ons-toolbar.*?>.+?ons-toolbar.*?>/', '', $retarr['view']);
	}

	// set headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	// echo
	echo json_encode($retarr);
}

// ons-select
function ons_select($name, $valarr, $selval=null, $attr='', $data='')
{
	$data = '<ons-select name="'.$name.'", '.$attr.'>';
		// loop
		foreach($valarr as $key => $val){
			$issel = ($key == $selval) ? 'selected' : '';

			$data.= '<option value="'.$key.'" '.$issel.'>';
				$data.= $val;
			$data.= '</option>';
		}
	$data.= '</ons-select>';

	return $data;
}


// ons-select
function ons_multi_select($name, $valarr, $selval=null, $attr='', $data='')
{
	$data = '<ons-select name="'.$name.'", '.$attr.' multiple="true">';
		// loop
		foreach($valarr as $group => $optarr){
			$data .= '<optgroup label="'.$group.'">';

			// loop
			foreach($optarr as $key => $val){
				$issel = ($key == $selval) ? 'selected' : '';

				$data.= '<option value="'.$key.'" '.$issel.'>';
					$data.= $val;
				$data.= '</option>';
			}

			$data .= '</optgroup>';
		}
	$data.= '</ons-select>';

	return $data;
}


/* End of file mobile_helper.php */
/* Location: ./application/helpers/mobile_helper.php */
?>
