<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Utilities Helper
 */

/**
 * Make URI.
 *
 * @param $string
 * @return mixed|string
 */
function make_uri($string)
{
	// sanitize
	$string = sanitize_string($string, '-');

	// clean up
	$searcharray = array(' ', '--',);
	$replacearray = array('-');
	$data = str_replace($searcharray, $replacearray, strtolower(trim($string)));

	$data = trim($data, '-');

	return $data;
}


/**
 * Sanitize string.
 *
 * @param $string
 * @param string $char
 * @return mixed
 */
function sanitize_string($string, $char='-')
{
	$string = preg_replace('#\W#', $char, strtolower(trim($string)));
	$string = str_replace(array($char.$char.$char, $char.$char), $char, $string);

	return $string;
}


/**
 * Create hashtag.
 *
 * @param string $str
 * @return string
 */
function create_hashtag($str='')
{
	$str = trim($str, '#');
	$str = '#'.make_uri($str);

	return $str;
}


/**
 * Get empty table array.
 *
 * @param $tablename
 * @return mixed
 */
function get_empty_field_array($tablename)
{
	// get ci
	$ci =& get_instance();

	$fields = $ci->db->list_fields($tablename);

	foreach($fields as $fieldname){
		$array[$fieldname] = '';
	}

	return $array;
}


/**
 * DB Table to key-val array.
 *
 * @param $table
 * @param $valuefield
 * @param $displayfield
 * @param null $where
 * @param array $retarr
 * @param string $joinchar
 * @return array
 */
function table_to_array($table, $valuefield, $displayfield, $where=null, $retarr=array('' => 'Select...'), $joinchar=' ')
{
	// get ci
	$ci =& get_instance();

	// check
	$displayfield = (is_array($displayfield)) ? $displayfield : array($displayfield);

	// set
	$wherestr = null;
	if(isset($where['where-str'])){
		$wherestr = $where['where-str'];
		unset($where['where-str']);
	}

	// query db
	$ci->db->select('*');
	$ci->db->from($table);
		if($where){
			foreach($where as $wherefield => $wherevalue){
				// check
				if(stristr($wherevalue, 'NULL')){
					$ci->db->where("$wherefield $wherevalue");
				}else{
					$ci->db->where($wherefield, $wherevalue);
				}
				if($wherestr){
					$ci->db->where($wherestr);
				}
			}
		}

	// loop
	foreach($displayfield as $fieldname){
		$ci->db->order_by($fieldname);
	}
	$rs = $ci->db->get()->result_array();

	foreach($rs as $row){
		$row[$valuefield] = strip_tags($row[$valuefield]);

		$display = '';
		foreach($displayfield as $fieldname){
			$display .= strip_tags($row[$fieldname]).$joinchar;
		}

		$retarr[$row[$valuefield]] = trim($display);
	}

	return $retarr;
}

/**
 * Create nested dropdown.
 *
 * @param $name
 * @param $fieldarray
 * @param string $headeroptions
 * @param null $parentid
 * @param string $spacer
 * @param array $retarr
 * @return array
 */
function nested_table_to_array($fieldarray, $headeroptions='', $parentid=null, $spacer='', $retarr=array())
{
	// get ci
	$ci =& get_instance();

	$table = $fieldarray['table'];
	$valuefield = $fieldarray['value'];
	$displayfield = $fieldarray['display'];
	$parentfield = $fieldarray['parent'];
	$where = (isset($fieldarray['where'])) ? $fieldarray['where'] : array();
	$where = (isset($fieldarray['topwhere'])) ? $where+$fieldarray['topwhere'] : $where;

	// clear
	$fieldarray['topwhere'] = null;

	// query db
	$ci->db->select('*');
	$ci->db->from($table);
	if(!$parentid){
		$ci->db->where("($parentfield IS NULL OR $parentfield=0)");
	}else{
		$ci->db->where($parentfield, $parentid);
	}
	if(is_array($where)){
		$ci->db->where($where);
	}
	$ci->db->where('deleted IS NULL');
	$ci->db->order_by($displayfield);
	$rs = $ci->db->get()->result_array();
	//echo '<pre>'.$ci->db->last_query();
	//echo '<pre>'; print_r($rs); exit;

	foreach($rs as $row){
		$value = $row[$valuefield];
		$display = $row[$displayfield];

		$retarr[$value] = $spacer.$display;

		$retarr = nested_table_to_array($fieldarray, '', $value, $spacer.' &nbsp - &nbsp;', $retarr);
	}

	return $retarr;
}


/**
 * Debug array.
 *
 * @param $array
 * @param string $spacer
 * @return string
 */
function debug_array($array, $spacer='')
{
	$data = '';

	// check
	if(!is_array($array)){
		echo '<pre>'; print_r($array); exit;
	}

	foreach($array as $key => $value){
		if(is_array($value)){
			$data .= "$spacer $key => <br>";
			$data .= debug_array($value, $spacer.' &nbsp; - &nbsp; ');
		}else{
			$data .=  "$spacer $key => $value <br>";
		}
	}

	return $data;
}


/**
 * Get datetime.
 *
 * @return bool|string
 */
function datetime()
{
	return date("Y-m-d H:i:s");
}


/**
 * Trim array values.
 *
 * @param $arr
 * @param array $retarr
 * @return array
 */
function trim_array($arr, $retarr=array())
{
	// check
	if(!is_array($arr)){
		return $arr;
	}

	// loop
	foreach($arr as $key => $val){
		$key = trim($key);

		$retarr[$key] = trim($val);
	}

	return $retarr;
}


/**
 * Generate password.
 *
 * @param int $len
 * @return string
 */
function generate_password($len=6)
{
	return substr(md5(rand().rand()), 0, $len);
}


/**
 * Generate random number.
 *
 * @param int $len
 * @param string $str
 * @return string
 */
function generate_number($len=5, $str='')
{
	// loop
	for($i=0; $i<=$len; $i++){
		$str .= mt_rand(0,9);
	}

	return $str;
}


/**
 * Save tags.
 *
 * @param $arr
 * @param null $parentid
 */
function savetags($arr, $parentid=null)
{
	// get ci
	$ci =& get_instance();

	// loop
	foreach($arr as $tag => $children){
		// clean
		$tag = ucwords(strtolower(trim($tag)));

		// debug
		//echo "parent: $parentid -- save: $tag <br />";

		// save
		$tagid = $ci->tag_model->save(array('hash' => create_hashtag($tag), 'title' => $tag, 'parentid' => $parentid));

		// check
		if(is_array($children)){
			savetags($children, $tagid);
		}
	}
}

/**
 * Clean number.
 *
 * @param $str
 * @return string
 */
function clean_number($str)
{
	$str = trim( str_replace(' ', '', $str) );

	return $str;
}


// clean cell number
function clean_cell_number($str)
{
	$str = trim( str_replace(' ', '', $str) );
	$str = ltrim($str, '+');
	$str = '27'.substr($str, -9);

	return $str;
}


/**
 * Clean e-mail.
 *
 * @param $str
 * @return string
 */
function clean_email($str)
{
	$str = strtolower(trim($str));

	return $str;
}


/**
 * Set captcha.
 *
 * @return array
 */
function set_captcha(){
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->helper('captcha');

	// set
	$arr = array(
		'word' => generate_number(5),
		'img_path' => '.'.ltrim(CAPTCHA_DIR, '.'),
		'img_url' => CAPTCHA_DIR,
		'img_width' => 180,
		'img_height' => 50,
    );

    // create captcha
	$retarr = create_captcha($arr);

	//echo '<pre>'; print_r($retarr); exit;

	// set session
	$ci->session->set_userdata('CAPTCHA', $retarr['word']);

	return $retarr;
}


/**
 * Clean captchas.
 */
function clean_captchas()
{
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->helper('file');

	// read
	$arr = get_dir_file_info('.'.ltrim(CAPTCHA_DIR, '.'));
	//echo '<pre>'; print_r($arr); exit;

	// set
	$today = date("Y-m-d H:i:s");
	//$today = date("Y-m-d H:i:s", strtotime("+1 day"));
	$today = new DateTime($today);

	// loop
	foreach($arr as $filename => $row){
		// check
		$filedate = new DateTime(date("Y-m-d H:i:s", $row['date']));
		$diff = $today->diff($filedate);
		//print_r($diff);

		if($diff->invert == 1 && $diff->days >= 1){
			unlink($row['relative_path'].$filename);
		}
	}
}


/**
 * Result set to key-val array.
 *
 * @param $valarr
 * @param $rs
 * @param array $retarr
 * @return array
 */
function rs_to_keyval_array($rs, $keyfield, $valfield, $retarr=array())
{
	// loop
	foreach($rs as $row){
		$key = (isset($row[$keyfield])) ? $row[$keyfield] : null;
		$val = (isset($row[$valfield])) ? $row[$valfield] : null;

		$retarr[$key] = $val;
	}

	return $retarr;
}


/**
 * Get first array key.
 *
 * @param $arr
 * @return mixed
 */
function get_first_key($arr)
{
	// reset
	reset($arr);

	// key
	$key = key($arr);

	return $key;
}


/**
 * Implode sql query.
 *
 * @param $prefix
 * @param $arr
 * @param string $join
 * @param string $data
 * @return string
 */
function implode_sql($prefix, $arr, $join='OR ', $data='')
{
	$data = "$prefix'".implode("' $join $prefix'", $arr)."'";
	$data = "($data)";

	return $data;
}


/**
 * Paginate.
 *
 * @param $total
 * @param $limit
 * @param $offset
 * @param int $linkslimit
 * @param string $data
 * @return string
 */
function paginate($total, $limit, $offset, $linkslimit=25, $data='')
{
	// set
	$numpage = 1;
	$i = 0;
	$uri = site_url(uri_string());
	$first_url = $uri;
	$last_url = $uri.'?from='.(floor($total/$limit)*$limit);

	// check
	if($total <= $limit){
		return $data;
	}

	// set limits
	$btnlimit = floor($linkslimit/2);
	$btm_btnlimit = $offset-($btnlimit*$limit);
	$top_btnlimit = $offset+($btnlimit*$limit);

	// data
	$data .= '<nav class="text-right">';
		$data .= '<ul class="pagination no-margin">';
			// first
			$data .= '<li class="">';
				$data .= '<a href="'.$first_url.'" aria-label="Previous">';
					$data .= '<span aria-hidden="true">&laquo;</span>';
				$data .= '</a>';
			$data .= '</li>';

			// loop
			while($i<$total){
				$li_class = ($i == $offset) ? 'active' : '';
				$href_class = ($i == $offset) ? '' : '';

				// check
				if($i >= $btm_btnlimit && $i <= $top_btnlimit){
					$url = ($i>1) ? $uri.'?from='.(($numpage++)*$limit) : $uri;

					$data .= '<li class="'.$li_class.'"><a href="'.$url.'" class="'.$href_class.'">'.$numpage.'</a></li>';
				}

				// inc
				$i += $limit;
			}

			// last
			$data .= '<li>';
				$data .= '<a href="'.$last_url.'" aria-label="Next">';
					$data .= '<span aria-hidden="true">&raquo;</span>';
				$data .= '</a>';
			$data .= '</li>';
		$data .= '</ul>';
	$data .= '</nav>';

	return $data;
}


/**
 * Get date.
 *
 * @param $date
 * @param string $format
 * @return bool|null|string
 */
function get_date($date, $format="j F Y")
{
	$date = (trim($date) && time($date)) ? date($format, strtotime($date)) : null;

	return $date;
}


/**
 * Trim to null.
 *
 * @param $data
 * @return null|string
 */
function trim_to_null($data)
{
	if(is_array($data)){
		return $data;
	}

	$data = trim($data);
	$data = ($data && $data != 'null') ? $data : null;

	// date
	$data = (stristr($data, '0000-')) ? null : $data;

	return $data;
}


/**
 * Recursive array search.
 *
 * @param $needle
 * @param $haystack
 * @return bool|int|string
 */
function recursive_array_search($needle, $haystack){
	foreach($haystack as $key=>$value){
		$current_key=$key;
		if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
			return $current_key;
		}
	}
	return false;
}


/**
 * Recursive array key search.
 *
 * @param $needle
 * @param $haystack
 * @return bool
 */
function recursive_array_key_search($needle, $haystack){
	foreach($haystack as $key=>$value){
		$current_key=$key;
		if($needle===$key OR (is_array($value) && recursive_array_key_search($needle,$value) !== false)) {
			return $value;
		}
	}
	return false;
}


// hash string
function get_hash($str)
{
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->helper('phpass');

	$hasher = new PasswordHash(8, FALSE);
    $str = $hasher->HashPassword($str);

    return $str;
}


// check string
function check_hash($str, $stored_hash)
{
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->helper('phpass');

	$hasher = new PasswordHash(8, FALSE);
    $isvalid = $hasher->CheckPassword($str, $stored_hash);

    return $isvalid;
}


// encrypt
function encrypt($str)
{
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->library('encrypt');

	// encrypt
	$str = $ci->encrypt->encode($str);

	return $str;
}


// decrypt
function decrypt($str)
{
	// get ci
	$ci =& get_instance();

	// load
	$ci->load->library('encrypt');

	// encrypt
	$str = $ci->encrypt->decode($str);

	return $str;
}


// trim clean html
function trim_clean_html($html, $len=300)
{
	// strip
	$html = strip_tags($html);

	// limit
	$html = (strlen($html) > $len) ? substr($html, 0, $len).'...' : $html;

	return $html;
}


// fill string
function fill_string($str, $arr)
{
	// loop
	foreach($arr as $key => $val){
		$str = str_replace($key, $val, $str);
	}

	return $str;
}


// time array
function time_array($step=1800, $format='H:i', $lower=0, $upper=86400)
{
	$times = array();

	foreach(range($lower, $upper, $step) as $increment){
		$increment = gmdate('H:i', $increment);

		list($hour, $minutes) = explode(':', $increment);

		$date = new DateTime($hour.':'.$minutes);

		$times[(string) $increment] = $date->format($format);
	}

	return $times;
}


// format number
function format_money($num)
{
	$num = (!trim($num)) ? '' : number_format($num, 2, '.', ' ');

	return $num;
}


// clean dater
function clean_date($date, $str="Y-m-d")
{
	// format
	$date = (!trim($date) || stristr($date, '1970') || stristr($date, '0000')) ? '' : date($str, strtotime($date));

	return $date;
}


/// clean csv str
function clean_csv_str($str)
{
	$str = str_replace(array(','), ' ', $str);

	return $str;
}


// call api
function callapi($url, $method, $params=null)
{
	// sey
	$apiuser = 'flyingfoxapi';
	$apipass = 'kY3fTS848Euz';

	// init
	$curl = curl_init();

	// update
	$url = trim($url, '/').'/'.$method;

	switch($method){
		case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);

			if($params){
				curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			}
		break;
		case "PUT":
			curl_setopt($curl, CURLOPT_PUT, 1);
			break;
		default:
			if ($params){
				$url = sprintf("%s?%s", $url, http_build_query($params));
			}
		break;
	}

	// Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_USERPWD, "$apiuser:$apipass");

	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($curl);

	curl_close($curl);

	//echo '<pre>'; print_r($result); echo '</pre>'; exit;

	return $result;
}


/**
 * @param $interval
 * @param $datefrom
 * @param $dateto
 * @param bool $using_timestamps
 * @return false|float|int|string
 */
function datediff($interval, $datefrom, $dateto, $using_timestamps = false)
{
	/*
	$interval can be:
	yyyy - Number of full years
	q	- Number of full quarters
	m	- Number of full months
	y	- Difference between day numbers
		   (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
	d	- Number of full days
	w	- Number of full weekdays
	ww   - Number of full weeks
	h	- Number of full hours
	n	- Number of full minutes
	s	- Number of full seconds (default)
	*/

	if (!$using_timestamps) {
		$datefrom = strtotime($datefrom, 0);
		$dateto   = strtotime($dateto, 0);
	}

	$difference		= $dateto - $datefrom; // Difference in seconds
	$months_difference = 0;

	switch ($interval) {
		case 'yyyy': // Number of full years
			$years_difference = floor($difference / 31536000);
			if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
				$years_difference--;
			}

			if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
				$years_difference++;
			}

			$datediff = $years_difference;
		break;

		case "q": // Number of full quarters
			$quarters_difference = floor($difference / 8035200);

			while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
				$months_difference++;
			}

			$quarters_difference--;
			$datediff = $quarters_difference;
		break;

		case "m": // Number of full months
			$months_difference = floor($difference / 2678400);

			while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
				$months_difference++;
			}

			$months_difference--;

			$datediff = $months_difference;
		break;

		case 'y': // Difference between day numbers
			$datediff = date("z", $dateto) - date("z", $datefrom);
		break;

		case "d": // Number of full days
			$datediff = floor($difference / 86400);
		break;

		case "w": // Number of full weekdays
			$days_difference  = floor($difference / 86400);
			$weeks_difference = floor($days_difference / 7); // Complete weeks
			$first_day		= date("w", $datefrom);
			$days_remainder   = floor($days_difference % 7);
			$odd_days		 = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?

			if ($odd_days > 7) { // Sunday
				$days_remainder--;
			}

			if ($odd_days > 6) { // Saturday
				$days_remainder--;
			}

			$datediff = ($weeks_difference * 5) + $days_remainder;
		break;

		case "ww": // Number of full weeks
			$datediff = floor($difference / 604800);
		break;

		case "h": // Number of full hours
			$datediff = floor($difference / 3600);
		break;

		case "n": // Number of full minutes
			$datediff = floor($difference / 60);
		break;

		default: // Number of full seconds (default)
			$datediff = $difference;
		break;
	}

	return $datediff;
}


// array to table
function array_to_table($data=array())
{
	$rows = array();
	foreach ($data as $row) {
		$cells = array();
		foreach ($row as $cell) {
			$cells[] = "<td>{$cell}</td>";
		}
		$rows[] = "<tr>" . implode('', $cells) . "</tr>";
	}
	return '<table border="1">' . implode('', $rows) . '</table>';
}


// round values
function round_values($data)
{
	// check
	if(is_array($data)){
		// round
		array_walk_recursive(
			$data,
			function (&$value) {
				if (is_numeric($value)) {
					$value = round($value);
				}
			}
		);
	}
	if(is_numeric($data)){
		$data = round($data);
	}

	return $data;
}


// is valid e-mail
function is_valid_email($email){
	return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}


/* End of file utils_helper.php */
/* Location: ./application/helpers/utils_helper.php */
?>
