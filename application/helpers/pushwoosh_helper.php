<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function pwCall($method, $data, $debug=DEBUG_PUSHMESSAGES){

	//echo '<pre>'; print_r($data); echo '</pre>'; exit;

	// debug
	if($debug){
		$debug = $data['notifications'][0]['content'];
		$debug .= '<hr />';
		$debug .= (isset($data['notifications'][0]['devices'])) ? 'Devices: <br />'.implode('<br />', $data['notifications'][0]['devices']) : '';

		// debug
		debug_output($data['notifications'][0]['debug-type'], $debug, 'pushmessages');

		return;
	}

	// send
	$url = 'https://cp.pushwoosh.com/json/1.3/' . $method;
	$request = json_encode(array('request' => $data));

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

	$response = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);

	if(defined('PW_DEBUG') && PW_DEBUG){
		echo '<pre>';
		print "[PW] request: $request\n";
		print "[PW] response: $response\n";
		print "[PW] info: " . print_r($info, true);
	}
}


function send_push_msg($msgtype, $id=null, $other_params=array())
{
	// get ci
	$ci =& get_instance();

	// set
	$companyname = (defined('COMPANYNAME')) ? COMPANYNAME : ADMIN_COMPANYNAME;
	$msg_arr = config_item('msg-arr');
	$params = array('send_date' => 'now');
	$tokenarr = array();
	$userarr = array();
	$emailarr = array();
	$smsarr = array();
	$strarr = array();
	$msg = (isset($msg_arr[$msgtype])) ? $msg_arr[$msgtype] : null;

	// debug
	if(DEBUG_PUSHMESSAGES){
		$params['debug-type'] = $msgtype;
	}

	// switch
	switch($msgtype){
		// complaint
		case 'complaint-logged':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// check
			if($complaintrs['categoryid'] == NOISECOMPLAINT_CATEGORYID){
				if($complaintrs['num_unresolved_noisecompliants_against_unit'] == 1){
					// send
					send_push_msg('noise-complaint-logged-level-1-to-complaint-originator', $id);
					send_push_msg('noise-complaint-logged-level-1-to-complaint-against', $id);
				}
				if($complaintrs['num_unresolved_noisecompliants_against_unit'] == 2){
					// send
					send_push_msg('noise-complaint-logged-level-2-to-complaint-against', $id);
					send_push_msg('noise-complaint-logged-level-2-to-caretaker', $id);
				}
				if($complaintrs['num_unresolved_noisecompliants_against_unit'] == 3){
					// send
					send_push_msg('noise-complaint-logged-level-3-to-complaint-against', $id);
					send_push_msg('noise-complaint-logged-level-3-to-estatemanager', $id);
				}
				if($complaintrs['num_unresolved_noisecompliants_against_unit'] > 3){
					// send
					send_push_msg('noise-complaint-logged-level-4-to-trustees-and-management-security', $id);
					send_push_msg('noise-complaint-logged-level-4-to-operational-manager', $id);
				}
			}else{
				send_push_msg('other-complaint-logged-to-complaint-originator', $id);
				send_push_msg('other-complaint-logged-to-operational-manager', $id);
			}
			return;
		break;
		case 'noise-complaint-logged-level-1-to-complaint-originator':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['createdbyuserid'];
			//$tokenarr[] = $complaintrs['pushtoken'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-1-to-complaint-against':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['againsttenantuserid'];
			$userarr[] = $complaintrs['againstowneruserid'];
			//$tokenarr[] = $complaintrs['againstpushtoken'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-2-to-complaint-against':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['againsttenantuserid'];
			$userarr[] = $complaintrs['againstowneruserid'];
			//$tokenarr[] = $complaintrs['againstpushtoken'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-2-to-caretaker':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['againstowneruserid'];
			$userarr[] = $complaintrs['caretakeruserid'];
			//$tokenarr[] = $complaintrs['lvl2pushtoken'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-3-to-complaint-against':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['againsttenantuserid'];
			$userarr[] = $complaintrs['againstowneruserid'];
			//$tokenarr[] = $complaintrs['againstpushtoken'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-3-to-estatemanager':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			//$userarr[] = $complaintrs['caretakeruserid'];
			$userarr[] = $complaintrs['estatemanageruserid'];
			//$tokenarr[] = $complaintrs['lvl3pushtoken'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-4-to-trustees-and-management-security':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// send e-mail/sms
			$userarr[] = $complaintrs['caretakeruserid'];
			$userarr[] = $complaintrs['estatemanageruserid'];
			$emailarr[] = $complaintrs['notifyemail'];
			$smsarr[] = $complaintrs['notifycell'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;
		case 'noise-complaint-logged-level-4-to-operational-manager':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// send e-mail/sms
			//$userarr[] = $complaintrs['caretakeruserid'];
			$userarr[] = $complaintrs['estatemanageruserid'];
			$emailarr[] = $complaintrs['notifyemail'];
			$smsarr[] = $complaintrs['notifycell'];
			$strarr = array(
					'[name]' => $complaintrs['againsttenantfirstname'],
					'[unit]' => $complaintrs['againstunit']
				);
		break;


		case 'other-complaint-logged-to-complaint-originator':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['createdbyuserid'];
			//$tokenarr[] = $complaintrs['pushtoken'];
		break;
		case 'other-complaint-logged-to-operational-manager':
			// get
			$complaintrs = $ci->complaint_model->get($id);

			// set
			$userarr[] = $complaintrs['caretakeruserid'];
			$userarr[] = $complaintrs['estatemanageruserid'];
			//$tokenarr[] = $complaintrs['notifypushtoken'];
			$emailarr[] = $complaintrs['notifyemail'];
			$smsarr[] = $complaintrs['notifycell'];
		break;
		case 'complaint-new-comment-to-request-originator-and-operations-and-trustee':
			// get
			$commentrs = $ci->comment_model->get($id);
			$complaintrs = $ci->complaint_model->get($commentrs['complaintid']);

			// set
			if($commentrs['iscomplaintowner']){
				$userarr[] = $complaintrs['caretakeruserid'];
				//$tokenarr[] = $complaintrs['caretakerpushtoken'];
				//$tokenarr[] = $complaintrs['notifypushtoken'];
				$emailarr[] = $complaintrs['notifyemail'];
				$smsarr[] = $complaintrs['notifycell'];
			}else{
				$userarr[] = $complaintrs['createdbyuserid'];
				//$tokenarr[] = $complaintrs['pushtoken'];
			}
			$strarr = array('[name]' => $commentrs['createdby']);
		break;

		// maintenance
		case 'maintenance-request-logged-to-request-originator':
		case 'maintenance-sos-marked-items-to-request-originator':
			// get
			$maintenancers = $ci->maintenance_model->get($id);
			//$userrs = $ci->user_model->get($maintenancers['createdbyuserid']);

			// set
			$userarr[] = $maintenancers['createdbyuserid'];
			//$tokenarr[] = $userrs['pushtoken'];
		break;
		case 'maintenance-request-logged-to-operational-manager':
			// get
			$maintenancers = $ci->maintenance_model->get($id);
			//$notifyuserrs = ($maintenancers['notifyuserid']) ? $ci->user_model->get($maintenancers['notifyuserid']) : null;

			// set
			$userarr[] = $maintenancers['caretakeruserid'];
			$userarr[] = $maintenancers['notifyuserid'];
			//$tokenarr[] = $maintenancers['caretakerpushtoken'];
			//$tokenarr[] = $maintenancers['notifypushtoken'];
			$emailarr[] = $maintenancers['notifyemail'];
			$smsarr[] = $maintenancers['notifycell'];
		break;
		case 'maintenance-request-reassigned-to-request-originator':
			// get
			$maintenancers = $ci->maintenance_model->get($id);
			//$userrs = $ci->user_model->get($maintenancers['createdbyuserid']);

			// set
			$userarr[] = $maintenancers['createdbyuserid'];
			//$tokenarr[] = $userrs['pushtoken'];
		break;
		case 'maintenance-request-reassigned-to-subcontractor':
			// get
			$maintenancers = $ci->maintenance_model->get($id);
			//$userrs = $ci->user_model->get($maintenancers['subcontractoruserid']);

			// set
			$userarr[] = $maintenancers['createdbyuserid'];
			//$tokenarr[] = $userrs['pushtoken'];
		break;
		case 'maintenance-request-resolved-to-request-originator':
			// get
			$maintenancers = $ci->maintenance_model->get($id);
			//$userrs = $ci->user_model->get($maintenancers['createdbyuserid']);
			$closedbyuserrs = $ci->user_model->get($maintenancers['closedbyuserid']);

			// set
			$userarr[] = $maintenancers['createdbyuserid'];
			$userarr[] = $maintenancers['caretakeruserid'];
			$userarr[] = $maintenancers['subcontractoruserid'];
			//$tokenarr[] = $userrs['pushtoken'];
			$emailarr[] = $maintenancers['notifyemail'];
			$smsarr[] = $maintenancers['notifycell'];
			$strarr = array('[name]' => $closedbyuserrs['firstname']);
		break;
		case 'maintenance-new-comment-to-request-originator-and-operations-and-contractor':
			// get
			$commentrs = $ci->comment_model->get($id);
			$maintenancers = $ci->maintenance_model->get($commentrs['maintenanceid']);

			// set
			$userarr[] = $userrs['userid'];
			if($commentrs['ismaintenanceowner']){
				$userarr[] = $maintenancers['caretakeruserid'];
				$userarr[] = $maintenancers['subcontractoruserid'];
				//$tokenarr[] = $maintenancers['caretakerpushtoken'];
				//$tokenarr[] = $maintenancers['subcontractorpushtoken'];
				//$tokenarr[] = $maintenancers['notifypushtoken'];
				$emailarr[] = $maintenancers['notifyemail'];
				$smsarr[] = $maintenancers['notifycell'];
			}else{
				$userarr[] = $maintenancers['createdbyuserid'];
				//$tokenarr[] = $maintenancers['pushtoken'];
			}
			$strarr = array('[name]' => $commentrs['createdby']);
		break;
		case 'maintenance-sos-marked-items-to-assigned-party':
			// get
			$maintenancers = $ci->maintenance_model->get($id);
			//$userrs = $ci->user_model->get($maintenancers['subcontractoruserid']);

			// set
			$userarr[] = $maintenancers['subcontractoruserid'];
			//$tokenarr[] = $maintenancers['subcontractorpushtoken'];
		break;

		// news

		// ads
		case 'ads-new-ads-to-all-users':
			// get
			$userrs = $ci->user_model->get(null, array('u.companyid' => $id));

			// loop
			foreach($userrs as $row){
				$userarr[] = $row['userid'];

				if(trim($row['pushtoken']) && !in_array($row['pushtoken'], $tokenarr)){
					$tokenarr[] = $row['pushtoken'];
				}
			}
		break;

		// bookings
		case 'booking-new-booking-to-booking-originator':
		case 'booking-cancel-booking-to-booking-originator':
			// get
			$bookingrs = $ci->booking_model->get($id);
			//$userrs = $ci->user_model->get($bookingrs['createdbyuserid']);

			// set
			$userarr[] = $bookingrs['createduserid'];
			//$tokenarr[] = $userrs['pushtoken'];
			$strarr = array('[type]' => $bookingrs['facility']);
		break;

		/*case 'booking-new-booking-to-booking-originator':
			// get
			$rs = $ci->maintenance_model->get($id);

			// set
			$tokenarr[] = $rs['pushtoken'];
			$strarr = array('[caretaker]' => $rs['caretaker']);
		break;*/

		// appointments
		case 'appointment-new-appointment-to-tenant':
			// get
			$appointmentrs = $ci->appointment_model->get($id);
			//$userrs = $ci->user_model->get($appointmentrs['tenantuserid']);

			// set
			$userarr[] = $appointmentrs['createdbyuserid'];
			//$tokenarr[] = $userrs['pushtoken'];
			$strarr = array(
					'[subcontractor]' => $appointmentrs['subcontractor'],
					'[date]' => $appointmentrs['date'],
					'[time]' => $appointmentrs['time']
				);
		break;
		case 'appointment-new-appointment-to-subcontractor':
			// get
			$appointmentrs = $ci->appointment_model->get($id);
			//$userrs = $ci->user_model->get($appointmentrs['subcontractoruserid']);

			// set
			$userarr[] = $appointmentrs['createdbyuserid'];
			//$tokenarr[] = $userrs['pushtoken'];
			//$emailarr[] = $userrs['email'];
			$strarr = array(
					'[unit]' => $appointmentrs['unit'],
					'[date]' => $appointmentrs['date'],
					'[time]' => $appointmentrs['time']
				);
		break;

		// custom
		case 'once-off-msg':
			$msg = $other_params['msg'];
			$tokenarr = $other_params['tokenarr'];
		break;

		default:
			return false;
		break;
	}

	// fill
	foreach($userarr as $i => $userid){
		// get
		$userrs = ($userid) ? $ci->user_model->get($userid) : null;

		if($userrs){
			$tokenarr[] = $userrs['pushtoken'];
			$cellarr[] = $userrs['cell'];
			$emailarr[] = $userrs['email'];
		}
	}

	// add
	$params['content'] = $msg = fill_string($msg, $strarr);

	// devices
	$params['devices'] = $tokenarr = array_filter( array_map('trim', $tokenarr) );
	$params['devices'] = $tokenarr = (count($tokenarr)) ? $tokenarr : array();

	// check
	if(!DEBUG_PUSHMESSAGES && !count($params['devices'])){
		return;
	}

	// send email
	$emailarr = array_filter( array_map('trim', $emailarr) );
	if(count($emailarr)){
		$msg = '<p>'.$msg.'</p>';

		$emarr = array(
				'to-email' => $emailarr,
				'subject' => 'New '.$companyname.' Notification',
				'msg' => $msg
			);
		send_email($emarr);
	}

	// send sms
	$smsarr = array_filter( array_map('trim', $smsarr) );
	if(count($smsarr)){
		// load
		$ci->load->library('clickatell');

		// send
		$ci->clickatell->send_message($smsarr, $msg);
	}

	// set
	$jsonarr = array(
		'userid' => array_filter( array_map('trim', $userarr)),
		'email' => array_filter( array_map('trim', $emailarr)),
		'cell' => array_filter( array_map('trim', $smsarr)),
		'pw-tokens' => array_filter( array_map('trim', $tokenarr))
	);

	// log
	$msglogarr = array();
	$msglogarr['complaintid'] = (isset($complaintrs)) ? $complaintrs['complaintid'] : null;
	$msglogarr['maintenanceid'] = (isset($maintenancers)) ? $maintenancers['maintenanceid'] : null;
	$msglogarr['bookingid'] = (isset($bookingrs)) ? $bookingrs['bookingid'] : null;
	$msglogarr['message'] = $msg;
	$msglogarr['jsondata'] = json_encode($jsonarr);
	$msglogarr['type'] = $msgtype;

	// loop
	$userarr = array_filter( array_map('trim', $userarr) );
	if(count($userarr)){
		foreach($userarr as $userid){
			// get
			$userrs = $ci->user_model->get($userid);

			// set
			$msglogarr['touserid'] = $userrs['userid'];

			// save
			$ci->message_model->save($msglogarr);
		}
	}

	//echo '<pre>'; print_r($params); echo '</pre>';

	// send
	pwCall('createMessage', array(
				'application' => PW_APPLICATION,
				'auth' => PW_AUTH,
				'notifications' => array($params)
			)
		);
}

/* End of file puswoosh_helper.php */
/* Location: ./application/helpers/pushwoosh_helper.php */
?>
