<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Upload image.
 * 
 * @param $fieldname
 * @param $path
 * @param array $params
 * @return null
 */
function upload_image($fieldname, $path, $params=array())
{
	// set defaults
	$filename = null;
	
	// check
	if((isset($_FILES[$fieldname])) && trim($_FILES[$fieldname]['tmp_name']) && (file_exists($_FILES[$fieldname]['tmp_name']))){
		// config	
		$config['upload_path'] = $path;
		$config['allowed_types'] = (isset($params['allowed-types'])) ? $params['allowed-types'] : IMG_FILETYPES;
		$config['max_size'] = (isset($params['max-size'])) ? $params['max-size'] : MAX_FILESIZE;
		$config['max_width'] = (isset($params['max-width'])) ? $params['max-width'] : '1200';
		$config['max_height'] = (isset($params['max-height'])) ? $params['max-height'] : '1200';   
		$config['width'] = (isset($params['resize-width'])) ? $params['resize-width'] : '1200';
		$config['height'] = (isset($params['resize-height'])) ? $params['resize-height'] : '1200';   
		$config['encrypt_name'] = (isset($params['encrypt-filename'])) ? $params['encrypt-filename'] : false;
		
		// upload
		$filename = upload($fieldname, $config);
	}
	
	return $filename;
}
	
	
/**
 * Upload file.
 * 
 * @param $fieldname
 * @param $path
 * @param array $params
 * @return null
 */
function upload_file($fieldname, $path, $params=array())
{
	// set defaults
	$filename = null;

	// check
	if((isset($_FILES[$fieldname])) && (file_exists($_FILES[$fieldname]['tmp_name']))){
		// config	
		$config['upload_path'] = $path;
		$config['allowed_types'] = (isset($params['allowed-types'])) ? $params['allowed-types'] : FILE_FILETYPES;
		$config['max_size'] = (isset($params['max-size'])) ? $params['max-size'] : MAX_FILESIZE;	
		
		// upload
		$filename = upload($fieldname, $config);	 
	}
	
	return $filename;
}
	
	
/**
 * Upload handler.
 *  
 * @param $fieldname
 * @param $config
 * @return bool|null
 */
function upload($fieldname, $config){
	// get ci
	$ci =& get_instance();
	
	// set defaults
	$retarr = null;
	$filename = null;
	$uploadpath = trim($config['upload_path'], '/').'/';
	
	// check
	$config['upload_path'] = ltrim($config['upload_path'], '/');
	$config['width'] = (isset($config['width'])) ? $config['width'] : MAX_IMG_WIDTH;
	$config['height'] = (isset($config['height'])) ? $config['height'] : MAX_IMG_HEIGHT;

	// load
	$ci->load->library('upload', $config);

	// upload
	if($ci->upload->do_upload($fieldname)){
		// upload data
		$retarr = $ci->upload->data();

		// msg
		$ci->msg->add('The file was successfully uploaded.', 'success');
	}else{
		// errors					 
		$error = $ci->upload->display_errors('', '');
		//echo $error; exit;
		
		// msg
		$ci->msg->add($error, 'danger');
		
		return false;
	}
	
	// set
	$filename = ($retarr) ? $retarr['file_name'] : $filename;
	
	// check and resize
	if($retarr && $retarr['is_image'] && ($retarr['image_width'] > $config['width'] || $retarr['image_height'] > $config['height']) ){
		// load
		$ci->load->library('image_lib');
		
		// resize
		$imgconfig = array();
		$imgconfig['source_image'] = $uploadpath.$filename;
		$imgconfig['width'] = (isset($config['width'])) ? $config['width'] : 1200;
		$imgconfig['height'] = (isset($config['height'])) ? $config['height'] : 1200;
		
		// load
		$ci->image_lib->initialize($imgconfig);
		
		// resize
		if(!$ci->image_lib->resize()){
			//echo $config['source_image'];
			$error =  $ci->image_lib->display_errors();
			
			// msg
			$ci->msg->add($error, 'danger');
		}

		// cleart
		$ci->image_lib->clear();
	}
	
	return $filename;
}


// create thumbnail
function create_thumb($filepath, $config=array())
{
	// get ci
	$ci =& get_instance();
	
	// load
	$ci->load->library('image_lib');
	
	// set
	$filename = end(explode('/', $filepath));
	$filepath = str_replace($filename, '', $filepath);
	$newfilename = end(array_reverse(explode('.', $filename)));
	$newfileext = end(explode('.', $filename));
	$savedimgpath = '.'.ltrim($filepath.$filename, '.');
	list($width, $height, $type, $attr) = getimagesize($savedimgpath);
	
	// create	
	$config['source_image'] = $savedimgpath;
	$config['width'] = (isset($config['resize-width'])) ? $config['resize-width'] : IMG_SMALL_WIDTH;
	$config['width'] = (isset($config['resize-width'])) ? $config['resize-width'] : IMG_SMALL_WIDTH;
	$config['height'] = (isset($config['resize-height'])) ? $config['resize-height'] : IMG_SMALL_HEIGHT;
	
	// update
	$post_str = (isset($config['post-str'])) ? $config['post-str'] : time();
	$newfilename = $newfilename.'-'.$post_str.'.'.$newfileext;
	$config['new_image'] = '.'.ltrim($filepath.$newfilename, '.');
	
	// init
	$ci->image_lib->initialize($config); 
	
	// resize
	if(!$ci->image_lib->resize()){
		$error =  $ci->image_lib->display_errors();
			
		// msg
		$ci->msg->add($error, 'danger');
	}
	
	// clear
	$ci->image_lib->clear();
	
	return $newfilename;
}


// display image
function display_image($filepath, $params=array(), $data='')
{
	// check
	if(file_exists('.'.$filepath)){
		$data .= '<img src="'.$filepath.'" class="img-responsive img-thumbnail" />';
	}
	
	return $data;
}
	
/* End of file site_helper.php */
/* Location: ./application/helpers/site_helper.php */
?>