<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Send e-mail.
 * 
 * @param $setarr
 * @param bool|false $attarr
 * @param bool|true $debug
 */
function send_email($setarr, $attarr=array(), $debug=DEBUG_EMAIL)
{	
	// get ci
	$ci =& get_instance();
	
	// load
	$ci->load->library('email');
	
	// clear
	$ci->email->clear(TRUE);
	
	// set
	$defarr = config_item('default-email-arr');
	
	// check
	$setarr['from-email'] = (isset($setarr['from-email'])) ? $setarr['from-email'] : $defarr['from-email']; 
	$setarr['from-name'] = (isset($setarr['from-name'])) ? $setarr['from-name'] : $defarr['from-name']; 
	$setarr['to-email'] = (isset($setarr['to-email'])) ? $setarr['to-email'] : $defarr['to-email']; 
	$setarr['subject'] = (isset($setarr['subject'])) ? $setarr['subject'] : $defarr['subject']; 
	$setarr['msg'] = (isset($setarr['msg'])) ? $setarr['msg'] : $defarr['msg'];
		
	// check
	if(TEST_EMAIL){
		$setarr['to-email'] = TEST_EMAIL;
	}
	
	// implode
	$setarr['to-email'] = (is_array($setarr['to-email'])) ? trim(implode(',', array_map('trim', $setarr['to-email'])), ',') : trim($setarr['to-email']);
	$setarr['to-email'] = str_replace(';', ',', $setarr['to-email']);
	
	// load view
	$msg = $ci->load->view('admin/_master-template-email', array('setarr' => $setarr), true);
	
	//echo $msg; exit;
	
	// config
	$config['mailtype'] = 'html';
	
	// init
	$ci->email->initialize($config);

	// setup
	$ci->email->from($setarr['from-email'], $setarr['from-name']);
	$ci->email->to($setarr['to-email']);
	$ci->email->subject($setarr['subject']);
	$ci->email->message($msg);
	
	// attachments
	foreach($attarr as $filepath){
		$filepath = './'.ltrim($filepath, '/');
		
		// attach
		$ci->email->attach($filepath);
	}
	
	// debug/send
	if($debug){
		$topmsg = 'To: '.$setarr['to-email'].'<br />';
		$topmsg .= 'Subject: '.$setarr['subject'].'<br />';
		$topmsg .= 'Attachments: '.implode(', ', $attarr).'<br />';
		$topmsg .= '<hr />';
		$msg = $topmsg.$msg;
		
		// debug
		debug_output($setarr['subject'], $msg, 'email');
	}else{
		// check
		if($ci->email->send()){
			// success
		}else{
			echo $ci->email->print_debugger(); exit;
		}
	}	
	
	// clear
	$ci->email->clear(TRUE);
	
}

/* End of file email_helper.php */
/* Location: ./application/helpers/email_helper.php */
?>