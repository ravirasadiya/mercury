<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Debug output.
 * 
 * @param $filename
 * @param $data
 * @param string $type
 */
function debug_output($filename, $data, $type='')
{
	// set
	$filenamearr[] = date("Ymd-His").'-('.str_replace(array('.', ' '), '', microtime()).')';
	$filenamearr[] .= ($type) ? $type : null;
	$filenamearr[] .= str_replace(array('/', '.', '\\'), '-', make_uri($filename)).'.html';
	
	$filename = implode(' -- ', array_filter( array_map('trim', $filenamearr) ) );
	
	//echo $filename; exit;
	
	// check
	@mkdir('./debug/'.$type, 0777, true);
	
	// write file
	file_put_contents('debug/'.$type.'/'.$filename, $data);
}

/* End of file debug_helper.php */
/* Location: ./application/helpers/debug_helper.php */
?>