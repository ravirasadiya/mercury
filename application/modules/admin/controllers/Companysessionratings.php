<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companysessionratings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companies'] = 'Company Session Ratings';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->companysessionrating_model->get(null, array('csr.companyid' => ADMIN_COMPANYID));

		// set
		$data = array('loadview' => 'admin/sessionratings/company-competency-plans-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($companysessionratingid=null)
	{
		// check
		$isprofile = ($companysessionratingid == 'plan') ? 1 : 0;
		$companysessionratingid = ($isprofile) ? null : $companysessionratingid;
		$companyid = ($isprofile) ? ADMIN_COMPANYID : ADMIN_COMPANYID; // DEBUG redundant
		$action = $this->input->get('action');

		// get
		$rs = (!$isprofile && $companysessionratingid) ? $this->companysessionrating_model->get($companysessionratingid) : $this->utils->create_empty_field_array('companysessionrating');

		// check
		if($isprofile){
			// get
			$companysessionratingrs = $this->companysessionrating_model->get(null, array('csr.companyid' => $companyid), null, true);

			$companysessionratingid = ($companysessionratingrs) ? $companysessionratingrs['companysessionratingid'] : $companysessionratingid;
			$rs = ($companysessionratingrs) ? $companysessionratingrs : $rs;
		}

		// set
		$data = array('loadview' => 'admin/sessionratings/company-sessionratings-edit', 'rs' => $rs);

		// get
		$data['sessionratingitemrs'] = $this->sessionratingitem_model->get();
		$data['companysessionratingrefarr'] = $this->companysessionratingref_model->get_keyval_arr(array('csrf.companysessionratingid' => $companysessionratingid));

		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');
			$items = $this->input->post('items');

			//echo '<pre>'; print_r($items); echo '</pre>'; exit;

			// update
			$form['companyid'] = $companyid;

			// save
			$companysessionratingid = $this->companysessionrating_model->save($form, $companysessionratingid);

			// update
			$this->companysessionratingref_model->update($items, $companysessionratingid);
			//exit;

			// msg
			$this->msg->add('The session rating details was successfully saved.', 'success');

			// redirect
			if($isprofile){
				redirect('/admin/companysessionratings/save/plan'.($action ? '?action='.$action : ''));
			}else{
				redirect('/admin/companysessionratings');
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($companyid)
	{
		// delete
		$rs = $this->companysessionrating_model->delete($companyid);

		// msg
		$this->msg->add('The company session rating plan was successfully deleted.', 'success');

		// redirect
		redirect('/admin/companysessionratings');
	}
}

/* End of file companysessionratings.php */
/* Location: ./application/controllers/companysessionratings.php */
