<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	private $usertypearr = array(
			'admin' => 'admin',
			'managing-director' => 'directors',
			'sales-manager' => 'managers',
			'sales-person' => 'teammembers'
		);

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/users'] = 'Users';
	}


	// mng
	public function index($type='admin')
	{
		// set
		$filtertype = array_search($type, $this->usertypearr);
		$otherWhere = array();
		
		if($filtertype=='managing-director'){
			$otherWhere = array('where-str'=>'u.type in ( "managing-director","managing-director_sales-person","managing-director_sales-manager","managing-director_sales-manager_sales-person" )');
		} else if($filtertype=='sales-manager'){
			$otherWhere = array('where-str'=>'u.type in ("managing-director_sales-manager","managing-director_sales-manager_sales-person","sales-manager" )');
		} else if($filtertype=='sales-person'){
			$otherWhere = array('where-str'=>'u.type in ("sales-person","managing-director_sales-person","managing-director_sales-manager_sales-person" )');
		} else{
			$where = array('u.type' => $filtertype);
		}

		// check
		if(ADMIN_COMPANYID){
			$where['u.companyid'] = ADMIN_COMPANYID;
		}

		// get
		$rs = $this->user_model->get(null, $where, $otherWhere);

		// set
		$data = array('loadview' => 'admin/users/users-mng', 'rs' => $rs, 'type' => $type);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($type='admin', $userid=null)
	{
		// set
		$filtertype = array_search($type, $this->usertypearr);

		// check
		$myprofile = ($type == 'myprofile') ? true : false;
		$userid = ($myprofile) ? ADMIN_USERID : $userid;

		// get
		$rs = ($userid) ? $this->user_model->get($userid) : $this->utils->create_empty_field_array('user');

		// set
		$data = array('loadview' => 'admin/users/users-edit', 'rs' => $rs, 'type' => $type);

		// get
		$data['companycompetencyplanrs'] = $this->companycompetencyplan_model->get(null, array('cut.companyid' => ADMIN_COMPANYID), null, true);
		$companycompetencyplanid = ($data['companycompetencyplanrs']) ? $data['companycompetencyplanrs']['companycompetencyplanid'] : null;

		// get
		$data['competencygrouprs'] = $this->competencygroup_model->get();
		$data['competencyparameterrs'] = $this->competencyparameter_model->get();
		$data['group_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL')) : array();
		$data['parameter_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL')) : array();
		$data['group_usercompetencylimitarr'] = ($userid) ? table_to_array('usercompetencylimit', 'competencygroupid', 'value', array('userid' => $userid, 'competencyparameterid' => 'IS NULL'), '') : array();
		$data['param_usercompetencylimitarr'] = ($userid) ? table_to_array('usercompetencylimit', 'competencyparameterid', 'value', array('userid' => $userid, 'competencyparameterid' => 'IS NOT NULL'), '') : array();

		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');
			$groups = $this->input->post('groups');
			$params = $this->input->post('params');
			$form['isenabled'] = (isset($form['isenabled'])) ? 1 : 'null';
			$form['onbehalfof'] = (isset($form['onbehalfof'])) ? 1 : 'null';

			//echo '<pre>'; print_r($params); echo '</pre>'; exit;

			// check
			// if($filtertype=='managing-director'){
			// 	$directors = $this->input->post('role_directors') ? $this->input->post('role_directors') : 0 ;
			// 	$managers = $this->input->post('role_managers') ? $this->input->post('role_managers') : 0 ;
			// 	$member = $this->input->post('role_member') ? $this->input->post('role_member') : 0 ;
			// 	if($directors==1){
			// 		$form['type'] = 'managing-director';
			// 	}
			// 	if($directors==1 && $member==1){
			// 		$form['type'] = 'managing-director_sales-person';
			// 	}
			// 	if($directors==1 && $managers==1) {
			// 		$form['type'] = 'managing-director_sales-manager';
			// 	}
			// 	if($directors==1 && $managers==1 && $member==1) {
			// 		$form['type'] = 'managing-director_sales-manager_sales-person';	
			// 	}
			// } else{
			// 	$form['type'] = $filtertype;
			// }
			if(!$userid){
				$form['companyid'] = ADMIN_COMPANYID;
				// unset($form['role_directors']);
				// unset($form['role_managers']);
				// unset($form['role_member']);
			}
			if($rs['isenabled'] && $form['isenabled'] != 1){
				$form['disabledon'] = date("Y-m-d H:i:s");
			}
			if(!$rs['isenabled'] && $form['isenabled'] == 1){
				$form['disabledon'] = 'null';
			}

			// get
			if($filtertype == 'managing-director' && $userid!=null){
				$email_userrs = array();
			} else{
				$email_userrs = (isset($form['email'])) ? $this->user_model->get(null, array('u.type' => $filtertype, 'u.email' => clean_email($form['email'])), null, true) : null;
			}

			if($email_userrs && $email_userrs['userid'] != $userid){
				// merge
				$data['rs'] = array_merge($data['rs'], $form 	);

				// msg
				$this->msg->add('The e-mail is already registered to another user.', 'danger', true);
			}else{
				// create unique dir
				//$form['uploadsdir'] = $this->utils->create_udir($rs['uploadsdir']);

				// update
				if(isset($form['password'])){
					if($form['password']){
						$user_password = $form['password'];
						$form['password'] = get_hash($form['password']);
					}else{
						unset($form['password']);
					}
				}

				// save
				$condition = true;
				if($condition){
					$form['companyid'] = ADMIN_COMPANYID;
					$directors = $this->input->post('role_directors') ? $this->input->post('role_directors') : 0 ;
					$managers = $this->input->post('role_managers') ? $this->input->post('role_managers') : 0 ;
					$member = $this->input->post('role_member') ? $this->input->post('role_member') : 0 ;
					

					if($directors==1){
						$form['type'] = 'managing-director';
						if(!$userid){
							$userid = $this->user_model->save($form, $userid);
						} else{
							$director_user = $this->user_model->get($userid);
							$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
							$director_exists = false;
							if(count($userrs)!=0){
								foreach ($userrs as $user) {
									if($user['type'] == 'managing-director'){
										$director_exists = true;
										$director_id = $user['userid'];
									}
								}
							}
							$form['password'] = $director_user['password'];
							if($director_exists){
								$form['isenabled'] = 1;
								$userid = $this->user_model->save($form,$director_id);
							} else{
								$userid = $this->user_model->save($form);
							}
						}
					}
					if($directors==0){
						$form['type'] = 'managing-director';
						$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
						$manager_exists = false;
						if(count($userrs)!=0){
							foreach ($userrs as $user) {
								if($user['type'] == 'managing-director'){
									$manager_exists = true;
									$manager_id = $user['userid'];
								}
							}
						}
						if($manager_exists){
							$form['isenabled'] = 0;
							$userid = $this->user_model->save($form,$manager_id);
						}
					}

					if($member==1){
						$form['type'] = 'sales-person';
						if(!$userid){
							$userid = $this->user_model->save($form, $userid);
						} else{
							$member_user = $this->user_model->get($userid);
							$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
							$person_exists = false;
							if(count($userrs)!=0){
								foreach ($userrs as $user) {
									if($user['type'] == 'sales-person'){
										$person_exists = true;
										$person_id = $user['userid'];
									}
								}
							}
							$form['password'] = $member_user['password'];
							if($person_exists){
								$form['isenabled'] = 1;
								$userid = $this->user_model->save($form,$person_id);
							} else{
								$userid = $this->user_model->save($form);
							}
						}
					}
					if($member==0){
						$form['type'] = 'sales-person';
						$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
						$person_exists = false;
						if(count($userrs)!=0){
							foreach ($userrs as $user) {
								if($user['type'] == 'sales-person'){
									$person_exists = true;
									$person_id = $user['userid'];
								}
							}
						}
						if($person_exists){
							$form['isenabled'] = 0;
							$userid = $this->user_model->save($form,$person_id);
						}
					}

					if($managers==1) {
						$form['type'] = 'sales-manager';
						if(!$userid){
							$userid = $this->user_model->save($form, $userid);
						} else{
							$manager_user = $this->user_model->get($userid);
							$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
							$manager_exists = false;
							if(count($userrs)!=0){
								foreach ($userrs as $user) {
									if($user['type'] == 'sales-manager'){
										$manager_exists = true;
										$manager_id = $user['userid'];
									}
								}
							}
							$form['password'] = $manager_user['password'];
							if($manager_exists){
								$form['isenabled'] = 1;
								$userid = $this->user_model->save($form,$manager_id);
							} else{
								$userid = $this->user_model->save($form);
							}
						}
					}
					if($managers==0) {
						$form['type'] = 'sales-manager';
						$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
						$manager_exists = false;
						if(count($userrs)!=0){
							foreach ($userrs as $user) {
								if($user['type'] == 'sales-manager'){
									$manager_exists = true;
									$manager_id = $user['userid'];
								}
							}
						}
						if($manager_exists){
							$form['isenabled'] = 0;
							$userid = $this->user_model->save($form,$manager_id);
						}
					}

				} else{
					$form['type'] = $filtertype;
					$userid = $this->user_model->save($form, $userid);
				}

				// change password for all user
				$emails_userrs = (isset($form['email'])) ? $this->user_model->get(null, array( 'u.email' => clean_email($form['email'])), null) : null;
				if(isset($user_password) && $user_password!=null){
					if(count($emails_userrs)>1){
						$new_arr['password'] = get_hash($user_password);
						foreach ($emails_userrs as $email_user) {
							$this->user_model->save($new_arr,$email_user['userid']);
						}
					}
				}

				// check
				if($groups){
					// update
					$this->usercompetencylimit_model->update($groups, $userid, array('where-str' => 'ucl.competencyparameterid IS NULL'), 'usercompetencylimitid');
				}
				if($params){
					// update
					$this->usercompetencylimit_model->update($params, $userid, array('where-str' => 'ucl.competencyparameterid IS NOT NULL'), 'usercompetencylimitid');
				}

				// check - send e-mail
				if(isset($user_password)){
					// get
					$userrs = $this->user_model->get($userid);

					// update
					$userrs['password'] = $user_password;

					// set
					$emarr = array(
							'to-email' => $userrs['email'],
							'subject' => ADMIN_TITLE.' Login Details',
							'msg' => $this->load->view('/_email/user-login-details', array('userrs' => $userrs), true)
						);

					// send
					send_email($emarr);
				}
				// check - send e-mail
				if(!$rs['isenabled'] && $form['isenabled'] == 1){
					// get
					$userrs = $this->user_model->get($userid);

					// set
					$emarr = array(
							'to-email' => $userrs['email'],
							'subject' => ADMIN_TITLE.' Profile Update',
							'msg' => $this->load->view('/_email/user-login-approved', array('userrs' => $userrs), true)
						);

					// send
					send_email($emarr);
				}

				// msg
				$this->msg->add('The user details was successfully saved.', 'success');

				// redirect
				if($myprofile){
					redirect('/admin/users/save/myprofile');
				}else{
					redirect('/admin/users/'.$type);
				}
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($type, $userid)
	{
		// delete
		$rs = $this->user_model->delete($userid);

		// msg
		$this->msg->add('The user details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/users/'.$type);
	}


	// import
	public function import($type='admin', $action=null)
	{
		// set
		$filtertype = array_search($type, $this->usertypearr);
		$data = array('loadview' => 'admin/users/users-import','type' => $type);

		// set
		$default_clmarr = array(
				'title' => 'Title',
				'firstname' => 'Firstname',
				'lastname' => 'Lastname',
				'mobile' => 'Mobile',
				'officeno' => 'Office No',
				'email' => 'E-Mail',
				'password' => 'Password',
				'isenabled' => 'Is Enabled',
				'onbehalfof' => 'Actions On Behalf Of',
			);

		// download template
		if($action == 'downloadtemplate'){
			// set
			$sheetarr = array();
			$sheetname = 'User Details';
			$filename = 'User-Details-Import-Template.xls';

			// set
			$sheetarr[$sheetname][] = $default_clmarr;

			// save
			$this->utils->save_excel($sheetarr, $filename);
		}

		// process
		if($this->input->post('fp')){
			// set
			$error = false;

			// upload
			$filename = upload_file('userfile', IMPORTS_DIR);
			//echo '<pre>'; print_r($filename); echo '</pre>'; exit;

			// read file
			$xlsarr = $this->utils->read_excel(IMPORTS_DIR.$filename);
			//echo '<pre>'; print_r($xlsarr); echo '</pre>';

			// check
			$diffarr = array_diff($xlsarr[1], $default_clmarr);
			//echo '<pre>'; print_r($diffarr); echo '</pre>'; exit;

			// check
			if(count($diffarr)){
				// loop
				foreach($diffarr as $field){
					// msg
					$this->msg->add('Please note the column: '.$field.' is not as per the template document.', 'danger');
				}
			}else{
				// set
				$default_clmarr_keys = array_keys($default_clmarr);
				$default_clm_count = count($default_clmarr_keys);
				$saverowarr = array();

				// loop
				foreach($xlsarr as $i => $row){
					// check
					if($i <= 1){
						continue;
					}

					// set
					$insarr = array_combine($default_clmarr_keys, $row);
					$error = false;

					// check
					$insarr['isenabled'] = ($insarr['isenabled'] === true || $insarr['isenabled'] == 1 || stristr($insarr['isenabled'], 'yes')) ? 1 : 0;
					$insarr['onbehalfof'] = ($insarr['onbehalfof'] === true || $insarr['onbehalfof'] == 1 || stristr($insarr['onbehalfof'], 'yes')) ? 1 : 0;

					// count
					$valid_clm_count = count(array_filter($insarr, 'strlen'));

					// update
					$insarr['companyid'] = ADMIN_COMPANYID;
					$insarr['type'] = $filtertype;
					//$insarr['password'] = get_hash($insarr['password']);

					// check
					/*if($valid_clm_count != $default_clm_count){
						// msg
						$this->msg->add('Line: '.$i.' Skipped - no empty columns allowed.', 'danger');

						continue;
					}*/
					// check
					if(!$insarr['email'] || !is_valid_email($insarr['email'])){
						// msg
						$this->msg->add('Line: '.$i.' Skipped - '.$insarr['email'].' is not a valid e-mail address.', 'danger');

						continue;
					}
					// check
					$email_userrs = (isset($insarr['email'])) ? $this->user_model->get(null, array('u.email' => clean_email($insarr['email'])), null, true) : null;

					// check
					if($email_userrs){
						// msg
						$this->msg->add('Line: '.$i.' Skipped - The e-mail '.$insarr['email'].' is already registered.', 'danger');

						continue;
					}

					// append
					$saverowarr[] = $insarr;
				} // end-foreach
				//echo '<pre>'; print_r($saverowarr); echo '</pre>'; exit;

				// save - loop
				foreach($saverowarr as $row){
					// set
					$user_password = $row['password'];
					$row['password'] = get_hash($row['password']);

					// save
					$userid = $this->user_model->save($row);

					// get
					$userrs = $this->user_model->get($userid);

					// check - send e-mail
					if($user_password){
						// update
						$userrs['password'] = $user_password;

						// set
						$emarr = array(
								'to-email' => $userrs['email'],
								'subject' => ADMIN_TITLE.' Login Details',
								'msg' => $this->load->view('/_email/user-login-details', array('userrs' => $userrs), true)
							);

						// send
						send_email($emarr);
					}
					// check - send e-mail
					if($row['isenabled'] == 1){
						// set
						$emarr = array(
								'to-email' => $userrs['email'],
								'subject' => ADMIN_TITLE.' Profile Update',
								'msg' => $this->load->view('/_email/user-login-approved', array('userrs' => $userrs), true)
							);

						// send
						send_email($emarr);
					}
				} // end-foreach

				// check
				if(count($saverowarr)){
					// msg
					$this->msg->add('The user details was successfully imported.', 'success');
				}
			} // end-if

			// redirect
			redirect('/admin/users/import/'.$type);
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */
