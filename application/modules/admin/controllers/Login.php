<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	// index
	public function index($action='login')
	{
		// set
		define('BODY_CLASS', 'account-bg');
		$data['rs'] = get_empty_field_array('user');

		// post
		if($this->input->post('loginfp')){
			$form = array_filter( array_map('trim', $this->input->post('form')) );

			// merge
			$data['rs'] = array_merge($data['rs'], $form);

			// get
			$userrs = $this->user_model->get(null, array('u.email' => clean_email($form['email'])), null, true);

			// check
			if($userrs){
				// check
				if(!check_hash($form['password'], $userrs['password'])){
					$this->msg->add('Invalid password.', 'danger');
				}else if(!$userrs['isenabled']){
					$this->msg->add('Profile is not enabled.', 'danger');
				}else{
					// session
					$this->session->set_userdata('ADMIN_USERID', $userrs['userid']);

					// admin
					if($userrs['type'] == 'admin'){
						$this->session->set_userdata('ISADMIN', 1);
					}

					// redirect
					redirect('/admin/dashboard');
				}
			}else{
				$this->msg->add('Invalid e-mail address.', 'danger');
			}
		}

		// reset
		if($this->input->post('resetfp')){
			$form = array_filter( array_map('trim', $this->input->post('form')) );

			// get
			$userrs = $this->user_model->get(null, array('u.email' => clean_email($form['email'])), null, true);

			// check
			if($userrs){
				// set
				$userrs['password'] = $pass = generate_password(8);

				// save
				$this->user_model->save(array('password' => get_hash($pass)), $userrs['userid']);

				// send e-mail
				$emarr = array(
						'to-email' => $userrs['email'],
						'subject' => ADMIN_TITLE.' Password Reset',
						'msg' => $this->load->view('/_email/user-password-reset', array('userrs' => $userrs), true)
					);
				send_email($emarr);

				// msg
				$this->msg->add('Your password was reset and the new login details e-mailed to you.', 'success', true);

				// redirect
				redirect('/admin/login');
			}else{
				// msg
				$this->msg->add('Invalid e-mail address.', 'danger', true);

				// set
				$data['emailnotfound'] = 1;
			}
		}


		// load view
		$data['loadview'] = 'admin/login';
		$data['action'] = $action;
		$data['islogin'] = true;
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// forgot password
	public function forgotpassword()
	{
		$this->index('forgotpassword');
	}


	// log out
	public function logout()
	{
		// set
		/*$sessionarr = array('ADMIN_USERRS', 'ISADMIN');
		echo 1; exit;

		// null session
		foreach($sessionarr as $var){
			$this->session->set_userdata($var, null);
		}*/
		$this->session->sess_destroy();

		// msg
		$this->msg->add('You have successfully logged out.', 'success');

		// redirect
		redirect('/admin/login');
	}

}

/* End of file login.php */
/* Location: ./application/modules/admin/controllers/login.php */
