<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companies'] = 'Companies';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->company_model->get();

		// set
		$data = array('loadview' => 'admin/companies/companies-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($companyid=null)
	{
		// check
		$isprofile = ($companyid == 'profile') ? 1 : 0;
		$companyid = ($isprofile) ? ADMIN_COMPANYID : $companyid;
		// get
		$rs = ($companyid) ? $this->company_model->get($companyid) : $this->utils->create_empty_field_array('company');

		// set
		$data = array('loadview' => 'admin/companies/companies-edit', 'rs' => $rs);
		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');
			$form['isenabled'] = (isset($form['isenabled'])) ? 1 : 'null';
			$form['finyearstartmonth'] =  date('Y').'-'.$form['finyearstartmonth'].'-01';
			// save
			$companyid = $this->company_model->save($form, $companyid);

			// check dir
			$this->site->check_company_dir($companyid);

			// msg
			$this->msg->add('The company details was successfully saved.', 'success');

			// redirect
			if($isprofile){
				redirect('/admin/companies/save/profile');
			}else{
				redirect('/admin/companies');
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($companyid)
	{
		// delete
		$rs = $this->company_model->delete($companyid);

		// msg
		$this->msg->add('The company details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/companies');
	}

	// select
	public function select($companyid=null)
	{
		// check
		if($companyid){
			// set
			$this->session->set_userdata('ADMIN_COMPANYID', $companyid);
		}else{
			// unset
			$this->session->set_userdata('ADMIN_COMPANYID', null);
		}

		// redirect
		redirect('admin/companies');
	}
}

/* End of file companies.php */
/* Location: ./application/controllers/companies.php */
