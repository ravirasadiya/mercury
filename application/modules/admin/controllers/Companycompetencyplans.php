<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companycompetencyplans extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companies'] = 'Company Competency Plans';
	}


	// mng
	public function index($type=null,$id=null)
	{
		// get
		if($type==''){
			$rs = $this->companycompetencyplan_model->get(null, array('cut.companyid' => ADMIN_COMPANYID));
			$data = array('loadview' => 'admin/competencies/company-competency-plans-mng', 'rs' => $rs);
		}
		if($type=='content'){
			$companyuserteamids = table_to_array('companyuserteam', 'companyuserteamid', 'companyuserteamid', array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL'), null);
			$companycompetencyplans = $this->companycompetencyplan_model->get(null,array(),array('where-str'=>'cut.companyuserteamid in ('.implode(",",$companyuserteamids).') ',''));
			$rs = array();
			$temp = array();
			foreach ($companycompetencyplans as $companycompetencyplan) {
				$companycompetencyplanid = $companycompetencyplan['companycompetencyplanid'];

				$companycompetencyplanrefs = $this->companycompetencyplanref_model->get(null,array('ccpr.companycompetencyplanid'=>$companycompetencyplanid),array('where-str'=>"ccpr.competencysuggestionid IS NULL AND ccpr.competencyparameterid IS NOT NULL"));

				foreach ($companycompetencyplanrefs as $companycompetencyplanref) {

					$competencygroupid = $companycompetencyplanref['competencygroupid'];
					$competencygroup = $this->competencygroup_model->get($competencygroupid);
					$content['competencygroup_title'] = $competencygroup['title'];

					$competencyparameterid = $companycompetencyplanref['competencyparameterid'];
					$content['companycompetencyplanrefid'] = $companycompetencyplanref['companycompetencyplanrefid'];

					$parameters = $this->competencyparameter_model->get($competencyparameterid);
					$content['competencyparameter_title'] = @$parameters['title'];
					$content['competencyparameterid'] = $competencyparameterid;

					$companycompetencyplanid = $companycompetencyplanref['companycompetencyplanid'];
					$content['companycompetencyplanid'] = $companycompetencyplanid;

					$companycompetencyplan = $this->companycompetencyplan_model->get($companycompetencyplanid);
					$companyuserteamid = $companycompetencyplan['companyuserteamid'];

					$companyuserteam = $this->companyuserteam_model->get($companyuserteamid);
					$content['companyuserteamid'] = $companyuserteam['companyuserteamid'];
					$content['companyuserteam_title'] = $companyuserteam['title'];
					$content['companycompetencyplan_title'] = $companycompetencyplan['title'];

					$content_count = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameterid,'c.islocked'=>'1'),array('where-str'=>'c.createdcompanyid IS NULL AND c.deletedon IS NULL'));

					$content['content_count'] = count($content_count);

					$local_content_count = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameterid,'c.islocked'=>'1'),array('where-str'=>'c.createdcompanyid IS NOT NULL AND c.deletedon IS NULL'));
					$content['local_content_count'] = count($local_content_count);

					// $companycompetencyplanref = $this->companycompetencyplanref_model->get(null,array('ccpr.companygroupparamsuggrefid'=>$companygroupparamsuggrefid));

					// $companyuserteamid = $companycompetencyplanref['companyuserteamid'];
					// $companyuserteam = $this->companyuserteam_model->get($companyuserteamid);
					// $content['companyuserteam_title'] = $parameters['title'];

					$rs[] = array_merge($temp,$content);
				}
				
			}

			// $companycompetencyplanids = $this->companycompetencyplan_model->get(null,array('cut.companyid' => ADMIN_COMPANYID));

			// competencysuggestionid
			
			// $contentrs = $this->content_model->get_company(null);

			// $rs = array();

			// if(count($contentrs)!=0){
			// 	foreach ($contentrs as $content) {
			// 		/* create all data temp array */
			// 		$temp = array();
					
			// 		$competencyparameterid = $content['competencyparameterid'];
			// 		$companygroupparamsuggref = $this->companygroupparamsuggref_model->get(null,array('cgpsr.companyid'=>ADMIN_COMPANYID,'cgpsr.competencyparameterid'=>$competencyparameterid));
			// 		if(!empty($companygroupparamsuggref)){
			// 			$companygroupparamsuggrefid = $companygroupparamsuggref[0]['companygroupparamsuggrefid'];
			// 			$companycompetencyplanref = $this->companycompetencyplanref_model->get(null,array('ccpr.companygroupparamsuggrefid'=>$companygroupparamsuggrefid));
			// 			if(!empty($companycompetencyplanref)){
			// 				$companycompetencyplanid = $companycompetencyplanref[0]['companycompetencyplanid'];
			// 				$companycompetencyplan = $this->companycompetencyplan_model->get($companycompetencyplanid);
			// 				$content['companycompetencyplan_title'] = @$companycompetencyplan['title'];
			// 				$companyuserteam = $this->companyuserteam_model->get($companycompetencyplan['companyuserteamid']);
			// 				$content['companyuserteam_title'] = $companyuserteam['title'];
			// 				$rs[] = array_merge($temp,$content);
			// 			}
			// 		}
			// 	}
			// }
			$data = array('loadview' => 'admin/competencies/company-competency-plans-content-mng', 'rs' => $rs);
		}
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
		// set
		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}

	// save
	public function save($companycompetencyplanid=null,$content_id=null,$type=null,$planid=null)
	{
		/* add time */
		if($companycompetencyplanid=='new'){
			// check
			$isprofile = ($companycompetencyplanid == 'plan') ? 1 : 0;
			$companycompetencyplanid = ($isprofile) ? null : $companycompetencyplanid;
			$companyid = ($isprofile) ? ADMIN_COMPANYID : ADMIN_COMPANYID; // DEBUG redundant
			$action = $this->input->get('action');

			// get
			$rs = (!$isprofile && $companycompetencyplanid) ? $this->companycompetencyplan_model->get($companycompetencyplanid) : $this->utils->create_empty_field_array('companycompetencyplan');
			// check
			/*if($isprofile){
				// get
				$companycompetencyplanrs = $this->companycompetencyplan_model->get(null, array('cut.companyid' => $companyid), null, true);

				$companycompetencyplanid = ($companycompetencyplanrs) ? $companycompetencyplanrs['companycompetencyplanid'] : $companycompetencyplanid;
				$rs = ($companycompetencyplanrs) ? $companycompetencyplanrs : $rs;
			}*/

			// set
			$data = array('loadview' => 'admin/competencies/company-competency-plans-edit', 'rs' => $rs, 'action' => $action);

			// get
			$data['competencygrouprs'] = $this->competencygroup_model->get(null,array(),array('order'=>array('cg.competencygroupid'=>'ASC')));
			$data['competencyparameterrs'] = $this->competencyparameter_model->get(null,array(),array('order'=>array('cp.competencyparameterid'=>'ASC')));
			$data['competencysuggestionrs'] = $this->competencysuggestion_model->get(null,array(),array('order'=>array('cg.competencygroupid'=>'ASC')));
			//$data['group_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL'), 'competencygroupid') : array();
			$data['group_companycompetencyplanrefrs'] = ($action == 'parameters' || $action == 'suggestions') ? $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL')) : array();
			//echo '<pre>'; print_r($data['group_companycompetencyplanrefrs']); echo '</pre>'; exit;
			//$data['parameter_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'), 'competencyparameterid') : array();
			$data['parameter_companycompetencyplanrefrs'] = ($action == 'suggestions') ? $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL')) : array();

			//echo '<pre>'; print_r($data['parameter_companycompetencyplanrefrs']); echo '</pre>'; exit;
			$data['suggestion_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL')) : array();
			$data['compplanrefarr'] = $this->companycompetencyplanref_model->get_keyval_hier_arr($companycompetencyplanid);
			$data['companyuserteamarr'] = $this->companyuserteam_model->get_manager_team_array($companyid);
			$data['group_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_groups($companyid);
			$data['param_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_params($companyid);
			$data['sugg_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_suggs($companyid);
		}

		/* edit time */
		if($companycompetencyplanid!='content' && $companycompetencyplanid!='new'){	
			// check
			$isprofile = ($companycompetencyplanid == 'plan') ? 1 : 0;
			$companycompetencyplanid = ($isprofile) ? null : $companycompetencyplanid;
			$companyid = ($isprofile) ? ADMIN_COMPANYID : ADMIN_COMPANYID; // DEBUG redundant
			$action = $this->input->get('action');

			// get
			$rs = (!$isprofile && $companycompetencyplanid) ? $this->companycompetencyplan_model->get($companycompetencyplanid) : $this->utils->create_empty_field_array('companycompetencyplan');
			// check
			/*if($isprofile){
				// get
				$companycompetencyplanrs = $this->companycompetencyplan_model->get(null, array('cut.companyid' => $companyid), null, true);

				$companycompetencyplanid = ($companycompetencyplanrs) ? $companycompetencyplanrs['companycompetencyplanid'] : $companycompetencyplanid;
				$rs = ($companycompetencyplanrs) ? $companycompetencyplanrs : $rs;
			}*/

			// set
			$data = array('loadview' => 'admin/competencies/company-competency-plans-edit', 'rs' => $rs, 'action' => $action);


			// get
			$data['competencygrouprs'] = $this->competencygroup_model->get(null,array(),array('order'=>array('cg.competencygroupid'=>'ASC')));
			$data['competencyparameterrs'] = $this->competencyparameter_model->get(null,array(),array('order'=>array('cp.competencyparameterid'=>'ASC')));

			$data['competencysuggestionrs'] = $this->competencysuggestion_model->get(null,array(),array('order'=>array('cg.competencygroupid'=>'ASC')));
			//$data['group_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL'), 'competencygroupid') : array();
			$data['group_companycompetencyplanrefrs'] = ($action == 'parameters' || $action == 'suggestions') ? $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL','order'=>array('cg.competencygroupid'=>'ASC'))) : array();
			//echo '<pre>'; print_r($data['group_companycompetencyplanrefrs']); echo '</pre>'; exit;
			//$data['parameter_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'), 'competencyparameterid') : array();
			$data['parameter_companycompetencyplanrefrs'] = ($action == 'suggestions') ? $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL','order'=>array('cg.competencygroupid'=>'ASC'))) : array();
			#echo "<pre>";
			#var_dump($data['parameter_companycompetencyplanrefrs']);exit();

			//echo '<pre>'; print_r($data['parameter_companycompetencyplanrefrs']); echo '</pre>'; exit;
			$data['suggestion_companycompetencyplanrefrs'] = ($companycompetencyplanid) ? $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL','order'=>array('cg.competencygroupid'=>'ASC'))) : array();
			$data['compplanrefarr'] = $this->companycompetencyplanref_model->get_keyval_hier_arr($companycompetencyplanid);
			$data['companyuserteamarr'] = $this->companyuserteam_model->get_manager_team_array($companyid);
			$data['group_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_groups($companyid);
			$data['param_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_params($companyid);
			$data['sugg_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_suggs($companyid);
		}

		// var_dump($data);exit();
		if($companycompetencyplanid=='content'){

			if($type!=''){
				$contentid = ($type!='add')?$type:null;
				if($planid==null){
					$competencyplan = $this->companycompetencyplanref_model->get($content_id);
				} else{
					$competencyplan = $this->companycompetencyplanref_model->get($planid);
				}

				$competencyparameterid = $competencyplan['competencyparameterid'];
				// edit
				if($type=='add'){
					$rs = $this->content_model->get(null,array('c.competencyparameterid'=>$content_id));
				} else if($type!='add'){
					$rs[0] = $this->content_model->get($contentid ,array('c.competencyparameterid'=>$content_id));
				}

				$competencyparameter = $this->competencyparameter_model->get($competencyparameterid);
				$rs_content['competencyparameter_title'] = $competencyparameter['title'];

				$temp = array();
				$competencyparameterid = $competencyparameterid;
				$companygroupparamsuggref = $this->companygroupparamsuggref_model->get(null,array('cgpsr.companyid'=>ADMIN_COMPANYID,'cgpsr.competencyparameterid'=>$competencyparameterid));
				if(!empty($companygroupparamsuggref) && $type!='add'){
					$companygroupparamsuggrefid = $competencyplan['companygroupparamsuggrefid'];
					$companycompetencyplanref = $this->companycompetencyplanref_model->get(null,array('ccpr.companygroupparamsuggrefid'=>$companygroupparamsuggrefid));
					$companycompetencyplanid = $competencyplan['companycompetencyplanid'];
					$companycompetencyplan = $this->companycompetencyplan_model->get($companycompetencyplanid);
					$rs_content['companycompetencyplan_title'] = $companycompetencyplan['title'];
					$companyuserteam = $this->companyuserteam_model->get($companycompetencyplan['companyuserteamid']);
					$rs_content['companyuserteam_title'] = $companyuserteam['title'];
				}
				if($type=='add'){
					$competencyplan = $this->companycompetencyplanref_model->get($content_id);
					$companycompetencyplanid = $competencyplan['companycompetencyplanid'];
					$competencyparameterid = $competencyplan['competencyparameterid'];

					// $companycompetencyplanidnew = $planid;
					// $competencyparameter = $this->competencyparameter_model->get($content_id);
					$competencyparameterid = $competencyparameter['competencyparameterid'];
					$competencygroupid = $competencyparameter['competencygroupid'];
					
					$companycompetencyplan = $this->companycompetencyplan_model->get($companycompetencyplanid);
					$companyuserteamid = $companycompetencyplan['companyuserteamid'];

					$companyuserteam = $this->companyuserteam_model->get($companyuserteamid);
					$rs_content['companyuserteamid'] = $companyuserteam['companyuserteamid'];
					$rs_content['companyuserteam_title'] = $companyuserteam['title'];
					$rs_content['companycompetencyplan_title'] = $companycompetencyplan['title'];
				}
				$data = array('loadview' => 'admin/competencies/company-competency-plans-content-edit-content', 'rs_content' => $rs_content,'rs'=>$rs);
			} else{
				$competencyplan = $this->companycompetencyplanref_model->get($content_id);
				$competencyparameterid = $competencyplan['competencyparameterid'];
				// List content for competency plan

				$content_in_company = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameterid),array('where-str'=>'c.createdcompanyid='.ADMIN_COMPANYID));
				$content_from_admin = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameterid,'c.islocked'=>'1'),array('where-str'=>'c.createdcompanyid IS NULL'));

				$rs = array_merge($content_in_company, $content_from_admin);
				$data = array('loadview' => 'admin/competencies/company-competency-plans-content-edit', 'rs' => $rs);
			}
		}

		//echo '<pre>'; print_r($data['competencygrouprs']); echo '</pre>'; exit;
		//echo '<pre>'; print_r($data['group_companygroupparamsuggrefarr']); echo '</pre>'; exit;

		// process
		if($this->input->post('fp')){

			// post
			$form = $this->input->post('form');
			if($type!='' || $type=='add'){

				$contentid = ($type=='add')?null:$type;

				/* create variable */
				if($planid==null){
					$competencyplan = $this->companycompetencyplanref_model->get($content_id);
				} else{
					$content_id = $planid;
					$competencyplan = $this->companycompetencyplanref_model->get($planid);
				}
				$competencyparameterid = $competencyplan['competencyparameterid'];
				$parametersid = $competencyparameterid;
				$content['competencyparameterid'] = $parametersid;
				$content['title'] = $form['title'];
				$content['filename'] = $form['filename'];
				$content['islocked'] = (isset($form['islocked'])) ? 1 : 0;
				$content['createdcompanyid'] = ADMIN_COMPANYID;
				$content['type'] = 'content';
				/* save content */
				$saved_contentid = $this->content_model->save($content,$contentid);

				/* upload file */
				if(!empty($_FILES['file']['name'])){
					$file = upload_file('file', CONTENT_DIR);
					$contentattachment['contentid'] = $saved_contentid;
					$contentattachment['filelocation'] = $file;
					/* save content attechment */
					$this->contentattachment_model->save($contentattachment,$contentid);
				}

				// msg
				$this->msg->add('The content details was successfully saved.', 'success');

				// redirect
				redirect('/admin/companycompetencyplans/save/content/'.$content_id);

			}

			$groups = $this->input->post('groups');
			$parameters = $this->input->post('parameters');
			$suggestions = $this->input->post('suggestions');
			$redirecturi = $this->input->post('redirecturi');
			$isnew = (!$companycompetencyplanid) ? 1 : 0;

			//echo "$isnew - $redirecturi"; exit;

			//echo '<pre>'; print_r($groups); echo '</pre>'; exit;

			// update
			//$form['companyid'] = $companyid;

			// get
			$rs = $this->companycompetencyplan_model->get(null, array('ccp.companyuserteamid' => $form['companyuserteamid']), null, true);


			// check
			if($rs && $rs['companycompetencyplanid'] != $companycompetencyplanid){
				//
				$this->msg->add('You can only have one plan per team.', 'success');

				// redirect
				redirect('/admin/companycompetencyplans/save/'.$companycompetencyplanid);
			}else{
				// save
				$companycompetencyplanid = ($companycompetencyplanid=='new') ? null : $companycompetencyplanid;
				$companycompetencyplanid = $this->companycompetencyplan_model->save($form, $companycompetencyplanid);

				// update
				if($isnew && $redirecturi){
					// replace
					$redirecturi = str_replace('/save', '/save/'.$companycompetencyplanid, $redirecturi);
				}

				// update
				if($groups){
					// save

					$this->companycompetencyplanref_model->update_groups($groups, $companycompetencyplanid);
				}
				if($parameters){
					// save
					$this->companycompetencyplanref_model->update_parameters($parameters, $companycompetencyplanid);
				}
				if($suggestions){
					// save
					$this->companycompetencyplanref_model->update_suggestions($suggestions, $companycompetencyplanid);
				}
				//exit;

				// update
				//$redirecturi = 'admin/companycompetencyplans/save/'.$companycompetencyplanid;

				// msg
				$this->msg->add('The competency details was successfully saved.', 'success');

				// redirect
				if($redirecturi){
					redirect($redirecturi);
				}else if($isprofile){
					redirect('/admin/companycompetencyplans/save/plan'.($action ? '?action='.$action : ''));
				}else{
					redirect('/admin/companycompetencyplans');
				}
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($companyid,$parametersid=null,$contentid=null,$planid=null)
	{
		if($parametersid!=null&&$contentid!=null){
			// delete
			$rs = $this->content_model->delete($contentid,true);

			// msg
			$this->msg->add('The competency plan content was successfully deleted.', 'success');

			// redirect
			if($planid==null){
				redirect('/admin/companycompetencyplans/save/content/'.$parametersid);
			} else{
				redirect('/admin/companycompetencyplans/save/content/'.$planid);
			}
		} else{
			// delete
			$rs = $this->companycompetencyplan_model->delete($companyid);

			// msg
			$this->msg->add('The company details was successfully deleted.', 'success');

			// redirect
			redirect('/admin/companycompetencyplans');
		}
	}

	// select
	public function select($companyid=null)
	{
		// check
		if($companyid){
			// set
			$this->session->set_userdata('ADMIN_COMPANYID', $companyid);
		}else{
			// unset
			$this->session->set_userdata('ADMIN_COMPANYID', null);
		}

		// redirect
		redirect('admin/companies');
	}

	// Group Drag Add
	public function group_update_ajax_add($group=null,$companycompetencyplanrefid=null,$companycompetencyplanid=null,$sortorder=null)
	{
		$groups[$companycompetencyplanrefid] = $group;
		$id = $this->companycompetencyplanref_model->update_groups_ajax_add($groups, $companycompetencyplanid,$sortorder);
		echo $id;
	}

	// Group Drag Remove
	public function group_update_ajax_delete($companycompetencyplanrefid)
	{
		// remove group parameters first
		$companycompetencyplanrefgroup = $this->companycompetencyplanref_model->get($companycompetencyplanrefid);
		$companycompetencyplanid = $companycompetencyplanrefgroup['companycompetencyplanid'];
		$competencygroupid = $companycompetencyplanrefgroup['competencygroupid'];
		$companycompetencyplanref = $this->companycompetencyplanref_model->get(null,array('ccpr.companycompetencyplanid'=>$companycompetencyplanid,'ccpr.competencygroupid'=>$competencygroupid));
		if(count($companycompetencyplanref)!=0){
			foreach ($companycompetencyplanref as $companycompetencyplan) {
				$this->companycompetencyplanref_model->delete($companycompetencyplan['companycompetencyplanrefid'],true);
			}
		}
		$id = $this->companycompetencyplanref_model->update_groups_ajax_remove($companycompetencyplanrefid);
		return $id;
	}

	// Parameters Drag Add
	public function parameters_update_ajax_add($competencygroupid,$competencyparameterid,$companycompetencyplanid,$sortorder=null)
	{
		$parameters[$competencygroupid] = array('0'=>$competencyparameterid);
		$id = $this->companycompetencyplanref_model->update_parameters_ajax_add($parameters, $companycompetencyplanid,$sortorder);
		echo $id;
	}

	// Suggestion Drag Add
	public function suggestion_update_ajax_add($competencygroupid,$competencyparameterid,$competencysuggestionid,$companycompetencyplanid,$newsuggestioncount=null){
		$suggestion[$competencygroupid] = array($competencyparameterid=>array('-1'=>$competencysuggestionid));
		$id = $this->companycompetencyplanref_model->update_suggestions_ajax_add($suggestion, $companycompetencyplanid,$newsuggestioncount);
		echo $id;
	}
	

	// Parameters Drag Delete
	public function parameters_update_ajax_delete($companycompetencyplanrefid)
	{
		$id = $this->companycompetencyplanref_model->update_parameters_ajax_delete($companycompetencyplanrefid);
		echo $id;
	}

	public function parameters_sort_ajax($companycompetencyplanrefid,$sortorder=null)
	{
		$array = array('sortorder'=>$sortorder);
		$rs = $this->companycompetencyplanref_model->save($array,$companycompetencyplanrefid);
		if($rs){
			echo "success";
		} else{
			echo "failed";
		}
	}

	public function suggestion_sort_ajax($companycompetencyplanrefid,$sortorder=null)
	{
		$array = array('sortorder'=>$sortorder);
		$rs = $this->companycompetencyplanref_model->save($array,$companycompetencyplanrefid);
		if($rs){
			echo "success";
		} else{
			echo "failed";
		}
	}

	public function suggestion_update_ajax_delete($companycompetencyplanrefid){
		$id = $this->companycompetencyplanref_model->update_suggestion_ajax_delete($companycompetencyplanrefid);
		echo $id;
	}
	

}

/* End of file companies.php */
/* Location: ./application/controllers/companies.php */
