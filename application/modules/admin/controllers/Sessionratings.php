<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sessionratings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/sessionratings'] = 'Session Ratings';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->sessionratingitem_model->get();

		// set
		$data = array('loadview' => 'admin/sessionratings/sessionratings-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($sessionratingid=null)
	{
		// get
		$rs = ($sessionratingid) ? $this->sessionratingitem_model->get($sessionratingid) : $this->utils->create_empty_field_array('sessionratingitem');

		// set
		$data = array('loadview' => 'admin/sessionratings/sessionratings-edit', 'rs' => $rs);

		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');

			// save
			$this->sessionratingitem_model->save($form, $sessionratingid);

			// msg
			$this->msg->add('The session rating details was successfully saved.', 'success');

			// redirect
			redirect('/admin/sessionratings');
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($sessionratingid)
	{
		// delete
		$rs = $this->sessionratingitem_model->delete($sessionratingid);

		// msg
		$this->msg->add('The session rating details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/sessionratings');
	}
}

/* End of file sessionratings.php */
/* Location: ./application/controllers/sessionratings.php */
