<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companyuserteams extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companyuserteams'] = 'Company User Teams';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->companyuserteam_model->get(null, array('cut.companyid' => ADMIN_COMPANYID));

		// set
		$data = array('loadview' => 'admin/companyuserteams/companyuserteams-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($companyuserteamid=null)
	{
		// get
		$rs = ($companyuserteamid) ? $this->companyuserteam_model->get($companyuserteamid) : $this->utils->create_empty_field_array('companyuserteam');

		// set
		$data = array('loadview' => 'admin/companyuserteams/companyuserteams-edit', 'rs' => $rs);

		// get dropdowns
		$data['mduserarr'] = table_to_array('user', 'userid', array('firstname', 'lastname'), array('companyid' => ADMIN_COMPANYID, 'type' => 'managing-director', 'deletedon' => 'IS NULL'), null);
		$data['mduserarr'] = ($data['mduserarr']) ? $data['mduserarr'] : array();
		$data['smuserarr'] = table_to_array('user', 'userid', array('firstname', 'lastname'), array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL', 'where-str'=>'type in ("managing-director_sales-manager","managing-director_sales-manager_sales-person","sales-manager" )'), null);
		$data['smuserarr'] = ($data['smuserarr']) ? $data['smuserarr'] : array();
		//$data['spuserarr'] = table_to_array('user', 'userid', array('firstname', 'lastname'), array('companyid' => ADMIN_COMPANYID, 'type' => 'sales-person', 'deletedon' => 'IS NULL'), null);
		$data['spuserrs'] = $this->user_model->get(null, array('u.companyid' => ADMIN_COMPANYID),array('where-str'=>'u.type in ("sales-person","managing-director_sales-person","managing-director_sales-manager_sales-person" )'));
		//$data['spuserarr'] = ($data['spuserarr']) ? $data['spuserarr'] : array();

		// get
		$data['companyuserteamrefarr'] = $this->companyuserteamref_model->get_keyval_arr(array('cutr.companyuserteamid' => $companyuserteamid));
		//echo '<pre>'; print_r($data['companyuserteamrefarr']); echo '</pre>'; exit;

		// process
		if($this->input->post('fp')){
			// post
			$form = array_map('trim', $this->input->post('form'));
			$users = $this->input->post('users');
			//echo '<pre>'; print_r($users); echo '</pre>'; exit;

			// get
			$checkrs = $this->companyuserteam_model->get(null, array('cut.companyid' => ADMIN_COMPANYID, 'cut.title' => $form['title']), null, true);

			// check
			if($checkrs && $checkrs['companyuserteamid'] != $companyuserteamid){
				// merge
				$data['rs'] = array_merge($data['rs'], $form);

				// msg
				$this->msg->add('The company team name is already being used. Please use another name.', 'danger', true);
			}else{
				// check
				if(!$companyuserteamid){
					$form['companyid'] = ADMIN_COMPANYID;
				}

				// check
				if($users){
					// save
					$companyuserteamid = $this->companyuserteam_model->save($form, $companyuserteamid);
				}

				// update
				$this->companyuserteamref_model->update($users, $companyuserteamid);

				// msg
				$this->msg->add('The company team details was successfully saved.', 'success');

				// redirect
				redirect('/admin/companyuserteams');
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($companyuserteamid)
	{
		// delete
		$rs = $this->companyuserteam_model->delete($companyuserteamid);

		// msg
		$this->msg->add('The company team details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/companyuserteams');
	}
}

/* End of file companyuserteams.php */
/* Location: ./application/controllers/companyuserteams.php */
