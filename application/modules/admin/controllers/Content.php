<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/content'] = 'Content';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->content_model->get('',array('type !='=>'content'));

		// set
		$data = array('loadview' => 'admin/content/content-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($contentid=null)
	{
		// get
		$rs = ($contentid) ? $this->content_model->get($contentid) : $this->utils->create_empty_field_array('content');

		// set
		$data = array('loadview' => 'admin/content/content-edit', 'rs' => $rs);

		// process
		if($this->input->post('fp')){
			// post
			$form = array_map('trim', $this->input->post('form'));

			// check
			if($form['videourl'] && $rs['videourl'] != $form['videourl']){
				// get vimeo
				$form['videoembedcode'] = $this->vimeo->get_video($row['videourl']);
			}

			// upload
			$filename = upload_file('filename', CONTENT_DIR);

			// check
			if($filename){
				$form['filename'] = $filename;
			}

			// save
			$this->content_model->save($form, $contentid);

			// msg
			$this->msg->add('The content details was successfully saved.', 'success');

			// redirect
			redirect('/admin/content');
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($contentid)
	{
		// delete
		$rs = $this->content_model->delete($contentid);

		// msg
		$this->msg->add('The content details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/content');
	}


	// delete file
	public function delfile($contentid)
	{
		// set
		$redirect = '/admin/content/save/'.$contentid;

		// set
		$inparr = array();
		$inparr['filename'] = 'null';

		// get
		$rs = $this->content_model->get($contentid);

		// delete
		if($rs['filename'] && file_exists('.'.$rs['filepath'])){
			// delete
			unlink('.'.$rs['filepath']);
		}

		// save
		$this->content_model->save($inparr, $contentid);

		// msg
		$this->msg->add('The file was successfully deleted.', 'success');


		// redirect
		redirect($redirect);
	}

	public function delattechment($parametersid,$contentid,$contentattachmentid)
	{
		$redirect = '/admin/competencies/save/content/'.$parametersid.'/'.$contentid;

		// set
		$this->contentattachment_model->delete($contentattachmentid,true);

		redirect($redirect);	
	}
}

/* End of file content.php */
/* Location: ./application/controllers/content.php */
