<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use NNV\OneSignal\OneSignal;
use NNV\OneSignal\API\Notification;
use ChartJs\ChartJS;

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	public function index()
	{
		// set
		$data = array('loadview' => 'admin/dashboard');

		// defauot
		$data['form'] = array('companyid' => ADMIN_COMPANYID, 'datefrom' => '', 'dateto' => '', 'createdbyuserid' => '', 'datecreated' => '');

		$fromdate = date('2018-01-01');
		$todate = date('2018-12-31');

		//echo datediff('ww', $fromdate, $todate); exit;

		// check
		if(ADMIN_COMPANYID){
			// get
			$data['statsarr'] = $this->report_model->session_stats($data['form']);
			//$data['statsarr'] = $this->report_model->session_competency_stats($data['form']);
			$comp_statsarr = $data['statsarr'][ADMIN_COMPANYID];
			//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

			// overview
			$data['totalsummary_chart'] = chart_overview($comp_statsarr);
			$data['chart_director_percentages'] = chart_director_percentages($comp_statsarr);
			$data['chart_director_sessions'] = chart_director_sessions($comp_statsarr);
			$data['chart_manager_percentages'] = chart_manager_percentages($comp_statsarr);
			$data['chart_manager_sessions'] = chart_manager_sessions($comp_statsarr);
			$data['chart_teammember_percentages'] = chart_teammember_percentages($comp_statsarr);
			$data['chart_teammember_sessions'] = chart_teammember_sessions($comp_statsarr);
		}


		// load view
		$this->load->view('_master-template', array('data' => $data));
	}

}

/* End of file dashboard.php */
/* Location: ./application/modules/admin/controllers/dashboard.php */
