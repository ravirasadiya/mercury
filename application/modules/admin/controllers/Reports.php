<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/reports'] = 'Reports';
	}


	// assessed vs coaching sessions
	public function assessedvscoachingsessions()
	{
		// set
		$data = array('loadview' => 'admin/reports/assessedvscoachingsessions-edit');

		// set
		$data['rs'] = array(
				'fromdate' => date("Y-m-01", strtotime("-3 months")),
				'todate' => date("Y-m-d"),
			);

		// dropdowns
		$where = (ADMIN_COMPANYID) ? array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL') : array('deletedon' => 'IS NULL');
		$data['companyarr'] = table_to_array('company', 'companyid', 'title', $where, array());

		// check
		if($this->input->post('fp')){
			$form = $this->input->post('form');
			$form['companyarr'] = (!isset($form['companyarr'])) ? $data['companyarr'] : $form['companyarr'];

			// get
			$other = array('where-str' => '
					smu.companyid IN ('.implode(', ', $form['companyarr']).') AND
					cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND
					(DATE(cs.date)>="'.$form['fromdate'].'" AND DATE(cs.date)<="'.$form['todate'].'")
				');
			$coachingsessionrs = $this->coachingsession_model->get(null, null, $other);

			// check
			if(!count($coachingsessionrs)){
				// msg
				$this->msg->add('There are no sessions te generate a report with.', 'warning', true);
			}else{
				// encode
				$form = ($form && is_array($form)) ? json_encode($form) : 'null';

				// queue
				$arr = array('params' => $form, 'type' => 'assessed-vs-coaching-sessions', 'action' => 'download-report');
				$actionqueueid = $this->actionqueuelib->add($arr);
				//echo $actionqueueid; exit;

				// process
				$this->actionqueuelib->process($actionqueueid);
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// people coached
	public function peoplecoached()
	{
		// set
		$data = array('loadview' => 'admin/reports/peoplecoached-edit');

		// set
		$data['rs'] = array(
				'fromdate' => date("Y-m-01", strtotime("-3 months")),
				'todate' => date("Y-m-d"),
			);

		// dropdowns
		$where = (ADMIN_COMPANYID) ? array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL') : array('deletedon' => 'IS NULL');
		$data['companyarr'] = table_to_array('company', 'companyid', 'title', $where, array());

		// check
		if($this->input->post('fp')){
			$form = $this->input->post('form');
			$form['companyarr'] = (!isset($form['companyarr'])) ? $data['companyarr'] : $form['companyarr'];
			//echo '<pre>'; print_r($form); echo '</pre>';

			// get
			$other = array('where-str' => '
					smu.companyid IN ('.implode(', ', $form['companyarr']).') AND
					cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND
					(DATE(cs.date)>="'.$form['fromdate'].'" AND DATE(cs.date)<="'.$form['todate'].'")
				');
			$coachingsessionrs = $this->coachingsession_model->get(null, null, $other);
			//echo '<pre>'; print_r($coachingsessionrs); echo '</pre>'; exit;

			// check
			if(!count($coachingsessionrs)){
				// msg
				$this->msg->add('There are no sessions te generate a report with.', 'warning', true);
			}else{
				// encode
				$form = ($form && is_array($form)) ? json_encode($form) : 'null';

				// queue
				$arr = array('params' => $form, 'type' => 'people-coached', 'action' => 'download-report');
				$actionqueueid = $this->actionqueuelib->add($arr);

				// process
				$this->actionqueuelib->process($actionqueueid);
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// coaching session adherence
	public function coachingsessionadherence()
	{
		// set
		$data = array('loadview' => 'admin/reports/coachingsessionadherence-edit');

		// set
		$data['rs'] = array(
				'fromdate' => date("Y-m-01", strtotime("-3 months")),
				'todate' => date("Y-m-d"),
			);

		// dropdowns
		$where = (ADMIN_COMPANYID) ? array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL') : array('deletedon' => 'IS NULL');
		$data['companyarr'] = table_to_array('company', 'companyid', 'title', $where, array());

		// check
		if($this->input->post('fp')){
			$form = $this->input->post('form');
			$form['companyarr'] = (!isset($form['companyarr'])) ? $data['companyarr'] : $form['companyarr'];

			// get
			$other = array('where-str' => '
					smu.companyid IN ('.implode(', ', $form['companyarr']).') AND
					cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND
					(DATE(cs.date)>="'.$form['fromdate'].'" AND DATE(cs.date)<="'.$form['todate'].'")
				');
			$coachingsessionrs = $this->coachingsession_model->get(null, null, $other);

			// check
			if(!count($coachingsessionrs)){
				// msg
				$this->msg->add('There are no sessions te generate a report with.', 'warning', true);
			}else{
				// encode
				$form = ($form && is_array($form)) ? json_encode($form) : 'null';

				// queue
				$arr = array('params' => $form, 'type' => 'coaching-session-adherence', 'action' => 'download-report');
				$actionqueueid = $this->actionqueuelib->add($arr);
				//echo $actionqueueid; exit;

				// process
				$this->actionqueuelib->process($actionqueueid);
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// developments
	public function developments()
	{
		// set
		$data = array('loadview' => 'admin/reports/developments-edit');

		// set
		$data['rs'] = array(
				'fromdate' => date("Y-m-01", strtotime("-3 months")),
				'todate' => date("Y-m-d"),
			);

		// dropdowns
		$where = (ADMIN_COMPANYID) ? array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL') : array('deletedon' => 'IS NULL');
		$data['companyarr'] = table_to_array('company', 'companyid', 'title', $where, array());

		// check
		if($this->input->post('fp')){
			$form = $this->input->post('form');
			$form['companyarr'] = (!isset($form['companyarr'])) ? $data['companyarr'] : $form['companyarr'];

			// get
			$other = array('where-str' => '
					smu.companyid IN ('.implode(', ', $form['companyarr']).') AND
					cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND
					(DATE(cs.date)>="'.$form['fromdate'].'" AND DATE(cs.date)<="'.$form['todate'].'")
				');
			$coachingsessionrs = $this->coachingsession_model->get(null, null, $other);

			// check
			if(!count($coachingsessionrs)){
				// msg
				$this->msg->add('There are no sessions te generate a report with.', 'warning', true);
			}else{
				// encode
				$form = ($form && is_array($form)) ? json_encode($form) : 'null';

				// queue
				$arr = array('params' => $form, 'type' => 'developments', 'action' => 'download-report');
				$actionqueueid = $this->actionqueuelib->add($arr);
				//echo $actionqueueid; exit;

				// process
				$this->actionqueuelib->process($actionqueueid);
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// ratings per competency
	public function ratingspercompetency()
	{
		// set
		$data = array('loadview' => 'admin/reports/ratingspercompetency-edit');

		// set
		$data['rs'] = array(
				'fromdate' => date("Y-m-01", strtotime("-3 months")),
				'todate' => date("Y-m-d"),
			);

		// dropdowns
		$where = (ADMIN_COMPANYID) ? array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL') : array('deletedon' => 'IS NULL');
		$data['companyarr'] = table_to_array('company', 'companyid', 'title', $where, array());

		// check
		if($this->input->post('fp')){
			$form = $this->input->post('form');
			$form['companyarr'] = (!isset($form['companyarr'])) ? $data['companyarr'] : $form['companyarr'];

			// get
			$other = array('where-str' => '
					smu.companyid IN ('.implode(', ', $form['companyarr']).') AND
					cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND
					(DATE(cs.date)>="'.$form['fromdate'].'" AND DATE(cs.date)<="'.$form['todate'].'")
				');
			$coachingsessionrs = $this->coachingsession_model->get(null, null, $other);

			// check
			if(!count($coachingsessionrs)){
				// msg
				$this->msg->add('There are no sessions te generate a report with.', 'warning', true);
			}else{
				// encode
				$form = ($form && is_array($form)) ? json_encode($form) : 'null';

				// queue
				$arr = array('params' => $form, 'type' => 'ratings-per-competency', 'action' => 'download-report');
				$actionqueueid = $this->actionqueuelib->add($arr);
				//echo $actionqueueid; exit;

				// process
				$this->actionqueuelib->process($actionqueueid);
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// team member evaluations
	public function teammemberevaluations()
	{
		// set
		$data = array('loadview' => 'admin/reports/teammemberevaluations-edit');

		// set
		$data['rs'] = array(
				'fromdate' => date("Y-m-01", strtotime("-3 months")),
				'todate' => date("Y-m-d"),
			);

		//$this->actionqueuelib->process(7); exit;

		// dropdowns
		$where = (ADMIN_COMPANYID) ? array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL') : array('deletedon' => 'IS NULL');
		$data['companyarr'] = table_to_array('company', 'companyid', 'title', $where, array());

		// check
		if($this->input->post('fp')){
			$form = $this->input->post('form');
			$form['companyarr'] = (!isset($form['companyarr'])) ? $data['companyarr'] : $form['companyarr'];

			// get
			$other = array('where-str' => '
					smu.companyid IN ('.implode(', ', $form['companyarr']).') AND
					cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND
					(DATE(cs.date)>="'.$form['fromdate'].'" AND DATE(cs.date)<="'.$form['todate'].'")
				');
			$coachingsessionrs = $this->coachingsession_model->get(null, null, $other);

			// check
			if(!count($coachingsessionrs)){
				// msg
				$this->msg->add('There are no sessions te generate a report with.', 'warning', true);
			}else{
				// encode
				$form = ($form && is_array($form)) ? json_encode($form) : 'null';

				// queue
				$arr = array('params' => $form, 'type' => 'team-member-evaluations', 'action' => 'download-report');
				$actionqueueid = $this->actionqueuelib->add($arr);
				//echo $actionqueueid; exit;

				// process
				$this->actionqueuelib->process($actionqueueid);
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// json get report filter selects
	public function json_get_report_filter_selects()
	{
		// set
		$form = $this->input->get('form');
		$retarr = array();

		// set defaults
		$form['companyarr'] = (isset($form['companyarr']) && is_array($form['companyarr'])) ? $form['companyarr'] : array();
		$form['mdarr'] = (isset($form['mdarr']) && is_array($form['mdarr'])) ? $form['mdarr'] : array();
		$form['grouparr'] = (isset($form['grouparr']) && is_array($form['grouparr'])) ? $form['grouparr'] : array();
		$form['smarr'] = (isset($form['smarr']) && is_array($form['smarr'])) ? $form['smarr'] : array();
		$form['teamarr'] = (isset($form['teamarr']) && is_array($form['teamarr'])) ? $form['teamarr'] : array();
		$form['sparr'] = (isset($form['sparr']) && is_array($form['sparr'])) ? $form['sparr'] : array();

		//echo '<pre>'; print_r($form); echo '</pre>';

		// check
		if(!count($form['companyarr'])){
			echo json_encode($retarr); exit;
		}

		// set
		$retarr['formarr'] = $form;

		// directors
		$arr = array();
		$mduseridarr = array();
		foreach($form['companyarr'] as $companyid){
			// company
			$companyrs = $this->company_model->get($companyid);
			// users
			$where = array('u.companyid' => $companyid, 'u.type' => 'managing-director');
			$mduserrs = $this->user_model->get(null, $where);
			$mduserrs = ($mduserrs) ? rs_to_keyval_array($mduserrs, 'userid', 'fullname') : array();

			// append
			$mduseridarr = array_replace($mduseridarr, $mduserrs);

			// check
			if(count($form['mdarr'])){
				$mduseridarr = array_intersect_key($mduseridarr, array_combine($form['mdarr'], $form['mdarr']));
			}

			// check
			if(count($mduserrs)){
				$arr[$companyrs['title']] = $mduserrs;
			}
		}
		$retarr['mduserrs'] = $arr;
		//echo '<pre>'; print_r($mduseridarr); echo '</pre>';
		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		// groups
		$arr = array();
		$groupidarr = array();
		foreach($mduseridarr as $userid => $user){
			$where = array('cug.userid' => $userid);
			$grouprs = $this->companyusergroup_model->get(null, $where);
			$grouprs = ($grouprs) ? rs_to_keyval_array($grouprs, 'companyusergroupid', 'title') : array();

			// append
			$groupidarr = array_replace($groupidarr, $grouprs);

			// check
			if(count($form['grouparr'])){
				$groupidarr = array_intersect_key($groupidarr, array_combine($form['grouparr'], $form['grouparr']));
			}

			// check
			if(count($grouprs)){
				$arr[$user] = $grouprs;
			}
		}
		$retarr['grouprs'] = $arr;
		//echo '<pre>'; print_r($groupidarr); echo '</pre>';
		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		// sales managers
		$arr = array();
		$smuseridarr = array();
		foreach($groupidarr as $groupid => $group){
			// users
			$where = array('sm_cug.companyusergroupid' => $groupid, 'u.type' => 'sales-manager');
			$smuserrs = $this->user_model->get(null, $where);
			$smuserrs = ($smuserrs) ? rs_to_keyval_array($smuserrs, 'userid', 'fullname') : array();

			// append
			$smuseridarr = array_replace($smuseridarr, $smuserrs);

			// check
			if(count($form['smarr'])){
				$smuseridarr = array_intersect_key($smuseridarr, array_combine($form['smarr'], $form['smarr']));
			}

			// check
			if(count($smuserrs)){
				$arr[$group] = $smuserrs;
			}
		}
		$retarr['smuserrs'] = $arr;
		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		// teams
		$arr = array();
		$teamidarr = array();
		foreach($smuseridarr as $userid => $user){
			$where = array('cut.userid' => $userid);
			$teamrs = $this->companyuserteam_model->get(null, $where);
			$teamrs = ($teamrs) ? rs_to_keyval_array($teamrs, 'companyuserteamid', 'title') : array();

			// append
			$teamidarr = array_replace($teamidarr, $teamrs);

			// check
			if(count($form['teamarr'])){
				$teamidarr = array_intersect_key($teamidarr, array_combine($form['teamarr'], $form['teamarr']));
			}

			// check
			if(count($teamrs)){
				$arr[$user] = $teamrs;
			}
		}
		$retarr['teamrs'] = $arr;
		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		// sales persons
		$arr = array();
		$spuseridarr = array();
		foreach($teamidarr as $teamid => $team){
			// users
			$where = array('sp_cut.companyuserteamid' => $teamid, 'u.type' => 'sales-person');
			$spuserrs = $this->user_model->get(null, $where);
			$spuserrs = ($spuserrs) ? rs_to_keyval_array($spuserrs, 'userid', 'fullname') : array();

			// append
			$spuseridarr = array_replace($spuseridarr, $spuserrs);

			// check
			if(count($spuserrs)){
				$arr[$team] = $spuserrs;
			}
		}
		$retarr['spuserrs'] = $arr;
		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		// return
		echo json_encode($retarr);
	}

}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */
