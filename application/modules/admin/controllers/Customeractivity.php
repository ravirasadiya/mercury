<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Customeractivity extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companies'] = 'Activity';
	}

	// mng
	public function index($type=null)
	{
		// get
		$rs = array();
		if($type=='list'){
			$rs = $this->customeractivity_model->get(null);
			$data = array('loadview' => 'admin/customeractivity/customeractivity-list-mng', 'rs' => $rs);
		} elseif($type=='feedback'){
			$data = array('loadview' => 'admin/customeractivity/customeractivity-feedback-mng', 'rs' => $rs);
		}
		$this->load->view('admin/_master-template', array('data' => $data));
	}

	// save
	public function save($type,$customeractivityid=null)
	{
		// get
		$data = array('type' => $type);

		if($type=='list'){
			// get
			$data['rs'] = ($customeractivityid) ? $this->customeractivity_model->get($customeractivityid) : $this->utils->create_empty_field_array('customeractivity');
			// Dropdown
			$data['groups'] = table_to_array('customeractivitygroups', 'customeractivitygroupid', 'name', array('companyid' => ADMIN_COMPANYID, 'status' => 'active', 'deletedon' => 'IS NULL'), null);

			$finyear = array(''=>'Select Year');
			$startYear = 2000;
			$endYear = date('Y');
			for ($i=$endYear; $i >= $startYear; $i--) { 
				$finyear[$i] = $i;
			}
			$data['finyear'] = $finyear;
			$where = array('activityusageid'=>$customeractivityid,'c.userid !='=>'null');
			// $where_team = array('activityusageid'=>$customeractivityid,'c.companyuserteamid !='=>'null');
			$data['rs_users'] = $this->activityallocation_model->get(null, 'user', $where);
			// $data['rs_teams'] = $this->activityallocation_model->get(null, 'team',$where_team);
			$data['customeractivityid'] = $customeractivityid;

			// view
			$data['loadview'] = 'admin/customeractivity/customeractivity-list-edit';
		}
		if($type=='feedback'){
			// get
			$data['rs'] = array();

			// view
			$data['loadview'] = 'admin/customeractivity/customeractivity-feedback-edit';
		}

		// process
		if($this->input->post('fp')){
			// post
			$form = array_map('trim', $this->input->post('form'));
			if($type=='list'){
				$arr['companyid'] = ADMIN_COMPANYID;
				$arr['activityname'] = $form['activityname'];
				$arr['customeractivitygroupid'] = $form['customeractivitygroupid'];
				$arr['activityyear'] = $form['activityyear'];
				$arr['activityfrequency'] = $form['activityfrequency'];
				$arr['activitydescr'] = $form['activitydescr'];
				$arr['activitystatus'] = $form['activitystatus'];
				$arr['defaulttarget'] = $form['defaulttarget'];

				$customeractivityid = $this->customeractivity_model->save($arr,$customeractivityid);

				/* activity usage table insert */
				$company = $this->company_model->get(ADMIN_COMPANYID);
				$time=strtotime($company['finyearstartmonth']);
				$month=date("m",$time);

				$finyearstartdate = $arr['activityyear'].'-'.$month.'-01';
				$finyearenddate = date('Y-m-t',strtotime($finyearstartdate));

				$usage_form['customeractivityid'] = $customeractivityid;
				$usage_form['finyearstartdate'] = $finyearstartdate;
				$usage_form['finyearenddate'] = $finyearenddate;
				$usage_form['frequency'] = $form['activityfrequency'];
				$usage_form['activitystatus'] = $form['activitystatus'];

				if($this->activityusage_model->get(null,array('au.customeractivityid'=>$customeractivityid),null,true)){
					$activityusage = $this->activityusage_model->get(null,array('au.customeractivityid'=>$customeractivityid),null,true);
					$activityusageid = $activityusage['activityusageid'];
				} else{
					$activityusageid = null;
				}
				$activityusageid = $this->activityusage_model->save($usage_form,$activityusageid);

				/* insert activity instance table */
				/*
					#activityinstance_model
					$activityusageid
					startdate 
					enddate
					$form['activitystatus']
				*/
				$instanceids = table_to_array('activityinstance', 'activityinstanceid', 'activityinstanceid', array('activityusageid' => $activityusageid), null);
				if(count($instanceids)!=0){
					foreach ($instanceids as $instanceid) {
						$this->activityinstance_model->delete($instanceid,true);
					}
				}

				$frequency_count = 0;
				$startdate = $finyearstartdate;
				$date = $startdate;
				$frequency_count = 12;
				for ($i=1; $i <= $frequency_count; $i++) { 
					$newstartdate = $form['activityyear'].'-'.str_pad($i,2,"0",STR_PAD_LEFT).'-01';

					if($form['activityfrequency']=='monthly'){
						$newenddate = date('Y-m-t',strtotime($newstartdate));
					} else if($form['activityfrequency']=='every_2_months'){
						$newenddate = date('Y-m-t',strtotime('+1 month',strtotime($newstartdate)));
					} else if($form['activityfrequency']=='every_3_months'){
						$newenddate = date('Y-m-t',strtotime('+2 month',strtotime($newstartdate)));
					} else if($form['activityfrequency']=='every_4_months'){
						$newenddate = date('Y-m-t',strtotime('+3 month',strtotime($newstartdate)));
					} else if($form['activityfrequency']=='every_6_months'){
						$newenddate = date('Y-m-t',strtotime('+5 month',strtotime($newstartdate)));
					} else if($form['activityfrequency']=='once_per_year'){
						$newenddate = date('Y-m-t',strtotime('+11 month',strtotime($newstartdate)));
					}
					
					$instance['activityusageid'] = $activityusageid;
					$instance['adhocfeedback'] = null;
					$instance['feedbacktargetdate'] = null;
					$instance['periodstartdate'] = $newstartdate;
					$instance['periodenddate'] = $newenddate;
					$instance['status'] = null;
					$this->activityinstance_model->save($instance);

					if($form['activityfrequency']=='monthly'){
					} else if($form['activityfrequency']=='every_2_months'){
						$i++;
					} else if($form['activityfrequency']=='every_3_months'){
						$i++;$i++;
					} else if($form['activityfrequency']=='every_4_months'){
						$i++;$i++;$i++;	
					} else if($form['activityfrequency']=='every_6_months'){
						$i++;$i++;$i++;$i++;$i++;
					} else if($form['activityfrequency']=='once_per_year'){
						$i++;$i++;$i++;$i++;$i++;$i++;$i++;$i++;$i++;$i++;$i++;
					}
				}

				// msg
				$this->msg->add('The activity saved successfully.', 'success');

				// view
				redirect('admin/customeractivity/list');
			}
			if($type=='feedback'){
				var_dump($form);exit();	
			}
		}

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}

	public function add($type,$id=null)
	{
		$data['customeractivityid'] = $id;
		if($type=='user')
		{

			$data['userids'] = table_to_array('activityallocation', 'activityallocationid', 'userid', array('activityusageid' => $id,'userid !='=>null), null);

			$data['userids'] = $data['userids'] ? $data['userids'] : array();


			// set
			$where = array('u.isenabled'=>'1');
			// check
			if(ADMIN_COMPANYID){
				$where['u.companyid'] = ADMIN_COMPANYID;
			}
			// get
			$data['rs'] = $this->user_model->get(null, $where,'userid NOT IN ('.implode(",",$data['userids']).')',null);

			// view
			$data['loadview'] = 'admin/customeractivity/customeractivity-user-mng';
		} 
		if($type=='team'){
			// get
			$data['rs'] = $this->companyuserteam_model->get(null, array('cut.companyid' => ADMIN_COMPANYID));

			// view
			$data['loadview'] = 'admin/customeractivity/customeractivity-team-mng';
		}

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}

	// add user
	public function addUser($activityid,$userid)
	{
		if(!empty($activityid) && !empty($userid))
		{
			$arr['activityusageid'] = $activityid;
			$arr['userid'] = $userid;
			if(!$this->activityallocation_model->get(null,'user',array('c.activityusageid'=>$activityid,'c.userid'=>$userid)))
			{
				$this->activityallocation_model->save($arr);
			}
			
			// msg
			$this->msg->add('The user added successfully.', 'success');
		}
		redirect('/admin/customeractivity/add/user/'.$activityid);
	}

	// remove user
	public function removeUser($activityid,$userid)
	{
		if(!empty($activityid) && !empty($userid))
		{
			$arr['activityusageid'] = $activityid;
			$arr['userid'] = $userid;
			if($this->activityallocation_model->get(null,'user',array('c.activityusageid'=>$activityid,'c.userid'=>$userid)))
			{
				$rs = $this->activityallocation_model->get(null,'user',array('c.activityusageid'=>$activityid,'c.userid'=>$userid));
				$this->activityallocation_model->delete($rs[0]['activityallocationid'],true);
			}
			
			// msg
			$this->msg->add('The user removed successfully.', 'success');
		}
		redirect('/admin/customeractivity/save/list/'.$activityid);
	}

	// add team
	public function addTeam($activityid,$teamid)
	{
		if(!empty($activityid) && !empty($teamid))
		{
			$arr['activityusageid'] = $activityid;
			$arr['companyuserteamid'] = $teamid;
			if(!$this->activityallocation_model->get(null,'user',array('c.activityusageid'=>$activityid,'c.companyuserteamid'=>$teamid)))
			{
				$this->activityallocation_model->save($arr);
			}
			// msg
			$this->msg->add('The team added successfully.', 'success');
		}
		redirect('/admin/customeractivity/add/team/'.$activityid);
	}

	// remove team
	public function removeTeam($activityid,$teamid)
	{
		if(!empty($activityid) && !empty($teamid))
		{
			$arr['activityusageid'] = $activityid;
			$arr['companyuserteamid'] = $teamid;
			if($this->activityallocation_model->get(null,'team',array('c.activityusageid'=>$activityid,'c.companyuserteamid'=>$teamid)))
			{
				$rs = $this->activityallocation_model->get(null,'team',array('c.activityusageid'=>$activityid,'c.companyuserteamid'=>$teamid));
				$this->activityallocation_model->delete($rs[0]['activityallocationid'],true);
			}
			// msg
			$this->msg->add('The team added successfully.', 'success');
		}
		redirect('/admin/customeractivity/save/list/'.$activityid);
	}


	public function delete($type,$id)
	{
		if($type=='list')
		{
			// save
			$this->customeractivity_model->delete($id,true);

			// msg
			$this->msg->add('The activity successfully deleted.', 'success');
		}
		if($type=='feedback')
		{

		}
		redirect('/admin/customeractivity/'.$type);
	}
}