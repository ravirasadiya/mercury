<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Customeractivitygroups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companies'] = 'Activity Groups';
	}

	// mng
	public function index()
	{
		// get
		$rs = $this->customeractivitygroups_model->get(null);
		$data = array('loadview' => 'admin/customeractivitygroups/customeractivitygroups-list-mng', 'rs' => $rs);
		
		$this->load->view('admin/_master-template', array('data' => $data));
	}

	// save
	public function save($customeractivitygroupsid=null)
	{
		// get
		$rs = ($customeractivitygroupsid) ? $this->customeractivitygroups_model->get($customeractivitygroupsid) : $this->utils->create_empty_field_array('customeractivitygroups');

		// set
		$data = array('loadview' => 'admin/customeractivitygroups/customeractivitygroups-list-edit', 'rs' => $rs);


		// process
		if($this->input->post('fp')){
			// post
			$form = array_map('trim', $this->input->post('form'));

			$arr['companyid'] = ADMIN_COMPANYID;
			$arr['name'] = $form['name'];
			$arr['description'] = $form['description'];
			$arr['status'] = $form['status'];
			$customeractivitygroupsid = $this->customeractivitygroups_model->save($arr,$customeractivitygroupsid);

			// msg
			$this->msg->add('The activity group saved successfully.', 'success');

			// view
			redirect('admin/customeractivitygroups');
		}

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}

	public function add($type,$id)
	{
		$data['customeractivitygroupsid'] = $id;
		if($type=='user')
		{
			// set
			$where = array();
			// check
			if(ADMIN_COMPANYID){
				$where['u.companyid'] = ADMIN_COMPANYID;
			}
			// get
			$data['rs'] = $this->user_model->get(null, $where);

			// view
			$data['loadview'] = 'admin/Customeractivitygroups/Customeractivitygroups-user-mng';
		} 
		if($type=='team'){
			// get
			$data['rs'] = $this->companyuserteam_model->get(null, array('cut.companyid' => ADMIN_COMPANYID));

			// view
			$data['loadview'] = 'admin/Customeractivitygroups/Customeractivitygroups-team-mng';
		}

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	public function delete($id)
	{
		// save
		$this->customeractivitygroups_model->delete($id,true);

		// msg
		$this->msg->add('The activity group successfully deleted.', 'success');
		redirect('/admin/customeractivitygroups');
	}
}