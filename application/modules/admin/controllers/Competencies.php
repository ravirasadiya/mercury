<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Competencies extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/competencies'] = 'Competencies';
	}


	// mng
	public function index($type)
	{
		// set
		$data = array('type' => $type);

		// competency groups
		if($type == 'groups'){
			// get
			$data['rs'] = $this->competencygroup_model->get();

			// view
			$data['loadview'] = 'admin/competencies/competencygroups-mng';
		}
		// competency parameters
		if($type == 'parameters'){
			// get
			$data['rs'] = $this->competencyparameter_model->get();
			
			// view
			$data['loadview'] = 'admin/competencies/competencyparameters-mng';
		}
		// competency suggestions
		if($type == 'suggestions'){
			// get
			$data['rs'] = $this->competencysuggestion_model->get();

			// view
			$data['loadview'] = 'admin/competencies/competencysuggestions-mng';
		}
		if($type == 'content'){
			// get
			// $data['rs'] = $this->competencyparameter_model->get();
			// $data['rs'] = $this->utils->create_empty_field_array('content');

			// view
			$data['loadview'] = 'admin/competencies/competencyparameterscontent-edit';
			// // get
			// $data['rs'] = array();
			// // var_dump($type);exit();
			// // view
			// $data['loadview'] = 'admin/competencies/competencyparameterscontent-edit';
		}

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($type, $id=null,$contentid=null)
	{
		// set
		$data = array('type' => $type);

		// competency groups
		if($type == 'groups'){
			// get
			$data['rs'] = ($id) ? $this->competencygroup_model->get($id) : $this->utils->create_empty_field_array('competencygroup');
			
			// view
			$data['loadview'] = 'admin/competencies/competencygroups-edit';
		}

		// competency parameters
		if($type == 'parameters'){
			// get next auto incerement id 
			if($id==null){
				$id = $this->competencyparameter_model->get_next_id();
				// $data['rs'] = $this->utils->create_empty_field_array('competencyparameter');
				redirect('admin/competencies/save/parameters/'.$id);
			} else{
				$data['rs'] = $this->competencyparameter_model->get($id);
			}
			
			$data['rs_content']  = ($id) ? $this->content_model->get('',array('competencyparameterid' =>$id,'type'=>'content')) : $this->utils->create_empty_field_array('content');
			// get
			$data['competencygrouparr'] = table_to_array('competencygroup', 'competencygroupid', 'title', array('deletedon' => 'IS NULL'));

			// view
			$data['loadview'] = 'admin/competencies/competencyparameters-edit';
		}

		// competency suggestions
		if($type == 'suggestions'){
			// get
			$data['rs'] = ($id) ? $this->competencysuggestion_model->get($id) : $this->utils->create_empty_field_array('competencysuggestion');
			$data['competencygrouparr'] = table_to_array('competencygroup', 'competencygroupid', 'title', array('deletedon' => 'IS NULL'));
			$data['competencyparameterarr'] = ($id) ? table_to_array('competencyparameter', 'competencyparameterid', 'title', array('competencygroupid' => $data['rs']['competencygroupid'], 'deletedon' => 'IS NULL')) : array('' => 'Select...');

			// view
			$data['loadview'] = 'admin/competencies/competencysuggestions-edit';
		}

		if($type == 'content'){
			$data['rs_content']  = ($contentid) ? $this->content_model->get($contentid,array('competencyparameterid' =>$id,'type'=>'content')) : $this->utils->create_empty_field_array('content');
			$data['rs'] = array("parametersid"=>$id);

			// view
			$data['loadview'] = 'admin/competencies/competencyparameterscontent-edit';
		}

		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');

			// check
			if($type == 'groups'){
				// save
				$this->competencygroup_model->save($form, $id);

				// msg
				$this->msg->add('The competency group details was successfully saved.', 'success');
			}
			if($type == 'parameters'){
				// save
				$this->competencyparameter_model->save($form, $id);

				// msg
				$this->msg->add('The competency paramater details was successfully saved.', 'success');
			}
			if($type == 'suggestions'){
				// save
				$this->competencysuggestion_model->save($form, $id);

				// msg
				$this->msg->add('The development suggestion details was successfully saved.', 'success');
			}
			if($type == 'content'){
				/* create variable */
				$parametersid = $form['parametersid'];
				$content['competencyparameterid'] = $parametersid;
				$content['title'] = $form['title'];
				$content['filename'] = $form['filename'];
				$content['islocked'] = (isset($form['islocked'])) ? 1 : 0;
				$content['type'] = 'content';
				/* save content */
				$saved_contentid = $this->content_model->save($content,$contentid);

				/* upload file */
				if(!empty($_FILES['file']['name'])){
					$file = upload_file('file', CONTENT_DIR);
					$contentattachment['contentid'] = $saved_contentid;
					$contentattachment['filelocation'] = $file;
					/* save content attechment */
					$this->contentattachment_model->save($contentattachment,$contentid);
				}

				// msg
				$this->msg->add('The content details was successfully saved.', 'success');

				// redirect
				redirect('/admin/competencies/save/parameters/'.$parametersid.'?type=content');

			}
			// redirect
			redirect('/admin/competencies/'.$type);
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($type, $id, $contentid=null,$planid=null)
	{
		// check
		if($type == 'groups'){
			// save
			$this->competencygroup_model->delete($id);

			// msg
			$this->msg->add('The competency group details was successfully deleted.', 'success');
		}
		if($type == 'parameters'){
			// save
			$this->competencyparameter_model->delete($id);

			// msg
			$this->msg->add('The competency paramater details was successfully deleted.', 'success');
		}
		if($type == 'suggestions'){
			// save
			$this->competencysuggestion_model->delete($id);

			// msg
			$this->msg->add('The development suggestion details was successfully deleted.', 'success');
		}
		if($type == 'content'){
			$rs = $this->content_model->get($contentid);
			if(count($rs)!=0){
				// $rs = $this->content_model->delete($contentid,true);
				// foreach ($rs as $key => $value) {
				// 	if($rs['filelocation'] && file_exists('.'.$rs['filelocation'])){
				// 		// delete
				// 		unlink('.'.$rs['filelocation']);
				// 	}
				// }
			}
			$this->content_model->delete($contentid);
			if($planid==null){
				redirect('/admin/competencies/save/parameters/'.$id.'?type=content');
			} else{
				redirect('/admin/competencies/save/parameters/'.$planid);
			}
		}

		// redirect
		redirect('/admin/competencies/'.$type);
	}


	// json get competency parameters
	public function json_get_competency_parameters($competencygroupid)
	{
		// set
		$retarr = array();

		// get
		$retarr['rs'] = table_to_array('competencyparameter', 'competencyparameterid', 'title', array('competencygroupid' => $competencygroupid, 'deletedon' => 'IS NULL'), array());

		// echo
		echo json_encode($retarr);
	}
}

/* End of file competencies.php */
/* Location: ./application/controllers/competencies.php */
