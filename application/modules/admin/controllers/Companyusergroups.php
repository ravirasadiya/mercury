<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Companyusergroups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/companyusergroups'] = 'Company User Groups';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->companyusergroup_model->get(null, array('cug.companyid' => ADMIN_COMPANYID));

		// set
		$data = array('loadview' => 'admin/companyusergroups/companyusergroups-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($companyusergroupid=null)
	{
		// get
		$rs = ($companyusergroupid) ? $this->companyusergroup_model->get($companyusergroupid) : $this->utils->create_empty_field_array('companyusergroup');

		// set
		$data = array('loadview' => 'admin/companyusergroups/companyusergroups-edit', 'rs' => $rs);

		// get dropdowns
		$data['mduserarr'] = table_to_array('user', 'userid', array('firstname', 'lastname'), array('companyid' => ADMIN_COMPANYID, 'where-str' => 'type in ("managing-director","managing-director_sales-person","managing-director_sales-manager","managing-director_sales-manager_sales-person")', 'deletedon' => 'IS NULL'), null);
		$data['mduserarr'] = ($data['mduserarr']) ? $data['mduserarr'] : array();
		$data['smuserarr'] = table_to_array('user', 'userid', array('firstname', 'lastname'), array('companyid' => ADMIN_COMPANYID, 'type' => 'sales-manager', 'deletedon' => 'IS NULL'), null);
		$data['smuserarr'] = ($data['smuserarr']) ? $data['smuserarr'] : array();
		$data['smuserrs'] = $this->user_model->get(null, array('u.companyid' => ADMIN_COMPANYID),array('where-str'=>'u.type in ("managing-director_sales-manager","managing-director_sales-manager_sales-person","sales-manager")'));
		$data['companyuserteamarr'] = table_to_array('companyuserteam', 'companyuserteamid', 'title', array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL'), null);

		// get
		$data['companyusergrouprefarr'] = $this->companyusergroupref_model->get_keyval_arr(array('cugr.companyusergroupid' => $companyusergroupid));
		//echo '<pre>'; print_r($data['companyusergrouprefarr']); echo '</pre>'; exit;

		// process
		if($this->input->post('fp')){
			// post
			$form = array_map('trim', $this->input->post('form'));
			$users = $this->input->post('users');
			//echo '<pre>'; print_r($users); echo '</pre>'; exit;

			// get
			$checkrs = $this->companyusergroup_model->get(null, array('cug.companyid' => ADMIN_COMPANYID, 'cug.title' => $form['title']), null, true);

			// check
			if($checkrs && $checkrs['companyusergroupid'] != $companyusergroupid){
				// merge
				$data['rs'] = array_merge($data['rs'], $form);

				// msg
				$this->msg->add('The company group name is already being used. Please use another name.', 'danger', true);
			}else{
				// check
				if(!$companyusergroupid){
					$form['companyid'] = ADMIN_COMPANYID;
				}

				// check
				if($users){
					// save
					$companyusergroupid = $this->companyusergroup_model->save($form, $companyusergroupid);
				}

				// update
				$this->companyusergroupref_model->update($users, $companyusergroupid);

				// msg
				$this->msg->add('The company group details was successfully saved.', 'success');

				// redirect
				redirect('/admin/companyusergroups');
			}
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($companyusergroupid)
	{
		// delete
		$rs = $this->companyusergroup_model->delete($companyusergroupid);

		// msg
		$this->msg->add('The company group details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/companyusergroups');
	}
}

/* End of file companyusergroups.php */
/* Location: ./application/controllers/companyusergroups.php */
