<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set
		$this->site->breadcrumbarr['/admin/notifications'] = 'Notifications';
	}


	// mng
	public function index()
	{
		// get
		$rs = $this->notification_model->get(null, array('cut.companyid' => ADMIN_COMPANYID));

		// set
		$data = array('loadview' => 'admin/notifications/notifications-mng', 'rs' => $rs);

		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($notificationid=null)
	{
		// get
		$rs = ($notificationid) ? $this->notification_model->get($notificationid) : $this->utils->create_empty_field_array('notification');

		// set
		$data = array('loadview' => 'admin/notifications/notifications-edit', 'rs' => $rs);

		// dropdowns
		$data['companyuserteamarr'] = table_to_array('companyuserteam', 'companyuserteamid', 'title', array('companyid' => ADMIN_COMPANYID, 'deletedon' => 'IS NULL'), array());

		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');

			// save
			$this->notification_model->save($form, $notificationid);

			// msg
			$this->msg->add('The notification details was successfully saved.', 'success');

			// redirect
			redirect('/admin/notifications');
		}


		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($notificationid)
	{
		// delete
		$rs = $this->notification_model->delete($notificationid);

		// msg
		$this->msg->add('The notification details was successfully deleted.', 'success');

		// redirect
		redirect('/admin/notifications');
	}
}

/* End of file notifications.php */
/* Location: ./application/controllers/notifications.php */
