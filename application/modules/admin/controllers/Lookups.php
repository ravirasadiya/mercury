<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lookups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// set
		$this->site->breadcrumbarr['/admin/dashboard'] = 'Lookups';
	}

	
	// index
	public function index($type=null)
	{
		// get
		$rs = $this->lookup_model->get(null, array('l.type' => $type));
		
		// set
		$data = array('loadview' => 'admin/lookups/lookups-mng', 'type' => $type, 'rs' => $rs);

		
		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// save
	public function save($type=null, $lookupid=null)
	{	
		// get
		$rs = ($lookupid) ? $this->lookup_model->get($lookupid) : $this->utils->create_empty_field_array('lookup');
		
		// set
		$data = array('loadview' => 'admin/lookups/lookups-edit', 'type' => $type, 'rs' => $rs);
		
		// process
		if($this->input->post('fp')){
			// post
			$form = $this->input->post('form');
			
			// check
			if(!$lookupid){
				$form['type'] = $type;
			}
			
			// get
			$this->lookup_model->save($form, $lookupid);
			
			// msg
			$this->msg->add('The lookup details was successfully saved.', 'success');
			
			// redirect
			redirect('/admin/lookups/'.$type);
		}

		
		// load
		$this->load->view('admin/_master-template', array('data' => $data));
	}


	// delete
	public function delete($type, $lookupid)
	{
		// delete
		$rs = $this->lookup_model->delete($lookupid);
		
		// msg
		$this->msg->add('The lookup details was successfully deleted.', 'success');
		
		// redirect
		redirect('/admin/lookups/'.$type);
	}
}

/* End of file lookups.php */
/* Location: ./application/controllers/lookups.php */