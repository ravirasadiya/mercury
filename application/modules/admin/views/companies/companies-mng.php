

	<header class="page-header">
		<h2>Manage News</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/companies/save" class="btn btn-primary btn-sm pull-right">+ Add Company</a>
				Manage Companies
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">E-Mail</th>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">Tel</th>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">Contact Person</th>
					<th class="width-100" data-filterable="true" data-sortable="true">Eabled</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Select</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$companyid = $row['companyid'];

					$seluri = ($companyid == ADMIN_COMPANYID) ? '/admin/companies/select/' : '/admin/companies/select/'.$companyid;
					$edituri = '/admin/companies/save/'.$companyid;
					$deluri = '/admin/companies/delete/'.$companyid;
				?>
				<tr>
					<td><?=$row['title'];?></td>
					<td><?=$row['email'];?></td>
					<td><?=$row['tel'];?></td>
					<td><?=$row['contactperson'];?></td>
					<td class="text-center"><?=$row['enabled'];?></td>
					<td class="text-center">
						<?if($companyid == ADMIN_COMPANYID){?>
							<a href="<?=$seluri;?>" class="btn btn-primary btn-xs">De-Select</a>
						<?}else{?>
							<a href="<?=$seluri;?>" class="btn btn-default btn-xs">Select</a>
						<?}?>
					</td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
