

	<header class="page-header">
		<h2>Add/Update Company Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">Title:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">E-Mail:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tel:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[tel]', 'value' => $rs['tel'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Contact Person:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[contactperson]', 'value' => $rs['contactperson'], 'class' => 'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Contact Person E-Mail:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[contactpersonemail]', 'value' => $rs['contactpersonemail'], 'class' => 'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Contact Person Tel:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[contactpersontel]', 'value' => $rs['contactpersontel'], 'class' => 'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Fin Year Start Month:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[finyearstartmonth]', config_item('months'), substr($rs['finyearstartmonth'],5,2), 'class="form-control validate[required]"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Enabled:</label>
									<div class="col-sm-8 col-md-4">
										<label class="checkbox-inline">
											<?=form_checkbox(array('name' => 'form[isenabled]', 'value' => 1, 'checked' => (!$rs['companyid'] || $rs['isenabled'] ? true : false)));?>
											Yes
										</label>
									</div>
								</div>


							</div>

						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
