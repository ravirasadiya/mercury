

	<header class="page-header">
		<h2>Ratings Per Competency Report</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'reportform', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">From Date:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[fromdate]', 'value' => $rs['fromdate'], 'class' => 'form-control is-date'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">To Date:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[todate]', 'value' => $rs['todate'], 'class' => 'form-control is-date'));?>
									</div>
								</div>
								<!--<div class="form-group">
									<label class="col-sm-2 control-label">Frequency:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[frequency]', config_item('reports-freq-arr'), '', 'class="form-control"');?>
									</div>
								</div>-->
								<div class="form-group">
									<label class="col-sm-2 control-label">Company:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[companyarr][]', $companyarr, array(ADMIN_COMPANYID), 'multiple="multiple" class="form-control trigger-onload-change" '.config_item('multi-sel-default-settings'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Directors:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[mdarr][]', array(), '', 'multiple="multiple" class="form-control" '.config_item('multi-sel-default-settings'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Groups:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[grouparr][]', array(), '', 'multiple="multiple" class="form-control" '.config_item('multi-sel-default-settings'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Managers:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[smarr][]', array(), '', 'multiple="multiple" class="form-control" '.config_item('multi-sel-default-settings'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Teams:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[teamarr][]', array(), '', 'multiple="multiple" class="form-control" '.config_item('multi-sel-default-settings'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Team Members:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[sparr][]', array(), '', 'multiple="multiple" class="form-control" '.config_item('multi-sel-default-settings'));?>
									</div>
								</div>

							</div>

						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
