
	<header class="page-header">
		<h2>Add/Update User Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<h2>User Details</h2>

							<hr />

						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-sm-4 control-label">Title:</label>
									<div class="col-sm-6">
										<?=form_dropdown('form[title]', config_item('user-title-arr'), $rs['title'], 'class="form-control"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Firstname:</label>
									<div class="col-sm-6">
										<?=form_input(array('name' => 'form[firstname]', 'value' => $rs['firstname'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Lastname:</label>
									<div class="col-sm-6">
										<?=form_input(array('name' => 'form[lastname]', 'value' => $rs['lastname'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" >E-Mail:</label>
									<div class="col-sm-6">
										<?
											$disabled_email = false;
											if($rs['userid']){
												$disabled_email = true;
											}
										?>
										<?php if($disabled_email): ?>
											<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required,custom[email]]','readonly'=>'true'));?>
										<?php else: ?>
											<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required,custom[email]]'));?>
										<?php endif; ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Mobile:</label>
									<div class="col-sm-6">
										<?=form_input(array('name' => 'form[mobile]', 'value' => $rs['mobile'], 'class' => 'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Office No.:</label>
									<div class="col-sm-6">
										<?=form_input(array('name' => 'form[officeno]', 'value' => $rs['officeno'], 'class' => 'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Enabled:</label>
									<div class="col-sm-6">
										<label class="checkbox-inline">
											<?=form_checkbox(array('name' => 'form[isenabled]', 'value' => 1, 'checked' => (!$rs['userid'] || $rs['isenabled'] ? true : false)));?>
											Yes
										</label>
									</div>
								</div>
								<?php if($this->uri->segment(4)=="managers" || $rs['type']=='sales-manager'): ?>
									<div class="form-group">
										<label class="col-sm-4 control-label">Actions On Behalf Of:</label>
										<div class="col-sm-6">
											<label class="checkbox-inline">
												<?=form_checkbox(array('name' => 'form[onbehalfof]', 'value' => 1, 'checked' => (!$rs['userid'] || @$rs['onbehalfof'] ? true : false)));?>
												Yes
											</label>
										</div>
									</div>
								<?php endif; ?>
							</div>

							<div class="col-md-6">

								<?if($rs['userid']){?>
									<div class="form-group">
										<label class="col-sm-4 control-label">Update Password:</label>
										<div class="col-sm-6">
											<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control', 'id' => 'password'));?>
										</div>
									</div>
								<?}else{?>
									<div class="form-group">
										<label class="col-sm-4 control-label">Password:</label>
										<div class="col-sm-6">
											<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control validate[required]', 'id' => 'password'));?>
										</div>
									</div>
								<?}?>
								<div class="form-group">
									<label class="col-sm-4 control-label">Password (again):</label>
									<div class="col-sm-6">
										<?=form_password(array('name' => 'password', 'value' => '', 'class' => 'form-control validate[equals[password]]'));?>
										<?if($rs['userid']){?>
											<small>* Leave the password fields blank to keep it unchanged.</small>
										<?}?>
									</div>
								</div>
										<?php 
											$userrs = $this->user_model->get(null,array('u.email'=>$rs['email']));
											$type_array = array();
											if(count($userrs)!=0){
												foreach ($userrs as $user) {
													if($user['isenabled']==1){
														array_push($type_array,$user['type']);
													}
												}
											}
											$role_directors = in_array('managing-director', $type_array);
											$role_managers = in_array('sales-manager', $type_array);
											$role_member = in_array('sales-person', $type_array);
											$isnew = $this->uri->segment(5);

											if($type=='directors' && empty($isnew)){
												$role_directors = true;
											}
											if($type=='managers' && empty($isnew)){
												$role_managers = true;
											}
											if($type=='teammembers' && empty($isnew)){
												$role_member = true;
											}
										?>
										<div class="row">
											<div class="col-lg-10">
												<div class="roles-box">
													<div class="row">
														<div class="col-lg-12">
															<label class="col-sm-11 col-sm-offset-1 roles-title">Roles</label>

															<label class="col-sm-5 control-label">Director</label>
															<div class="col-sm-5">
																<label class="checkbox-inline" <?php if($type=='directors' && $role_directors): ?> style="pointer-events: none;" <?php endif; ?>>
																	<?=form_checkbox(array('name' => 'role_directors', 'value' => 1, 'checked' => $role_directors ? true : false));?>
																	<?php if($role_directors==true): ?>
																		Yes
																	<?php else: ?>
																		No
																	<?php endif; ?>
																</label>
															</div>

															<label class="col-sm-5 control-label">Manager</label>
															<div class="col-sm-5">
																<label class="checkbox-inline" <?php if($type=='managers' && $role_managers): ?> style="pointer-events: none;" <?php endif; ?>>
																	<?=form_checkbox(array('name' => 'role_managers', 'value' => 1, 'checked' => $role_managers ? true : false));?>
																	<?php if($role_managers==true): ?>
																		Yes
																	<?php else: ?>
																		No
																	<?php endif; ?>
																</label>
															</div>

															<label class="col-sm-5 control-label">Team Member</label>
															<div class="col-sm-5">
																<label class="checkbox-inline" <?php if($type=='teammembers' && $role_member): ?> style="pointer-events: none;" <?php endif; ?>>
																	<?=form_checkbox(array('name' => 'role_member', 'value' => 1, 'checked' => $role_member ? true : false));?>
																	<?php if($role_member==true): ?>
																		Yes
																	<?php else: ?>
																		No
																	<?php endif; ?>
																</label>
															</div>

															<label class="col-sm-12"></label>

														</div>
													</div>
												</div>
											</div>
										</div>
									<!-- </div> -->
							</div>
						</div> <!-- /row -->

						<?if($type == 'teammembers'){?>

								<p>&nbsp;</p>

							<h2>Choose Session Frequency Limits</h2>

								<hr />

							<div class="form-group">
								<label class="col-sm-2 control-label">Number Of Sessions:</label>
								<div class="col-sm-3">
									<?=form_input(array('name' => 'form[numsessions]', 'value' => $rs['numsessions'], 'class' => 'form-control validate[required]'));?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Session Frequency:</label>
								<div class="col-sm-3">
									<?=form_dropdown('form[sessionfrequency]', config_item('session-freq-arr'), $rs['sessionfrequency'], 'class="form-control validate[required]"');?>
								</div>
							</div>

						<?}?>

						<?if($type == 'managers' || $type == 'teammembers'){?>

								<p>&nbsp;</p>

							<h2>Choose Competency Limits</h2>

								<hr />

							<?
								foreach($group_companycompetencyplanrefrs as $grow){
									$competencygroupid = $grow['competencygroupid'];

									$val = (isset($group_usercompetencylimitarr[$competencygroupid])) ? $group_usercompetencylimitarr[$competencygroupid] : 50;
							?>

							<div class="well well-sm">
								<div class="row">
									<div class="col-sm-8 col-md-4">

									</div>
									<div class="col-sm-4 col-md-2">
										<p class="fnt-b">Limit</p>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-8 col-md-4">
										<p class="form-control-static fnt-b fnt-s-16"><?=$grow['group'];?></p>
									</div>
									<div class="col-sm-4 col-md-2">
										<?=form_input(array('name' => 'groups['.$competencygroupid.']', 'value' => $val, 'class' => 'form-control'));?>
									</div>
								</div>

								<?
									// loop
									foreach($parameter_companycompetencyplanrefrs as $prow){
										$competencyparameterid = $prow['competencyparameterid'];

										// check
										if($prow['competencygroupid'] != $competencygroupid){
											continue;
										}

										$val = (isset($param_usercompetencylimitarr[$competencyparameterid])) ? $param_usercompetencylimitarr[$competencyparameterid] : 50;
								?>
									<div class="row">
										<div class="col-sm-8 col-md-4">
											<p class="form-control-static"><?=$prow['parameter'];?></p>
										</div>
										<div class="col-sm-4 col-md-2">
											<?=form_input(array('name' => 'params['.$competencygroupid.'|'.$competencyparameterid.']', 'value' => $val, 'class' => 'form-control'));?>
										</div>
									</div>
								<?}?>
								</div>
							<?}?>

						<?}?>

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->



<?if(false){?>
	<div class="content-header">
		<h2 class="content-header-title"></h2>


	</div> <!-- /.content-header -->

	<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
	<?=form_hidden('fp', 1);?>

		<h3>Details</h3>

		<div class="form-group">
			<label class="col-sm-2 control-label">Firstname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[firstname]', 'value' => $rs['firstname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Lastname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[lastname]', 'value' => $rs['lastname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">E-Mail:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<!--<div class="form-group">
			<label class="col-sm-2 control-label">Language:</label>
			<div class="col-sm-4">
				<?=form_dropdown('form[languageid]', $languagearr, $rs['languageid'], 'class="form-control validate[required]"');?>
			</div>
		</div>-->
		<div class="form-group">
			<label class="col-sm-2 control-label">User Roles</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[isadmin]', 'value' => 1, 'checked' => ($rs['isadmin'] ? true : false)));?>
					Is Admin
				</label><br />
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[issupervisor]', 'value' => 1, 'checked' => ($rs['issupervisor'] ? true : false)));?>
					Is Supervisor
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Enabled:</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[enabled]', 'value' => 1, 'checked' => ($rs['enabled'] ? true : false)));?>
					Yes
				</label>
			</div>
		</div>

			<p>&nbsp;</p>

		<?if($rs['userid']){?>
			<h3>Update Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control', 'id' => 'password'));?>
				</div>
			</div>
		<?}else{?>
			<h3>Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control validate[required]', 'id' => 'password'));?>
				</div>
			</div>
		<?}?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Password (again):</label>
			<div class="col-sm-4">
				<?=form_password(array('name' => 'password', 'value' => '', 'class' => 'form-control validate[equals[password]]'));?>
			</div>
		</div>

			<p>&nbsp;</p>

			<hr />

		<div class="row">
			<div class="col-md-3">
				<a href="/admin/users" class="btn btn-block btn-default">Cancel</a>
			</div>
			<div class="col-md-6"></div>
			<div class="col-md-3">
				<button type="submit" class="btn btn-block btn-primary">Submit</button>
			</div>
		</div>

	<?=form_close();?>



	<div class="content-header">
		<h2 class="content-header-title">Add/Update User Details</h2>

		<?=$this->site->display_breadcrumbs();?>
	</div> <!-- /.content-header -->

	<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
	<?=form_hidden('fp', 1);?>

		<h3>Details</h3>

		<div class="form-group">
			<label class="col-sm-2 control-label">Firstname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[firstname]', 'value' => $rs['firstname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Lastname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[lastname]', 'value' => $rs['lastname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">E-Mail:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<!--<div class="form-group">
			<label class="col-sm-2 control-label">Language:</label>
			<div class="col-sm-4">
				<?=form_dropdown('form[languageid]', $languagearr, $rs['languageid'], 'class="form-control validate[required]"');?>
			</div>
		</div>-->
		<div class="form-group">
			<label class="col-sm-2 control-label">User Roles</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[isadmin]', 'value' => 1, 'checked' => ($rs['isadmin'] ? true : false)));?>
					Is Admin
				</label><br />
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[issupervisor]', 'value' => 1, 'checked' => ($rs['issupervisor'] ? true : false)));?>
					Is Supervisor
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Enabled:</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[enabled]', 'value' => 1, 'checked' => ($rs['enabled'] ? true : false)));?>
					Yes
				</label>
			</div>
		</div>

			<p>&nbsp;</p>

		<?if($rs['userid']){?>
			<h3>Update Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control', 'id' => 'password'));?>
				</div>
			</div>
		<?}else{?>
			<h3>Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control validate[required]', 'id' => 'password'));?>
				</div>
			</div>
		<?}?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Password (again):</label>
			<div class="col-sm-4">
				<?=form_password(array('name' => 'password', 'value' => '', 'class' => 'form-control validate[equals[password]]'));?>
			</div>
		</div>

			<p>&nbsp;</p>

			<hr />

		<div class="row">
			<div class="col-md-3">
				<a href="/admin/users" class="btn btn-block btn-default">Cancel</a>
			</div>
			<div class="col-md-6"></div>
			<div class="col-md-3">
				<button type="submit" class="btn btn-block btn-primary">Submit</button>
			</div>
		</div>

	<?=form_close();?>

<?}?>
