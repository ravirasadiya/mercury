

	<header class="page-header">
		<h2>Manage Users</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<div class="pull-right">
					<a href="/admin/users/import/<?=$type;?>" class="btn btn-warning btn-sm">Import Users</a>
					<a href="/admin/users/save/<?=$type;?>" class="btn btn-primary btn-sm">+ Add User</a>
				</div>
				Manage Users
			</h2>
		</header>
		<div class="panel-body">

			<?if(in_array($type, array('admin', 'directors'))){?>
			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Name</th>
					<th class="width-250" data-filterable="true" data-sortable="true">E-Mail</th>
					<th class="width-250" data-filterable="true" data-sortable="true">Office No.</th>
					<th class="width-100" data-filterable="true" data-sortable="true">Enabled</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$userid = $row['userid'];

					$edituri = '/admin/users/save/'.$type.'/'.$userid;
					$deluri = '/admin/users/delete/'.$type.'/'.$userid;
				?>
				<tr>
					<td><?=$row['fullname'];?></td>
					<td><?=$row['email'];?></td>
					<td><?=$row['officeno'];?></td>
					<td class="text-center"><?=$row['enabled'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<!-- <a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a> -->
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>
			<?}?>

			<?if(in_array($type, array('managers'))){?>
			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Director</th>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Group</th>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Name</th>
					<th class="width-250" data-filterable="true" data-sortable="true">E-Mail</th>
					<th class="width-250" data-filterable="true" data-sortable="true">Office No.</th>
					<th class="width-100" data-filterable="true" data-sortable="true">Enabled</th>
					<th class="width-100" data-filterable="true" data-sortable="true">Actions ObO</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$userid = $row['userid'];

					$edituri = '/admin/users/save/'.$type.'/'.$userid;
					$deluri = '/admin/users/delete/'.$type.'/'.$userid;
				?>
				<tr>
					<td><?=$row['director'];?></td>
					<td><?=$row['smgroup'];?></td>
					<td><?=$row['fullname'];?></td>
					<td><?=$row['email'];?></td>
					<td><?=$row['officeno'];?></td>
					<td class="text-center"><?=($row['isenabled'] ? 'Yes' : 'No');?></td>
					<td class="text-center"><?=$row['onbehalfed'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>
			<?}?>

			<?if(in_array($type, array('teammembers'))){?>
			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Sales Manager</th>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Team</th>
					<th data-filterable="true" data-sortable="true" data-direction="asc">Name</th>
					<th class="width-250" data-filterable="true" data-sortable="true">E-Mail</th>
					<th class="width-250" data-filterable="true" data-sortable="true">Office No.</th>
					<th class="width-100" data-filterable="true" data-sortable="true">Enabled</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$userid = $row['userid'];

					$edituri = '/admin/users/save/'.$type.'/'.$userid;
					$deluri = '/admin/users/delete/'.$type.'/'.$userid;
				?>
				<tr>
					<td><?=$row['salesmanager'];?></td>
					<td><?=$row['spteam'];?></td>
					<td><?=$row['fullname'];?></td>
					<td><?=$row['email'];?></td>
					<td><?=$row['officeno'];?></td>
					<td class="text-center"><?=($row['isenabled'] ? 'Yes' : 'No');?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<!-- <a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a> -->
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>
			<?}?>

		</div>
	</section>
