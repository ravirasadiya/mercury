
	<header class="page-header">
		<h2>Import Users</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">
							<div class="pull-right">
								<a href="/admin/users/import/<?=$type;?>/downloadtemplate" class="btn btn-success btn-sm">Download Template</a>
							</div>
							<?
								$heading = 'Import - ';
								// switch
								switch($type){
									case 'directors':
										$heading .= 'Directors';
									break;
									case 'managers':
										$heading .= 'Managers';
									break;
									case 'teammembers':
										$heading .= 'Team-Members';
									break;
								}
								echo $heading;
							?>
						</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-6">

								<div class="form-group">
									<label class="col-sm-4 control-label">File:</label>
									<div class="col-sm-6">
										<?=form_upload(array('name' => 'userfile', 'value' => '', 'class' => 'form-control validate[required]'));?>
									</div>
								</div>

							</div>

							<div class="col-md-6">

							</div>
						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
