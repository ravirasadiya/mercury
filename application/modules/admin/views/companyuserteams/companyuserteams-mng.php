

	<header class="page-header">
		<h2>Manage Teams</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/companyuserteams/save" class="btn btn-primary btn-sm pull-right">+ Add Team</a>
				Manage Teams
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">Group</th>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">Sales Manager</th>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$companyuserteamid = $row['companyuserteamid'];

					$edituri = '/admin/companyuserteams/save/'.$companyuserteamid;
					$deluri = '/admin/companyuserteams/delete/'.$companyuserteamid;
				?>
				<tr>
					<td><?=$row['group'];?></td>
					<td><?=$row['manager'];?></td>
					<td><?=$row['title'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
