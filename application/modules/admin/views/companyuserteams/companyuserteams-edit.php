
	<header class="page-header">
		<h2>Add/Update Team Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-4 col-md-2 control-label">Sales Manager:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[userid]', $smuserarr, $rs['userid'], 'class="form-control validate[required]"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 col-md-2 control-label">Title:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>

							</div>

						</div> <!-- /row -->

							<hr />

						<h4>Choose Team Member</h4>

						<div class="well well-sm">
						<?
							$num = 0;
							foreach($spuserrs as $urow){
								$userid = $urow['userid'];
								$name = $urow['fullname'];

								// check
								if($urow['companyuserteamid'] && $urow['companyuserteamid'] != $rs['companyuserteamid']){
									continue;
								}

								$companyuserteamrefid = array_search($userid, $companyuserteamrefarr);

								$i = ($companyuserteamrefid) ? $companyuserteamrefid : (0-$num);
								$ischecked = ($i > 0) ? true : false;

								$num++;
						?>
							<div class="checkbox">
								<label>
									<?=form_checkbox(array('name' => 'users['.$i.']', 'value' => $userid, 'checked' => $ischecked));?>
									<?=$name;?>
								</label>
							</div>
						<?}?>
						</div>

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->



<?if(false){?>
	<div class="content-header">
		<h2 class="content-header-title"></h2>


	</div> <!-- /.content-header -->

	<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
	<?=form_hidden('fp', 1);?>

		<h3>Details</h3>

		<div class="form-group">
			<label class="col-sm-2 control-label">Firstname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[firstname]', 'value' => $rs['firstname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Lastname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[lastname]', 'value' => $rs['lastname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">E-Mail:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<!--<div class="form-group">
			<label class="col-sm-2 control-label">Language:</label>
			<div class="col-sm-4">
				<?=form_dropdown('form[languageid]', $languagearr, $rs['languageid'], 'class="form-control validate[required]"');?>
			</div>
		</div>-->
		<div class="form-group">
			<label class="col-sm-2 control-label">User Roles</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[isadmin]', 'value' => 1, 'checked' => ($rs['isadmin'] ? true : false)));?>
					Is Admin
				</label><br />
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[issupervisor]', 'value' => 1, 'checked' => ($rs['issupervisor'] ? true : false)));?>
					Is Supervisor
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Enabled:</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[enabled]', 'value' => 1, 'checked' => ($rs['enabled'] ? true : false)));?>
					Yes
				</label>
			</div>
		</div>

			<p>&nbsp;</p>

		<?if($rs['userid']){?>
			<h3>Update Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control', 'id' => 'password'));?>
				</div>
			</div>
		<?}else{?>
			<h3>Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control validate[required]', 'id' => 'password'));?>
				</div>
			</div>
		<?}?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Password (again):</label>
			<div class="col-sm-4">
				<?=form_password(array('name' => 'password', 'value' => '', 'class' => 'form-control validate[equals[password]]'));?>
			</div>
		</div>

			<p>&nbsp;</p>

			<hr />

		<div class="row">
			<div class="col-md-3">
				<a href="/admin/users" class="btn btn-block btn-default">Cancel</a>
			</div>
			<div class="col-md-6"></div>
			<div class="col-md-3">
				<button type="submit" class="btn btn-block btn-primary">Submit</button>
			</div>
		</div>

	<?=form_close();?>



	<div class="content-header">
		<h2 class="content-header-title">Add/Update User Details</h2>

		<?=$this->site->display_breadcrumbs();?>
	</div> <!-- /.content-header -->

	<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
	<?=form_hidden('fp', 1);?>

		<h3>Details</h3>

		<div class="form-group">
			<label class="col-sm-2 control-label">Firstname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[firstname]', 'value' => $rs['firstname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Lastname:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[lastname]', 'value' => $rs['lastname'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">E-Mail:</label>
			<div class="col-sm-4">
				<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required]'));?>
			</div>
		</div>
		<!--<div class="form-group">
			<label class="col-sm-2 control-label">Language:</label>
			<div class="col-sm-4">
				<?=form_dropdown('form[languageid]', $languagearr, $rs['languageid'], 'class="form-control validate[required]"');?>
			</div>
		</div>-->
		<div class="form-group">
			<label class="col-sm-2 control-label">User Roles</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[isadmin]', 'value' => 1, 'checked' => ($rs['isadmin'] ? true : false)));?>
					Is Admin
				</label><br />
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[issupervisor]', 'value' => 1, 'checked' => ($rs['issupervisor'] ? true : false)));?>
					Is Supervisor
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Enabled:</label>
			<div class="col-sm-4">
				<label class="checkbox-inline">
					<?=form_checkbox(array('name' => 'form[enabled]', 'value' => 1, 'checked' => ($rs['enabled'] ? true : false)));?>
					Yes
				</label>
			</div>
		</div>

			<p>&nbsp;</p>

		<?if($rs['userid']){?>
			<h3>Update Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control', 'id' => 'password'));?>
				</div>
			</div>
		<?}else{?>
			<h3>Password</h3>
			<div class="form-group">
				<label class="col-sm-2 control-label">Password:</label>
				<div class="col-sm-4">
					<?=form_password(array('name' => 'form[password]', 'value' => '', 'class' => 'form-control validate[required]', 'id' => 'password'));?>
				</div>
			</div>
		<?}?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Password (again):</label>
			<div class="col-sm-4">
				<?=form_password(array('name' => 'password', 'value' => '', 'class' => 'form-control validate[equals[password]]'));?>
			</div>
		</div>

			<p>&nbsp;</p>

			<hr />

		<div class="row">
			<div class="col-md-3">
				<a href="/admin/users" class="btn btn-block btn-default">Cancel</a>
			</div>
			<div class="col-md-6"></div>
			<div class="col-md-3">
				<button type="submit" class="btn btn-block btn-primary">Submit</button>
			</div>
		</div>

	<?=form_close();?>

<?}?>
