


	<!-- start: page -->
	<section class="body-sign">
		<div class="center-sign">
			<a href="/" class="logo pull-left">
				<img src="<?=ADMIN_THEMES_DIR;?>custom/img/logo.png" height="54" alt="<?=ADMIN_TITLE;?>" />
			</a>

			<?if($action == 'login'){?>
			<div class="panel panel-sign">
				<div class="panel-title-sign mt-xl text-right">
					<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
				</div>
				<div class="panel-body">
					
					<?=form_open('', array('name' => 'login', 'class' => 'from account-form validate'));?>	 
					<?=form_hidden('loginfp', 1);?>
			
						<?=$this->msg->display_all(); ?>

						<div class="form-group mb-lg">
							<label>Username</label>
							<div class="input-group input-group-icon">
								<?=form_input(array('name' => 'form[email]', 'value' => $rs['email'], 'class' => 'form-control validate[required,custom[email]]'));?>
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-user"></i>
									</span>
								</span>
							</div>
						</div>

						<div class="form-group mb-lg">
							<div class="clearfix">
								<label class="pull-left">Password</label>
								<a href="/admin/login/forgotpassword" class="pull-right">Lost Password?</a>
							</div>
							<div class="input-group input-group-icon">
								<?=form_password(array('name' => 'form[password]', 'value' => $rs['password'], 'class' => 'form-control validate[required]'));?>
								<span class="input-group-addon">
									<span class="icon icon-lg">
										<i class="fa fa-lock"></i>
									</span>
								</span>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-8">
								<div class="checkbox-custom checkbox-default">
									<!--<input id="RememberMe" name="rememberme" type="checkbox"/>
									<label for="RememberMe">Remember Me</label>-->
								</div>
							</div>
							<div class="col-sm-4 text-right">
								<button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
								<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
							</div>
						</div>

					<?=form_close();?>
					
				</div>
			</div>
			<?}?>
		
			<?if($action == 'forgotpassword'){?>	
			<div class="panel panel-sign">
				<div class="panel-title-sign mt-xl text-right">
					<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Recover Password</h2>
				</div>
				<div class="panel-body">
					<?if(!$this->msg->hasmsg()){?>
					<div class="alert alert-info">
						<p class="m-none text-weight-semibold h6">Enter your e-mail below and we will send you reset instructions!</p>
					</div>
					<?}?>

					<?=form_open('', array('name' => 'reset', 'class' => 'validate'));?>	 
					<?=form_hidden('resetfp', 1);?>

						<?=$this->msg->display_all(); ?>
				
						<div class="form-group mb-none">
							<div class="input-group">
								<?=form_input(array('name' => 'form[email]', 'value' => '', 'class' => 'form-control validate[required,custom[email]]'));?>
								<span class="input-group-btn">
									<button class="btn btn-primary" type="submit">Reset!</button>
								</span>
							</div>
						</div>

						<p class="text-center mt-lg">Remembered? <a href="/admin/login">Sign In!</a></p>
					<?=form_close();?>
					
				</div>
			</div>
			<?}?>

			<p class="text-center text-muted mt-md mb-md">&copy; Copyright <?=date("Y");?>. All Rights Reserved.</p>
		</div>
	</section>
	<!-- end: page -->	
	
	