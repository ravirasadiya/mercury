

	<header class="page-header">
		<h2>Add/Update Competency Plan Content</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/companycompetencyplans/save/content/<?= $this->uri->segment(5); ?>/add" class="btn btn-primary btn-sm pull-right">+ Add/Update Content</a>
				Competency Plan Content
			</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped mb-none" id="datatable-default">
				<thead>
					<tr>
						<th class="width-100" data-filterable="true" data-sortable="true">Name</th>
						<th data-filterable="true" data-sortable="true">File Name</th>
						<th data-filterable="true" data-sortable="true">File Ext</th>
						<th data-filterable="true" data-sortable="true">Enabled</th>
						<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
					</tr>
				</thead>
				<tbody>
				<?
					if(count($rs)!=0){
					foreach($rs as $row){
						if(isset($row['contentid']) && $row['contentid']!=null):
							$refid = $this->uri->segment(5);
							$contentid = $row['contentid'];
							$parametersid = $row['competencyparameterid'];
							$edituri = '/admin/companycompetencyplans/save/content/'.$parametersid.'/'.$contentid.'/'.$refid;
							$deluri = '/admin/companycompetencyplans/delete/content/'.$parametersid.'/'.$contentid.'/'.$refid;
							$ext = pathinfo($row['filelocation'], PATHINFO_EXTENSION);
							$enabled = (isset($row['islocked'])&&$row['islocked']=='1')?'Yes':'No';
					?>
					<tr>
						<td><?=$row['title'];?></td>
						<td><?=$row['filename'];?></td>
						<td><?= $ext ?></td>
						<td><?= $enabled ?></td>
						<td class="text-center">
							<?php if($row['createdcompanyid']==ADMIN_COMPANYID): ?>
								<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
								<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
							<?php endif; ?>
						</td>
					</tr>
					<?		
						endif;
						}
					}?>
				</tbody>
			</table>

		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-6">
					<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
				</div>
				<div class="col-md-6">

				</div>
			</div> <!-- /row -->
		</footer>
	</section>
