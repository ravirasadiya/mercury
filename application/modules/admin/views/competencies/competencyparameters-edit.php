<?php $type = $this->input->get('type', TRUE); ?>

	<header class="page-header">
		<h2>Add/Update Competency Parameter Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

				<section class="panel">
					<ul class="nav nav-tabs">
					    <li <?php if($type==''): ?> class="active" <?php endif; ?>><a data-toggle="tab" href="#details">Details</a></li>
					    <li <?php if($type=='content'): ?> class="active" <?php endif; ?>><a data-toggle="tab" href="#content">Content</a></li>
				    </ul>
				    <div class="tab-content">
					    <div id="details" class="tab-pane fade  <?php if($type==''): ?> in active <?php endif; ?>">
					      	<!-- <header class="panel-heading-sub">
								<h2 class="panel-title">Details</h2>
							</header> -->
					      	<div class="panel-body">

								<div class="row">
									<div class="col-md-12">
										<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
										<?=form_hidden('fp', 1);?>

										<div class="form-group">
											<label class="col-sm-2 control-label">Group:</label>
											<div class="col-sm-8 col-md-8">
												<?=form_dropdown('form[competencygroupid]', $competencygrouparr, $rs['competencygroupid'], 'class="form-control validate[required]"');?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Title:</label>
											<div class="col-sm-8 col-md-8">
												<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Description:</label>
											<div class="col-sm-10 col-md-10">
												<?=form_textarea(array('name' => 'form[description]','rows' => 5, 'value' => $rs['description'], 'class' => 'form-control'));?>
											</div>
										</div>
										<footer class="panel-footer">
											<div class="row">
												<div class="col-md-6">
													<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
												</div>
												<div class="col-md-6 text-right">
													<button type="submit" class="btn btn-primary btn-lg">Submit</button>
												</div>
											</div> <!-- /row -->
										</footer>
										<?=form_close();?>
									</div>

								</div> <!-- /row -->
							</div>
					    </div>
					    <div id="content" class="tab-pane fade <?php if($type=='content'): ?> in active <?php endif; ?>">
					    	<!-- <header class="panel-heading-sub">
								<h2 class="panel-title">Content</h2>
							</header> -->

							<header class="panel-heading-sub">
								<h2 class="panel-title">
									<a href="/admin/competencies/save/content/<?= $this->uri->segment(5, 0) ?>" class="btn btn-primary btn-sm pull-right">+ Add Content</a>
									Manage Content
								</h2>
							</header>
						    <div class="panel-body">

								<table class="table table-bordered table-striped mb-none" id="datatable-default">
								<thead>
									<tr>
										<th class="width-100" data-filterable="true" data-sortable="true">Name</th>
										<th data-filterable="true" data-sortable="true">File Name</th>
										<th data-filterable="true" data-sortable="true">File Ext</th>
										<th data-filterable="true" data-sortable="true">Enabled</th>
										<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
									</tr>
								</thead>
								<tbody>
								<?
									if(count($rs_content)!=0){
									foreach($rs_content as $row){
										if(isset($row['contentid']) && $row['contentid']!=null):
											$contentid = $row['contentid'];
											$parametersid = $row['competencyparameterid'];
											$edituri = '/admin/competencies/save/content/'.$parametersid.'/'.$contentid;
											$deluri = '/admin/competencies/delete/content/'.$parametersid.'/'.$contentid;
											$ext = pathinfo($row['filelocation'], PATHINFO_EXTENSION);
											$enabled = (isset($row['islocked'])&&$row['islocked']=='1')?'Yes':'No';
									?>
									<tr>
										<td><?=$row['title'];?></td>
										<td><?=$row['filename'];?></td>
										<td><?= $ext ?></td>
										<td><?= $enabled ?></td>
										<td class="text-center">
											<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
											<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
										</td>
									</tr>
									<?		
										endif;
										}
									}?>
								</tbody>
								</table>
							</div>
						  </div>
					</div>
					
				</section>
					

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
