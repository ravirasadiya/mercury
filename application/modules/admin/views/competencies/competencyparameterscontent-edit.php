	<header class="page-header">
		<h2>Add/Update Competency Parameter Content</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
				<?=form_hidden('fp', 1);?>
				<?=form_hidden('form[parametersid]',isset($rs['parametersid'])?$rs['parametersid']:'');?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">Name:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_input(array('name' => 'form[title]', 'value' => @$rs_content['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">File Name:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_input(array('name' => 'form[filename]', 'value' => @$rs_content['filename'], 'class' => 'form-control validate[required] filename','readonly'=>'readonly'));?>
									</div>
								</div>

 								<div class="form-group">
									<label class="col-sm-2 control-label">File Extension:</label>
									<div class="col-sm-4 col-md-4">
										<?php $ext = pathinfo(@$rs_content['filelocation'], PATHINFO_EXTENSION); ?>
										<?=form_input(array('name' => 'form[extension]', 'value' => $ext, 'class' => 'form-control validate[required] fileextension','readonly'=>'readonly'));?>
									</div>
									<div class="col-sm-4 col-md-4">
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Upload File:</label>
									<div class="col-sm-6 col-md-4">
										<?=form_upload(array('name' => 'file', 'value' => '', 'class' => 'form-control fileuploader'));?>
									</div>
									<div class="col-sm-4 col-md-4">
										<!--<?if(@$rs_content['filelocation'] && file_exists('.'.$rs_content['filelocation'])){?>
											<a href="<?=$rs_content['filelocation'];?>" target="_blank" class="btn btn-primary btn-sm">View</a>
											<a href="/admin/content/delattechment/<?= $rs['parametersid'] ?>/<?= @$rs_content['contentid'] ?>/<?= @$rs_content['contentattachmentid'] ?>" target="_blank" class="btn btn-danger btn-sm confirm-del">Delete</a>
										<?}?> -->
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Enabled:</label>
									<div class="col-sm-10">
										<label class="checkbox-inline">
											<?=form_checkbox(array('name' => 'form[islocked]', 'value' => 1, 'checked' => (@$rs_content['islocked']=='1') ? true : false));?>
											Yes
										</label>
									</div>
								</div>

							</div>

						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/admin/competencies/save/parameters/<?php echo $this->uri->segment(5); ?>?type=content" class="btn btn-default btn-lg">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->