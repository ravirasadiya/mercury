

	<header class="page-header">
		<h2>Add/Update Development Suggestion Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>
	
	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('/'.uri_string().'?action='.$action, array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>
			<?=form_hidden('redirecturi', '');?>
			<input type="hidden" id="companycompetencyplanid" value="<?= $rs['companycompetencyplanid'] ?>">

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">Title:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Team:</label>
									<div class="col-sm-8 col-md-4">
										<?php 
											$isnew = "";
											if($this->uri->segment(4)!="new"){
												$isnew = "readonly";
											}
										?>
										<?=form_dropdown('form[companyuserteamid]', $companyuserteamarr, $rs['companyuserteamid'], 'id="companyuserteamid" class="form-control validate[required]"" data-confirmteamchangeid="'.$rs['companyuserteamid'].'"'.$isnew);?>
									</div>
								</div>
								<!--<div class="form-group">
									<label class="col-sm-2 control-label">Description:</label>
									<div class="col-sm-10 col-md-10">
										<?=form_textarea(array('name' => 'form[description]','rows' => 5, 'value' => $rs['description'], 'class' => 'form-control'));?>
									</div>
								</div>-->

							</div>

						</div> <!-- /row -->

							<hr />

						<?php if($this->uri->segment(4)!="new"): ?>

						<div class="btn-group btn-group-justified comp-save-redirect-ctn">
							<a href="<?=current_url();?>" class="btn btn-primary" role="button">Select Groups</a>
							<a href="<?=current_url();?>?action=parameters" class="btn btn-info" role="button">Select Parameters</a>
							<a href="<?=current_url();?>?action=suggestions" class="btn btn-warning" role="button">Select Suggestions</a>
						</div>

						<?if(!$action){?>
							<h2>Choose Groups</h2>

								<hr />

							<div class="group-headings">
								<div class="group-heading">Available Groups</div>
								<div class="group-heading">Selected Groups</div>
							</div>

							<div class="sort-table">
								<div class="well well-sm sort-cnt w-50 sort-table-cell" id="group_listing">
									<input type="hidden" id="competencygrouprs_count" value="<?= count($competencygrouprs) ?>">
								<?
									$newinc = count($group_companygroupparamsuggrefarr);
									foreach($competencygrouprs as $i => $row){
										$competencygroupid = $row['competencygroupid'];
										$companycompetencyplanrefid = (isset($compplanrefarr[$competencygroupid])) ? $compplanrefarr[$competencygroupid]['refid'] : null;
										$i = ($companycompetencyplanrefid) ? $companycompetencyplanrefid : (0-$i);
										$ischecked = ($i > 0) ? true : false;
										$ii = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : ++$newinc;
										if($ischecked==false):
								?>
									<div class="selected-group list" data-sid="<?=$ii;?>" data-groupid="<?= $competencygroupid ?>" data-companycompetencyplanrefid="<?= $i ?>">
										<label>
											<!-- <?=form_checkbox(array('name' => 'groups['.$i.']', 'value' => $competencygroupid, 'checked' => $ischecked));?> -->

											<span class="group-span" data-id="<?=$ii;?>" data-gcounter=""></span>
												<span data-oldgroupid="<?= $competencygroupid ?>"><?= $competencygroupid ?>.</span>
												<?=$row['title'].' <i>('.$row['numparams'].')</i>';?>
										</label>
									</div>
								<? endif; }?>
								</div>

								<div class="well well-sm sort-cnt w-50 sort-table-cell" id="selected_group">
								<?
									$counter = 1;
									$newinc = count($group_companygroupparamsuggrefarr);
									foreach($competencygrouprs as $i => $row){
										$competencygroupid = $row['competencygroupid'];
										$companycompetencyplanrefid = (isset($compplanrefarr[$competencygroupid])) ? $compplanrefarr[$competencygroupid]['refid'] : null;
										$ordercount = (isset($compplanrefarr[$competencygroupid])) ? $compplanrefarr[$competencygroupid]['ordercount'] : null;
										$i = ($companycompetencyplanrefid) ? $companycompetencyplanrefid : (0-$i);
										$ischecked = ($i > 0) ? true : false;
										$ii = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : ++$newinc;
										if($ischecked==true):
								?>
									<div class="selected-group list" data-sid="<?=$ii;?>" data-groupid="<?= $competencygroupid ?>" data-companycompetencyplanrefid="<?= $i ?>" data-maincounter="<?= $ordercount ?>"> <!-- class='checkbox' -->
										<label>
											<!-- <?=form_checkbox(array('name' => 'groups['.$i.']', 'value' => $competencygroupid, 'checked' => $ischecked));?> -->
											<span class="group-span" data-id="<?=$ii;?>" data-gcounter="<?= $ordercount ?>"><?= $counter++ ?> - </span>
											<span data-oldgroupid="<?= $competencygroupid ?>">(<?= $competencygroupid ?>).</span>
											<?=$row['title'].' <i>('.$row['numparams'].')</i>';?>
										</label>
									</div>
								<? endif; }?>
								</div>
							</div>
						<?}?>

						<?if($action == 'parameters'){?>
							<h2>Choose Parameters</h2>

							<hr />

							<div class="group-headings">
								<div class="group-heading">Available Parameters</div>
								<div class="group-heading">Selected Parameters</div>
							</div>

							<div class="sort-cnt" id="mainparameters">
								<!-- Available -->
								<?
									$counter_loop = 1;
									usort($group_companycompetencyplanrefrs, function($a, $b) {
									    return $a['sortorder'] - $b['sortorder'];
									});
									foreach($group_companycompetencyplanrefrs as $grow){
										$parameter_count = 0;
										$competencygroupid = $grow['competencygroupid'];
										$grefarr = $compplanrefarr[$competencygroupid];
										$g_ii = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;
										$pcounter = 0;
										$ordercount_para = (isset($compplanrefarr[$competencygroupid])) ? $compplanrefarr[$competencygroupid]['ordercount'] : null;
								?>
										<div class="row main-parameters list" data-sid="<?=$g_ii;?>" data-groupsid="<?= $competencygroupid ?>" data-parameterordercount="<?= $ordercount_para ?>">
											<div class="col-lg-12">
												<div class="sort-table">
													<div class="well well-sm w-50 ">
														<? if(strlen($competencygroupid.'. '.$grow['group'])>48): ?>			
															<?php $pcounter = $pcounter+1; ?>
														<? endif; ?> 
														<h4 class="fnt-b"><?=$competencygroupid.'. '.$grow['group'];?></h4>
														<div class="parameters_listing_first_<?=$g_ii;?>" data-sid="<?=$g_ii;?>">
															<!-- <div class="sort-cnt "> -->
															<?
																$newinc = (isset($param_companygroupparamsuggrefarr[$competencygroupid])) ? count($param_companygroupparamsuggrefarr[$competencygroupid]) : 0;
																foreach($competencyparameterrs as $i => $row){
																	// checvk
																	if($row['competencygroupid'] != $competencygroupid){
																		continue;
																	}

																	$competencyparameterid = $row['competencyparameterid'];
																	$companycompetencyplanrefid = (isset($grefarr['parameters'][$competencyparameterid])) ? $grefarr['parameters'][$competencyparameterid]['refid'] : null;

																	$i = ($companycompetencyplanrefid) ? $companycompetencyplanrefid : (0-$i);
																	$ischecked = ($i > 0) ? true : false;

																	$ii = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : ++$newinc;
																	$row['title'] = $row['title'];
																	if($ischecked==false):
																	// $row['title'] = $g_ii.'.'.$ii.'. '.$row['title'];
															?>
																<?php $pcounter = $pcounter+1; ?>
																<div class="selected-group-<?=$g_ii;?> list" data-masterid="<?=$g_ii;?>" data-sid="<?=$g_ii.$ii;?>" data-competencyparameterid="<?= $competencyparameterid ?>" data-competencygroupid="<?= $competencygroupid ?>" data-companycompetencyplanrefid="<?= $i ?>" data-counter="<?= $ordercount_para ?>">
																	<label>
																		<!-- <?=form_checkbox(array('name' => 'parameters['.$competencygroupid.']['.$i.']', 'value' => $competencyparameterid, 'checked' => $ischecked, 'class'=>'parameters_checkbox','data-gid'=>$g_ii,'data-iid'=>$ii));?> -->
																		<span data-competencyparameterid="<?= $competencyparameterid ?>"><?= $competencygroupid.'.'.$competencyparameterid.'.' ?></span>
																		<span data-masterid="<?=$g_ii.$ii;?>" data-mainid="<?= $competencyparameterid ?>"><? $ii ?></span>
																		<span data-subid="<?= $competencyparameterid ?>"></span><?=$row['title'].' <i>('.$row['numsuggestions'].')</i>';?>
																		<!-- <span data-masterid="<?=$g_ii.$ii;?>"><? $ii ?></span> -->
																		<? if(strlen($row['title'].' <i>('.$row['numsuggestions'].')</i>')>70): ?>			
																			<?php $pcounter = $pcounter+1; ?>
																		<? endif; ?> 
																	</label>
																</div>
															<? endif; }?>
														</div>
													</div>

													<div class="well well-sm w-50 ">
														<h4 class="fnt-b"><?=$ordercount_para.'. '.$grow['group'];?></h4>
														<div class="parameters_listing_second_<?=$g_ii;?>" data-sid="<?=$g_ii;?>">
															<!-- <div class="sort-cnt "> -->
															<?
																$counter = 1;
																$newinc = (isset($param_companygroupparamsuggrefarr[$competencygroupid])) ? count($param_companygroupparamsuggrefarr[$competencygroupid]) : 0;
																foreach($competencyparameterrs as $i => $row){
																	// checvk
																	if($row['competencygroupid'] != $competencygroupid){
																		continue;
																	}

																	$competencyparameterid = $row['competencyparameterid'];
																	$companycompetencyplanrefid = (isset($grefarr['parameters'][$competencyparameterid])) ? $grefarr['parameters'][$competencyparameterid]['refid'] : null;
																	$ordercount = (isset($grefarr['parameters'][$competencyparameterid])) ? $grefarr['parameters'][$competencyparameterid]['ordercount'] : null;

																	$i = ($companycompetencyplanrefid) ? $companycompetencyplanrefid : (0-$i);
																	$ischecked = ($i > 0) ? true : false;

																	$ii = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : ++$newinc;
																	// $row['title'] = $g_ii.'.'.$ii.'. '.$row['title'];
																	if($ischecked==true):
															?>
																<?php $pcounter = $pcounter+1; ?>
																<div class="selected-group-<?=$g_ii;?> list" data-masterid="<?=$g_ii;?>" data-sid="<?=$g_ii.$ii;?>" data-competencyparameterid="<?= $competencyparameterid ?>" data-competencygroupid="<?= $competencygroupid ?>" data-companycompetencyplanrefid="<?= $i ?>" data-counter="<?= $ordercount_para ?>" data-maincounter="<?= $ordercount ?>">
																	<label>
																		<!-- <?=form_checkbox(array('name' => 'parameters['.$competencygroupid.']['.$i.']', 'value' => $competencyparameterid, 'checked' => $ischecked, 'class'=>'parameters_checkbox','data-gid'=>$g_ii,'data-iid'=>$ii));?> -->
																		
																		<span data-masterid="<?=$g_ii.$ii;?>" data-mainid="<?= $competencyparameterid ?>"><?= $ordercount_para.'.' ?></span>
																		<span data-subid="<?= $competencyparameterid ?>" data-pcounter="<?= $ordercount ?>"><?= $ordercount ?></span>
																		<span data-competencyparameterid="<?= $competencyparameterid ?>"> - (<?= $competencygroupid.'.'.$competencyparameterid ?>) </span>
																		<?=$row['title'].' <i>('.$row['numsuggestions'].')</i>';?>
																		<? if(strlen($row['title'].' <i>('.$row['numsuggestions'].')</i>')>70): ?>			
																			<?php $pcounter = $pcounter+1; ?>
																		<? endif; ?>
																	</label>
																</div>
															<? endif; } $counter_loop++; ?>
															<!-- </div> -->
														</div>
													</div>
													<input type="hidden" class="parameters_listing_first_<?=$g_ii;?>_count" value="<?= $pcounter ?>">
												</div>
											</div>
										</div>
									
									<? }$pcounter = 0; ?>
								<?}?>
							</div>

						<?if($action == 'suggestions'){?>
							<h2>Choose Suggestions</h2>

							<hr />

							<div class="group-headings">
								<div class="group-heading">Available Suggestions</div>
								<div class="group-heading">Selected Suggestions</div>
							</div>

							<div class="sort-cnt" id="mainsuggestion">
								<?
									usort($group_companycompetencyplanrefrs, function($a, $b) {
									    return $a['sortorder'] - $b['sortorder'];
									});
									foreach($group_companycompetencyplanrefrs as $grow){
										$competencygroupid = $grow['competencygroupid'];
										$grefarr = $compplanrefarr[$competencygroupid];
										$g_ii = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;
										$ordercount = (isset($compplanrefarr[$competencygroupid])) ? $compplanrefarr[$competencygroupid]['ordercount'] : null;
								?>
									<div class="row main-suggestions list" data-suggestiongroupid="<?= $competencygroupid ?>" data-suggestiongrouporder="<?= $ordercount ?>" >
										<div class="col-lg-12">
											<div class="sort-table">
												<div class="well well-sm w-50" data-sugggroupid="<?= $competencygroupid ?>" data-sid="<?=$g_ii;?>" data-suggestioncount="<?= $ordercount ?>">
													<h4 class="fnt-b"><?= $competencygroupid.'. '.$grow['group'];?></h4>
													<? if(strlen($competencygroupid.'. '.$grow['group'])>=60): ?>			
														<?php $parasuggestionscount = $parasuggestionscount+1; ?>
													<? endif; ?> 
													<?
														usort($parameter_companycompetencyplanrefrs, function($a, $b) {
														    return $a['sortorder'] - $b['sortorder'];
														});
														// loop
														foreach($parameter_companycompetencyplanrefrs as $prow){
															$competencyparameterid = $prow['competencyparameterid'];

															// check
															if($prow['competencygroupid'] != $competencygroupid){
																continue;
															}

															$prefarr = $compplanrefarr[$competencygroupid]['parameters'][$competencyparameterid];
															$p_ii = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;
															$ordercount_para = (isset($grefarr['parameters'][$competencyparameterid])) ? $grefarr['parameters'][$competencyparameterid]['ordercount'] : null;
															$parasuggestionscount = 0;
													?>
														<div class="select-suggestion-<?= $competencygroupid ?>" data-subsuggestions="<?=$ordercount_para?>">
															<div  data-suggestionsid="<?= $competencygroupid ?>">
																<p>&nbsp;</p>
																<h5 class="fnt-b col-md-offset-1"><?=$competencyparameterid.'. '.$prow['parameter'];?></h5>
																<hr />
																<div class="sort-cnt available-suggestion-<?= $ordercount.$ordercount_para ?>" data-suggestionboxgroupparaid="<?= $ordercount.$ordercount_para ?>" data-suggestionboxgroupidnew="<?= $ordercount ?>" data-suggestionboxparaidnew="<?= $ordercount_para ?>" >
																	<?
																		$newinc = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? count($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid]) : 0;
																		foreach($competencysuggestionrs as $i => $row){
																			// check
																			if($row['competencyparameterid'] != $competencyparameterid){
																				continue;
																			}

																			$competencysuggestionid = $row['competencysuggestionid'];
																			$companycompetencyplanrefid = (isset($prefarr['suggestions'][$competencysuggestionid])) ? $prefarr['suggestions'][$competencysuggestionid]['refid'] : null;

																			$i = ($companycompetencyplanrefid) ? $companycompetencyplanrefid : (0-$i);
																			$ischecked = ($i > 0) ? true : false;

																			$ii = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : ++$newinc;
																			// $row['title'] = $g_ii.'.'.$p_ii.'.'.$ii.'. '.$row['title'];
																			// $row['title'] = $g_ii.'.'.$p_ii.'.'.$ii.'. '.$row['title'];
																			if($ischecked==false):
																	?>
																		<div class="col-md-offset-2" data-sid="<?=$g_ii.$p_ii.$ii;?>" data-sugboxuniqueid="<?= $ordercount.$ordercount_para ?>" data-suggcompetencysuggestionid="<?= $competencysuggestionid ?>" data-suggcompetencygroupid="<?= $competencygroupid ?>" data-suggcompetencyparameterid="<?= $competencyparameterid ?>" data-suggcompanycompetencyplanref="<?= $companycompetencyplanrefid ?>">
																			<label>

																				<!-- <?=form_checkbox(array('name' => 'suggestions['.$competencygroupid.']['.$competencyparameterid.']['.$i.']', 'value' => $competencysuggestionid, 'checked' => $ischecked));?> -->
																				<span data-spansugggroupid="<?= $competencygroupid ?>"><?= $competencygroupid ?>.</span>
																				<span data-spansuggparameterid="<?= $competencyparameterid ?>"><?= $competencyparameterid ?>.</span>
																				<span data-suggcompetencysuggestionid="<?= $competencysuggestionid ?>" data-suggcompetencygroupid="<?= $competencygroupid ?>" data-suggcompetencyparameterid="<?= $competencyparameterid ?>" data-spansuggestionscount="<?= $ii ?>" data-newsuggestioncount=""><?= $competencysuggestionid ?></span>
																				<span data-oldsuggparagroupid=""></span>
																				<?=$row['title'];?>
																				<? if(strlen($competencygroupid.'.'.$competencyparameterid.'.'.$ii.$row['title'])>=60): ?>			
																					<?php $parasuggestionscount = $parasuggestionscount+1; ?>
																				<? endif; ?> 
																			</label>
																		</div>
																		<?php endif; ?>
																		<?php $parasuggestionscount++ ?>
																	<?}?>
																	<input type="hidden" class="parasuggestionscount_<?=$ordercount.$ordercount_para;?>_count" value="<?= $parasuggestionscount ?>" data-suggestionboxgroupid="<?= $ordercount ?>" data-suggestionboxparaid="<?= $ordercount_para ?>">
																</div>
															</div>
														</div>
													<?}?>
												</div>
												<div class="well well-sm w-50 ">
													<h4 class="fnt-b"><?= $ordercount.'. '.$grow['group'];?></h4>
													<?
														// loop
														foreach($parameter_companycompetencyplanrefrs as $prow){
															$competencyparameterid = $prow['competencyparameterid'];

															// check
															if($prow['competencygroupid'] != $competencygroupid){
																continue;
															}

															$prefarr = $compplanrefarr[$competencygroupid]['parameters'][$competencyparameterid];
															$p_ii = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;
															$ordercount_para = (isset($grefarr['parameters'][$competencyparameterid])) ? $grefarr['parameters'][$competencyparameterid]['ordercount'] : null;
													?>
														<div data-subselectsuggestions="<?=$ordercount_para?>" data-suggestionsid="<?= $competencygroupid ?>">
															<p>&nbsp;</p>
															<h5 class="fnt-b col-md-offset-1"><?=$ordercount_para.'. '.$prow['parameter'];?></h5>
															<hr />
															<div class="sort-cnt selected-suggestion selected-suggestion-<?= $ordercount.$ordercount_para ?>" data-suggestionboxgroupparaid="<?= $ordercount.$ordercount_para ?>" data-suggestionboxgroupidnew="<?= $ordercount ?>" data-suggestionboxparaidnew="<?= $ordercount_para ?>">
																<?
																	$newinc = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? count($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid]) : 0;
																	foreach($competencysuggestionrs as $i => $row){
																		// check
																		if($row['competencyparameterid'] != $competencyparameterid){
																			continue;
																		}

																		$competencysuggestionid = $row['competencysuggestionid'];
																		$companycompetencyplanrefid = (isset($prefarr['suggestions'][$competencysuggestionid])) ? $prefarr['suggestions'][$competencysuggestionid]['refid'] : null;
																		$suggordercount = (isset($prefarr['suggestions'][$competencysuggestionid])) ? $prefarr['suggestions'][$competencysuggestionid]['ordercount'] : null;

																		$i = ($companycompetencyplanrefid) ? $companycompetencyplanrefid : (0-$i);
																		$ischecked = ($i > 0) ? true : false;

																		$ii = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : ++$newinc;
																		// $row['title'] = $g_ii.'.'.$p_ii.'.'.$ii.'. '.$row['title'];
																		// $row['title'] = $g_ii.'.'.$p_ii.'.'.$ii.'. '.$row['title'];
																		if($ischecked==true):
																?>
																	<div class="col-md-offset-2" data-sid="<?=$g_ii.$p_ii.$ii;?>" data-sugboxuniqueid="<?= $ordercount.$ordercount_para ?>" data-suggcompetencysuggestionid="<?= $competencysuggestionid ?>" data-suggcompetencygroupid="<?= $competencygroupid ?>" data-suggcompetencyparameterid="<?= $competencyparameterid ?>" data-suggcompanycompetencyplanref="<?= $companycompetencyplanrefid ?>" data-sortablenewsuggestioncount="<?= $suggordercount ?>">
																		<label>
																			<!-- <?=form_checkbox(array('name' => 'suggestions['.$competencygroupid.']['.$competencyparameterid.']['.$i.']', 'value' => $competencysuggestionid, 'checked' => $ischecked));?> -->
																			<span data-spansugggroupid="<?= $competencygroupid ?>"><?= $ordercount ?>.</span>
																			<span data-spansuggparameterid="<?= $competencyparameterid ?>"><?= $ordercount_para ?>.</span>
																			<span data-suggcompetencysuggestionid="<?= $competencysuggestionid ?>" data-suggcompetencygroupid="<?= $competencygroupid ?>" data-suggcompetencyparameterid="<?= $competencyparameterid ?>" data-spansuggestionscount="<?= $ii ?>" data-newsuggestioncount="<?= $suggordercount ?>" data-suggnumberfinder="<?=$g_ii.$p_ii.$ii;?>"><?= $ii ?></span>
																			<span data-oldsuggparagroupid><?= '('.$competencygroupid.'.'.$competencyparameterid.'.'.$competencysuggestionid.')' ?></span>
																			<!-- 
																			<span data-spansugggroupid="<?= $ordercount ?>"></span>
																			<span data-spansuggparameterid="<?= $ordercount_para ?>"></span>
																			<span data-suggcompetencysuggestionid="<?= $competencysuggestionid ?>" data-suggcompetencygroupid="<?= $competencygroupid ?>" data-suggcompetencyparameterid="<?= $competencyparameterid ?>" data-spansuggestionscount="<?= $ii ?>"><?= $ii ?></span> -->
																			<?=$row['title'];?>
																		</label>
																	</div>
																	<?php endif; ?>
																<?}?>
															</div>
														</div>
													<?}?>
												</div>												
											</div>
										</div>
									</div>
								<?}?>
							</div>
							<!-- <div id="mainsuggestion2">
							</div> -->
						<?}?>

						<?php endif; ?>

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->