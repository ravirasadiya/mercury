

	<header class="page-header">
		<h2>Manage Development Suggestions</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/competencies/save/<?=$type;?>" class="btn btn-primary btn-sm pull-right">+ Add Development Suggestion</a>
				Manage Development Suggestions
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="width-100" data-filterable="true" data-sortable="true">ID</th>
					<th data-filterable="true" data-sortable="true">Group</th>
					<th data-filterable="true" data-sortable="true">Parameter</th>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$competencysuggestionid = $row['competencysuggestionid'];

					$edituri = '/admin/competencies/save/'.$type.'/'.$competencysuggestionid;
					$deluri = '/admin/competencies/delete/'.$type.'/'.$competencysuggestionid;
				?>
				<tr>
					<td class="text-center"><?=$row['id'];?></td>
					<td><?=$row['competencygroup'];?></td>
					<td><?=$row['competencyparameter'];?></td>
					<td><?=$row['title'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
