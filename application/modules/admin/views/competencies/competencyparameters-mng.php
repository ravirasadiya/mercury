

	<header class="page-header">
		<h2>Manage Competency Parameters</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/competencies/save/<?=$type;?>" class="btn btn-primary btn-sm pull-right">+ Add Competency Parameter</a>
				Manage Competency Parameters
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="width-100" data-filterable="true" data-sortable="true">ID</th>
					<th data-filterable="true" data-sortable="true">Group</th>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$competencyparameterid = $row['competencyparameterid'];

					$edituri = '/admin/competencies/save/'.$type.'/'.$competencyparameterid;
					$deluri = '/admin/competencies/delete/'.$type.'/'.$competencyparameterid;
				?>
				<tr>
					<td class="text-center"><?=$row['id'];?></td>
					<td><?=$row['competencygroup'];?></td>
					<td><?=$row['title'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
