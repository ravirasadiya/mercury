

	<header class="page-header">
		<h2>Manage Competency Plan Content</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<!-- <a href="/admin/companycompetencyplans/save/content" class="btn btn-primary btn-sm pull-right">+ Add Competency Plan</a> -->
				Manage Competency Plans
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-250" data-filterable="true" data-sortable="true">Team</th>
					<th data-filterable="true" data-sortable="true">Group</th>
					<th data-filterable="true" data-sortable="true">Parameter</th>
					<th data-filterable="true" data-sortable="true">StdComp Content</th>
					<th data-filterable="true" data-sortable="true">Content</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$competencyparameterid = $row['competencyparameterid'];
					$companycompetencyplanrefid = $row['companycompetencyplanrefid'];
					$companycompetencyplanid = $row['companycompetencyplanid'];

					$viewuri = '/admin/companycompetencyplans/save/content/'.$companycompetencyplanrefid;
					// $edituri = '/admin/companycompetencyplans/save/content/'.$competencyparameterid;
					// $deluri = '/admin/-/delete/'.$competencyparameterid;
				?>
				<tr>
					<td><?=$row['companycompetencyplan_title'];?></td>
					<td><?=$row['companyuserteam_title'];?></td>
					<td><?=$row['competencygroup_title'];?></td>
					<td><?=$row['competencyparameter_title'];?></td>
					<td><?=$row['content_count'];?></td>
					<td><?=$row['local_content_count'];?></td>
					<td class="text-center">
						<a href="<?=$viewuri;?>" class="btn btn-default btn-xs">View</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
