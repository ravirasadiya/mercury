

	<header class="page-header">
		<h2>Manage Activity Groups</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/customeractivitygroups/save" class="btn btn-primary btn-sm pull-right">+ Add Group</a>
				Manage Group
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Activity Group</th>
					<th data-filterable="true" data-sortable="true">Status</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$customeractivitygroupid = $row['customeractivitygroupid'];

					$edituri = '/admin/customeractivitygroups/save/'.$customeractivitygroupid;
					$deluri = '/admin/customeractivitygroups/delete/'.$customeractivitygroupid;
				?>
				<tr>
					<td><?=$row['name'];?></td>
					<td><?=$row['status'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>