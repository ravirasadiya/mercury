

	<section class="body">

		<!-- start: header -->
		<header class="header">
			<div class="logo-container">
				<a href="/admin/dashboard" class="logo">
					<img src="<?=ADMIN_THEMES_DIR;?>custom/img/logo.png" alt="<?=ADMIN_TITLE;?>" height="45">
				</a>
				<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>


			</div>

			<!-- start: search & user box -->
			<div class="header-right">
				<?if(ADMIN_COMPANYNAME){?>
					<button type="button" class="btn btn-info">Working On: <em><?=ADMIN_COMPANYNAME;?></em></button>
				<?}?>

				<span class="separator"></span>

				<div id="userbox" class="userbox">
					<a href="#" data-toggle="dropdown">
						<figure class="profile-picture">
							<img src="<?=ADMIN_THEMES_DIR;?>custom/img/default-user.png" alt="Joseph Doe" class="img-circle" data-lock-picture="<?=ADMIN_THEMES_DIR;?>custom/img/default-user.png" />
						</figure>
						<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
							<span class="name"><?=ADMIN_FULLNAME;?></span>
							<span class="role">administrator</span>
						</div>

						<i class="fa custom-caret"></i>
					</a>

					<div class="dropdown-menu">
						<ul class="list-unstyled">
							<li class="divider"></li>
							<li>
								<a href="/admin/users/save/myprofile">My Profile</a>
							</li>
							<li>
								<a href="/admin/login/logout">Logout</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- end: search & user box -->
		</header>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<aside id="sidebar-left" class="sidebar-left">

				<div class="sidebar-header">
					<div class="sidebar-title">
						Navigation
					</div>
					<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>

				<div class="nano">
					<div class="nano-content">

						<nav id="menu" class="nav-main" role="navigation">
							<?=$this->utils->create_menu(config_item('admin-main-menu'), config_item('admin-main-menu-config'));?>
						</nav>

					</div>

					<script>
						// Maintain Scroll Position
						if (typeof localStorage !== 'undefined') {
							if (localStorage.getItem('sidebar-left-position') !== null) {
								var initialPosition = localStorage.getItem('sidebar-left-position'),
									sidebarLeft = document.querySelector('#sidebar-left .nano-content');

								sidebarLeft.scrollTop = initialPosition;
							}
						}
					</script>

				</div>

			</aside>
			<!-- end: sidebar -->

			<section role="main" class="content-body">

			<?=$this->msg->display_all();?>
