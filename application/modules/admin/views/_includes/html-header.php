
	<!-- Basic -->
	<meta charset="UTF-8">

	<title><?=ADMIN_TITLE;?> - Admin</title>

	<meta name="keywords" content="HTML5 Admin Template" />
	<meta name="description" content="Porto Admin - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap/css/bootstrap.css" />

	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-ui/jquery-ui.theme.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/morris.js/morris.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/select2/css/select2.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/select2-bootstrap-theme/select2-bootstrap.min.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />

	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="<?=ADMIN_THEMES_DIR;?>default/stylesheets/theme-custom.css">

	<!-- Head Libs -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/modernizr/modernizr.js"></script>


	<!-- Custom -->
	<link href="<?=ADMIN_THEMES_DIR;?>custom/js/jquery-validation-engine/css/validationEngine.jquery.css" rel="stylesheet">
	<link href="<?=ADMIN_THEMES_DIR;?>custom/css/custom-css.css" rel="stylesheet">


	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" /> -->
