

	<!-- Vendor -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery/jquery.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/magnific-popup/jquery.magnific-popup.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-placeholder/jquery-placeholder.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/select2/js/select2.js"></script>

	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/tables/examples.datatables.default.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/tables/examples.datatables.row.with.details.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/tables/examples.datatables.tabletools.js"></script>

	<!-- Specific Page Vendor -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-ui/jquery-ui.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-appear/jquery-appear.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/flot/jquery.flot.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/flot.tooltip/flot.tooltip.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/flot/jquery.flot.pie.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/flot/jquery.flot.categories.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/flot/jquery.flot.resize.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jquery-sparkline/jquery-sparkline.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/raphael/raphael.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/morris.js/morris.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/gauge/gauge.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/snap.svg/snap.svg.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/liquid-meter/liquid.meter.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/jquery.vmap.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>default/vendor/bootstrap-timepicker/bootstrap-timepicker.js"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/theme.js"></script>

	<!-- Theme Custom -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/theme.custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/theme.init.js"></script>

	<!-- Examples -->
	<script src="<?=ADMIN_THEMES_DIR;?>default/javascripts/dashboard/examples.dashboard.js"></script>

	<!-- Plugins -->
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/jquery-validation-engine/js/languages/jquery.validationEngine-en.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/jquery-validation-engine/js/jquery.validationEngine.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/json-js/json2.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/eModal.min.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/jquery.filter-list.js"></script>

	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/ckeditor/ckeditor.js"></script>
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/ckeditor/adapters/jquery.js"></script>

	<!-- Carts -->
	<script src="/application/vendor/Ejdamm/Chart.js-PHP/js/Chart.min.js"></script>
    <script src="/application/vendor/Ejdamm/Chart.js-PHP/js/driver.js"></script>
    <script>
		(function() {
			loadChartJsPhp();
		})();
    </script>

	<!-- Init -->
	<script src="<?=ADMIN_THEMES_DIR;?>custom/js/custom-scripts.js"></script>
