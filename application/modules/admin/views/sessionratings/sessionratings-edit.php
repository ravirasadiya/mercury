

	<header class="page-header">
		<h2>Add/Update Session Ratings Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">Title:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<!--<div class="form-group">
									<label class="col-sm-2 control-label">Description:</label>
									<div class="col-sm-10 col-md-10">
										<?=form_textarea(array('name' => 'form[description]','rows' => 5, 'value' => $rs['description'], 'class' => 'form-control'));?>
									</div>
								</div>-->

							</div>

						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
