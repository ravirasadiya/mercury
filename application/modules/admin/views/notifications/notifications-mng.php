

	<header class="page-header">
		<h2>Manage Notifications</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/notifications/save" class="btn btn-primary btn-sm pull-right">+ Add Notification</a>
				Manage Notifications
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="width-200" data-filterable="true" data-sortable="true">Team</th>
					<th data-filterable="true" data-sortable="true">Message</th>
					<th data-filterable="true" data-sortable="true">Interval</th>
					<th data-filterable="true" data-sortable="true">Type</th>
					<th data-filterable="true" data-sortable="true">Recipient</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				// set
				$numarr = config_item('num-select-arr');
				$intervalarr = config_item('notification-interval-arr');
				$positionarr = config_item('notification-position-arr');
				$recipientarr = config_item('notification-recipient-arr');
				$typearr = config_item('notification-type-arr');

				// loop
				foreach($rs as $row){
					$notificationid = $row['notificationid'];

					$num = (isset($numarr[$row['num']])) ? $numarr[$row['num']] : 'None';
					$interval = (isset($intervalarr[$row['interval']])) ? $intervalarr[$row['interval']] : 'None';
					$position = (isset($positionarr[$row['position']])) ? $positionarr[$row['position']] : 'None';
					$recipient = (isset($recipientarr[$row['recipient']])) ? $recipientarr[$row['recipient']] : 'None';
					$type = (isset($typearr[$row['type']])) ? $typearr[$row['type']] : 'None';
					$iteration = "Every $num $interval / $position";

					$edituri = '/admin/notifications/save/'.$notificationid;
					$deluri = '/admin/notifications/delete/'.$notificationid;
				?>
				<tr>
					<td><?=$row['team'];?></td>
					<td><?=$row['message'];?></td>
					<td><?=$iteration;?></td>
					<td><?=$type;?></td>
					<td><?=$recipient;?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
