

	<header class="page-header">
		<h2>Add/Update Notification Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">Team:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_dropdown('form[companyuserteamid]', $companyuserteamarr, $rs['companyuserteamid'], 'class="form-control"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Notification:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_input(array('name' => 'form[message]', 'value' => $rs['message'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Number:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_dropdown('form[num]', config_item('num-select-arr'), $rs['num'], 'class="form-control"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Interval:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_dropdown('form[interval]', config_item('notification-interval-arr'), $rs['interval'], 'class="form-control"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Timing:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_dropdown('form[position]', config_item('notification-position-arr'), $rs['position'], 'class="form-control"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Type:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_dropdown('form[type]', config_item('notification-type-arr'), $rs['type'], 'class="form-control"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Recipient:</label>
									<div class="col-sm-8 col-md-8">
										<?=form_dropdown('form[recipient]', config_item('notification-recipient-arr'), $rs['recipient'], 'class="form-control"');?>
									</div>
								</div>

							</div>

						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
