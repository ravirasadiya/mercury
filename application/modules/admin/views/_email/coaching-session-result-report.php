
	<p>Hi Sales Manager and Team Member.</p>

	<p>Attached please find the Coaching Session Result Report completed on <?=date("Y-m-d", strtotime($coachingsessionrs['completedon']));?>.</p>

	<table width="750" cellpadding="1" cellspacing="1">
	<tr>
		<td width="150" class="aln-r">Manager:</td>
		<td width="600"><?=$coachingsessionrs['salesmanager'];?></td>
	</tr>
	<tr>
		<td class="aln-r">Date:</td>
		<td><?=date("Y-m-d");?></td>
	</tr>
	<tr>
		<td class="aln-r">Team:</td>
		<td><?=$coachingsessionrs['spteam'];?></td>
	</tr>
	<tr>
		<td class="aln-r">Session Completed:</td>
		<td><?=date("Y-m-d", strtotime($coachingsessionrs['completedon']));?></td>
	</tr>
	<tr>
		<td class="aln-r">Member:</td>
		<td><?=$coachingsessionrs['salesperson'];?></td>
	</tr>
	<tr>
		<td class="aln-r">Created By:</td>
		<td><?=$createdbyuserrs['fullname'];?></td>
	</tr>
	<tr>
		<td class="aln-r">Customer:</td>
		<td><?=$coachingsessionrs['customertitle'];?></td>
	</tr>
	</table>
