



	<header class="page-header">
		<h2>Manage Lookups</h2>
	
		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>
		
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				Manage Lookup Values
				<a href="/admin/lookups/save/<?=$type;?>" class="btn btn-success btn-sm pull-right"><span class="fa fa-plus"></span> Add</a>
			</h2>
		</header>
		<div class="panel-body">
			
			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">Type</th>
					<th data-filterable="true" data-sortable="true">Value</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<? 
				foreach($rs as $row){
					$lookupid = $row['lookupid'];
					$row['value'] = ($row['parent1']) ? $row['parent1'].' > '.$row['value'] : $row['value'];
					
					$edituri = '/admin/lookups/save/'.$type.'/'.$lookupid;
					$deluri = '/admin/lookups/delete/'.$type.'/'.$lookupid;
				?>
				<tr>
					<td><?=$row['type'];?></td>
					<td><?=$row['value'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>
			
		</div>
	</section>

