

	
				
	<header class="page-header">
		<h2>Add/Update Lookup Details</h2>
	
		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>
	
	
	<!-- start: page -->
	<div class="row">
		
		<div class="col-md-12">
			
			<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>	 
			<?=form_hidden('fp', 1);?>
			
				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">
					
						<div class="row">
							<div class="col-md-12">
							
								<div class="form-group">
									<label class="col-sm-2 control-label">Value:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[value]', 'value' => $rs['value'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								
							</div>
							
						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>
			
			<?=form_close();?>
			
		</div>

	</div> <!-- /row -->
	<!-- end: page -->

	