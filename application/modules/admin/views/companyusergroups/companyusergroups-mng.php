

	<header class="page-header">
		<h2>Manage Groups</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/companyusergroups/save" class="btn btn-primary btn-sm pull-right">+ Add Group</a>
				Manage Groups
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="width-250" data-filterable="true" data-sortable="true" data-direction="asc">Managing Director</th>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$companyusergroupid = $row['companyusergroupid'];

					$edituri = '/admin/companyusergroups/save/'.$companyusergroupid;
					$deluri = '/admin/companyusergroups/delete/'.$companyusergroupid;
				?>
				<tr>
					<td><?=$row['director'];?></td>
					<td><?=$row['title'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
