
	<header class="page-header">
		<h2>Manage Activity List</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/customeractivity/save/list" class="btn btn-primary btn-sm pull-right">+ Add List</a>
				Manage List
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Year</th>
					<th data-filterable="true" data-sortable="true">Group Name</th>
					<th data-filterable="true" data-sortable="true">Activity Name</th>
					<th data-filterable="true" data-sortable="true">Alloc To User</th>
					<th data-filterable="true" data-sortable="true">Feedback</th>
					<th data-filterable="true" data-sortable="true">Status</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$customeractivityid = $row['customeractivityid'];

					$edituri = '/admin/customeractivity/save/list/'.$customeractivityid;
					$deluri = '/admin/customeractivity/delete/list/'.$customeractivityid;
				?>
				<tr>
					<td><?=$row['activityyear'];?></td>
					<td><?=$row['groupname'];?></td>
					<td><?=$row['activityname'];?></td>
					<td><?=$row['user_count'];?></td>
					<td></td>
					<td><?=$row['activitystatus'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>