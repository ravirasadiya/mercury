
	<header class="page-header">
		<h2>Manage Activity Team</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<!-- <header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/companyactivity/save/list" class="btn btn-primary btn-sm pull-right">+ Add List</a>
				Manage List
			</h2>
		</header> -->
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Group</th>
					<th data-filterable="true" data-sortable="true">Team</th>
					<th data-filterable="true" data-sortable="true">Sales Manager</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$companyuserteamid = $row['companyuserteamid'];

					$seluri = '/admin/customeractivity/addTeam/'.$customeractivityid.'/'.$companyuserteamid;
				?>
				<tr>
					<td><?=$row['group'];?></td>
					<td><?=$row['title'];?></td>
					<td><?=$row['manager'];?></td>
					<td class="text-center">
						<a href="<?=$seluri;?>" class="btn btn-default btn-xs">Select</a>
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>