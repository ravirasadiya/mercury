
	<header class="page-header">
		<h2>Add/Update Activity Feedback</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-4 col-md-2 control-label">Sales Manager:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[userid]', $smuserarr, $rs['userid'], 'class="form-control validate[required]"');?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 col-md-2 control-label">Title:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>

							</div>

						</div> <!-- /row -->

							<hr />

						<h4>Choose Team Member</h4>

						<div class="well well-sm">
						<?
							$num = 0;
							foreach($spuserrs as $urow){
								$userid = $urow['userid'];
								$name = $urow['fullname'];

								// check
								if($urow['companyuserteamid'] && $urow['companyuserteamid'] != $rs['companyuserteamid']){
									continue;
								}

								$companyuserteamrefid = array_search($userid, $companyuserteamrefarr);

								$i = ($companyuserteamrefid) ? $companyuserteamrefid : (0-$num);
								$ischecked = ($i > 0) ? true : false;

								$num++;
						?>
							<div class="checkbox">
								<label>
									<?=form_checkbox(array('name' => 'users['.$i.']', 'value' => $userid, 'checked' => $ischecked));?>
									<?=$name;?>
								</label>
							</div>
						<?}?>
						</div>

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->