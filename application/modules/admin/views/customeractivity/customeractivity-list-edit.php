
	<header class="page-header">
		<h2>Add/Update Activity List</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

				<section class="panel">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#details">Details</a></li>
					    <li><a data-toggle="tab" href="#users">Users</a></li>
					    <!-- <li><a data-toggle="tab" href="#teams">Teams</a></li> -->
				    </ul>

					<!-- <header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header> -->
					<div class="tab-content">

					    <div id="details" class="tab-pane fade in active">
							<div class="panel-body">
								<?=form_open('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
								<?=form_hidden('fp', 1);?>

									<div class="row">
										<div class="col-md-12">

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Group:</label>
												<div class="col-sm-8 col-md-4">
													<?=form_dropdown('form[customeractivitygroupid]',@$groups, @$rs['customeractivitygroupid'], 'class="form-control validate[required]"');?>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Name:</label>
												<div class="col-sm-8 col-md-6">
													<?=form_input(array('name' => 'form[activityname]', 'value' => @$rs['activityname'], 'class' => 'form-control validate[required]'));?>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Fin Year:</label>
												<div class="col-sm-8 col-md-4">
													<?=form_dropdown('form[activityyear]', $finyear, @$rs['activityyear'], 'class="form-control validate[required]"');?>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Frequency:</label>
												<div class="col-sm-8 col-md-4">
													<?=form_dropdown('form[activityfrequency]',config_item('frequency'), @$rs['activityfrequency'], 'class="form-control validate[required]"');?>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Default Target:</label>
												<div class="col-sm-8 col-md-6">
													<?=form_input(array('name' => 'form[defaulttarget]', 'value' => @$rs['defaulttarget'], 'class' => 'form-control validate[required]'));?>
												</div>
											</div>

											<!-- <div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Unit of Measure:</label>
												<div class="col-sm-8 col-md-4">
													<?=form_input(array('name' => 'form[unitofmeasure]', 'value' => @$rs['unitofmeasure'], 'class' => 'form-control validate[required]'));?>
												</div>
											</div> -->

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Description:</label>
												<div class="col-sm-8 col-md-10">
													<?=form_textarea(array('name' => 'form[activitydescr]', 'value' => @$rs['activitydescr'], 'class' => 'form-control'));?>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-4 col-md-2 control-label">Status:</label>
												<div class="col-sm-8 col-md-4">
													<?=form_dropdown('form[activitystatus]',config_item('status'), @$rs['activitystatus'], 'class="form-control validate[required]"');?>
												</div>
											</div>

										</div>

									</div> <!-- /row -->

									<footer class="panel-footer">
										<div class="row">
											<div class="col-md-6">
												<a href="/admin/customeractivity/list" class="btn btn-default btn-lg">Cancel</a>
											</div>
											<div class="col-md-6 text-right">
												<button type="submit" class="btn btn-primary btn-lg">Submit</button>
											</div>
										</div> <!-- /row -->
									</footer>

								<?=form_close();?>
							</div>
					    </div>

					    <div id="users" class="tab-pane fade">
					    	<header class="panel-heading-sub">
								<h2 class="panel-title">
									<a href="/admin/customeractivity/add/user/<?= @$rs['customeractivityid'] ?>" class="btn btn-primary btn-sm pull-right">+ Add User</a>
									Manage Users
								</h2>
							</header>

					    	<div class="panel-body">
						    	<table class="table table-bordered table-striped mb-none datatable-class" id="datatable-default-menu">
									<thead>
										<tr>
											<th data-filterable="true" data-sortable="true">Title Name Surname</th>
											<th data-filterable="true" data-sortable="true">Email Address</th>
											<th data-filterable="true" data-sortable="true">Taget Value</th>
											<th data-filterable="true" data-sortable="true">Enabled</th>
											<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
										</tr>
									</thead>
									<tbody>
									<?
										foreach($rs_users as $row){
											$userid = $row['userid'];
											$rmuri = '/admin/customeractivity/removeUser/'.$customeractivityid.'/'.$userid;
										?>
										<tr>
											<td><?=$row['title'].' '.$row['firstname'].' '.$row['lastname']; ?></td>
											<td><?=$row['email'];?></td>
											<td></td>
											<td><?=$row['enabled'];?></td>
											<td class="text-center">
												<a href="<?=$rmuri;?>" class="btn btn-default btn-xs">Remove</a>
											</td>
										</tr>
									<?}?>
									</tbody>
								</table>
							</div>
					    </div>

					    <!-- <div id="teams" class="tab-pane fade">
					    	<header class="panel-heading-sub">
								<h2 class="panel-title">
									<a href="/admin/customeractivity/add/team/<?= @$rs['customeractivityid'] ?>" class="btn btn-primary btn-sm pull-right">+ Add Team</a>
									Manage Teams
								</h2>
							</header>

					    	<div class="panel-body">
						    	<table class="table table-bordered table-striped mb-none datatable-class" id="datatable-default-menu">
									<thead>
										<tr>
											<th data-filterable="true" data-sortable="true">Group</th>
											<th data-filterable="true" data-sortable="true">Team</th>
											<th data-filterable="true" data-sortable="true">Sales Manager</th>
											<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
										</tr>
									</thead>
									<tbody>
									<?
											foreach($rs_teams as $row){

												$companyuserteamid = $row['companyuserteamid'];

												$rmuri = '/admin/customeractivity/removeTeam/'.$customeractivityid.'/'.$companyuserteamid;
											?>
											<tr>
												<td><?=$row['group'];?></td>
												<td><?=$row['title'];?></td>
												<td><?=$row['manager'];?></td>
												<td class="text-center">
													<a href="<?=$rmuri;?>" class="btn btn-default btn-xs">Remove</a>
												</td>
											</tr>
										<?}?>
									</tbody>
								</table>
							</div>
					    </div> -->
				    </div>

				</section>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->