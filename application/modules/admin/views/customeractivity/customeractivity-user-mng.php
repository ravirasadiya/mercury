
	<header class="page-header">
		<h2>Manage Activity User</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">
				<a href="/admin/customeractivity/save/list/<?= $this->uri->segment(5) ?>" class="btn btn-primary">Back</a>
			</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Title Name Surname</th>
					<th data-filterable="true" data-sortable="true">Email Address</th>
					<th data-filterable="true" data-sortable="true">Enabled</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				foreach($rs as $row){
					$userid = $row['userid'];
					$seluri = '/admin/customeractivity/addUser/'.$customeractivityid.'/'.$userid;

					if(!in_array($userid,$userids)):
				?>
				<tr>
					<td><?=$row['title'].' '.$row['firstname'].' '.$row['lastname']; ?></td>
					<td><?=$row['email'];?></td>
					<td><?=$row['enabled'];?></td>
					<td class="text-center">
						<a href="<?=$seluri;?>" class="btn btn-default btn-xs">Select</a>
					</td>
				</tr>
			<? endif; }?>
			</tbody>
			</table>

		</div>
	</section>