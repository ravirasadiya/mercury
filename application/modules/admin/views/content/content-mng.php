

	<header class="page-header">
		<h2>Manage News</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Manage News</h2>
		</header>
		<div class="panel-body">

			<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th data-filterable="true" data-sortable="true">Title</th>
					<th class="width-200" data-filterable="true" data-sortable="true" data-direction="asc">Type</th>
					<th class="width-150" data-filterable="false" data-sortable="false">Action</th>
				</tr>
			</thead>
			<tbody>
			<?
				$contenttypearr = config_item('content-type-arr');

				foreach($rs as $row){
					$contentid = $row['contentid'];

					$row['type'] = (isset($contenttypearr[$row['type']])) ? $contenttypearr[$row['type']] : '';

					$edituri = '/admin/content/save/'.$contentid;
					$deluri = '/admin/content/delete/'.$contentid;
				?>
				<tr>
					<td><?=$row['title'];?></td>
					<td><?=$row['type'];?></td>
					<td class="text-center">
						<a href="<?=$edituri;?>" class="btn btn-primary btn-xs">Edit</a>
						<!--<a href="<?=$deluri;?>" class="btn btn-danger btn-xs confirm-del">Delete</a>-->
					</td>
				</tr>
			<?}?>
			</tbody>
			</table>

		</div>
	</section>
