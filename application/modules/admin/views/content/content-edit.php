

	<header class="page-header">
		<h2>Add/Update Content Page Details</h2>

		<div class="right-wrapper pull-right">
			<?=$this->site->display_breadcrumbs();?>
		</div>
	</header>


	<!-- start: page -->
	<div class="row">

		<div class="col-md-12">

			<?=form_open_multipart('', array('name' => 'form', 'class' => 'form-horizontal validate'));?>
			<?=form_hidden('fp', 1);?>

				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">Details</h2>
					</header>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">

								<div class="form-group">
									<label class="col-sm-2 control-label">Title:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[title]', 'value' => $rs['title'], 'class' => 'form-control validate[required]'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Text:</label>
									<div class="col-sm-10 col-md-8">
										<?=form_textarea(array('name' => 'form[text]', 'value' => $rs['text'], 'class' => 'form-control wysiwyg-editor admin'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Video URL:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_input(array('name' => 'form[videourl]', 'value' => $rs['videourl'], 'class' => 'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">File/Image:</label>
									<div class="col-sm-6 col-md-4">
										<?=form_upload(array('name' => 'filename', 'value' => '', 'class' => 'form-control'));?>
									</div>
									<div class="col-sm-4 col-md-4">
										<?if($rs['filename'] && file_exists('.'.$rs['filepath'])){?>
											<a href="<?=$rs['filepath'];?>" target="_blank" class="btn btn-primary btn-sm">View</a>
											<a href="/admin/content/delfile/<?=$rs['contentid'];?>" target="_blank" class="btn btn-danger btn-sm confirm-del">Delete</a>
										<?}?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Type:</label>
									<div class="col-sm-8 col-md-4">
										<?=form_dropdown('form[type]', config_item('content-type-arr'), $rs['type'], 'class="form-control"');?>
									</div>
								</div>

							</div>
						</div> <!-- /row -->

					</div>
					<footer class="panel-footer">
						<div class="row">
							<div class="col-md-6">
								<a href="/" class="btn btn-default btn-lg is-cancel-btn">Cancel</a>
							</div>
							<div class="col-md-6 text-right">
								<button type="submit" class="btn btn-primary btn-lg">Submit</button>
							</div>
						</div> <!-- /row -->
					</footer>
				</section>

			<?=form_close();?>

		</div>

	</div> <!-- /row -->
	<!-- end: page -->
