<!doctype html>
<html class="fixed">
<head>
	<?= $this->load->view('admin/_includes/html-header'); ?>
</head>
<body <?=(defined('BODY_CLASS') ? 'class="'.BODY_CLASS.'"' : '');?>>
	
	<?php if(!isset($data['islogin'])){ $this->load->view('admin/_includes/body-header'); } ?>
	
	<?=$this->load->view($data['loadview'], ((isset($data)) ? $data : array()) );?>
	
	<?php if(!isset($data['islogin'])){ $this->load->view('admin/_includes/body-footer'); }?>
	
	<?php $this->load->view('admin/_includes/modals');?>
	
	<?php $this->load->view('admin/_includes/notifications');?>
	
	<?php $this->load->view('admin/_includes/html-footer');?>
	<div class="modal"><!-- Place at bottom of page --></div>
</body>
</html>