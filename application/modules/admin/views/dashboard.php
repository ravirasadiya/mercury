
    <header class="page-header">
        <h2>Dashboard</h2>

        <div class="right-wrapper pull-right">
            <?=$this->site->display_breadcrumbs();?>
        </div>
    </header>

    <?if(isset($statsarr)){?>
    <!-- start: page -->
    <?if(false){?>
    <div class="row">

        <div class="col-md-12">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Report Filters</h2>
                </header>
                <div class="panel-body">

                    <?=form_open('', array('name' => 'dashboardform', 'class' => 'form-horizontal validate'));?>
                    <?=form_hidden('fp', 1);?>

                    <div class="row">
                        <div class="col-md-5">

                            <div class="form-group">
                                <label class="col-sm-4 col-md-4 control-label">Date From:</label>
                                <div class="col-sm-8 col-md-8">
                                    <?=form_input(array('name' => 'form[datefrom]', 'value' => $form['datefrom'], 'class' => 'form-control validate[required]'));?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-md-4 control-label">Created By:</label>
                                <div class="col-sm-8 col-md-8">
                                    <?=form_dropdown('form[createdbyuserid]', array(), $form['createdbyuserid'], 'class="form-control validate[required]"');?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5">

                            <div class="form-group">
                                <label class="col-sm-4 col-md-4 control-label">Date To:</label>
                                <div class="col-sm-8 col-md-8">
                                    <?=form_input(array('name' => 'form[dateto]', 'value' => $form['dateto'], 'class' => 'form-control validate[required]'));?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-md-4 control-label">Created On:</label>
                                <div class="col-sm-8 col-md-8">
                                    <?=form_input(array('name' => 'form[datecreated]', 'value' => $form['datecreated'], 'class' => 'form-control validate[required]'));?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <p class="form-control-static"></p>
                            </div>
                            <div class="_form-group">
                                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                            </div>
                        </div>

                    </div> <!-- /row -->

                    <?=form_close();?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

    </div> <!-- /row -->
    <?}?>

    <div class="row">

        <div class="col-md-12">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?=ADMIN_COMPANYNAME;?></h2>
                </header>
                <div class="panel-body">

                    <?php $totalsummary_chart;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

    </div> <!-- /row -->

    <div class="row">

        <!-- director-charts -->
        <div class="col-md-6">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Director % Sessions Held</h2>
                </header>
                <div class="panel-body">

                    <?php $chart_director_percentages;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

        <div class="col-md-6">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Director Sessions</h2>
                </header>
                <div class="panel-body">

                    <?php $chart_director_sessions;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

    </div> <!-- /row -->

    <div class="row">

        <!-- manager-charts -->
        <div class="col-md-6">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Manager % Sessions Held</h2>
                </header>
                <div class="panel-body">

                    <?php $chart_manager_percentages;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

        <div class="col-md-6">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Manager Sessions</h2>
                </header>
                <div class="panel-body">

                    <?php $chart_manager_sessions;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

    </div> <!-- /row -->

    <div class="row">

        <!-- teammember-charts -->
        <div class="col-md-6">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Team-Member % Sessions Held</h2>
                </header>
                <div class="panel-body">

                    <?php $chart_teammember_percentages;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

        <div class="col-md-6">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Team-Member Sessions</h2>
                </header>
                <div class="panel-body">

                    <?php $chart_teammember_sessions;?>

                </div>
                <footer class="panel-footer">

                </footer>
            </section>

        </div>

    </div> <!-- /row -->

    <div class="row">

        <!-- listings -->
        <div class="col-md-12">

            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Sessions Report</h2>
                </header>
                <div class="panel-body">

                    <table class="table table-bordered table-striped mb-none stats-table" id="datatable-default">
        			<thead>
        				<tr>
                            <th>Director</th>
                            <th>Group</th>
                            <th>Manager</th>
                            <th>Team</th>
                            <th>Member</th>
                            <th class="width-150">Sessions Scheduled</th>
                            <th class="width-150">Sessions Held</th>
                            <th class="width-150">% Sessions Held</th>
        				</tr>
        			</thead>
        			<tbody>
        			<?
                        // companies
    				    foreach($statsarr as $companyid => $dirarr){
    				?>
                    <tr style="background-color: gainsboro;">
                        <td colspan="5"><?=$dirarr['title'];?></td>
                        <td class="text-center"><?=($dirarr['num-scheduled']+$dirarr['num-completed']);?></td>
                        <td class="text-center"><?=$dirarr['num-completed'];?></td>
                        <td class="text-center"><?=$dirarr['percentage-completed'];?>%</td>
                    </tr>
            			<?
                            // directors
        				    foreach($dirarr['directors'] as $mduserid => $dirrow){
        				?>
                        <tr style="background-color: deepskyblue;">
                            <td colspan="5"><?=$dirrow['director'];?></td>
                            <td class="text-center"><?=($dirrow['num-scheduled']+$dirrow['num-completed']);?></td>
                            <td class="text-center"><?=$dirrow['num-completed'];?></td>
                            <td class="text-center"><?=$dirrow['percentage-completed'];?>%</td>
                        </tr>
                			<?
                                // groups
            				    foreach($dirrow['groups'] as $companyusergroupid => $grouprow){
            				?>
                            <tr style="background-color: skyblue;">
                                <td></td>
                                <td colspan="4"><?=$grouprow['title'];?></td>
                                <td class="text-center"><?=($grouprow['num-scheduled']+$grouprow['num-completed']);?></td>
                                <td class="text-center"><?=$grouprow['num-completed'];?></td>
                                <td class="text-center"><?=$grouprow['percentage-completed'];?>%</td>
                            </tr>
                    			<?
                                    // managers
                				    foreach($grouprow['managers'] as $smuserid => $managerrow){
                				?>
                                <tr style="background-color: yellowgreen;">
                                    <td colspan="2"></td>
                                    <td colspan="3"><?=$managerrow['manager'];?></td>
                                    <td class="text-center"><?=($managerrow['num-scheduled']+$managerrow['num-completed']);?></td>
                                    <td class="text-center"><?=$managerrow['num-completed'];?></td>
                                    <td class="text-center"><?=$managerrow['percentage-completed'];?>%</td>
                                </tr>
                                    <?
                                        // teams
                                        foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
                                    ?>
                                    <tr style="background-color: lightgreen;">
                                        <td colspan="3"></td>
                                        <td colspan="2"><?=$teamrow['title'];?></td>
                                        <td class="text-center"><?=($teamrow['num-scheduled']+$teamrow['num-completed']);?></td>
                                        <td class="text-center"><?=$teamrow['num-completed'];?></td>
                                        <td class="text-center"><?=$teamrow['percentage-completed'];?>%</td>
                                    </tr>
                                        <?
                                            // teams
                                            foreach($teamrow['users'] as $userid => $userrow){
                                        ?>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td><?=$userrow['name'];?></td>
                                            <td class="text-center"><?=($userrow['num-scheduled']+$userrow['num-completed']);?></td>
                                            <td class="text-center"><?=$userrow['num-completed'];?></td>
                                            <td class="text-center"><?=$userrow['percentage-completed'];?>%</td>
                                        </tr>
                                        <?}?>
                                    <?}?>
            			        <?}?>
            			    <?}?>
        			    <?}?>
        			<?}?>
        			</tbody>
        			</table>

                </div>
                    <footer class="panel-footer">

                </footer>
            </section>

        </div>

    </div> <!-- /row -->
    <!-- end: page -->
    <?}?>
