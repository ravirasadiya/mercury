<html>
<head>
	<title>PDF Document</title>
	<style type="text/css">
	body {
		font-size: 11px;
		font-family: 'Helvetica';
	}
	table, td, p {
		font-size: 11px;
	}

	/* table */
	table {
		page-break-after: avoid;
		// arial
	}
	table.bordered {
		background-color: #000;
	}
	table.bordered tr {
		background-color: #FFF;
	}
	table thead tr th, table.bordered thead tr th {
		background-color: #d9d9d9;
	}
	table tbody tr.row:nth-child(even) td {
		border-bottom: dotted 1px #CCCCCC;
	}
	table tbody tr.row:nth-child(even) td {
		background-color: #FAFAFA;
	}
	/*table tbody tr.blue > td:not(.no-bg) {*/
	table tbody tr.blue {
		background-color: #9bc2e6;
	}
	/*table tbody tr.green > td:not(.no-bg) {*/
	table tbody tr.green {
		background-color: #a9d08e;
	}
	table tbody tr.orange {
		background-color: #ffc000;
	}
	/*table tbody tr.lightgreen > td:not(.no-bg) {*/
	table tbody tr.lightgreen {
		background-color: #c6e0b4;
	}
	table tbody td.no-bg {
		background-color: #FFF;
	}

	/* -- notes -- */
	table.notes tr:nth-child(even) td {
		background-color: #FAFAFA;
	}
	table.notes td {
		padding: 4px;
7	}
	table.notes td > div {
		border-top: dotted 1px #CCC;
		padding: 4px;
	}

	/* -- headings -- */
	.sec-heading {
		background: #d9d9d9;
		text-align: center;
		padding: 5px;
		margin: 0;
		border: solid 1px #000;
	}

	/* elements */
	.fnt-20		{ font-size: 20px; }
	.aln-c		{ text-align: center; }
	.aln-r		{ text-align: right; }
	.fnt-b		{ font-weight: bold; }
	.fnt-i		{ font-style: italic;}

	/* -- fixes -- */
	.page td img { max-width: none; }
	</style>
</head>
<body>

	<h1 class="sec-heading">REPORT: RATINGS PER COMPETENCY</h1>

		<p>&nbsp;</p>

	<?
		// companies
		foreach($statsarr as $companyid => $dirarr){
			$compdescarr = $dirarr['compdescarr'];

			// directors
			foreach($dirarr['directors'] as $mduserid => $grouprow){
				// check
				if(!is_numeric($mduserid)){
					continue;
				}

				// groups
				foreach($grouprow['groups'] as $companyusergroupid => $grouprow){
					// check
					if(!is_numeric($companyusergroupid)){
						continue;
					}

					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
						// check
						if(!is_numeric($smuserid)){
							continue;
						}

						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
							// check
							if(!is_numeric($companyuserteamid)){
								continue;
							}
							//echo '<pre>'; print_r($teamrow); echo '</pre>'; exit;

							// sessions
							foreach($teamrow['users'] as $userid => $userrow){

								// pagebreak
								if(isset($pagebreak)){
									echo '<div style="page-break-after: always;"></div>';
								}
								$pagebreak = 1;
						?>

							<h2 class="sec-heading"><?=$userrow['name'];?><br /><small><?=$teamrow['title'];?></small></h2>

								<p>&nbsp;</p>

							<table width="100%" cellpadding="1" cellspacing="1">
							<tr>
								<td width="20%">Manager:</td>
								<td width="30%"><?=$userrow['manager'];?></td>
								<td width="20%">Coaching Sessions:</td>
								<td width="30%"><?=$userrow['num-sessions'];?></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>Team Sessions:</td>
								<td><?=$teamrow['num-sessions'];?></td>
							</tr>
							<tr>
								<td>Date From:</td>
								<td><?=$dirarr['fromdate'];?></td>
								<td>Created By:</td>
								<td><?=$createdbyuserrs['fullname'];?></td>
							</tr>
							<tr>
								<td>Date To:</td>
								<td><?=$dirarr['todate'];?></td>
								<td>Create Date:</td>
								<td><?=date("Y-m-d");?></td>
							</tr>
							</table>

								<p>&nbsp;</p>

							<table width="100%" cellpadding="1" cellspacing="1"  border="0" class="bordered">
							<thead>
								<tr class="blue">
									<th width="10%">Group #</th>
									<th width="25%">Competency Group</th>
									<th width="10%">Competency #</th>
									<th width="25%">Competency</th>
									<th width="10%">Evaluations Held</th>
									<th width="10%">Avg Percentage</th>
									<th width="10%">Team Avg Percentage</th>
								</tr>
							</thead>
							<tbody>
								<?
									foreach($userrow['comparr']['groups'] as $groupid => $paramrow){
										$comparr = $compdescarr[$groupid];
										$g_i = $comparr['row']['index'];
								?>
								<tr class="orange">
									<td class="aln-c"><?=$g_i.'.';?></td>
									<td colspan="7"><?=$comparr['row']['title'];?></td>
								</tr>
									<?
										foreach($paramrow['params'] as $paramid => $sessrow){
											$comparr = $compdescarr[$groupid][$paramid];
											$team_comparr = $teamrow['comparr']['groups'][$groupid]['params'][$paramid];
											$p_i = $comparr['row']['index'];
									?>
										<tr>
											<td colspan="2"></td>
											<td class="aln-c"><?=$g_i.'.'.$p_i.'.';?></td>
											<td><?=$comparr['row']['title'];?></td>
											<td class="aln-c"><?=$sessrow['evaltimes'];?></td>
											<td class="aln-c"><?=$sessrow['evalavg'];?>%</td>
											<td class="aln-c"><?=$team_comparr['evalavg'];?>%</td>
										</tr>
									<?}?>
								<?}?>
							</tbody>
							</table>

							<?
								// set
								$chartarr = (isset($usergroups_chartimgpatharr[$userid])) ? $usergroups_chartimgpatharr[$userid] : array();
								$chartarr = (count($chartarr)) ? array_chunk($chartarr, 2) : array();
							?>

							<?if(count($chartarr)){?>
								<div style="page-break-after: always;"></div>

								<table width="100%" cellpadding="1" cellspacing="1"  border="0">
								<tbody>
								<?foreach($chartarr as $row){?>
								<tr>
									<td width="50%"><?=(isset($row[0]) ? '<img src="'.site_url($row[0]).'" width="350" />' : '');?></td>
									<td width="50%"><?=(isset($row[1]) ? '<img src="'.site_url($row[1]).'" width="350" />' : '');?></td>
								</tr>
								<?}?>
								</tbody>
								</table>
							<?}?>

							<?
								// set
								$chartarr = (isset($userparams_chartimgpatharr[$userid])) ? $userparams_chartimgpatharr[$userid] : array();
								$chartarr = (count($chartarr)) ? array_chunk($chartarr, 2) : array();
							?>

							<?if(count($chartarr)){?>
								<div style="page-break-after: always;"></div>

								<table width="100%" cellpadding="1" cellspacing="1"  border="0">
								<tbody>
								<?foreach($chartarr as $row){?>
								<tr>
									<td width="50%"><?=(isset($row[0]) ? '<img src="'.site_url($row[0]).'" width="350" />' : '');?></td>
									<td width="50%"><?=(isset($row[1]) ? '<img src="'.site_url($row[1]).'" width="350" />' : '');?></td>
								</tr>
								<?}?>
								</tbody>
								</table>
							<?}?>

						<?
							} // end-users
						} // end-teams
					} // end-managers
				} // end-groups
			} // end-directors
		} // end companies
	?>

</body>
</html>
