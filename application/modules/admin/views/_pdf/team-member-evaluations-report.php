<html>
<head>
	<title>PDF Document</title>
	<style type="text/css">
	body {
		font-size: 11px;
		font-family: 'Helvetica';
	}
	table, td, p {
		font-size: 11px;
	}

	/* table */
	table {
		page-break-after: avoid;
		// arial
	}
	table.bordered {
		background-color: #000;
	}
	table.bordered tr {
		background-color: #FFF;
	}
	table thead tr th, table.bordered thead tr th {
		background-color: #d9d9d9;
	}
	table tbody tr.row:nth-child(even) td {
		border-bottom: dotted 1px #CCCCCC;
	}
	table tbody tr.row:nth-child(even) td {
		background-color: #FAFAFA;
	}
	/*table tbody tr.blue > td:not(.no-bg) {*/
	table tbody tr.blue {
		background-color: #9bc2e6;
	}
	/*table tbody tr.green > td:not(.no-bg) {*/
	table tbody tr.green {
		background-color: #a9d08e;
	}
	table tbody tr.orange {
		background-color: #ffc000;
	}
	/*table tbody tr.lightgreen > td:not(.no-bg) {*/
	table tbody tr.lightgreen {
		background-color: #c6e0b4;
	}
	table tbody td.no-bg {
		background-color: #FFF;
	}

	/* -- notes -- */
	table.notes tr:nth-child(even) td {
		background-color: #FAFAFA;
	}
	table.notes td {
		padding: 4px;
	}
	table.notes td > div {
		border-top: dotted 1px #CCC;
		padding: 4px;
	}

	/* -- headings -- */
	.sec-heading {
		background: #d9d9d9;
		text-align: center;
		padding: 5px;
		margin: 0;
		border: solid 1px #000;
	}

	/* elements */
	.fnt-20		{ font-size: 20px; }
	.aln-c		{ text-align: center; }
	.aln-r		{ text-align: right; }
	.fnt-b		{ font-weight: bold; }
	.fnt-i		{ font-style: italic;}
	</style>
</head>
<body>

	<h1 class="sec-heading">REPORT: TEAM-MEMBER EVALUATIONS</h1>

		<p>&nbsp;</p>

	<?
		// companies
		foreach($statsarr as $companyid => $comparr){
			$ratingdescarr = $comparr['ratingdescarr'];

			// pagebreak
			if(isset($pagebreak)){
				echo '<div style="page-break-after: always;"></div>';
			}
			$pagebreak = 1;
			?>

			<h2 class="sec-heading"><?=$comparr['title'];?></h2>

				<p>&nbsp;</p>

			<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td>Date From:</td>
				<td><?=$comparr['fromdate'];?></td>
				<td>Created By:</td>
				<td><?=$createdbyuserrs['fullname'];?></td>
			</tr>
			<tr>
				<td>Date To:</td>
				<td><?=$comparr['todate'];?></td>
				<td>Create Date:</td>
				<td><?=date("Y-m-d");?></td>
			</tr>
			</table>

			<?if(isset($comparr['chartimgpatharr']['totalsummary-chart'])){?>
			<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td colspan="2" class="aln-c">
					<img src="<?=site_url($comparr['chartimgpatharr']['totalsummary-chart']);?>" width="500" />
				</td>
			</tr>
			</table>
			<?}?>

			<table width="100%" cellpadding="1" cellspacing="1"  border="0" class="bordered">
			<thead>
				<tr class="blue">
					<th width="35%">Manager</th>
					<!--<th width="10%">Feedback Item #</th>-->
					<th width="45%">Feedback Item</th>
					<th width="10%">Evaluations Provided</th>
					<th width="10%">Avg Percentage</th>
				</tr>
			</thead>
			<tbody>

			<?
			// directors
			foreach($comparr['directors'] as $mduserid => $grouprow){
				// groups
				foreach($grouprow['groups'] as $companyusergroupid => $grouprow){
						// managers
						foreach($grouprow['managers'] as $smuserid => $managerrow){
					?>
					<tr class="orange">
						<td colspan="3"><?=$managerrow['name'];?></td>
						<td class="aln-c"><?=$managerrow['ratings']['num-avg'];?>%</td>
					</tr>
						<?
							foreach($managerrow['rating-rows'] as $ratingid => $ratingrow){
								$rating = $ratingdescarr[$ratingid];
						?>
							<tr>
								<td></td>
								<!--<td class="aln-c"></td>-->
								<td><?=$rating;?></td>
								<td class="aln-c"><?=$ratingrow['num-rated'];?></td>
								<td class="aln-c"><?=$ratingrow['num-avg'];?>%</td>
							</tr>
						<?
						} // end-ratings
					} // end-managers
				} // end-groups
			} // end-directors
			?>
			</tbody>
			</table>
			<?
		} // end companies
	?>

</body>
</html>
