<html>
<head>
	<title>PDF Document</title>
	<style type="text/css">
	body {
		font-size: 11px;
		font-family: 'Helvetica';
	}
	table, td, p {
		font-size: 11px;
	}

	/* table */
	table {
		page-break-after: avoid;
		// arial
	}
	table.bordered {
		background-color: #000;
	}
	table.bordered tr {
		background-color: #FFF;
	}
	table thead tr th, table.bordered thead tr th {
		background-color: #d9d9d9;
	}
	table tbody tr.row:nth-child(even) td {
		border-bottom: dotted 1px #CCCCCC;
	}
	table tbody tr.row:nth-child(even) td {
		background-color: #FAFAFA;
	}
	/*table tbody tr.blue > td:not(.no-bg) {*/
	table tbody tr.grey {
		background-color: #d9d9d9;
	}
	table tbody tr.blue {
		background-color: #9bc2e6;
	}
	table tbody tr.lightblue {
		background-color: #bdd7ee;
	}
	/*table tbody tr.green > td:not(.no-bg) {*/
	table tbody tr.green {
		background-color: #a9d08e;
	}
	table tbody tr.lightgreen {
		background-color: #c6e0b4;
	}
	table tbody td.no-bg {
		background-color: #FFF;
	}

	/* -- notes -- */
	table.notes tr:nth-child(even) td {
		background-color: #FAFAFA;
	}
	table.notes td {
		padding: 4px;
	}
	table.notes td > div {
		border-top: dotted 1px #CCC;
		padding: 4px;
	}

	/* -- headings -- */
	.sec-heading {
		background: #d9d9d9;
		text-align: center;
		padding: 5px;
		margin: 0;
		border: solid 1px #000;
	}

	/* elements */
	.fnt-20		{ font-size: 20px; }
	.aln-c		{ text-align: center; }
	.aln-r		{ text-align: right; }
	.fnt-b		{ font-weight: bold; }
	.fnt-i		{ font-style: italic;}
	</style>
</head>
<body>

	<h1 class="sec-heading">REPORT: COACHING SESSION ADHERENCE</h1>

		<p>&nbsp;</p>

	<?
		// companies
		foreach($statsarr as $companyid => $dirarr){
			// pagebreak
			if(isset($pagebreak)){
				echo '<div style="page-break-after: always;"></div>';
			}
			$pagebreak = 1;
	?>

	<h2 class="sec-heading"><?=$dirarr['title'];?></h2>

		<p>&nbsp;</p>

	<table width="100%" cellpadding="1" cellspacing="1">
	<tr>
		<td>Date From:</td>
		<td><?=$dirarr['fromdate'];?></td>
		<td>Created By:</td>
		<td><?=$createdbyuserrs['fullname'];?></td>
	</tr>
	<tr>
		<td>Date To:</td>
		<td><?=$dirarr['todate'];?></td>
		<td>Create Date:</td>
		<td><?=date("Y-m-d");?></td>
	</tr>
	</table>

		<p>&nbsp;</p>

	<table width="100%" cellpadding="1" cellspacing="1">
	<tr>
		<td colspan="2" class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['totalsummary-chart']);?>" width="400" />
		</td>
	</tr>
	<tr>
		<td width="50%" class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['chart-director-percentages']);?>" width="365" />
		</td>
		<td width="50%" class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['chart-director-sessions']);?>" width="365" />
		</td>
	</tr>
	<tr>
		<td class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['chart-manager-percentages']);?>" width="365" />
		</td>
		<td class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['chart-manager-sessions']);?>" width="365" />
		</td>
	</tr>
	<tr>
		<td class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['chart-teammember-percentages']);?>" width="365" />
		</td>
		<td class="aln-c">
			<img src="<?=site_url($dirarr['chartimgpatharr']['chart-teammember-sessions']);?>" width="365" />
		</td>
	</tr>
	</table>

	<table width="100%" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th>Director</th>
			<th>Group</th>
			<th>Manager</th>
			<th>Team</th>
			<th>Member</th>
			<th class="width-150">Required Sessions</th>
			<th class="width-150">Sessions Held</th>
			<th class="width-150">% Adherence</th>
		</tr>
	</thead>
	<tbody>
		<tr class="grey">
			<td colspan="5"><?=$dirarr['title'];?></td>
			<td class="aln-c"><?=$dirarr['num-required'];?></td>
			<td class="aln-c"><?=$dirarr['num-completed'];?></td>
			<td class="aln-c"><?=$dirarr['required-percentage-completed'];?>%</td>
		</tr>
		<?
			// directors
			foreach($dirarr['directors'] as $mduserid => $dirrow){
		?>
		<tr class="blue">
			<td colspan="5"><?=$dirrow['director'];?></td>
			<td class="aln-c"><?=$dirrow['num-required'];?></td>
			<td class="aln-c"><?=$dirrow['num-completed'];?></td>
			<td class="aln-c"><?=$dirrow['required-percentage-completed'];?>%</td>
		</tr>
			<?
				// groups
				foreach($dirrow['groups'] as $companyusergroupid => $grouprow){
			?>
			<tr class="lightblue">
				<td></td>
				<td colspan="4"><?=$grouprow['title'];?></td>
				<td class="aln-c"><?=$grouprow['num-required'];?></td>
				<td class="aln-c"><?=$grouprow['num-completed'];?></td>
				<td class="aln-c"><?=$grouprow['required-percentage-completed'];?>%</td>
			</tr>
				<?
					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
				?>
				<tr class="green">
					<td colspan="2"></td>
					<td colspan="3"><?=$managerrow['manager'];?></td>
					<td class="aln-c"><?=$managerrow['num-required'];?></td>
					<td class="aln-c"><?=$managerrow['num-completed'];?></td>
					<td class="aln-c"><?=$managerrow['required-percentage-completed'];?>%</td>
				</tr>
					<?
						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
					?>
					<tr class="lightgreen">
						<td colspan="3"></td>
						<td colspan="2"><?=$teamrow['title'];?></td>
						<td class="aln-c"><?=$teamrow['num-required'];?></td>
						<td class="aln-c"><?=$teamrow['num-completed'];?></td>
						<td class="aln-c"><?=$teamrow['required-percentage-completed'];?>%</td>
					</tr>
						<?
							// teams
							foreach($teamrow['users'] as $userid => $userrow){
						?>
						<tr>
							<td colspan="4"></td>
							<td><?=$userrow['title'];?></td>
							<td class="aln-c"><?=$userrow['num-required'];?></td>
							<td class="aln-c"><?=$userrow['num-completed'];?></td>
							<td class="aln-c"><?=$userrow['required-percentage-completed'];?>%</td>
						</tr>
						<?}?>
					<?}?>
				<?}?>
			<?}?>
		<?}?>
	</tbody>
	</table>
	<?}?>

</body>
</html>
