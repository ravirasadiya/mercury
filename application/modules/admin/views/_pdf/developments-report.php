<html>
<head>
	<title>PDF Document</title>
	<style type="text/css">
	body {
		font-size: 11px;
		font-family: 'Helvetica';
	}
	table, td, p {
		font-size: 11px;
	}

	/* table */
	table {
		page-break-after: avoid;
		// arial
	}
	table.bordered {
		background-color: #000;
	}
	table.bordered tr {
		background-color: #FFF;
	}
	table thead tr th, table.bordered thead tr th {
		background-color: #d9d9d9;
	}
	table tbody tr.row:nth-child(even) td {
		border-bottom: dotted 1px #CCCCCC;
	}
	table tbody tr.row:nth-child(even) td {
		background-color: #FAFAFA;
	}
	/*table tbody tr.blue > td:not(.no-bg) {*/
	table tbody tr.blue {
		background-color: #9bc2e6;
	}
	/*table tbody tr.green > td:not(.no-bg) {*/
	table tbody tr.green {
		background-color: #a9d08e;
	}
	table tbody tr.orange {
		background-color: #ffc000;
	}
	/*table tbody tr.lightgreen > td:not(.no-bg) {*/
	table tbody tr.lightgreen {
		background-color: #c6e0b4;
	}
	table tbody td.no-bg {
		background-color: #FFF;
	}

	/* -- notes -- */
	table.notes tr:nth-child(even) td {
		background-color: #FAFAFA;
	}
	table.notes td {
		padding: 4px;
	}
	table.notes td > div {
		border-top: dotted 1px #CCC;
		padding: 4px;
	}

	/* -- headings -- */
	.sec-heading {
		background: #d9d9d9;
		text-align: center;
		padding: 5px;
		margin: 0;
		border: solid 1px #000;
	}

	/* elements */
	.fnt-20		{ font-size: 20px; }
	.aln-c		{ text-align: center; }
	.aln-r		{ text-align: right; }
	.fnt-b		{ font-weight: bold; }
	.fnt-i		{ font-style: italic;}
	</style>
</head>
<body>

	<h1 class="sec-heading">REPORT: DEVELOPMENTS</h1>

		<p>&nbsp;</p>

	<?
		// companies
		foreach($statsarr as $companyid => $comparr){
			$compdescarr = $comparr['compdescarr'];

			// directors
			foreach($comparr['directors'] as $mduserid => $dirrow){
				// check
				if(!is_numeric($mduserid)){
					continue;
				}

				// groups
				foreach($dirrow['groups'] as $companyusergroupid => $grouprow){
					// check
					if(!is_numeric($companyusergroupid)){
						continue;
					}

					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
						// check
						if(!is_numeric($smuserid)){
							continue;
						}

						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
							// check
							if(!is_numeric($companyuserteamid)){
								continue;
							}

							// sessions
							foreach($teamrow['users'] as $userid => $userrow){

								// pagebreak
								if(isset($pagebreak)){
									echo '<div style="page-break-after: always;"></div>';
								}
								$pagebreak = 1;
						?>

							<h2 class="sec-heading"><?=$userrow['name'];?><br /><small><?=$teamrow['title'];?></small></h2>

								<p>&nbsp;</p>

							<table width="100%" cellpadding="1" cellspacing="1">
							<tr>
								<td width="20%">Manager:</td>
								<td width="30%"><?=$userrow['manager'];?></td>
								<td width="20%">Coaching Sessions:</td>
								<td width="30%"><?=$userrow['num-sessions'];?></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>Team Sessions:</td>
								<td><?=$teamrow['num-sessions'];?></td>
							</tr>
							<tr>
								<td>Date From:</td>
								<td><?=$comparr['fromdate'];?></td>
								<td>Created By:</td>
								<td><?=$createdbyuserrs['fullname'];?></td>
							</tr>
							<tr>
								<td>Date To:</td>
								<td><?=$comparr['todate'];?></td>
								<td>Create Date:</td>
								<td><?=date("Y-m-d");?></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td>Completed:</td>
								<td><?=$userrow['num-suggs-completed'];?></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Missed:</td>
								<td><?=$userrow['num-suggs-missed'];?></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Planned:</td>
								<td><?=$userrow['num-suggs-planned'];?></td>
								<td></td>
								<td></td>
							</tr>
							</table>

							<table width="100%" cellpadding="1" cellspacing="1">
							<tr>
								<td colspan="2" class="aln-c">
									<img src="<?=site_url($userrow['chartimgpatharr']['totalsummary-chart']);?>" width="500" />
								</td>
							</tr>
							</table>

							<table width="100%" cellpadding="1" cellspacing="1"  border="0" class="bordered">
							<thead>
								<tr class="blue">
									<th width="10%">Group #</th>
									<th width="15%">Competency Group</th>
									<th width="10%">Competency #</th>
									<th width="15%">Competency</th>
									<th width="10%">Action Item</th>
									<th width="10%">Date Created</th>
									<th width="10%">Date Completed</th>
									<th width="10%">Duration</th>
									<th width="10%">Status</th>
								</tr>
							</thead>
							<tbody>
								<?
									foreach($userrow['comparr']['groups'] as $groupid => $grouprow){
										$descarr = $compdescarr[$groupid];
										$g_i = $descarr['row']['index'];
								?>
								<tr class="orange">
									<td class="aln-c"><?=$g_i.'.';?></td>
									<td colspan="8"><?=$descarr['row']['title'];?></td>
								</tr>
									<?
										foreach($grouprow['params'] as $paramid => $paramrow){
											$descarr = $compdescarr[$groupid][$paramid];
											$p_i = $descarr['row']['index'];

											// check
											if(!isset($paramrow['suggs'])){
												continue;
											}
									?>
										<tr>
											<td colspan="2"></td>
											<td class="aln-c"><?=$g_i.'.'.$p_i.'.';?></td>
											<td colspan="6"><?=$descarr['row']['title'];?></td>
										</tr>
										<?
											foreach($paramrow['suggs'] as $suggrow){
												$suggid = $suggrow['id'];
												$descarr = $compdescarr[$groupid][$paramid][$suggid];
												$s_i = $descarr['row']['index'];
										?>
											<tr>
												<td colspan="4"></td>
												<td><?=$descarr['row']['title'];?></td>
												<td class="aln-c"><?=$suggrow['createdon'];?></td>
												<td class="aln-c"><?=$suggrow['completedon'];?></td>
												<td class="aln-c"><?=$suggrow['duration'];?></td>
												<td class="aln-c"><?=$suggrow['status'];?></td>
											</tr>
										<?}?>
									<?}?>
								<?}?>
							</tbody>
							</table>

						<?
							} // end-users
						} // end-teams
					} // end-managers
				} // end-groups
			} // end-directors
		} // end companies
	?>

</body>
</html>
