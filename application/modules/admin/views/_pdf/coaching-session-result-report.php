<html>
<head>
	<title>PDF Document</title>
	<style type="text/css">
	body {
		font-size: 11px;
		font-family: 'Helvetica';
	}
	table, td, p {
		font-size: 11px;
	}

	/* table */
	table {
		page-break-after: avoid;
		// arial
	}
	table.bordered {
		background-color: #000;
	}
	table.bordered tr {
		background-color: #FFF;
	}
	table thead tr th, table.bordered thead tr th {
		background-color: #d9d9d9;
	}
	table tbody tr.row:nth-child(even) td {
		border-bottom: dotted 1px #CCCCCC;
	}
	table tbody tr.row:nth-child(even) td {
		background-color: #FAFAFA;
	}
	/*table tbody tr.blue > td:not(.no-bg) {*/
	table tbody tr.blue {
		background-color: #9bc2e6;
	}
	/*table tbody tr.green > td:not(.no-bg) {*/
	table tbody tr.green {
		background-color: #a9d08e;
	}
	/*table tbody tr.lightgreen > td:not(.no-bg) {*/
	table tbody tr.lightgreen {
		background-color: #c6e0b4;
	}
	table tbody td.no-bg {
		background-color: #FFF;
	}

	/* -- notes -- */
	table.notes tr:nth-child(even) td {
		background-color: #FAFAFA;
	}
	table.notes td {
		padding: 4px;
	}
	table.notes td > div {
		border-top: dotted 1px #CCC;
		padding: 4px;
	}

	/* -- headings -- */
	.sec-heading {
		background: #d9d9d9;
		text-align: center;
		padding: 5px;
		margin: 0;
		border: solid 1px #000;
	}

	/* elements */
	.fnt-20		{ font-size: 20px; }
	.aln-c		{ text-align: center; }
	.aln-r		{ text-align: right; }
	.fnt-b		{ font-weight: bold; }
	.fnt-i		{ font-style: italic;}
	</style>
</head>
<body>

	<h1 class="sec-heading">COACHING SESSION RESULTS</h1>

		<p>&nbsp;</p>

	<table width="100%" cellpadding="1" cellspacing="1">
	<tr>
		<td width="20%">Manager:</td>
		<td width="30%"><?=$coachingsessionrs['salesmanager'];?></td>
		<td width="20%">Date:</td>
		<td width="30%"><?=date("Y-m-d");?></td>
	</tr>
	<tr>
		<td>Team:</td>
		<td><?=$coachingsessionrs['spteam'];?></td>
		<td>Session Completed:</td>
		<td><?=date("Y-m-d", strtotime($coachingsessionrs['completedon']));?></td>
	</tr>
	<tr>
		<td>Member:</td>
		<td><?=$coachingsessionrs['salesperson'];?></td>
		<td>Created By:</td>
		<td><?=$createdbyuserrs['fullname'];?></td>
	</tr>
	<tr>
		<td>Customer:</td>
		<td><?=$coachingsessionrs['customertitle'];?></td>
		<td></td>
		<td></td>
	</tr>
	</table>

		<p>&nbsp;</p>

	<h2 class="sec-heading">Competency Ratings</h2>

	<table width="100%" cellpadding="1" cellspacing="1"  border="0" class="bordered">
	<thead>
		<tr>
			<th width="25%">Competency Group</th>
			<th width="60%">Competency</th>
			<th width="15%">Rating</th>
		</tr>
	</thead>
	<tbody>
		<?
			foreach($statsarr['groups'] as $competencygroupid => $grouprow){
				$g_i = $grouprow['row']['index'];
				$ind = $g_i.'.';
		?>
		<tr class="blue">
			<td colspan="2"><?=$ind.' '.$grouprow['row']['title'];?></td>
			<td class="aln-c"><?=(($grouprow['row']['avg']) ? $grouprow['row']['avg'].'%' : '');?></td>
		</tr>
			<?
				foreach($grouprow['params'] as $competencyparameterid => $paramrow){
					$p_i = $paramrow['row']['index'];
					$ind = $g_i.'.'.$p_i.'.';
			?>
			<tr>
				<td></td>
				<td><?=$ind.' '.$paramrow['row']['title'];?></td>
				<td class="aln-c"><?=(($paramrow['row']['val']) ? $paramrow['row']['val'].'%' : '');?></td>
			</tr>
			<?}?>
		<?}?>
	</tbody>
	</table>

		<div style="page-break-after: always;"></div>

	<h2 class="sec-heading">Development Plan</h2>

	<table width="100%" cellpadding="1" cellspacing="1" border="0" class="bordered">
	<thead>
		<tr>
			<th width="25%">Competency Group</th>
			<th width="20%">Competency</th>
			<th width="40%">Development Suggestion</th>
			<th width="15%">Due Date</th>
		</tr>
	</thead>
	<tbody>
		<?
			foreach($statsarr['groups'] as $competencygroupid => $grouprow){
				$g_i = $grouprow['row']['index'];
				$ind = $g_i.'.';
		?>
		<tr class="green">
			<td colspan="4"><?=$ind.' '.$grouprow['row']['title'];?></td>
		</tr>
			<?
				foreach($grouprow['params'] as $competencyparameterid => $paramrow){
					$p_i = $paramrow['row']['index'];
					$ind = $g_i.'.'.$p_i.'.';

					if(!isset($paramrow['suggs'])){
						continue;
					}
			?>
			<tr class="lightgreen">
				<td class="no-bg"></td>
				<td colspan="3"><?=$ind.' '.$paramrow['row']['title'];?></td>
			</tr>
				<?
					foreach($paramrow['suggs'] as $competencysuggestionid => $suggrow){
						$s_i = $suggrow['row']['index'];
						$ind = $g_i.'.'.$p_i.'.'.$s_i.'.';
				?>
				<tr>
					<td class="no-bg"></td>
					<td class="no-bg"></td>
					<td><?=$ind.' '.$suggrow['row']['title'];?></td>
					<td class="aln-c"><?=$suggrow['row']['dueon'];?></td>
				</tr>
				<?}?>
			<?}?>
		<?}?>
	</tbody>
	</table>

		<div style="page-break-after: always;"></div>

	<h2 class="sec-heading">Notes</h2>

	<table width="100%" cellpadding="1" cellspacing="1" class="notes">
	<?foreach($usernoters as $row){?>
	<tr>
		<td>
			<?=nl2br($row['notes']);?>
			<div class="fnt-i aln-r"><?=$row['createdon'].' : '.$row['createdby'];?></div>
		</td>
	</tr>
	<?}?>
	</table>

</body>
</html>
