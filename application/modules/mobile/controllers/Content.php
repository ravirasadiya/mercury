<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	public function parts($part=null)
	{
		// set
		$data = array();
		$retarr = array();

		// switch
		switch($part){
			case 'modals':
				// get
				$data['termsrs'] = $this->content_model->get(null, array('c.type' => 'terms-and-conditions'), null, true);
				$data['pripolicyrs'] = $this->content_model->get(null, array('c.type' => 'privacy-policy'), null, true);

				// load view
				$retarr['view'] = $this->load->view('mobile/_parts/modals', $data, true);
			break;
		}


		// echo
		echo_json($retarr);
	}


	public function page($page=null)
	{
		// set
		$data = array('loadview' => 'mobile/content/welcome');
		$retarr = array();

		// switch
		switch($page){
			case 'welcomevideo':
				//get
				$data['welcomevidrs'] = $this->content_model->get(null, array('c.type' => 'welcome-video'), null, true);

				// set
				$data['videoembed'] = null;

				// check
				if($data['welcomevidrs'] && $data['welcomevidrs']['videourl']){
					// get vimeo
					$data['videoembed'] = ($data['welcomevidrs']['videoembedcode']) ? $data['welcomevidrs']['videoembedcode'] : $this->vimeo->get_video($data['welcomevidrs']['videourl']);
				}

				$data['welcomevideoheader'] = true;
				$data['nofooter'] = true;
				$data['loadview'] = 'mobile/content/welcome-video';
			break;
			case 'welcome':
				$data['nobackbtn'] = true;
				$data['loadview'] = 'mobile/content/welcome';
			break;
			case 'menu':
				// get
				$userrs = (USERID) ? $this->user_model->get(USERID) : null;

				// check
				if(!USERID){
					$retarr['logout'] = 1;
				}

				// get
				$data['userpackagelevelrs'] = $userpackagelevelrs = ($userrs) ? $this->userpackagelevel_model->get(null, array('o.userid' => USERID, 'upl.expireson >=' => date("Y-m-d")), array('where-str' => 'o.paidon IS NOT NULL'), true) : null;
				$data['packageplayertypers'] = $this->packageplayertype_model->get();
				$data['lessonrs'] = $lessonrs = ($data['userpackagelevelrs']) ? $this->lesson_model->get_hier_array(null, array('l.packagelevelid' => $data['userpackagelevelrs']['packagelevelid'])) : array();
				$data['lessonsbusyarr'] = array();
				$data['lessonscompletearr'] = array();
				$data['slideshowrs'] = $this->slideshow_model->get(null, array('s.type' => 'app-intro'), null, true);

				// check
				if($userrs && $userpackagelevelrs){
					// update
					$this->user_model->save(array('lastloginon' => date("Y-m-d H:i:s")), USERID);

					// check
					if($userpackagelevelrs['playercolor']){
						// set
						$retarr['customcss'] = config_item('playertype-css');
						$retarr['customcss'] = str_replace('[clr]', $userpackagelevelrs['playercolor'], $retarr['customcss']);
					}

					// get
					$userprogressrs = $this->userprogress_model->get_lesson_hier_array(null, array('up.userid' => USERID, 'up.type' => 'video'));
					//echo '<pre>'; print_r($userprogressrs); echo '</pre>'; exit;
					//echo '<pre>'; print_r($lessonrs); echo '</pre>'; exit;

					// loop
					foreach($lessonrs as $week => $weekarr){


						// loop
						foreach($weekarr as $day => $dayarr){

							// loop
							$iscomplete = 0;
							$isbusy = 0;

							foreach($dayarr as $slotnum => $row){
								$numwatched = (isset($userprogressrs[$row['lessonid']]['video'])) ? count($userprogressrs[$row['lessonid']]['video']) : 0;
								//echo "$week -> $day -> $slotnum -> ".$row['numvideos'].' : '.$numwatched.'<br />';

								if($row['slotnum'] < 5){
									if(!$numwatched){
										//$isbusy = 0;
										$iscomplete = 0;
										//echo ' -- not started <br />';
										//break;
									}
									if($numwatched){
										$isbusy = 1;
										$iscomplete = 0;
										//echo ' -- is busy <br />';
									}
									if($row['numvideos'] == $numwatched){
										$iscomplete = 1;
										//echo ' -- is complete <br />';
									}
								}

							}
							if($isbusy){
								$data['lessonsbusyarr'][$week][$day] = 1;
							}
							if($iscomplete){
								$data['lessonscompletearr'][$week][$day] = 1;
							}
						}


					}
					//echo '<pre>'; print_r($data['lessonsbusyarr']); echo '</pre>';
					//echo '<pre>'; print_r($data['lessonscompletearr']); echo '</pre>'; exit;

					$data['loadview'] = 'mobile/packages/package-overview';
				}else{
					$retarr['customcss'] = 'body.loggedinmenu-page .page__background { background-color: #000; }';

					$data['loadview'] = 'mobile/content/menu';
				}
			break;
			case 'coolvidportal':
				// get
				$data['lessoncontentrs'] = $this->lessoncontent_model->get(null, array('lc.isfree' => 1));

				$data['loadview'] = 'mobile/content/cool-vid-portal';
			break;
			case 'guidedtour':
				// set
				$slideshowid = $this->input->get('slideshowid');

				// get
				$data['slideshowsliders'] = $this->slideshowslide_model->get(null, array('ss.slideshowid' => $slideshowid));

				$data['noheader'] = true;
				$data['nofooter'] = true;
				$data['loadview'] = 'mobile/content/guided-tour';
			break;
			case 'video':
				// set
				$lessoncontentid = $this->input->get('lessoncontentid');

				// get
				$lessoncontentrs = $this->lessoncontent_model->get($lessoncontentid);

				// get vimeo
				$data['videoembed'] = ($lessoncontentrs['videoembedcode']) ? $lessoncontentrs['videoembedcode'] : $this->vimeo->get_video($lessoncontentrs['fileurl']);

				//$data['videoembedurl'] = base_url('embed-vimeo.php') . '?vimeoembed='.base64_encode(urlencode($data['videoembed']));

				$data['videoheader'] = true;
				$data['nofooter'] = true;
				$data['loadview'] = 'mobile/content/video';
			break;
			case 'squashterms':
				// get
				$data['contentrs'] = $this->content_model->get(null, array('c.type' => 'squash-terms'), null, true);

				$data['loadview'] = 'mobile/content/squash-terms';
			break;
		}

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// load page
	public function loadpage($dir, $view)
	{
		// set
		$data = array();
		$retarr = array();

		// load view
		$retarr['view'] = $this->load->view('mobile/'.$dir.'/'.$view, $data, true);


		// echo
		echo_json($retarr);
	}
}

/* End of file content.php */
/* Location: ./application/modules/admin/controllers/content.php */
