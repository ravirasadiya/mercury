<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	// fanclub
	public function fanclub()
	{
		// set
		$data = array();
		$retarr = array();

		// set
		$data['nousermenu'] = 1;
		$data['loadview'] = 'mobile/news/fanclub';

		// get
		$data['newsrs'] = $this->news_model->get(null, array('type' => 'nicol-david'), array('order' => array('n.date' => 'DESC', 'n.newsid' => 'DESC')));

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}

}

/* End of file news.php */
/* Location: ./application/modules/admin/controllers/news.php */
