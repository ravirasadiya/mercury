<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companyusers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	// list groups
	public function listgroups()
	{
		// set
		$data = array();
		$retarr = array();

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$where = array('cug.userid' => USERID);
		$data['rs'] = $this->companyuserteam_model->get(null, $where);
		//echo '<pre>'; print_r($data['rs']); echo '</pre>'; exit;
		$data['statsarr'] = $this->report_model->md_menu_stats(USERID);
		//echo '<pre>'; print_r($data['rs']); echo '</pre>'; exit;

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/companyusers/.php' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/companyusers/.php' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/companyusers/md-list-groups.php' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// list teams
	public function listteams()
	{
		// set
		$data = array();
		$retarr = array();
		$salesmanageruserid = $this->input->get('salesmanageruserid');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['smuserrs'] = ($salesmanageruserid) ? $this->user_model->get($salesmanageruserid) : null;
		$data['statsarr'] = ($type == 'sales-person') ? $this->report_model->sp_menu_stats(USERID) : array();
		$data['statsarr'] = ($type == 'sales-manager') ? $this->report_model->sm_menu_stats(USERID) : $data['statsarr'];
		$data['statsarr'] = ($type == 'managing-director') ? $this->report_model->md_menu_stats(USERID) : $data['statsarr'];

		// get
		$where = ($type == 'sales-manager') ? array('cut.userid' => USERID) : array('cut.userid' => $salesmanageruserid);
		$data['rs'] = $this->companyuserteam_model->get(null, $where);

		//echo '<pre>'; print_r($data['rs']); echo '</pre>';
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/companyusers/.php' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/companyusers/sm-list-teams.php' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/companyusers/md-list-teams.php' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);

		// echo
		echo_json($retarr);
	}


	// list team users
	public function listteamusers()
	{
		// set
		$data = array();
		$retarr = array();
		$companyuserteamid = $this->input->get('companyuserteamid');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['companyuserteamrs'] = $this->companyuserteam_model->get($companyuserteamid);
		$data['rs'] = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $companyuserteamid));
		$data['statsarr'] = ($type == 'sales-person') ? $this->report_model->sp_menu_stats(USERID) : array();
		$data['statsarr'] = ($type == 'sales-manager') ? $this->report_model->sm_menu_stats(USERID) : $data['statsarr'];
		$data['statsarr'] = ($type == 'managing-director') ? $this->report_model->md_menu_stats(USERID) : $data['statsarr'];

		//echo '<pre>'; print_r($data['companyuserteamrs']); echo '</pre>';
		//echo '<pre>'; print_r($data['rs']); echo '</pre>';
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/companyusers/.php' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/companyusers/sm-list-team-users.php' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/companyusers/md-list-team-users.php' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// list dev teams
	public function listdevteams()
	{
		// set
		$data = array();
		$retarr = array();

		// get
		$data['companyuserteamrs'] = $this->companyuserteam_model->get(null, array('cut.userid' => USERID));

		// set
		$data['loadview'] = 'mobile/companyusers/list-devplan-teams.php';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// list dev team users
	public function listdevteamusers()
	{
		// set
		$data = array();
		$retarr = array();
		$companyuserteamid = $this->input->get('companyuserteamid');

		// get
		$data['companyuserteamrs'] = $this->companyuserteam_model->get($companyuserteamid);
		$data['companyuserteamrefrs'] = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $companyuserteamid));

		// set
		$data['loadview'] = 'mobile/companyusers/list-devplan-team-users.php';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// get team users
	public function get_teamusers()
	{
		// set
		$data = array();
		$retarr = array();
		$companyuserteamid = $this->input->get('companyuserteamid');

		// get
		$retarr['companyuserteamrefrs'] = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $companyuserteamid));

		// echo
		echo_json($retarr);
	}

}

/* End of file companyusers.php */
/* Location: ./application/modules/admin/controllers/companyusers.php */
