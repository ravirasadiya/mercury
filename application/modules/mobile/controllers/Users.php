<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	private $typearr = array('sales-person' => 'SP', 'sales-manager' => 'SM', 'managing-director' => 'MD');

	public function __construct()
	{
		parent::__construct();
	}


	// login
	public function login()
	{
		// set
		$data = array();
		$retarr = array();
		$form = $this->input->get('form');

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$form['email'] = clean_email($form['email']);

			// get
			$userrs = $this->user_model->get(null, array('u.email' => $form['email'], 'u.type!=' => 'admin'),null);

			if(count($userrs)>1){
				$tempArray = array();
				foreach ($userrs as $userrsone) {
					$newArray = array("userid"=>$userrsone["userid"],"type"=>$userrsone["type"],"isenabled"=>$userrsone["isenabled"]);
					array_push($tempArray,$newArray);

					// check
					if(!check_hash($form['password'], $userrsone['password'])){

						// msg
						$retarr['msg'] = 'Sorry, that\'s an incorrect password.';
						$retarr['status'] = 0;
						echo_json($retarr); exit;
						break;
					}
				}

				// msg
				$retarr['email'] = $form['email'];
				$retarr['msg'] = 'Welcome back.';
				$retarr['ismultiroles'] = true;

				// // redirect
				$retarr['redirect'] = 'home.html';
				
				// $retarr['msg'] = 'Welcome';
				// $retarr['status'] = 0;

				// echo
				echo_json($retarr); exit;
			}

			$userrs = $this->user_model->get(null, array('u.email' => $form['email'], 'u.type!=' => 'admin'), null, true);

			// check
			if($userrs){
				$type = $userrs['type'];
				$userid = $userrs['userid'];

				// check
				if(!check_hash($form['password'], $userrs['password'])){
					// msg
					$retarr['msg'] = 'Sorry, that\'s an incorrect password.';
					$retarr['status'] = 0;
				}else if(!$userrs['isenabled']){
					$retarr['msg'] = 'Sorry, your profile is not enabled.';
					$retarr['status'] = 0;
				}else{
					// set
					$retarr['userid'] = $userrs['userid'];
					$retarr['usertype'] = (isset($this->typearr[$userrs['type']])) ? $this->typearr[$userrs['type']] : '';

					// get
					$companyusergrouprefrs = $this->companyusergroupref_model->get(null, array('cugr.userid' => $userid), null, true);
					$companyuserteamrefrs = $this->companyuserteamref_model->get(null, array('cutr.userid' => $userid), null, true);

					// check
					if(!$type){
						$retarr['msg'] = 'No user type detected, please contact support.';
						$retarr['status'] = 0;
					}else if(($type == 'sales-manager' || $type == 'sales-person') && (!$companyusergrouprefrs && !$companyuserteamrefrs)){
						$retarr['msg'] = 'You are not assigned to a group or team. Please contact support.';
						$retarr['status'] = 0;
					}else{
						// msg
						$retarr['msg'] = 'Welcome back.';

						// redirect
						$retarr['redirect'] = 'menu.html';
					}
				}
			}else{
				$retarr['msg'] = 'That\'s an invalid e-mail address.';
				$retarr['status'] = 0;
			}

			// echo
			echo_json($retarr); exit;
		}


		// set
		$data['loadview'] = 'mobile/users/login';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}

	// forgot password
	public function forgotpassword()
	{
		// set
		$data = array();
		$retarr = array();
		$form = $this->input->get('form');

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$form['email'] = clean_email($form['email']);

			// get
			$userrs = $this->user_model->get(null, array('u.email' => $form['email'], 'u.type!=' =>'admin'), null, true);

			// check
			if($userrs){
				// set
				$userrs['password'] = $pass = generate_password(5);

				// save
				$this->user_model->save(array('password' => get_hash($pass)), $userrs['userid']);

				// send e-mail
				$emarr = array(
						'to-email' => $userrs['email'],
						'subject' => ADMIN_TITLE.' Password Reset',
						'msg' => $this->load->view('admin/_email/user-password-reset', array('userrs' => $userrs), true)
					);
				send_email($emarr);

				// msg
				$retarr['msg'] = 'Your password was reset and the new login details e-mailed to you.';

				// redirect
				$retarr['redirect'] = 'login.html';
			}else{
				$retarr['msg'] = 'We could not find your e-mail address.';
				$retarr['status'] = 0;
			}

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = 'mobile/users/forgot-password';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// profile
	public function profile()
	{
		// set
		$data = array();
		$retarr = array();
		$form = $this->input->get('form');

		// get
		$data['rs'] = $userrs = $this->user_model->get(USERID);

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$form['email'] = clean_email($form['email']);

			// get
			$email_userrs = (isset($form['email'])) ? $this->user_model->get(null, array('u.email' => $form['email'], 'u.type!=' => 'admin'), null, true) : null;

			if($email_userrs && $email_userrs['userid'] != USERID){
				// msg
				$retarr['msg'] = 'The e-mail address is already registered. Please login or use the forgot password option.';
				$retarr['status'] = 0;
			}else{
				// hash
				if(isset($form['password']) && $form['password']){
					$form['password'] = get_hash($form['password']);
				}
				if(isset($form['password']) && !$form['password']){
					unset($form['password']);
				}

				// save
				$this->user_model->save($form, USERID);

				// msg
				$retarr['msg'] = 'You have successfully updated your profile.';

				//set
				$usertype = (isset($this->typearr[$userrs['type']])) ? $this->typearr[$userrs['type']] : '';

				// redirect
				$retarr['redirect'] = 'menu.html';
			}

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = 'mobile/users/profile';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// update tokens
	function updatetokens()
	{
		// post
		$form = $this->input->get('form');

		// get
		$userrs = $this->user_model->get(USERID);

		// check
		if($userrs && $form['deviceuuid'] && $form['pushtoken']){
			// save
			$this->user_model->save($form, USERID);
		}
	}


	// feedback
	public function feedback()
	{
		// set
		$data = array();
		$retarr = array();
		$form = $this->input->get('form');

		// get
		$data['userrs'] = $this->user_model->get(USERID);

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$form['email'] = clean_email($form['email']);

			// update
			$form['platform'] = (PLATFORM == 'ios') ? 'IOS' : 'Android';

			// send e-mail
			$emarr = array(
					'to-email' => TO_MOBILE_FEEDBACK_EMAIL,
					'subject' => ADMIN_TITLE.' Feedback',
					'msg' => $this->load->view('mobile/_email/admin-feedback', array('form' => $form), true)
				);
			send_email($emarr);

			// msg
			$retarr['msg'] = 'Hi, thank you for the feedback, we really appreciate it.';

			// redirect
			$retarr['redirect'] = 'loggedinmenu.html';

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = 'mobile/users/feedback';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// menu
	public function menu()
	{
		// set
		$data = array();
		$retarr = array();
		$data['missed_deadlines'] = '';

		// get
		$userrs = (USERID) ? $this->user_model->get(USERID) : null;
		$type = ($userrs) ? $userrs['type'] : null;
		$data['useremail'] = ($userrs) ? $userrs['email'] : null;

		// check
		if($type == 'sales-person'){
			// get
			$data['rs'] = $rs = $this->report_model->sp_menu_stats(USERID);
			$arr = array(round($rs['avg']), round(100-$rs['avg']));
			$arr = array_map('trim', $arr);
			$data['chartdata'] = json_encode($arr);

			// missed deadlines
			$data['misseddeadlinesrs'] = $rs = $this->report_model->sp_misseddeadlines_stats(USERID);
			$data['missed_deadlines'] = ($data['misseddeadlinesrs']['total']) ? '<i class="menu-badge">'.$data['misseddeadlinesrs']['total'].'</i>' : '';
		}
		if($type == 'sales-manager'){
			// get
			$data['rs'] = $rs = $this->report_model->sm_menu_stats(USERID);
			$arr = array(round($rs['avg']), round(100-$rs['avg']));
			$arr = array_map('trim', $arr);
			$data['chartdata'] = json_encode($arr);

			// missed deadlines
			$data['misseddeadlinesrs'] = $rs = $this->report_model->sm_misseddeadlines_stats(USERID);
			$data['missed_deadlines'] = ($data['misseddeadlinesrs']['total']) ? '<i class="menu-badge">'.$data['misseddeadlinesrs']['total'].'</i>' : '';
		}
		if($type == 'managing-director'){
			// get
			$data['rs'] = $rs = $this->report_model->md_menu_stats(USERID);
			$arr = array(round($rs['avg']), round(100-$rs['avg']));
			$arr = array_map('trim', $arr);
			$data['chartdata'] = json_encode($arr);

			// missed deadlines
			$data['misseddeadlinesrs'] = $rs = $this->report_model->md_misseddeadlines_stats(USERID);
			$data['missed_deadlines'] = ($data['misseddeadlinesrs']['total']) ? '<i class="menu-badge">'.$data['misseddeadlinesrs']['total'].'</i>' : '';
		}

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/users/sp-menu' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/users/sm-menu' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/users/md-menu' : $data['loadview'];
		$data['loadview'] = (!$type) ? 'mobile/users/login' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}

	// notes
	public function notes()
	{
		try {
			// set
			$data = array();
			$retarr = array();
			$usernoteid = $this->input->get('usernoteid');
			$salespersonaluserid = $this->input->get('salespersonaluserid');
			$salesmanageruserid = $this->input->get('salesmanageruserid');
			$coachingsessionid = $this->input->get('coachingsessionid');
			$companyuserteamid = $this->input->get('companyuserteamid');
			$ismodal = $this->input->get('ismodal');
			$form = $this->input->get('form');

			if($form==''){
				if($this->input->post('form')!=''){
					$usernoteid = $this->input->post('usernoteid');
					$salespersonaluserid = $this->input->post('salespersonaluserid');
					$salesmanageruserid = $this->input->post('salesmanageruserid');
					$coachingsessionid = $this->input->post('coachingsessionid');
					$companyuserteamid = $this->input->post('companyuserteamid');
					$ismodal = $this->input->post('ismodal');
					$form = $this->input->post('form');
					$notefile = $form['notefile'];
					$post_userid = $this->input->post('userid');
					unset($form['notefile']);
				}
			}

			//echo '<pre>'; print_r($form); echo '</pre>'; exit;

			// get
			$userrs = (USERID?USERID:(isset($post_userid)?$post_userid:null)) ? $this->user_model->get(USERID?USERID:(isset($post_userid)?$post_userid:null)) : null;
			$type = ($userrs) ? $userrs['type'] : null;

			// process
			if($form){
				// post
				$form = array_map('trim', $form);
				$norefresh = $this->input->get('norefresh');

				// update
				$form['userid'] = USERID?USERID:(isset($post_userid)?$post_userid:null);

				// if(isset($usernoteid) && !empty($usernoteid)){
				// 	$ondNote = $this->usernote_model->get($usernoteid);
				// }

				if($usernoteid!=null && $form['notes']==null){
					// $notedata = $this->usernote_model->get($usernoteid);
					// if($notedata['filelocation']==null){
					// 	// save
					// 	$noteid = $this->usernote_model->delete($usernoteid);
					// 	// msg
					// 	$retarr['msg'] = ($ismodal) ? 'The note was successfully delete.' : '';

					// 	echo_json($retarr); exit;
					// } else{
					// 	$form['notes'] = "Voice note";
					// 	$noteid = $this->usernote_model->save($form, $usernoteid);
					// 	$retarr['msg'] = ($ismodal) ? 'The note was successfully update.' : '';
					// 	echo_json($retarr); exit;
					// }
					$noteid = $this->usernote_model->delete($usernoteid);
						// msg
						$retarr['msg'] = 'The note was successfully deleted.';

						echo_json($retarr); exit;
				} elseif($form['notes']!=null){

					// save
					$noteid = $this->usernote_model->save($form, $usernoteid);

					if(isset($notefile) && !empty($notefile)){

						$contents_split = explode(',', $notefile);
						$encoded = $contents_split[count($contents_split)-1];
						$decoded = "";
						for ($i=0; $i < ceil(strlen($encoded)/256); $i++) {
						    $decoded = $decoded . base64_decode(substr($encoded,$i*256,256)); 
						}
						$timestamps = md5(time()).'.wav';
						$fp = fopen('uploads/_content/'.$timestamps, 'w');
						fwrite($fp, $decoded);
						fclose($fp);

						$content_form['usernoteid'] = $noteid;
						$content_form['type'] = 'content';
						$content_form['islocked'] = 1;

						$content_id = null;
						if($noteid){
							$content_info = $this->content_model->get(null,array('c.usernoteid'=>$noteid),null,true);
							$content_id = $content_info['contentid'];
						}

						$contentid = $this->content_model->save($content_form,$content_id);

						if($contentid){
							$attechment_lists = $this->contentattachment_model->get(null,array('c.contentid'=>$contentid));
							if(count($attechment_lists)!=0){
								foreach ($attechment_lists as $attechment) {
									$this->contentattachment_model->delete($attechment['contentattachmentid'],true);
								}
							}
						}

						$content_att_form['contentid'] = $contentid;
						$content_att_form['FileLocation'] = $timestamps;

						$this->contentattachment_model->save($content_att_form);
					}

					// msg
					$retarr['msg'] = ($ismodal) ? 'The note was successfully saved.' : '';

					//redirect
					// $retarr['redirect'] = 'user-notes.html';

				} elseif($form['notes']==null && !empty($notefile)){

					$form['notes'] = "Voice note";
					// save
					$noteid = $this->usernote_model->save($form, $usernoteid);

					if(isset($notefile) && !empty($notefile)){

						$contents_split = explode(',', $notefile);
						$encoded = $contents_split[count($contents_split)-1];
						$decoded = "";
						for ($i=0; $i < ceil(strlen($encoded)/256); $i++) {
						    $decoded = $decoded . base64_decode(substr($encoded,$i*256,256)); 
						}
						$timestamps = md5(time()).'.wav';
						$fp = fopen('uploads/_content/'.$timestamps, 'w');
						fwrite($fp, $decoded);
						fclose($fp);

						$content_form['usernoteid'] = $noteid;
						$content_form['type'] = 'content';
						$content_form['islocked'] = 1;

						$content_id = null;
						if($noteid){
							$content_info = $this->content_model->get(null,array('c.usernoteid'=>$noteid),null,true);
							$content_id = $content_info['contentid'];
						}

						$contentid = $this->content_model->save($content_form,$content_id);

						if($contentid){
							$attechment_lists = $this->contentattachment_model->get(null,array('c.contentid'=>$contentid));
							if(count($attechment_lists)!=0){
								foreach ($attechment_lists as $attechment) {
									$this->contentattachment_model->delete($attechment['contentattachmentid'],true);
								}
							}
						}

						$content_att_form['contentid'] = $contentid;
						$content_att_form['FileLocation'] = $timestamps;

						$this->contentattachment_model->save($content_att_form);
					}

					// msg
					$retarr['msg'] = ($ismodal) ? 'The note was successfully saved.' : '';
				} else{
					
					$retarr['msg'] = ($ismodal) ? 'Please add note or voice' : '';

					echo_json($retarr); exit;	
				}

				// echo
				if($norefresh){
					echo_json($retarr); exit;
				}
			}

			// set
			$where = null;
			$other = array('where-str' => '(cs.salesmanageruserid='.USERID.' OR cs.salespersonaluserid='.USERID.' OR un.userid='.USERID.') AND cs.date>DATE("'.FROM_LIMIT_DATE.'")');
			/*if($salespersonaluserid){
				$other = array('where-str' => '(cs.salesmanageruserid='.USERID.' AND cs.salespersonaluserid='.$salespersonaluserid.') OR (un.salesmanageruserid='.USERID.' AND un.salespersonaluserid='.$salespersonaluserid.')');
			}*/
			if($coachingsessionid){
				$where = array('un.coachingsessionid' => $coachingsessionid);
				$other = null;
			}

			// get
			//$data['usernoters'] = $this->usernote_model->get(null, array('un.userid' => USERID));
			$data['usernoters'] = $this->usernote_model->get(null, $where, $other);

			$data['spuserrs'] = ($salespersonaluserid) ? $this->user_model->get($salespersonaluserid) : null;
			$data['coachingsessionrs'] = ($coachingsessionid) ? $this->coachingsession_model->get($coachingsessionid) : null;
			$data['companyuserteamrs'] = ($companyuserteamid) ? $this->companyuserteam_model->get($companyuserteamid) : null;
			//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>'; exit;

			// check
			if($type == 'sales-manager' && !$salespersonaluserid){
				//$data['coachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salespersonaluserid' => $salespersonaluserid), array('where-str' => 'cs.cancelledon IS NULL AND cs.date>DATE("'.FROM_LIMIT_DATE.'")'));
				$data['sparr'] = array();

				// loop
				foreach($data['usernoters'] as $i => $row){
					// check
					if($row['salespersonaluserid']){
						$data['sparr'][$row['salespersonaluserid']] = $row['salesperson'];

						// unset
						unset($data['usernoters'][$i]);
					}
				}
				asort($data['sparr']);
			}
			if($type == 'sales-manager'){
				// check
				if(!$companyuserteamid && !$salespersonaluserid && !$coachingsessionid){
					$data['smteamarr'] = table_to_array('companyuserteam', 'companyuserteamid', 'title', array('userid' => USERID?USERID:(isset($post_userid)?$post_userid:null), 'deletedon' => 'IS NULL'), array());
				}
				if($salespersonaluserid && !$coachingsessionid){
					$data['coachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salespersonaluserid' => $salespersonaluserid), array('where-str' => 'cs.cancelledon IS NULL AND cs.date>DATE("'.FROM_LIMIT_DATE.'")'));
				}
				if($companyuserteamid && !$salespersonaluserid && !$coachingsessionid){
					$data['coachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salesmanageruserid' => USERID?USERID:(isset($post_userid)?$post_userid:null), 'sp_cut.companyuserteamid' => $companyuserteamid), array('where-str' => 'cs.cancelledon IS NULL AND cs.date>DATE("'.FROM_LIMIT_DATE.'")'));
					$data['coachingsessionrs'] = rs_to_keyval_array($data['coachingsessionrs'], 'salespersonaluserid', 'salesperson');
				}
				//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>'; exit;
			}
			if($type == 'sales-person' && !$coachingsessionid){
				$data['coachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salespersonaluserid' => USERID), array('where-str' => 'cs.cancelledon IS NULL AND cs.date>DATE("'.FROM_LIMIT_DATE.'")'));
				//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>'; exit;
			}




			// set
			$data['loadview'] = ($type == 'managing-director') ? '' : '';
			$data['loadview'] = ($type == 'sales-manager') ? 'mobile/users/sm-notes' : $data['loadview'];
			$data['loadview'] = ($type == 'sales-manager' && !$companyuserteamid && !$salespersonaluserid && !$coachingsessionid) ? 'mobile/users/sm-notes-teams' : $data['loadview'];
			
			$data['loadview'] = ($type == 'sales-manager' && $salespersonaluserid) ? 'mobile/users/sm-notes-sessions-user' : $data['loadview'];
			$data['loadview'] = ($type == 'sales-manager' && $coachingsessionid) ? 'mobile/users/sm-notes-user' : $data['loadview'];
			$data['loadview'] = ($type == 'sales-person') ? 'mobile/users/sp-notes-sessions' : $data['loadview'];
			$data['loadview'] = ($type == 'sales-person' && $coachingsessionid) ? 'mobile/users/sp-notes' : $data['loadview'];

			// load view
			$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
				
			// $fp = fopen('data.txt','a');//opens file in append mode  
			// fwrite($fp,json_encode($retarr));
			// fclose($fp);
			// echo default
			echo_json($retarr);
		} catch (Exception $e) {
			
		}
	}


	public function deadline()
	{
		// set
		$data = array();
		$retarr = array();
		$deadlineid = $this->input->get('deadlineid');
		$deadlinetype = $this->input->get('check');
		$form = $this->input->get('form');

		// get
		$userrs = (USERID) ? $this->user_model->get(USERID) : null;
		$type = ($userrs) ? $userrs['type'] : null;

		if($type == 'sales-manager')
		{	
			// post
			$form = array_map('trim', $form);
			$deadlineid = ($deadlineid) ? $deadlineid : null;

			// update
			$arr['userid'] = USERID;
			$arr['deadlinetype'] = $deadlinetype;
			$arr['companyuserteamid'] = $form['companyuserteamid'];
			$arr['noofdeadline'] = $form['noofdeadline'];
			if(!empty($form['notificationdate'])){
				$arr['notificationdate'] = $form['notificationdate'];
			}

			$deadlineid = $this->deadlinesetting_model->save($arr,$deadlineid);
			$retarr['deadlineid'] = json_encode($deadlineid);

			$retarr['msg'] = 'The deadline setting successfully saved.';

		}
		if($type == 'sales-person')
		{
			$retarr['msg'] = 'The deadline setting successfully saved.';
		}

		$retarr['redirect'] = 'deadlines.html';
		// load view
		// $retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);

		// echo default
		// echo json_encode($data);
		echo_json($retarr);
	}


	// save notes
	public function savenotes()
	{
		// set
		$data = array();
		$retarr = array();
		$form = $this->input->get('form');
		$norefresh = $this->input->get('norefresh');

		// process
		if($form){
			// post
			$form = array_map('trim', $form);

			// update
			$form['userid'] = USERID;

			// save
			$this->usernote_model->save($form);

			// msg
			$retarr['msg'] = 'The note was successfully saved.';

			// echo
			echo_json($retarr); exit;
		}

		// echo default
		echo_json($retarr);
	}

	// get note
	function json_get_note()
	{
		// get
		$coachingsessionid = $this->input->get('coachingsessionid');
		$usernoteid = $this->input->get('usernoteid');
		$notetype = $this->input->get('notetype');

		// set
		$retarr = array();

		// get
		$retarr['coachingsessionrs'] = ($coachingsessionid) ? $this->coachingsession_model->get($coachingsessionid) : null;
		$retarr['usernoters'] = ($usernoteid) ? $this->usernote_model->get($usernoteid) : null;
		$retarr['notetype'] = $notetype;

		echo json_encode($retarr);
	}

	function json_get_deadlinesetting()
	{
		$companyuserteamid = $this->input->get('companyuserteamid');
		$deadlineid = $this->input->get('deadlineid');
		$userid = $this->input->get('userid');

		// set
		$retarr = array();
		$where = array('companyuserteamid' => $companyuserteamid,'userid' => $userid);

		// get
		$retarr['deadlinesettingrs'] = ($userid && $companyuserteamid) ? $this->deadlinesetting_model->get(null,$where,null,true) : null;

		echo json_encode($retarr);
	}

	// activity feedback
	public function activityfeedback()
	{
		// set
		$data = array();
		$retarr = array();

		// set
		$data['loadview'] = 'mobile/users/activityfeedback';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}

	// Home page
	public function homepage()
	{
		// set
		$data = array();
		$retarr = array();
		$email = $this->input->get('email');

		$userrs = $this->user_model->get(null, array('u.email' => $email, 'u.type!=' => 'admin'), null);

		$tempArray = array();
		if(count($userrs)>1){
			foreach ($userrs as $userrsone) {
				$newArray = array("userid"=>$userrsone["userid"],"type"=>$userrsone["type"],"isenabled"=>$userrsone["isenabled"]);
				array_push($tempArray,$newArray);
			}
		}

		$data['multiusers'] = $tempArray;

		// set
		$data['loadview'] = 'mobile/users/homepage';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);

		// echo
		echo_json($retarr);
	}

	public function actiononbehalf()
	{
		// set
		$data = array();
		$retarr = array();

		$userid = $this->input->get('userid');
		$usertype = $this->input->get('usertype');
		$userrs = $this->user_model->get($userid);

		if($userrs['type']=='sales-manager' && $userrs['onbehalfof']=='1'){
			$retarr['actiononbehalf'] = true;
		} else{
			$retarr['actiononbehalf'] = false;
			$retarr['msg'] = 'You are not able to submit development plan.';
		}

		echo_json($retarr);	
	}
}

/* End of file users.php */
/* Location: ./application/modules/admin/controllers/users.php */
