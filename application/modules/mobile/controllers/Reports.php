<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// set limits
		set_time_limit(0);
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '2000M');
	}


	// list
	public function listreports()
	{
		// set
		$data = array();
		$retarr = array();
		$params = array('companyarr' => array(COMPANYID));

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// check
		if($type == 'sales-person'){
			// get
			//$data['rs'] = $rs = $this->report_model->sp_list_reports(USERID);

			// get
			$statsarr = $this->report_model->session_stats($params);

			$data['people_coached'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID]['num-total'] : 0;
			/*$statsarr = $this->report_model->session_evaluations_stats($params);
			$data['ratings_num_avg'] = round($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['salespersons'][USERID]['ratings']['num-avg']);
			$statsarr = $this->report_model->session_competency_stats($params);
			$data['ratings_per_competency'] = round($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID]['evalavg']);
			$statsarr = $this->report_model->session_competency_stats($params);
			//echo '<pre>'; print_r($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID]); echo '</pre>'; exit;
			$data['eval_times_per_session'] = 0; //round($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID]['avg']['eval-times-per-session-avg-avg']);*/
		}
		if($type == 'sales-manager'){
			// get
			//$data['rs'] = $rs = $this->report_model->sm_list_reports(USERID);

			// get
			$statsarr = $this->report_model->session_stats($params);
			$data['people_coached'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['num-coached'] : 0;
			$statsarr = $this->report_model->session_evaluations_stats($params);
			$data['ratings_num_avg'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['ratings']['num-avg'] : 0;
			$statsarr = $this->report_model->session_competency_stats($params);
			//echo '<pre>'; print_r($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]); echo '</pre>'; exit;
			$data['ratings_per_competency'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['evalavg'] : 0;
			//echo $data['ratings_per_competency']; exit;
			//$statsarr = $this->report_model->session_competency_stats($params);
			$data['dev_action_progress'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['num-suggs-completed'].'/'.$statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['num-suggs-total'] : 0;
			//$statsarr = $this->report_model->session_competency_stats($params);
			$data['eval_times_per_session'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['eval-times-per-session-avg'] : 0;
		}
		if($type == 'managing-director'){
			// get
			//$data['rs'] = $rs = $this->report_model->md_list_reports(USERID);

			// get
			$statsarr = $this->report_model->session_stats($params);
			$data['people_coached'] = (count($statsarr)) ? $statsarr[COMPANYID]['num-coached'] : 0;
			$statsarr = $this->report_model->session_evaluations_stats($params);
			$data['ratings_num_avg'] = (count($statsarr)) ? $statsarr[COMPANYID]['ratings']['num-avg'] : 0;
			$statsarr = $this->report_model->session_competency_stats($params);
			$data['ratings_per_competency'] = (count($statsarr)) ? $statsarr[COMPANYID]['evalavg'] : 0;
			//echo $data['ratings_per_competency']; exit;
			$data['dev_action_progress'] = (count($statsarr)) ? $statsarr[COMPANYID]['directors'][MDUSERID]['num-suggs-completed'].'/'.$statsarr[COMPANYID]['directors'][MDUSERID]['num-suggs-total'] : 0;
			//$statsarr = $this->report_model->session_competency_stats($params);
			$data['eval_times_per_session'] = (count($statsarr)) ? $statsarr[COMPANYID]['eval-times-per-session-avg'] : 0;
			//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		}

		// round
		//$data['rs'] = array_map('round', $data['rs']);

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-list-reports.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-list-reports.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-list-reports.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// evaluations
	public function evaluations()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// check
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_evaluations_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID])){
				// set
				$rs = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID];
				$rs['ratingdescarr'] = $statsarr[COMPANYID]['ratingdescarr'];

				// check
				if(!isset($rs['salespersons'][USERID]['rating-rows'])){
					$rs['salespersons'][USERID]['rating-rows'] = array();
				}
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_evaluations_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;
			$ratingdescarr = $statsarr[COMPANYID]['ratingdescarr'];

			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID])){
				// set
				$groupstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID];
				$smstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID];
				$rs = array();
				//echo '<pre>'; print_r($ratingdescarr); echo '</pre>'; exit;

				// loop
				foreach($ratingdescarr as $ratingid => $title){
					$groupratingrow = (isset($groupstatsarr['rating-rows'][$ratingid])) ? $groupstatsarr['rating-rows'][$ratingid] : 0;
					$ratingrow = (isset($smstatsarr['rating-rows'][$ratingid])) ? $smstatsarr['rating-rows'][$ratingid] : 0;

					// check
					if(!$groupratingrow || !$ratingrow){
						continue;
					}

					$rs[$ratingid] = array(
							'title' => $title,
							'avg' => $ratingrow['num-avg'],
							'group-avg' => $groupratingrow['num-avg']
						);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_evaluations_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;
			$ratingdescarr = $statsarr[COMPANYID]['ratingdescarr'];

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID])){
				// set
				$dirstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID];
				//echo '<pre>'; print_r($dirstatsarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($dirstatsarr['groups'] as $groupid => $grouparr){
					// loop
					foreach($grouparr['managers'] as $smuserid => $manrow){
						//$groupratingrow = $groupstatsarr['rating-rows'][$ratingid];
						//$ratingrow = $smstatsarr['rating-rows'][$ratingid];

						$rs[$smuserid] = array(
								'name' => $manrow['name'],
								'avg' => $manrow['ratings']['num-avg'],
								'group-avg' => $grouparr['ratings']['num-avg']
							);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-evaluations.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-evaluations.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-evaluations.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// evaluation ratings
	public function evaluationratings()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;
		$data['smuserid'] = $smuserid = $this->input->get('salesmanageruserid');

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['smuserrs'] = $this->user_model->get($smuserid);

		// check
		if($type == 'managing-director'){
			// get
			$smuserrs = $this->user_model->get($smuserid);
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_evaluations_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;
			$ratingdescarr = $statsarr[COMPANYID]['ratingdescarr'];
			//echo '<pre>'; print_r($ratingdescarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$smuserrs['companyusergroupid']])){
				// set
				$groupstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$smuserrs['companyusergroupid']];
				$manstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$smuserrs['companyusergroupid']]['managers'][$smuserid];
				//echo '<pre>'; print_r($smuserrs); echo '</pre>'; exit;
				//echo '<pre>'; print_r($manstatsarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($manstatsarr['rating-rows'] as $ratingid => $row){
					$title = $ratingdescarr[$ratingid];
					$groupavg = $groupstatsarr['rating-rows'][$ratingid]['num-avg'];

					$rs[$ratingid] = array(
							'title' => $title,
							'avg' => $row['num-avg'],
							'group-avg' => $groupavg
						);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-evaluation-ratings.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// development trends
	public function developmenttrends()
	{
		// set
		$data = array();
		$retarr = array();
		$numratings = $this->input->get('numratings');
		$companyuserteamid = $this->input->get('companyuserteamid');
		//$data['numratings'] = $numratings = ($numratings) ? $numratings : 4;
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		//$todate = date("Y-m-d");
		//$fromdate = date("Y-m-d", strtotime("-12 months"));
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// check
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			$sessionmonths = $statsarr[COMPANYID]['sessionmonths'];
			ksort($sessionmonths);
			//echo '<pre>'; print_r($compdescarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID])){
				// set
				$rs = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID];
				//echo '<pre>'; print_r($rs); echo '</pre>';
				$teamevalmontharr = $rs['eval-months'];
				$userstatsarr = $rs = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID];
				$userevalmontharr = $rs['eval-months'];
				//$rs['ratingdescarr'] = $statsarr[COMPANYID]['ratingdescarr'];
				//echo '<pre>'; print_r($sessionmonths); echo '</pre>';
				//echo '<pre>'; print_r($teamevalmontharr); echo '</pre>';
				//echo '<pre>'; print_r($userevalmontharr); echo '</pre>'; exit;
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

				// set
				$rs = array();
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":["spanGaps": "true", "layout": {"padding":["left": 10, "top": 10, "right": 10, "bottom": 20]}]}';
				$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true", "stepSize": "10"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjsonarr = json_decode($defjson, true);
				//echo '<pre>'; print_r($defjsonarr); echo '</pre>'; exit;

				// loop
				foreach($compdescarr as $competencygroupid => $grouparr){
					$group = $grouparr['row']['index'].'. '.$grouparr['row']['title'];
					$arr = $defjsonarr;

					// check
					if(!isset($userstatsarr['comparr']['groups'][$competencygroupid])){
						continue;
					}

					// loop
					foreach($sessionmonths as $yearmonth => $title){
						$useravg = (isset($userevalmontharr[$yearmonth][$competencygroupid])) ? $userevalmontharr[$yearmonth][$competencygroupid]['evalavg'] : null;
						$teamavg = (isset($teamevalmontharr[$yearmonth][$competencygroupid])) ? $teamevalmontharr[$yearmonth][$competencygroupid]['evalavg'] : null;

						$arr['labels'][] = date("M y", strtotime($title));
						$arr['datasets'][0]['data'][] = $useravg;
						$arr['datasets'][1]['data'][] = $teamavg;
					}
					// update
					$rs[$competencygroupid]['group'] = $group;
					$rs[$competencygroupid]['chartid'] = 'devtrend-group-'.$competencygroupid.'-'.rand(1000, 100000);
					$rs[$competencygroupid]['chartjson'] = json_encode($arr);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			// update
			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		// check
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			//echo '<pre>'; print_r($params); echo '</pre>'; exit;
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			$sessionmonths = $statsarr[COMPANYID]['sessionmonths'];
			ksort($sessionmonths);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID])){
				// set
				$manevalmontharr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID];
				$teamsevalmontharr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'];
				//echo '<pre>'; print_r($manevalmontharr); echo '</pre>';
				//echo '<pre>'; print_r($teamsevalmontharr); echo '</pre>'; exit;

				// set
				$rs = array();
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjsonarr = json_decode($defjson, true);
				//echo '<pre>'; print_r($defjsonarr); echo '</pre>'; exit;

				// loop
				foreach($teamsevalmontharr as $teamid => $teamarr){
					if(!is_numeric($teamid)){
						continue;
					}

					$teamevalmontharr = $teamarr['eval-months'];

					$name = $teamarr['title'];
					$manevalmontharr = $teamarr['eval-months'];
					$arr = $defjsonarr;
					//echo '<pre>'; print_r($manevalmontharr); echo '</pre>'; exit;

					// loop
					foreach($sessionmonths as $yearmonth => $title){
						$useravg = (isset($teamevalmontharr[$yearmonth])) ? $teamevalmontharr[$yearmonth]['evalavg'] : null;
						$teamavg = (isset($manevalmontharr[$yearmonth])) ? $manevalmontharr[$yearmonth]['evalavg'] : null;

						$arr['labels'][] = date("M y", strtotime($title));
						$arr['datasets'][0]['data'][] = ($useravg) ? round($useravg) : null;
						$arr['datasets'][1]['data'][] = ($teamavg) ? round($teamavg) : null;
					}
					// update
					$rs[$teamid]['name'] = $name;
					$rs[$teamid]['chartid'] = 'devtrend-manager-'.$teamid.'-'.rand(1000, 100000);
					$rs[$teamid]['chartjson'] = json_encode($arr);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			// update
			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		// check
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			$sessionmonths = $statsarr[COMPANYID]['sessionmonths'];
			ksort($sessionmonths);
			//echo '<pre>'; print_r($sessionmonths); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'])){
				// set
				$groupsevalmontharr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'];
				//echo '<pre>'; print_r($groupsevalmontharr); echo '</pre>'; exit;

				// set
				$rs = array();
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":["spanGaps": "true", "layout": {"padding":["left": 10, "top": 10, "right": 10, "bottom": 20]}]}';
				$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjsonarr = json_decode($defjson, true);

				// loop
				foreach($groupsevalmontharr as $groupid => $grouparr){
					if(!is_numeric($groupid)){
						continue;
					}
					$groupevalmontharr = $grouparr['eval-months'];

					// loop
					foreach($grouparr['managers'] as $smuserid => $manarr){
						if(!is_numeric($smuserid)){
							continue;
						}
						$name = $manarr['manager'];
						$manevalmontharr = $manarr['eval-months'];
						$arr = $defjsonarr;
						//echo '<pre>'; print_r($manevalmontharr); echo '</pre>'; exit;

						// loop
						foreach($sessionmonths as $yearmonth => $title){
							$useravg = (isset($manevalmontharr[$yearmonth])) ? $manevalmontharr[$yearmonth]['evalavg'] : null;
							$teamavg = (isset($groupevalmontharr[$yearmonth])) ? $groupevalmontharr[$yearmonth]['evalavg'] : null;

							$arr['labels'][] = date("M y", strtotime($title));
							$arr['datasets'][0]['data'][] = $useravg;
							$arr['datasets'][1]['data'][] = $teamavg;
						}
						// update
						$rs[$smuserid]['name'] = $name;
						$rs[$smuserid]['chartid'] = 'devtrend-manager-'.$smuserid.'-'.rand(1000, 100000);
						$rs[$smuserid]['chartjson'] = json_encode($arr);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			// update
			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-development-trends.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-development-trends.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-development-trends.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// development trends per team
	public function developmenttrendsperteam()
	{
		// set
		$data = array();
		$retarr = array();
		$smuserid = $this->input->get('salesmanageruserid');
		$teamid = $this->input->get('teamid');
		$numratings = $this->input->get('numratings');
		//$data['numratings'] = $numratings = ($numratings) ? $numratings : 4;
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		//$todate = date("Y-m-d");
		//$fromdate = date("Y-m-d", strtotime("-12 months"));
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// get
		$data['smuserrs'] = $this->user_model->get($smuserid);
		$data['teamrs'] = ($teamid) ? $this->companyuserteam_model->get($teamid) : null;

		// check
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			$sessionmonths = $statsarr[COMPANYID]['sessionmonths'];
			ksort($sessionmonths);
			//echo '<pre>'; print_r($sessionmonths); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$teamid]['eval-months'])){
				// set
				$teamevalmontharr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$teamid]['eval-months'];
				$userevalmontharr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$teamid]['users'];
				//echo '<pre>'; print_r($manevalmontharr); echo '</pre>'; exit;

				// set
				$rs = array();
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":["spanGaps": "true", "layout": {"padding":["left": 10, "top": 10, "right": 10, "bottom": 20]}]}';
				$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjsonarr = json_decode($defjson, true);

				// loop
				foreach($userevalmontharr as $userid => $userarr){
					$name = $userarr['name'];
					$userevalmontharr = $userarr['eval-months'];
					$arr = $defjsonarr;
					//echo '<pre>'; print_r($manevalmontharr); echo '</pre>'; exit;

					// loop
					foreach($sessionmonths as $yearmonth => $title){
						$useravg = (isset($userevalmontharr[$yearmonth])) ? $userevalmontharr[$yearmonth]['evalavg'] : null;
						$teamavg = (isset($teamevalmontharr[$yearmonth])) ? $teamevalmontharr[$yearmonth]['evalavg'] : null;

						$arr['labels'][] = date("M y", strtotime($title));
						$arr['datasets'][0]['data'][] = $useravg;
						$arr['datasets'][1]['data'][] = $teamavg;
					}
					// update
					$rs[$userid]['name'] = $name;
					$rs[$userid]['chartid'] = 'devtrend-user-'.$userid.'-'.rand(1000, 100000);
					$rs[$userid]['chartjson'] = json_encode($arr);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			// update
			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			$sessionmonths = $statsarr[COMPANYID]['sessionmonths'];
			ksort($sessionmonths);
			//echo '<pre>'; print_r($sessionmonths); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$data['smuserrs']['companyusergroupid']]['managers'][$smuserid]['teams'])){
				// set
				$manevalmontharr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$data['smuserrs']['companyusergroupid']]['managers'][$smuserid]['teams'];
				//echo '<pre>'; print_r($manevalmontharr); echo '</pre>'; exit;

				// set
				$rs = array();
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":["spanGaps": "true", "layout": {"padding":["left": 10, "top": 10, "right": 10, "bottom": 20]}]}';
				$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjsonarr = json_decode($defjson, true);

				// loop
				foreach($manevalmontharr as $teamid => $teamarr){
					if(!is_numeric($teamid)){
						continue;
					}
					$teamevalmontharr = $teamarr['eval-months'];

					// loop
					foreach($teamarr['users'] as $userid => $userarr){
						if(!is_numeric($userid)){
							continue;
						}
						$name = $userarr['name'];
						$userevalmontharr = $userarr['eval-months'];
						$arr = $defjsonarr;
						//echo '<pre>'; print_r($manevalmontharr); echo '</pre>'; exit;

						// loop
						foreach($sessionmonths as $yearmonth => $title){
							$useravg = (isset($userevalmontharr[$yearmonth])) ? $userevalmontharr[$yearmonth]['evalavg'] : null;
							$teamavg = (isset($teamevalmontharr[$yearmonth])) ? $teamevalmontharr[$yearmonth]['evalavg'] : null;

							$arr['labels'][] = date("M y", strtotime($title));
							$arr['datasets'][0]['data'][] = $useravg;
							$arr['datasets'][1]['data'][] = $teamavg;
						}
						// update
						$rs[$userid]['name'] = $name;
						$rs[$userid]['chartid'] = 'devtrend-user-'.$userid.'-'.rand(1000, 100000);
						$rs[$userid]['chartjson'] = json_encode($arr);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			// update
			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-development-trends-teams.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-development-trends-teams.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// development param trends
	public function developmentparamtrends()
	{
		// set
		$data = array();
		$retarr = array();
		$competencygroupid = $this->input->get('competencygroupid');
		$numratings = $this->input->get('numratings');
		//$data['numratings'] = $numratings = ($numratings) ? $numratings : 4;
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['competencygrouprs'] = $this->competencygroup_model->get($competencygroupid);

		// set
		//$todate = date("Y-m-d");
		//$fromdate = date("Y-m-d", strtotime("-12 months"));
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// check
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'][$competencygroupid];
			$sessionmonths = $statsarr[COMPANYID]['sessionmonths'];
			ksort($sessionmonths);
			//echo '<pre>'; print_r($compdescarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID])){
				// set
				$rs = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID];
				$teamevalmontharr = $rs['eval-months'];
				$userstatsarr = $rs = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID];
				$userevalmontharr = $rs['eval-months'];
				//$rs['ratingdescarr'] = $statsarr[COMPANYID]['ratingdescarr'];
				//echo '<pre>'; print_r($sessionmonths); echo '</pre>';
				//echo '<pre>'; print_r($teamevalmontharr); echo '</pre>';
				//echo '<pre>'; print_r($userevalmontharr); echo '</pre>'; exit;
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

				// set
				$rs = array();
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":["spanGaps": "true", "layout": {"padding":["left": 10, "top": 10, "right": 10, "bottom": 20]}]}';
				//$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjson = '{"labels":[],"datasets":[{"data":[],"borderColor":["#050e35"],"borderWidth":2, "pointRadius":1.5}, {"data":[],"borderColor":["#cccccc"],"borderWidth":2, "pointRadius":1.5}],"options":{"scales": {"yAxes": [{"ticks": {"beginAtZero":"true"}}]}, "spanGaps": "true", "layout": {"padding":{"left": 10, "top": 10, "right": 10, "bottom": 20}}}}';
				$defjsonarr = json_decode($defjson, true);

				// loop
				foreach($compdescarr as $competencyparameterid => $paramarr){
					// check
					if($competencyparameterid == 'row'){
						continue;
					}
					// check
					if(!isset($userstatsarr['comparr']['groups'][$competencygroupid]['params'][$competencyparameterid])){
						continue;
					}

					$param = $compdescarr['row']['index'].'.'.$paramarr['row']['index'].'. '.$paramarr['row']['title'];
					$arr = $defjsonarr;

					// loop
					foreach($sessionmonths as $yearmonth => $title){
						$useravg = (isset($userevalmontharr[$yearmonth][$competencygroupid]['params'][$competencyparameterid])) ? $userevalmontharr[$yearmonth][$competencygroupid]['params'][$competencyparameterid]['evalavg'] : null;
						$teamavg = (isset($teamevalmontharr[$yearmonth][$competencygroupid]['params'][$competencyparameterid])) ? $teamevalmontharr[$yearmonth][$competencygroupid]['params'][$competencyparameterid]['evalavg'] : null;

						$arr['labels'][] = date("M y", strtotime($title));
						$arr['datasets'][0]['data'][] = $useravg;
						$arr['datasets'][1]['data'][] = $teamavg;
					}

					// update
					$rs[$competencyparameterid]['param'] = $param;
					$rs[$competencyparameterid]['chartid'] = 'devtrend-group-'.$competencyparameterid.'-'.rand(1000, 100000);
					$rs[$competencyparameterid]['chartjson'] = json_encode($arr);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			// update
			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-development-param-trends.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sp-development-param-trends.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-development-param-trends.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// list competency reports
	public function listcompetencyreports()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// chevck
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID])){
				// set
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID];
				//echo '<pre>'; print_r($userevalarr['comparr']); echo '</pre>'; exit;
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'] as $competencygroupid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'. '.$compdescarr[$competencygroupid]['row']['title'];

					$rs[$competencygroupid] = array('group' => $title, 'avg' => $row['avg']['evalavg']);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			//echo '<pre>'; print_r($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID])){
				// set
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID];
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'] as $competencygroupid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'. '.$compdescarr[$competencygroupid]['row']['title'];

					$rs[$competencygroupid] = array('group' => $title, 'avg' => $row['avg']['evalavg']);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID])){
				// set
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID];
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'] as $competencygroupid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'. '.$compdescarr[$competencygroupid]['row']['title'];

					$rs[$competencygroupid] = array('group' => $title, 'avg' => $row['avg']['evalavg']);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-list-competency-reports.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-list-competency-reports.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-list-competency-reports.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// developments
	public function developments()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));
		//$todate = '2020-12-31';

		// check
		if($type == 'sales-person'){
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID])){
				$rs = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID];
				//echo '<pre>'; print_r($data['rs']); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
			//echo '<pre>'; print_r($data['rs']); echo '</pre>'; exit;
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-developments.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// list parameter reports
	public function listparameterreports()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// check
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID])){
				// set
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID];
				//echo '<pre>'; print_r($userevalarr['comparr']); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'] as $competencygroupid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'. '.$compdescarr[$competencygroupid]['row']['title'];

					$rs[$competencygroupid] = array('group' => $title, 'avg' => $row['avg']['eval-times-per-session-avg']);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID])){
				// set
				$manevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID];
				//echo '<pre>'; print_r($manevalarr['comparr']); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($manevalarr['comparr']['groups'] as $competencygroupid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'. '.$compdescarr[$competencygroupid]['row']['title'];

					$rs[$competencygroupid] = array('title' => $title, 'avg' => $row['avg']['eval-times-per-session-avg']);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID])){
				// set
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID];
				//echo '<pre>'; print_r($userevalarr['comparr']); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'] as $competencygroupid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'. '.$compdescarr[$competencygroupid]['row']['title'];

					$rs[$competencygroupid] = array('title' => $title, 'avg' => $row['avg']['eval-times-per-session-avg']);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-list-parameter-reports.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-list-parameter-reports.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-list-parameter-reports.php' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// parameter sassessed teams
	public function parametersassessedteams()
	{
		// set
		$data = array();
		$retarr = array();
		$competencygroupid = $this->input->get('competencygroupid');
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// get
		$data['competencygrouprs'] = $this->competencygroup_model->get($competencygroupid);

		// check
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'])){
				// set
				$teamevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'];
				//echo '<pre>'; print_r($teamevalarr); echo '</pre>';
				$title = $compdescarr[$competencygroupid]['row']['title'];
				$rs = array();

				// loop
				foreach($teamevalarr as $teamid => $teamarr){
					//echo 'team <hr />';
					/*echo "<h1>$teamid</h1>";
					echo '<pre>'; print_r($teamarr); echo '</pre>';
					echo '<hr />';
					continue;*/
					if(!is_numeric($teamid)){
						continue;
					}
					// loop
					foreach($teamarr['users'] as $userid => $row){
						if(!isset($row['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'])){
							continue;
						}
						$avg = (isset($row['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'])) ? $row['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'] : 0;
						$teamavg = (isset($teamarr['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'])) ? $teamarr['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'] : 0;
						$rs[$userid] = array('name' => $row['name'], 'avg' => $avg, 'team-avg' => $teamavg);
					}
				}
				//exit;
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'])){
				// set
				$groupevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'];
				//echo '<pre>'; print_r($groupevalarr); echo '</pre>'; exit;
				$title = $compdescarr[$competencygroupid]['row']['title'];
				$rs = array();

				// loop
				foreach($groupevalarr as $groupid => $grouparr){
					if(!is_numeric($groupid)){
						continue;
					}
					// loop
					foreach($grouparr['managers'] as $smuserid => $row){
						if(!is_numeric($smuserid)){
							continue;
						}
						if(!isset($row['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'])){
							continue;
						}
						$avg = (isset($row['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'])) ? $row['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg'] : 0;
						$rs[$smuserid] = array('name' => $row['manager'], 'avg' => $avg, 'group-avg' => $grouparr['comparr']['groups'][$competencygroupid]['avg']['eval-times-per-session-avg']);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-parameters-assessed-teams.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-parameters-assessed-teams.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// competency parameter team report
	public function competencyparameterteams()
	{
		// set
		$data = array();
		$retarr = array();
		$competencygroupid = $this->input->get('competencygroupid');
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// get
		$data['competencygrouprs'] = $this->competencygroup_model->get($competencygroupid);

		// check
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			//echo '<pre>'; print_r($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['comparr']['groups'][$competencygroupid]['avg']); echo '</pre>';
			//echo '<pre>'; print_r($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['comparr']['groups'][$competencygroupid]['avg']); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['comparr']['groups'][$competencygroupid]['avg']['evalavg'])){
				// set
				//$manevalavg = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['comparr']['groups'][$competencygroupid]['avg']['evalavg'];
				//echo '<pre>'; print_r($manevalavg); echo '</pre>';
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'];
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr as $teamid => $teamarr){
					if(!is_numeric($teamid)){
						continue;
					}

					// loop
					foreach($teamarr['users'] as $userid => $arr){
						if(!isset($arr['comparr']['groups'][$competencygroupid]['avg']['evalavg'])){
							continue;
						}
						// set
						$manevalavg = $teamarr['comparr']['groups'][$competencygroupid]['avg']['evalavg'];
						/*echo '<hr />';
						echo '<hr />';
						echo '<pre style="background: lightgreen;">'; print_r($teamarr['comparr']['groups'][$competencygroupid]['avg']); echo '</pre>';
						echo '<hr />';
						echo '<pre style="background: lightgrey;">'; print_r($arr['comparr']['groups'][$competencygroupid]['avg']); echo '</pre>';*/

						$evalavg = (isset($arr['comparr']['groups'][$competencygroupid]['avg']['evalavg'])) ? $arr['comparr']['groups'][$competencygroupid]['avg']['evalavg'] : 0;
						$rs[$userid] = array('name' => $arr['name'], 'avg' => $evalavg, 'group-avg' => $manevalavg);
					}
					//exit;
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['comparr']['groups'][$competencygroupid]['avg']['evalavg'])){
				// set
				//$groupevalavg = $statsarr[COMPANYID]['directors'][MDUSERID]['comparr']['groups'][$competencygroupid]['avg']['evalavg'];
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'];
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;

				// loop
				foreach($userevalarr as $groupid => $grouparr){
					if(!is_numeric($groupid)){
						continue;
					}

					// loop
					foreach($grouparr['managers'] as $smuserid => $smarr){
						if(!is_numeric($smuserid)){
							continue;
						}
						if(!isset($smarr['comparr']['groups'][$competencygroupid]['avg']['evalavg'])){
							continue;
						}
						// set
						$groupevalavg = $grouparr['comparr']['groups'][$competencygroupid]['avg']['evalavg'];

						$evalavg = (isset($smarr['comparr']['groups'][$competencygroupid]['avg']['evalavg'])) ? $smarr['comparr']['groups'][$competencygroupid]['avg']['evalavg'] : 0;
						$rs[$smuserid] = array('name' => $smarr['manager'], 'avg' => $evalavg, 'group-avg' => $groupevalavg);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-competency-parameter-teams.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-competency-parameter-teams.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// competency parameter report
	public function competencyparameters()
	{
		// set
		$data = array();
		$retarr = array();
		$competencygroupid = $this->input->get('competencygroupid');
		$salespersonaluserid = $this->input->get('salespersonaluserid');
		$salesmanageruserid = $this->input->get('salesmanageruserid');
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// get
		$data['competencygrouprs'] = $this->competencygroup_model->get($competencygroupid);
		$data['spuserrs'] = ($salespersonaluserid) ? $this->user_model->get($salespersonaluserid) : null;
		$data['smuserrs'] = ($salesmanageruserid) ? $this->user_model->get($salesmanageruserid) : null;

		// check
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($compdescarr); echo '</pre>'; exit;
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['comparr']['groups'][$competencygroupid]['params'])){
				// set
				$teamevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['comparr']['groups'][$competencygroupid]['params'];
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID]['comparr']['groups'][$competencygroupid]['params'];
				//echo '<pre>'; print_r($teamevalarr); echo '</pre>'; exit;

				// loop
				foreach($userevalarr as $competencyparameterid => $row){
					$title = (isset($compdescarr[$competencygroupid][$competencyparameterid]['row']['title'])) ? $compdescarr[$competencygroupid]['row']['index'].'.'.$compdescarr[$competencygroupid][$competencyparameterid]['row']['index'].'. '.$compdescarr[$competencygroupid][$competencyparameterid]['row']['title'] : 'Unknown';
					$teamavg = (isset($teamevalarr[$competencyparameterid]['evalavg'])) ? $teamevalarr[$competencyparameterid]['evalavg'] : 0;

					$rs[$competencyparameterid] = array('param' => $title, 'avg' => $row['evalavg'], 'team-avg' => $teamavg);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$data['spuserrs']['companyuserteamid']]['comparr']['groups'][$competencygroupid])){
				// set
				$teamevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$data['spuserrs']['companyuserteamid']]['comparr']['groups'][$competencygroupid];
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$data['spuserrs']['companyuserteamid']]['users'][$salespersonaluserid]['comparr']['groups'][$competencygroupid]['params'];
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr as $paramid => $paramarr){
					$teamevalavg = $teamevalarr['params'][$paramid]['evalavg'];
					$title = $compdescarr[$competencygroupid]['row']['index'].'.'.$compdescarr[$competencygroupid][$paramid]['row']['index'].'. '.$compdescarr[$competencygroupid][$paramid]['row']['title'];

					$rs[$paramid] = array('title' => $title, 'avg' => $paramarr['evalavg'], 'team-avg' => $teamevalavg);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['comparr']['groups'][$competencygroupid])){
				// set
				$groupevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['comparr']['groups'][$competencygroupid];
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$data['smuserrs']['companyusergroupid']]['managers'][$salesmanageruserid]['comparr']['groups'][$competencygroupid]['params'];
				//echo '<pre>'; print_r($userevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr as $paramid => $paramarr){
					$groupevalavg = $groupevalarr['params'][$paramid]['evalavg'];
					$title = $compdescarr[$competencygroupid]['row']['index'].'.'.$compdescarr[$competencygroupid][$paramid]['row']['index'].'. '.$compdescarr[$competencygroupid][$paramid]['row']['title'];

					$rs[$paramid] = array('title' => $title, 'avg' => $paramarr['evalavg'], 'group-avg' => $groupevalavg);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-competency-parameters.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-competency-parameters.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-competency-parameters.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// parameter assessed report
	public function parametersassessed()
	{
		// set
		$data = array();
		$retarr = array();
		$competencygroupid = $this->input->get('competencygroupid');
		$salespersonaluserid = $this->input->get('salespersonaluserid');
		$salesmanageruserid = $this->input->get('salesmanageruserid');
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['competencygrouprs'] = $this->competencygroup_model->get($competencygroupid);
		$data['spuserrs'] = ($salespersonaluserid) ? $this->user_model->get($salespersonaluserid) : null;
		$data['smuserrs'] = ($salesmanageruserid) ? $this->user_model->get($salesmanageruserid) : null;

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// check
		if($type == 'sales-person'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID])){
				// set
				$teamevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID];
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][SMUSERID]['teams'][TEAMID]['users'][USERID];
				//echo '<pre>'; print_r($teamevalarr); echo '</pre>'; exit;
				//echo '<pre>'; print_r($userevalarr['comparr']); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'][$competencygroupid]['params'] as $competencyparameterid => $row){
					$title = $compdescarr[$competencygroupid]['row']['index'].'.'.$compdescarr[$competencygroupid][$competencyparameterid]['row']['index'].'. '.$compdescarr[$competencygroupid][$competencyparameterid]['row']['title'];
					$teamvg = (isset($teamevalarr['comparr']['groups'][$competencygroupid]['params'][$competencyparameterid]['eval-times-per-session-avg'])) ? $teamevalarr['comparr']['groups'][$competencygroupid]['params'][$competencyparameterid]['eval-times-per-session-avg'] : 0;

					$rs[$competencyparameterid] = array('param' => $title, 'avg' => $row['eval-times-per-session-avg'], 'team-avg' => $teamvg);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$data['spuserrs']['companyuserteamid']])){
				// set
				$teamevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$data['spuserrs']['companyuserteamid']];
				$userevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'][$data['spuserrs']['companyuserteamid']]['users'][$salespersonaluserid];
				//echo '<pre>'; print_r($teamevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($userevalarr['comparr']['groups'][$competencygroupid]['params'] as $paramid => $row){
					$teamavg = $teamevalarr['comparr']['groups'][$competencygroupid]['params'][$paramid]['eval-times-per-session-avg'];
					$title = $compdescarr[$competencygroupid]['row']['index'].'.'.$compdescarr[$competencygroupid][$paramid]['row']['index'].'. '.$compdescarr[$competencygroupid][$paramid]['row']['title'];

					$rs[$paramid] = array('title' => $title, 'avg' => $row['eval-times-per-session-avg'], 'team-avg' => $teamavg);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$data['smuserrs']['companyusergroupid']])){
				// set
				$groupevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$data['smuserrs']['companyusergroupid']];
				$manevalarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][$data['smuserrs']['companyusergroupid']]['managers'][$salesmanageruserid];
				//echo '<pre>'; print_r($groupevalarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($manevalarr['comparr']['groups'][$competencygroupid]['params'] as $paramid => $row){
					$groupavg = $groupevalarr['comparr']['groups'][$competencygroupid]['params'][$paramid]['eval-times-per-session-avg'];
					$title = $compdescarr[$competencygroupid]['row']['index'].'.'.$compdescarr[$competencygroupid][$paramid]['row']['index'].'. '.$compdescarr[$competencygroupid][$paramid]['row']['title'];

					$rs[$paramid] = array('title' => $title, 'avg' => $row['eval-times-per-session-avg'], 'group-avg' => $groupavg);
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-parameters-assessed.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-parameters-assessed.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/sp-parameters-assessed.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// number coached
	public function numcoached()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));

		// check
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID])){
				// set
				$smstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID];
				//echo '<pre>'; print_r($smstatsarr); echo '</pre>'; exit;
				$totalavg = $smstatsarr['percentage-completed'];
				$rs = array();

				// loop
				foreach($smstatsarr['teams'] as $teamid => $teamrow){
					// loop
					foreach($teamrow['users'] as $userid => $row){
						$rs[$userid] = array(
								'fullname' => $row['name'],
								'avg' => $row['percentage-completed'],
								'num-completed' => $row['num-completed'],
								'num-total' => $row['num-total'],
								'total-avg' => $totalavg
							);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID])){
				// set
				$smstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID];
				//echo '<pre>'; print_r($smstatsarr); echo '</pre>'; exit;
				$totalavg = $smstatsarr['percentage-completed'];
				$rs = array();

				// loop
				foreach($smstatsarr['groups'] as $groupid => $grouprow){
					// loop
					foreach($grouprow['managers'] as $userid => $row){
						$rs[$userid] = array(
								'fullname' => $row['manager'],
								'avg' => $row['percentage-completed'],
								'num-completed' => $row['num-completed'],
								'num-total' => $row['num-total'],
								'total-avg' => $totalavg
							);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-numcoached.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-numcoached.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// developments completed report
	public function developmentscompleted()
	{
		// set
		$data = array();
		$retarr = array();

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-developments-completed.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-developments-completed.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// developments planned report
	public function developmentsplanned()
	{
		// set
		$data = array();
		$retarr = array();

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-developments-planned.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-developments-planned.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// developments remaining report
	public function developmentsremaining()
	{
		// set
		$data = array();
		$retarr = array();

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-developments-remaining.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-developments-remaining.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// development progress
	public function developmentprogress()
	{
		// set
		$data = array();
		$retarr = array();
		$nummonths = $this->input->get('nummonths');
		$data['nummonths'] = $nummonths = ($nummonths) ? $nummonths : 3;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// set
		$todate = date("Y-m-d");
		$fromdate = date("Y-m-01", strtotime("$todate -$nummonths months"));
		//echo "$todate - $fromdate";

		// check
		if($type == 'sales-manager'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'])){
				// set
				$teamstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID]['groups'][GROUPID]['managers'][USERID]['teams'];
				//echo '<pre>'; print_r($teamstatsarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($teamstatsarr as $teamid => $teamarr){
					if(!is_numeric($teamid)){
						continue;
					}
					// loop
					foreach($teamarr['users'] as $userid => $row){
						// set
						$rs[$userid] = array(
								'name' => $row['name'],
								'completed' => $row['num-suggs-completed'],
								'total' => $row['num-suggs-total'],
								'avg' => $row['num-suggs-perc-completed'],
								'team-avg' => $teamarr['num-suggs-perc-completed']
							);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}
		if($type == 'managing-director'){
			// get
			$params = array('companyarr' => array(COMPANYID), 'fromdate' => $fromdate, 'todate' => $todate);
			$statsarr = $this->report_model->session_competency_stats($params);
			//echo '<pre>'; print_r($statsarr); echo '</pre>'; exit;
			$compdescarr = $statsarr[COMPANYID]['compdescarr'];

			// check
			if(isset($statsarr[COMPANYID]['directors'][MDUSERID])){
				// set
				$dirstatsarr = $statsarr[COMPANYID]['directors'][MDUSERID];
				//echo '<pre>'; print_r($dirstatsarr); echo '</pre>'; exit;
				$rs = array();

				// loop
				foreach($dirstatsarr['groups'] as $groupid => $grouparr){
					if(!is_numeric($groupid)){
						continue;
					}
					// loop
					foreach($grouparr['managers'] as $smuserid => $manrow){
						if(!is_numeric($smuserid)){
							continue;
						}
						// set
						$rs[$smuserid] = array(
								'name' => $manrow['manager'],
								'total' => $manrow['num-suggs-total'],
								'avg' => $manrow['num-suggs-perc-completed'],
								'group-avg' => $grouparr['num-suggs-perc-completed']
							);
					}
				}
				//echo '<pre>'; print_r($rs); echo '</pre>'; exit;
			}

			$data['rs'] = (isset($rs)) ? $rs : array();
		}

		// set
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/reports/md-development-progress.php' : null;
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/reports/sm-development-progress.php' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/reports/.php' : $data['loadview'];


		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// reports filter
	public function modal_reportfilter()
	{
		// set
		$data = array();
		$retarr = array();
		$reptitlearr = config_item('report-title-arr');

		// get
		$data['userrs'] = $this->user_model->get(USERID);
		$type = $this->input->get('type');

		//echo $type;
		//echo '<pre>'; print_r($reptitlearr); echo '</pre>'; exit;

		// set
		$data['loadview'] = 'mobile/_parts/reports-filter-modal.php';
		$data['type'] = $type;
		$data['title'] = $reptitlearr[$type];

		// get
		$data['statsarr'] = $this->report_model->company_user_stats(array('companyid' => COMPANYID));
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// create report
	public function create_send_report()
	{
		// set
		$data = array();
		$retarr = array();

		// get
		$userrs = $this->user_model->get(USERID);
		$form = $this->input->get('form');
		//echo '<pre>'; print_r($form); echo '</pre>'; exit;
		$jsonform = ($form && is_array($form)) ? json_encode($form) : 'null';

		// queue
		$arr = array('params' => $jsonform, 'type' => $form['type'], 'action' => $form['action']);
		$actionqueueid = $this->actionqueuelib->add($arr);
		//echo $actionqueueid; exit;

		// process
		$response = $this->actionqueuelib->process($actionqueueid);
		//echo '<pre>'; print_r($response); echo '</pre>'; exit;

		// update
		$retarr['action'] = $form['action'];
		$retarr['msg'] = ($form['action'] == 'email-report') ? 'The report was successfully e-mailed.' : '';
		$retarr['fileurl'] = $response[0]['fileurl'];
		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		// echo
		echo_json($retarr);
	}
}

/* End of file reports.php */
/* Location: ./application/modules/admin/controllers/reports.php */
