<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coachingsessions extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	// list
	public function listall()
	{
		// set
		$data = array();
		$retarr = array();
		$form = $this->input->get('form');
		$todate = date("Y-m-d", strtotime("-1 day"));

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// check
		if($type == 'sales-person'){
			// get
			$where = array('cs.salespersonaluserid' => USERID);
			$other = array('where-str' => 'cs.completedon IS NULL AND cs.cancelledon IS NULL AND DATE(cs.date)>=DATE(NOW())');
			$data['coachingsessionrs'] = $this->coachingsession_model->get(null, $where, $other);
			$data['cancelledcoachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salespersonaluserid' => USERID), array('where-str' => 'cs.completedon IS NULL AND cs.cancelledon IS NOT NULL AND DATE(cs.date)>DATE("'.FROM_LIMIT_DATE.'")'));

			$data['loadview'] = 'mobile/coachingsessions/list-sessions-sp';
		}
		if($type == 'sales-manager'){
			// get
			$where = array('cs.salesmanageruserid' => USERID);
			$other = array('where-str' => '( cs.completedon IS NULL AND cs.cancelledon IS NULL AND ( DATE(cs.date)>=DATE(NOW()) OR DATE(cs.date)=DATE("'.$todate.'") ) )');
			$data['coachingsessionrs'] = $this->coachingsession_model->get(null, $where, $other);
			$data['cancelledcoachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salesmanageruserid' => USERID), array('where-str' => 'cs.completedon IS NULL AND cs.cancelledon IS NOT NULL AND DATE(cs.date)>DATE("'.FROM_LIMIT_DATE.'")'));

			$data['loadview'] = 'mobile/coachingsessions/list-sessions-sm';
		}
		//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>'; exit;

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);

		// echo
		echo_json($retarr);
	}


	// save
	public function save()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$data['movesession'] = $movesession = $this->input->get('movesession');
		$form = $this->input->get('form');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['coachingsessionrs'] = ($coachingsessionid) ? $this->coachingsession_model->get($coachingsessionid) : get_empty_field_array('coachingsession');
		$data['companyuserteamarr'] = table_to_array('companyuserteam', 'companyuserteamid', 'title', array('userid' => USERID, 'deletedon' => 'IS NULL'), array('' => 'Select A Team...'));
		$data['companyuserteamrefarr'] = $this->companyuserteamref_model->get_keyval_arr(array('cut.userid' => USERID), null, 'user', 'userid');
		$data['spuserrs'] = ($coachingsessionid) ? $this->user_model->get($data['coachingsessionrs']['salespersonaluserid']) : null;
		$data['companyuserteamrefrs'] = ($data['spuserrs']) ? $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $data['spuserrs']['companyuserteamid'])) : array('' => 'Select a Team Member...');
		$data['companyuserteamrefrs'] = ($coachingsessionid) ? rs_to_keyval_array($data['companyuserteamrefrs'], 'userid', 'user', array('' => 'Select a Team Member...')) : $data['companyuserteamrefrs'];
		$data['companyuserteamid'] = ($data['spuserrs']) ? $data['spuserrs']['companyuserteamid'] : null;

		//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>'; exit;

		// check
		/*if(!$coachingsessionid){
			$data['coachingsessionrs']['date'] = date("Y-m-d");
			$data['coachingsessionrs']['time'] = '12:00';
		}*/

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$form['createdbyuserid'] = USERID;

			// update
			if($type == 'sales-person'){
				$form['salespersonaluserid'] = USERID;

				// get
				$companyuserteamrefrs = $this->companyuserteamref_model->get(null, array('cutr.userid' => USERID), null, true);

				// update
				$form['salesmanageruserid'] = $companyuserteamrefrs['salesmanageruserid'];
			}
			if($type == 'sales-manager'){
				$form['salesmanageruserid'] = USERID;
			}

			// check
			$timefrom = $form['time'];
			$timeto = $form['time'];
			$where = null;
			$other = array('where-str' => '(
					cs.salesmanageruserid="'.$form['salesmanageruserid'].'") AND cs.completedon IS NULL AND cs.cancelledon IS NULL AND
					cs.date=DATE("'.$form['date'].'") AND
					(
						(TIME("'.$timefrom.'")>=TIME(cs.time - INTERVAL 45 MINUTE) AND TIME("'.$timefrom.'")<=TIME(cs.time + INTERVAL 45 MINUTE)) OR
						(TIME("'.$timeto.'")>=TIME(cs.time - INTERVAL 45 MINUTE) AND TIME("'.$timeto.'")<=TIME(cs.time + INTERVAL 45 MINUTE))
					)');
			$overlap_coachingsessionrs = $this->coachingsession_model->get(null, $where, $other);

			//echo '<pre>'; print_r($other); echo '</pre>';
			//echo '<pre>'; print_r($overlap_coachingsessionrs); echo '</pre>'; exit;

			// check
			if($overlap_coachingsessionrs){
				// load view
				$retarr['overlapmodalinfo'] = $this->load->view('mobile/_parts/list-sessions-modal.php', array('coachingsessionrs' => $overlap_coachingsessionrs), true);
			}else{
				// save
				$this->coachingsession_model->save($form, $coachingsessionid);

				// msg
				$retarr['msg'] = ($coachingsessionid) ? 'The session was successfully updated.' : 'The session was successfully created.';

				// redirect
				$retarr['redirect'] = 'menu.html';
			}

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/coachingsessions/new-session-sp' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/coachingsessions/new-session-sm' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);


		// echo
		echo_json($retarr);
	}


	// view
	public function view()
	{

		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');
		$savedraft = $this->input->get('savedraft');
		$yesterday = date("Y-m-d", strtotime("-1 day"));

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['coachingsessionrs'] = $this->coachingsession_model->get($coachingsessionid);
		$data['companyuserteamrefarr'] = $this->companyuserteamref_model->get_keyval_arr(array('cut.userid' => USERID), null, 'user', 'userid');
		$companycompetencyplanrs = $this->companycompetencyplan_model->get(null, array('ccp.companyuserteamid' => $data['coachingsessionrs']['companyuserteamid']), null, true);
		$companycompetencyplanid = $companycompetencyplanrs['companycompetencyplanid'];

		// check
		$data['coachingsessionrs']['isyesterday'] = (date("Y-m-d", strtotime($data['coachingsessionrs']['date'])) == $yesterday) ? 1 : 0;

		// check future
		if((date("Y-m-d", strtotime($data['coachingsessionrs']['date'])) > date("Y-m-d"))){
			// set
			$date = date("Y-m-d");

			// save
			$this->coachingsession_model->save(array('date' => $date), $coachingsessionid);

			// update
			$data['coachingsessionrs']['date'] = $date;
		}

		// get
		$data['group_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL'));
		$data['parameter_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'));
		$data['suggestion_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get_keyval_arr(array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL'));
		$data['usersessionparameteritemarr'] = table_to_array('usersessionparameteritem', 'companycompetencyplanrefid', 'value', array('coachingsessionid' => $coachingsessionid, 'deletedon' => 'IS NULL'), null);

		// incnums
		$data['group_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
		$data['param_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
		$data['sugg_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);

		//echo '<pre>'; print_r($data['group_companycompetencyplanrefrs']); echo '</pre>'; exit;

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$total = array_sum($form);

			// check
			if($total){
				// loop
				foreach($form as $companycompetencyplanrefid => $val){
					$arr = array();

					// check
					/*if(!$val){
						continue;
					}*/

					// update
					$arr['coachingsessionid'] = $coachingsessionid;
					$arr['companycompetencyplanrefid'] = $companycompetencyplanrefid;
					$arr['value'] = ($val) ? $val : '0';

					// save
					$usersessionparameteritemrs = $this->usersessionparameteritem_model->get(null, array('uspi.coachingsessionid' => $coachingsessionid, 'uspi.companycompetencyplanrefid' => $companycompetencyplanrefid), null, true);
					$usersessionparameteritemid = ($usersessionparameteritemrs) ? $usersessionparameteritemrs['usersessionparameteritemid'] : null;

					// save
					$this->usersessionparameteritem_model->save($arr, $usersessionparameteritemid);

					// update
					if(!$data['coachingsessionrs']['beganon']){
						$this->coachingsession_model->save(array('beganon' => date("Y-m-d H:i:s")), $coachingsessionid);
					}
				}

				// msg
				$retarr['msg'] = ($savedraft) ? 'The draft was successfully saved.' : 'The session parameters were successfully updated.';

				// redirect
				$retarr['redirect'] = ($savedraft) ? 'sessions.html' : 'view-session-suggestions.html';
				$retarr['params'] = array('coachingsessionid' => $coachingsessionid);
			}else{
				// msg
				$retarr['msg'] = 'Please select at least 1 parameter.';
			}

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/coachingsessions/view-session-sp' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/coachingsessions/view-session-sm' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;

		// $fp = fopen('data.txt','a');//opens file in append mode  
		// fwrite($fp,json_encode($companycompetencyplanid));
		// fclose($fp);

		// echo
		echo_json($retarr);
	}


	// view suggestions
	public function viewsuggestions()
	{

		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');
		$form = (is_array($form)) ? $form : array();
		$dueform = $this->input->get('dueform');
		$savedraft = $this->input->get('savedraft');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['coachingsessionrs'] = $this->coachingsession_model->get($coachingsessionid);
		$data['companyuserteamrefarr'] = $this->companyuserteamref_model->get_keyval_arr(array('cut.userid' => USERID), null, 'user', 'userid');
		//$companycompetencyplanrs = $this->companycompetencyplan_model->get(null, array('ccp.companyid' => $userrs['companyid']), null, true);
		$companycompetencyplanrs = $this->companycompetencyplan_model->get(null, array('ccp.companyuserteamid' => $data['coachingsessionrs']['companyuserteamid']), null, true);
		$companycompetencyplanid = $companycompetencyplanrs['companycompetencyplanid'];

		// get
		$data['group_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL'));
		$data['parameter_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'));
		$data['suggestion_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL'));

		$data['usersessionparameteritemarr'] = $this->usersessionparameteritem_model->get_keyrow_arr(array('uspi.coachingsessionid' => $coachingsessionid));
		$data['sm_usercompetencylimitarr'] = $this->usercompetencylimit_model->get_hier_keyval_arr(array('ucl.userid' => $data['coachingsessionrs']['salesmanageruserid']));
		$data['sp_usercompetencylimitarr'] = $this->usercompetencylimit_model->get_hier_keyval_arr(array('ucl.userid' => $data['coachingsessionrs']['salespersonaluserid']));
		$data['usersessionsuggestionitemarr'] = table_to_array('usersessionsuggestionitem', 'companycompetencyplanrefid', 'dueon', array('coachingsessionid' => $coachingsessionid, 'deletedon' => 'IS NULL'), null);

		$data['group_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
		$data['param_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
		$data['sugg_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);

		//echo '<pre>'; print_r($data['usersessionparameteritemarr']); echo '</pre>'; exit;
		//echo '<pre>'; print_r($data['sp_usercompetencylimitarr']); echo '</pre>';
		//echo '<pre>'; print_r($dueform); echo '</pre>';

		// process
		if($form || $dueform){
			// loop
			foreach($form as $competencygroupid => $parr){
				// loop
				foreach($parr as $competencyparameterid => $sarr){
					// loop
					foreach($sarr as $competencysuggestionid){
						$dueon = $dueform[$competencygroupid][$competencyparameterid][$competencysuggestionid];
						$dueon = ($dueon) ? $dueon : date("Y-m-d", strtotime("+7 days"));
						//echo "$competencygroupid - $competencyparameterid - $competencysuggestionid = $dueon <br />";

						// get
						$rs = $this->companycompetencyplanref_model->get(null, array('ccp.companyuserteamid' => $data['coachingsessionrs']['companyuserteamid'], 'ccpr.competencygroupid' => $competencygroupid, 'ccpr.competencyparameterid' => $competencyparameterid, 'ccpr.competencysuggestionid' => $competencysuggestionid), null, true);

						// get
						$usersessionsuggestionitemrs = $this->usersessionsuggestionitem_model->get(null, array('ussi.coachingsessionid' => $coachingsessionid, 'ussi.companycompetencyplanrefid' => $rs['companycompetencyplanrefid']), null, true);
						$usersessionsuggestionitemid = ($usersessionsuggestionitemrs) ? $usersessionsuggestionitemrs['usersessionsuggestionitemid'] : null;

						// set
						$arr = array(
							'coachingsessionid' => $coachingsessionid,
							'companycompetencyplanrefid' => $rs['companycompetencyplanrefid'],
							'dueon' => date("Y-m-d", strtotime($dueon))
						);
						//echo '<pre>'; print_r($arr); echo '</pre>';

						// save
						$this->usersessionsuggestionitem_model->save($arr, $usersessionsuggestionitemid);
					}
				}
			}

			// update
			if(!$savedraft){
				// save
				$this->coachingsession_model->save(array('completedon' => date("Y-m-d H:i:s")), $coachingsessionid);

				// queue
				$arr = array('coachingsessionid' => $coachingsessionid, 'type' => 'coaching-session-result', 'action' => 'email-report');
				$actionqueueid = $this->actionqueuelib->add($arr);

				// process
				$this->actionqueuelib->process($actionqueueid);
			}

			// msg
			$retarr['msg'] = ($savedraft) ? 'The draft was successfully saved.' : 'The suggestions were successfully created.';

			// redirect
			$retarr['redirect'] = ($savedraft) ? 'sessions.html' : 'menu.html';

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/coachingsessions/view-session-suggestions' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/coachingsessions/view-session-suggestions' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// view development plan
	function viewdevelopmentplanmodal()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');
		$form = (is_array($form)) ? $form : array();
		$dueform = $this->input->get('dueform');

		//echo '<pre>'; print_r($form); echo '</pre>';

		// get
		$group_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
		$param_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
		$sugg_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);

		// loop
		foreach($form as $competencygroupid => $parr){
			// get
			$rs = $this->competencygroup_model->get($competencygroupid);

			$rs['incnum'] = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;

			$grp_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$competencygroupid), array('where-str' => '(cgpsr.competencyparameterid IS NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
			$rs['sortorder'] = $grp_companygroupparamsuggref['sortorder'];

			$form[$competencygroupid]['rs'] = $rs;

			// loop
			foreach($parr as $competencyparameterid => $sarr){
				// get
				$rs = $this->competencyparameter_model->get($competencyparameterid);

				$rs['incnum'] = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;
				$para_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$competencygroupid,'cgpsr.competencyparameterid'=>$competencyparameterid), array('where-str' => '(cgpsr.competencyparameterid IS NOT NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
				$rs['sortorder'] = $para_companygroupparamsuggref['sortorder'];

				$form[$competencygroupid]['params'][$competencyparameterid]['rs'] = $rs;

				// loop
				foreach($sarr as $competencysuggestionid){
					$dueon = $dueform[$competencygroupid][$competencyparameterid][$competencysuggestionid];

					// get
					$rs = $this->competencysuggestion_model->get($competencysuggestionid);

					$rs['incnum'] = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : 0;
					$sugg_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$competencygroupid,'cgpsr.competencyparameterid'=>$competencyparameterid,'cgpsr.competencysuggestionid'=>$competencysuggestionid), array('where-str' => '(cgpsr.competencysuggestionid IS NOT NULL)'),true);
					$rs['sortorder'] = $sugg_companygroupparamsuggref['sortorder'];


					$rs['dueon'] = date("Y-m-d", strtotime($dueon));

					$form[$competencygroupid]['params'][$competencyparameterid]['suggestions'][$competencysuggestionid]['rs'] = $rs;
				}

				unset($form[$competencygroupid][$competencyparameterid]);
			}
		}

		// update
		$data['form'] = $form;

		//echo '<pre>'; print_r($form); echo '</pre>'; exit;

		// set
		$data['loadview'] = 'mobile/coachingsessions/development-plan-modal';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;
		

		// echo
		echo_json($retarr);
	}

	// view development plan
	function viewdevelopmentplansubmitmodal()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');
		$form = (is_array($form)) ? $form : array();
		$dueform = $this->input->get('dueform');

		//echo '<pre>'; print_r($form); echo '</pre>';

		// get
		$group_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
		$param_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
		$sugg_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);

		// loop
		foreach($form as $competencygroupid => $parr){
			// get
			$rs = $this->competencygroup_model->get($competencygroupid);

			$rs['incnum'] = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;

			$form[$competencygroupid]['rs'] = $rs;

			// loop
			foreach($parr as $competencyparameterid => $sarr){
				// get
				$rs = $this->competencyparameter_model->get($competencyparameterid);

				$rs['incnum'] = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;

				$form[$competencygroupid]['params'][$competencyparameterid]['rs'] = $rs;

				// loop
				foreach($sarr as $competencysuggestionid){
					$dueon = $dueform[$competencygroupid][$competencyparameterid][$competencysuggestionid];

					// get
					$rs = $this->competencysuggestion_model->get($competencysuggestionid);

					$rs['incnum'] = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : 0;

					$rs['dueon'] = date("Y-m-d", strtotime($dueon));

					$form[$competencygroupid]['params'][$competencyparameterid]['suggestions'][$competencysuggestionid]['rs'] = $rs;
				}

				unset($form[$competencygroupid][$competencyparameterid]);
			}
		}

		

		// update
		$data['form'] = $form;

		//echo '<pre>'; print_r($form); echo '</pre>'; exit;

		// set
		$data['loadview'] = 'mobile/coachingsessions/development-plan-submit-modal';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;
		

		// echo
		echo_json($retarr);
	}


	// view development plan add modal
	function viewadddevelopmentplanmodal()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');
		$form = (is_array($form)) ? $form : array();
		$dueform = $this->input->get('dueform');
		$data['userid'] = $this->input->get('userid');
		$data['teamuserid'] = $this->input->get('teamuserid');
		//echo '<pre>'; print_r($form); echo '</pre>';

		// get
		$group_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
		$param_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
		$sugg_companygroupparamsuggrefarr = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);

		// loop
		foreach($form as $competencygroupid => $parr){

			// get
			$rs = $this->competencygroup_model->get($competencygroupid);

			$rs['incnum'] = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;
			$grp_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$competencygroupid), array('where-str' => '(cgpsr.competencyparameterid IS NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
			$rs['sortorder'] = $grp_companygroupparamsuggref['sortorder'];
			

			$form[$competencygroupid]['rs'] = $rs;

			// loop
			foreach($parr as $competencyparameterid => $sarr){
				// get
				$rs = $this->competencyparameter_model->get($competencyparameterid);

				$rs['incnum'] = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;

				$para_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$competencygroupid,'cgpsr.competencyparameterid'=>$competencyparameterid), array('where-str' => '(cgpsr.competencyparameterid IS NOT NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
				$rs['sortorder'] = $para_companygroupparamsuggref['sortorder'];

				$form[$competencygroupid]['params'][$competencyparameterid]['rs'] = $rs;

				// loop
				foreach($sarr as $competencysuggestionid){
					$dueon = $dueform[$competencygroupid][$competencyparameterid][$competencysuggestionid];

					// get
					$rs = $this->competencysuggestion_model->get($competencysuggestionid);

					$rs['incnum'] = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : 0;

					$sugg_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$competencygroupid,'cgpsr.competencyparameterid'=>$competencyparameterid,'cgpsr.competencysuggestionid'=>$competencysuggestionid), array('where-str' => '(cgpsr.competencysuggestionid IS NOT NULL)'),true);
					$rs['sortorder'] = $sugg_companygroupparamsuggref['sortorder'];

					$rs['dueon'] = date("Y-m-d", strtotime($dueon));

					$form[$competencygroupid]['params'][$competencyparameterid]['suggestions'][$competencysuggestionid]['rs'] = $rs;
				}

				unset($form[$competencygroupid][$competencyparameterid]);
			}
		}



		// update
		$data['form'] = $form;
		//echo '<pre>'; print_r($form); echo '</pre>'; exit;

		// set
		$data['loadview'] = 'mobile/coachingsessions/development-plan-add-modal';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;

		
		// echo
		echo_json($retarr);
	}


	// cancel session
	function cancelsession()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['coachingsessionrs'] = $this->coachingsession_model->get($coachingsessionid);

		// process
		if($form){
			// clean
			$form = array_map('trim', $form);
			$form['cancelledbyuserid'] = USERID;
			$form['cancelledon'] = date("Y-m-d H:i:s");

			// save
			$this->coachingsession_model->save($form, $coachingsessionid);

			// msg
			$retarr['msg'] = 'The session was successfully cancelled.';

			// redirect
			$retarr['redirect'] = 'menu.html';

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = 'mobile/coachingsessions/cancel-session-modal';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// view development plan
	function viewdevelopmentplan()
	{
		// set
		$data = array();
		$retarr = array();
		$companyuserteamid = $this->input->get('companyuserteamid');
		$teamuserid = $this->input->get('teamuserid');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		 // check
		$teamuserid = ($type == 'sales-person') ? USERID : $teamuserid;

		// get
		$data['userrs'] = $this->user_model->get($teamuserid);

		// $fp = fopen('data.txt','a');//opens file in append mode  
		// fwrite($fp,json_encode($data['userrs']['userid']));
		// fclose($fp);

		if($type == 'sales-manager'){
			$data['teamuserid'] = $teamuserid;
			$usersessionsuggestionitemrs1=$this->usersessionsuggestionitem_model->get_dev_plan(null,array('ussi.userdevid' =>$teamuserid),array('where-str' =>'ussi.completedon IS NULL'),false,$data['userrs']['smuserid']);
		}
		if($type == 'sales-person'){
			$data['teamuserid'] = '';
			$usersessionsuggestionitemrs1=$this->usersessionsuggestionitem_model->get_dev_plan(null,array('ussi.userdevid' =>USERID),array('where-str' =>'ussi.completedon IS NULL'),false,$data['userrs']['smuserid']);
		}

		$usersessionsuggestionitemrs2=$this->usersessionsuggestionitem_model->get(null, array('cs.salespersonaluserid' => $teamuserid), array('where-str' => 'cs.completedon IS NOT NULL AND ussi.completedon IS NULL', 'having-str' => 'DATE(duedate)>DATE(NOW())', 'order' => array('ussi.dueon' => 'asc', 'ussi.createdon' => 'asc')));

		$data['usersessionsuggestionitemrs'] = array_merge($usersessionsuggestionitemrs1, $usersessionsuggestionitemrs2);
		//array_push($data['usersessionsuggestionitemrs'], var)
		$data1 = ($type == 'sales-person') ? $this->usersessionsuggestionitem_model->get(null,array('ussi.userdevid' =>USERID),array('where-str' =>'ussi.completedon IS NOT NULL')) : array();

		$data2 = ($type == 'sales-person') ? $this->usersessionsuggestionitem_model->get(null, array('cs.salespersonaluserid' => $teamuserid), array('where-str' => 'cs.completedon IS NOT NULL AND ussi.completedon IS NOT NULL', 'order' => array('ussi.completedon' => 'desc', 'ussi.createdon' => 'asc'))) : array();

		$data['usersessionsuggestionitemrs_completed'] = array_merge($data1, $data2);
		//$data['usersessionsuggestionitemrs'] = array_merge($usersessionsuggestionitemrs_completed, $usersessionsuggestionitemrs);

		//echo '<pre>'; print_r($data['usersessionsuggestionitemrs']); echo '</pre>'; exit;

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/coachingsessions/sp-devplan-list' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/coachingsessions/sm-devplan-list' : $data['loadview'];

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;



		// echo
		echo_json($retarr);
	}


	// complete suggestion
	function completesuggestion()
	{
		// set
		$data = array();
		$retarr = array();
		$usersessionsuggestionitemid = $this->input->get('usersessionsuggestionitemid');

		// set
		$arr['completedon'] = date("Y-m-d H:i:s");

		// save
		$this->usersessionsuggestionitem_model->save($arr, $usersessionsuggestionitemid);


		// echo
		echo_json($retarr);
	}


	// deadlines
	public function deadlines()
	{
		// set
		$data = array();
		$retarr = array();
		$rsarr = array();
		$companyusergroupid = $this->input->get('companyusergroupid');
		$companyuserteamid = $this->input->get('companyuserteamid');
		$salespersonaluserid = $this->input->get('salespersonaluserid');
		$salesmanageruserid = $this->input->get('salesmanageruserid');
		$listtype = $this->input->get('listtype');
		//$todate = date("Y-m-d", strtotime("-1 day"));
		//$fromdate = date("Y-m-d", strtotime("$todate -3 months"));
		$fromdate = USER_FROMDATE;
		$todate = USER_TODATE;

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['companyusergrouprs'] = ($companyusergroupid) ? $this->companyusergroup_model->get($companyusergroupid) : null;
		$data['companyuserteamrs'] = ($companyuserteamid) ? $this->companyuserteam_model->get($companyuserteamid) : null;
		$data['spuserrs'] = ($salespersonaluserid) ? $this->user_model->get($salespersonaluserid) : null;
		$data['smuserrs'] = ($salesmanageruserid) ? $this->user_model->get($salesmanageruserid) : null;

		// check
		if($type == 'sales-person'){
			// get sessions
			$where = array('cs.salespersonaluserid' => USERID);
			$other = array('where-str' => '( cs.completedon IS NULL AND cs.cancelledon IS NULL AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'")) )');
			$data['coachingsessionrs'] = $this->coachingsession_model->get(null, $where, $other);
			// get suggestions
			$where = array('cs.salespersonaluserid' => USERID);
			$other = array('where-str' => '( cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL AND (DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<=DATE("'.$todate.'")) )');
			$data['usersessionsuggestionitemrs'] = $this->usersessionsuggestionitem_model->get(null, $where, $other);
		}
		if($type == 'sales-manager'){
			$deadlinesetting = $this->deadlinesetting_model->get(null,array('dl.userid'=>$userrs["userid"],'dl.companyuserteamid'=>$companyuserteamid),null,true);
			$noofdeadline = (isset($deadlinesetting['noofdeadline'])) ? $deadlinesetting['noofdeadline'] : null;

			// get sessions
			$where = array('cs.salespersonaluserid' => $salespersonaluserid);
			$other = array('where-str' => '( cs.completedon IS NULL AND cs.cancelledon IS NULL AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'")) )');
			$data['coachingsessionrs'] = ($salespersonaluserid) ? $this->coachingsession_model->get(null, $where, $other) : null;
			$where = array('cs.salesmanageruserid' => USERID);
			$other = array('where-str' => '( cs.completedon IS NULL AND cs.cancelledon IS NULL AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'")) )');
			$data['coachingsessionrs'] = (!$salespersonaluserid) ? $this->coachingsession_model->get(null, $where, $other) : $data['coachingsessionrs'];
			// get suggestions
			$where = array('cs.salespersonaluserid' => $salespersonaluserid);
			$other = array('where-str' => '( cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL AND (DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<=DATE("'.$todate.'")) )');
			$data['usersessionsuggestionitemrs'] = ($salespersonaluserid) ? $this->usersessionsuggestionitem_model->get(null, $where, $other) : null;
			$where = array('cs.salesmanageruserid' => USERID);
			$other = array('where-str' => '( cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL AND (DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<=DATE("'.$todate.'")) )');
			$data['usersessionsuggestionitemrs'] = (!$salespersonaluserid) ? $this->usersessionsuggestionitem_model->get(null, $where, $other) : $data['usersessionsuggestionitemrs'];
			//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>';
			//echo '<pre>'; print_r($data['usersessionsuggestionitemrs']); echo '</pre>'; exit;



			// check
			if(!$companyuserteamid && !$salespersonaluserid){


				$data['spteamarr'] = array();
				$checkarr = array();

				// loop
				foreach($data['coachingsessionrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonaluserid'], $checkarr)){
						$data['spteamarr'][$row['companyuserteamid']] = $row['spteam'];

						$checkarr[] = $row['salespersonaluserid'];
					}
				}
				foreach($data['usersessionsuggestionitemrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonid'], $checkarr)){
						// get
						$spuserrs = $this->user_model->get($row['salespersonid']);

						$data['spteamarr'][$spuserrs['companyuserteamid']] = $spuserrs['spteam'];

						$checkarr[] = $row['salespersonid'];
					}
				}
				asort($data['spteamarr']);
			}

			// check
			if($companyuserteamid && !$salespersonaluserid){
				// set
				$data['sparr'] = array();

				// get
				$cutr_rs = $this->companyuserteamref_model->get(null, array('cutr.companyuserteamid' => $companyuserteamid));
				$cutr_idarr = rs_to_keyval_array($cutr_rs, 'userid', 'user');

				// loop
				foreach($data['coachingsessionrs'] as $i => $row){
					// check
					if(isset($cutr_idarr[$row['salespersonaluserid']])){
						$data['sparr'][$row['salespersonaluserid']] = $row['salesperson'];
					}
				}
				foreach($data['usersessionsuggestionitemrs'] as $i => $row){
					// check
					if(isset($cutr_idarr[$row['salespersonid']])){
						$data['sparr'][$row['salespersonid']] = $row['salesperson'];
					}
				}
				asort($data['sparr']);
			}
			//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		}
		if($type == 'managing-director'){
			// get ids
			$cuguseridarr = $this->companyusergroupref_model->get_keyval_arr(array('cug.userid' => USERID));
			$idarrstr = (count($cuguseridarr)) ? implode(', ', $cuguseridarr) : null;

			// get sessions
			$where = ($salespersonaluserid) ? array('cs.salespersonaluserid' => $salespersonaluserid) : null;
			$where = (!$salespersonaluserid && $salesmanageruserid) ? array('cs.salesmanageruserid' => $salesmanageruserid) : $where;
			$other = ($idarrstr) ? array('where-str' => '( cs.completedon IS NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND cs.salesmanageruserid IN ('.$idarrstr.') AND (DATE(cs.date)>=DATE("'.$fromdate.'") AND DATE(cs.date)<=DATE("'.$todate.'")) )') : array();
			//echo '<pre>'; print_r($other); echo '</pre>'; exit;
			$data['coachingsessionrs'] = $this->coachingsession_model->get(null, $where, $other);
			//echo count($data['coachingsessionrs']); exit;
			//get suggestions
			$where = ($salespersonaluserid) ? array('cs.salespersonaluserid' => $salespersonaluserid) : null;
			$where = (!$salespersonaluserid && $salesmanageruserid) ? array('cs.salesmanageruserid' => $salesmanageruserid) : $where;
			$other = ($idarrstr) ? array('where-str' => '( cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.deletedon IS NULL AND ussi.completedon IS NULL AND cs.salesmanageruserid IN ('.$idarrstr.') AND (DATE(ussi.dueon)>=DATE("'.$fromdate.'") AND DATE(ussi.dueon)<=DATE("'.$todate.'")) )') : array();
			//echo '<pre>'; print_r($other); echo '</pre>'; exit;
			$data['usersessionsuggestionitemrs'] = $this->usersessionsuggestionitem_model->get(null, $where, $other);
			//echo '<pre>'; print_r($data['usersessionsuggestionitemrs']); echo '</pre>'; exit;
			//echo count($data['usersessionsuggestionitemrs']); exit;

			// groups
			if($listtype == 'groups'){
				$data['smgrouparr'] = array();
				$checkarr = array();

				// loop
				foreach($data['coachingsessionrs'] as $i => $row){
					// check
					if(!in_array($row['salesmanageruserid'], $checkarr)){
						$data['smgrouparr'][$row['companyusergroupid']] = $row['smgroup'];

						$checkarr[] = $row['salesmanageruserid'];
					}
				}
				foreach($data['usersessionsuggestionitemrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonid'], $checkarr)){
						// get
						$smuserrs = $this->user_model->get($row['salesmanagerid']);

						$data['smgrouparr'][$smuserrs['companyusergroupid']] = $smuserrs['smgroup'];

						$checkarr[] = $row['salespersonid'];
					}
				}
				asort($data['smgrouparr']);
			}
			// managers
			if($listtype == 'managers'){
				$data['smanagerarr'] = array();
				$checkarr = array();

				//echo '<pre>'; print_r($data['usersessionsuggestionitemrs']); echo '</pre>'; exit;

				// loop
				foreach($data['coachingsessionrs'] as $i => $row){
					// check
					if(!in_array($row['salesmanageruserid'], $checkarr)){
						$data['smanagerarr'][$row['salesmanageruserid']] = $row['salesmanager'];

						$checkarr[] = $row['salesmanageruserid'];
					}
				}
				foreach($data['usersessionsuggestionitemrs'] as $i => $row){
					// check
					if(!in_array($row['salesmanagerid'], $checkarr)){
						// get
						$smuserrs = $this->user_model->get($row['salesmanagerid']);

						$data['smanagerarr'][$smuserrs['userid']] = $smuserrs['fullname'];

						$checkarr[] = $row['salesmanagerid'];
					}
				}
				asort($data['smanagerarr']);
			}
			// teams
			if($listtype == 'teams'){
				$data['spteamarr'] = array();
				$checkarr = array();

				// loop
				foreach($data['coachingsessionrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonaluserid'], $checkarr)){
						$data['spteamarr'][$row['companyuserteamid']] = $row['spteam'];

						$checkarr[] = $row['salespersonaluserid'];
					}
				}
				foreach($data['usersessionsuggestionitemrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonid'], $checkarr)){
						// get
						$spuserrs = $this->user_model->get($row['salespersonid']);

						$data['spteamarr'][$spuserrs['companyuserteamid']] = $spuserrs['spteam'];

						$checkarr[] = $row['salespersonid'];
					}
				}
				asort($data['spteamarr']);
			}
			// team users
			if($listtype == 'team-users'){
				$data['smteamuserarr'] = array();
				$checkarr = array();

				// loop
				foreach($data['coachingsessionrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonaluserid'], $checkarr)){
						$data['smteamuserarr'][$row['salespersonaluserid']] = $row['salesperson'];

						$checkarr[] = $row['salespersonaluserid'];
					}
				}
				foreach($data['usersessionsuggestionitemrs'] as $i => $row){
					// check
					if(!in_array($row['salespersonid'], $checkarr)){
						// get
						$spuserrs = $this->user_model->get($row['salespersonid']);

						$data['smteamuserarr'][$spuserrs['userid']] = $spuserrs['fullname'];

						$checkarr[] = $row['salespersonid'];
					}
				}
				asort($data['smteamuserarr']);
			}
		}

		//echo count($data['rsarr']);
		//echo '<pre>'; print_r($data); echo '</pre>';

		// loop
		$counter = 0;
		foreach($data['coachingsessionrs'] as $row){
			$id = $row['date'].'-'.$row['coachingsessionid'];

			$arr = array();
			$arr['salesmanager'] = $row['salesmanager'];
			$arr['salesperson'] = $row['salesperson'];
			$arr['customertitle'] = $row['customertitle'];
			$arr['date'] = $row['date'];
			$arr['data'][] = $row['customertitle'];
			$arr['type'] = 'Session';

			if(isset($deadlinesetting) && $deadlinesetting['deadlinetype']=='missed'){
				$sessiondate=strtotime($arr['date']);
				$notificationdate=strtotime($deadlinesetting['notificationdate']);
				if($sessiondate > $notificationdate)
				{
				} else{
					$rsarr[$id] = $arr;
				}
			} else{
				$rsarr[$id] = $arr;
			}

		}
		foreach($data['usersessionsuggestionitemrs'] as $row){
			$competencysuggestionid = $row['competencysuggestionid'];
			$id = $row['duedate'].'-'.$row['coachingsessionid'].'-'.$competencysuggestionid;

			// check
			if(isset($rsarr[$id])){
				$rsarr[$id]['data'][$competencysuggestionid] = $row['suggestion'];
			}else{
				$arr = array();
				$arr['salesmanager'] = $row['salesmanager'];
				$arr['salesperson'] = $row['salesperson'];
				$arr['customertitle'] = $row['customertitle'];
				$arr['date'] = $row['dueon'];
				$arr['data'][$competencysuggestionid] = $row['group_index'].'.'.$row['param_index'].'.'.$row['sugg_index'].'. '.$row['suggestion'];
				$arr['type'] = 'Action';

				if(isset($deadlinesetting) && $deadlinesetting['deadlinetype']=='missed'){
					$sessiondate=strtotime($arr['date']);
					$notificationdate=strtotime($deadlinesetting['notificationdate']);
					if($sessiondate > $notificationdate)
					{
					} else{
						$rsarr[$id] = $arr;
					}
				} else{
					$rsarr[$id] = $arr;
				}
			}
		}

		// sort
		ksort($rsarr);

		if(isset($noofdeadline)){
			$rsarr = array_splice($rsarr, 0, $noofdeadline);
		}

		// update
		$data['rsarr'] = array_reverse($rsarr);

		/*echo count($data['coachingsessionrs']).'<br />';
		echo count($data['usersessionsuggestionitemrs']);
		echo '<pre>'; print_r($data); echo '</pre>'; exit;*/

		// set
		$data['loadview'] = ($type == 'sales-person') ? 'mobile/coachingsessions/deadlines-sp' : '';
		$data['loadview'] = ($type == 'sales-manager') ? 'mobile/coachingsessions/deadlines-sm' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-manager'&& $companyuserteamid && !$salespersonaluserid) ? 'mobile/coachingsessions/deadlines-sm-users' : $data['loadview'];
		$data['loadview'] = ($type == 'sales-manager'&& !$companyuserteamid && !$salespersonaluserid) ? 'mobile/coachingsessions/deadlines-sm-teams' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director') ? 'mobile/coachingsessions/deadlines-md' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director' && $listtype == 'team-users') ? 'mobile/coachingsessions/deadlines-md-team-users' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director' && $listtype == 'teams') ? 'mobile/coachingsessions/deadlines-md-teams' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director' && $listtype == 'managers') ? 'mobile/coachingsessions/deadlines-md-managers' : $data['loadview'];
		$data['loadview'] = ($type == 'managing-director' && $listtype == 'groups') ? 'mobile/coachingsessions/deadlines-md-groups' : $data['loadview'];
		//echo $data['loadview']; exit;

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;
		$retarr['salespersonaluserid'] = $salespersonaluserid;
		// echo
		echo_json($retarr);
	}


	// list session evaluations
	function listsessionevals()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['coachingsessionrs'] = $this->coachingsession_model->get(null, array('cs.salespersonaluserid' => USERID), array('where-str' => 'cs.completedon IS NOT NULL AND cs.cancelledon IS NULL AND cs.ratedon IS NULL'));

		// set
		$data['loadview'] = 'mobile/coachingsessions/list-sessions-eval-sp';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}


	// rate sessions
	function ratesession()
	{
		// set
		$data = array();
		$retarr = array();
		$data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		$form = $this->input->get('form');
		$savedraft = $this->input->get('savedraft');

		// get
		$userrs = $this->user_model->get(USERID);
		$type = $userrs['type'];

		// get
		$data['coachingsessionrs'] = $this->coachingsession_model->get($coachingsessionid);
		$data['companysessionratingrefrs'] = $this->companysessionratingref_model->get(null, array('csr.companyid' => $userrs['companyid']));
		$data['usersessionratingitemarr'] = table_to_array('usersessionratingitem', 'companysessionratingrefid', 'value', array('coachingsessionid' => $coachingsessionid, 'deletedon' => 'IS NULL'), null);
		//echo '<pre>'; print_r($data['companysessionratingrefrs']); echo '</pre>'; exit;

		// process
		if($form){
			// clean
			$form = array_map('trim', $form);

			// loop
			foreach($form as $companysessionratingrefid => $val){
				// get
				$usersessionratingitemrs = $this->usersessionratingitem_model->get(null, array('usri.coachingsessionid' => $coachingsessionid, 'usri.companysessionratingrefid' => $companysessionratingrefid, 'usri.userid' => USERID), null, true);
				$usersessionratingitemid = ($usersessionratingitemrs) ? $usersessionratingitemrs['usersessionratingitemid'] : null;

				// set
				$arr = array(
						'coachingsessionid' => $coachingsessionid,
						'companysessionratingrefid' => $companysessionratingrefid,
						'value' => $val,
						'userid' => USERID
					);

				// save
				$this->usersessionratingitem_model->save($arr, $usersessionratingitemid);
			}

			// update
			if($savedraft && !$data['coachingsessionrs']['ratingbeganon']){
				$this->coachingsession_model->save(array('ratingbeganon' => date("Y-m-d H:i:s")), $coachingsessionid);
			}else{
				$this->coachingsession_model->save(array('ratedon' => date("Y-m-d H:i:s")), $coachingsessionid);
			}

			// msg
			$retarr['msg'] = ($savedraft) ? 'The draft was successfully saved.' : 'Your session ratings were successfully saved.';

			// redirect
			$retarr['redirect'] = ($savedraft) ? 'sp-rate-session.html' : 'menu.html';

			// redirect
			$retarr['redirect'] = 'menu.html';

			// echo
			echo_json($retarr); exit;
		}

		// set
		$data['loadview'] = 'mobile/coachingsessions/sp-rate-session';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;


		// echo
		echo_json($retarr);
	}

	// Add-development-plan
	public function adddevelopmentplan()
	{
		// set
		$data = array();
		$retarr = array();
		// $data['coachingsessionid'] = $coachingsessionid = $this->input->get('coachingsessionid');
		// $data['coachingsessionid'] = $coachingsessionid = "461";
		$form = $this->input->get('form');
		$teamuserid = $this->input->get('teamuserid');

		// $savedraft = $this->input->get('savedraft');
		// $yesterday = date("Y-m-d", strtotime("-1 day"));

		// get
		$userrs = $this->user_model->get(USERID);

		$type = $userrs['type'];
		if($type=='sales-person'){
			$companyuserteamid = $userrs['companyuserteamid'];
			$companyuserteamref = $this->companyuserteamref_model->get(null,array('cutr.userid' => USERID,'cutr.companyuserteamid'=>$companyuserteamid),null,true);
			$companycompetencyplan = $this->companycompetencyplan_model->get(null,array('ccp.companyuserteamid'=>$companyuserteamref['companyuserteamid']),null,true);
			$companycompetencyplanid = $companycompetencyplan['companycompetencyplanid'];
		} else{
			$companyuserteam = $this->companyuserteam_model->get(null,array('cut.userid'=>$userrs['userid']),null,true);
			$companyuserteamid = $companyuserteam['companyuserteamid'];
			// $companyuserteamref = $this->companyuserteamref_model->get(null,array('cutr.userid' => USERID,'cutr.companyuserteamid'=>$companyuserteamid),null,true);
			$companycompetencyplan = $this->companycompetencyplan_model->get(null,array('ccp.companyuserteamid'=>$companyuserteamid),null,true);
			$companycompetencyplanid = $companycompetencyplan['companycompetencyplanid'];
		}

		// $fp = fopen('data.txt','a');//opens file in append mode  
		// fwrite($fp,json_encode($companyuserteamref));
		// fclose($fp);
		// $companyuserteamrefarr = $this->companyuserteamref_model->get_keyval_arr(, null, 'user', 'userid');

		// get
		// $data['coachingsessionrs'] = $this->coachingsession_model->get($coachingsessionid);

		if($type=='sales-person'){

			// get
			$data['group_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL'));
			$data['parameter_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'));
			$data['suggestion_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL'));

			// incnums
			$data['group_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
			$data['param_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
			$data['sugg_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);
		}

		if($type=='sales-manager'){
			// get
			$data['group_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NULL'));
			$data['parameter_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencyparameterid IS NOT NULL AND ccpr.competencysuggestionid IS NULL'));
			$data['suggestion_companycompetencyplanrefrs'] = $this->companycompetencyplanref_model->get(null, array('ccpr.companycompetencyplanid' => $companycompetencyplanid), array('where-str' => 'ccpr.competencysuggestionid IS NOT NULL'));

			// incnums
			$data['group_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_groups(COMPANYID);
			$data['param_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_params(COMPANYID);
			$data['sugg_companygroupparamsuggrefarr'] = $this->companygroupparamsuggref_model->get_keyval_suggs(COMPANYID);
			
		}

		// process
		if($form){
			// post
			$form = array_map('trim', $form);
			$total = array_sum($form);

			$retarr['msg'] = 'Please select at least 1 parameter.';
			echo_json($retarr);
		}
		$data['user'] = $this->user_model->get(USERID);
		if($type=='sales-person'){
			$data['teamuserid'] = '';
			$data['loadview'] = ($type == 'sales-person') ? 'mobile/coachingsessions/adddevelopmentplan-sp' : $data['loadview'];
		} elseif($type=='sales-manager'){
			$data['teamuserid'] = $teamuserid;
			$data['loadview'] = ($type == 'sales-manager') ? 'mobile/coachingsessions/adddevelopmentplan-sm' : $data['loadview'];
		}


		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;

		// echo
		
		echo_json($retarr);
	}

	function savedevelopmentnote()
	{
		$form = $this->input->post('form');
		$teamuserid = $form['companyuserteamid'];

		unset($form['companyuserteamid']);

		if($form){
			$userrs = $this->user_model->get($form['userid']);
			if($userrs['type']=='sales-person'){
				$companyid = $userrs['companyid'];
				$managerid = $userrs['smuserid'];

				$competencygroupid = $form['competencygroupid'];
				$competencyparameterid = $form['competencyparameterid'];
				$competencysuggestionid = $form['competencysuggestionid'];
				$companygroupparamsuggref = $this->companygroupparamsuggref_model->get(null,array('cgpsr.companyid'=>$companyid,'cgpsr.competencygroupid'=>$competencygroupid,'cgpsr.competencyparameterid'=>$competencyparameterid,'cgpsr.competencysuggestionid'=>$competencysuggestionid),null,true);
				$companygroupparamsuggrefid = $companygroupparamsuggref['companygroupparamsuggrefid'];
				// $competencyparameterid = $this->input->get('competencyparameterid');

				$companyuserteam = $this->companyuserteam_model->get(null,array('cut.companyid'=>$companyid,'cut.userid'=>$managerid),null,true);

				$companyuserteamid = $companyuserteam['companyuserteamid'];
				$companycompetencyplan = $this->companycompetencyplan_model->get(null,array('ccp.companyuserteamid'=>$companyuserteamid),null,true);
				$companycompetencyplanid = $companycompetencyplan['companycompetencyplanid'];
			} elseif($userrs['type']=='sales-manager'){
				$companyid = $userrs['companyid'];
				$managerid = $userrs['userid'];

				$competencygroupid = $form['competencygroupid'];
				$competencyparameterid = $form['competencyparameterid'];
				$competencysuggestionid = $form['competencysuggestionid'];
				$companygroupparamsuggref = $this->companygroupparamsuggref_model->get(null,array('cgpsr.companyid'=>$companyid,'cgpsr.competencygroupid'=>$competencygroupid,'cgpsr.competencyparameterid'=>$competencyparameterid,'cgpsr.competencysuggestionid'=>$competencysuggestionid),null,true);
				$companygroupparamsuggrefid = $companygroupparamsuggref['companygroupparamsuggrefid'];
				// $competencyparameterid = $this->input->get('competencyparameterid');

				$companyuserteam = $this->companyuserteam_model->get(null,array('cut.companyid'=>$companyid,'cut.userid'=>$managerid),null,true);
				
				$companyuserteamid = $companyuserteam['companyuserteamid'];

				$companycompetencyplan = $this->companycompetencyplan_model->get(null,array('ccp.companyuserteamid'=>$companyuserteamid),null,true);
				$companycompetencyplanid = $companycompetencyplan['companycompetencyplanid'];
			}
			$timestamps = md5(time()).'.wav';

			$companycompetencyplanref = $this->companycompetencyplanref_model->get(null,array('ccpr.companycompetencyplanid'=>$companycompetencyplanid,'ccpr.competencygroupid'=>$competencygroupid,'ccpr.competencyparameterid'=>$competencyparameterid,'ccpr.competencysuggestionid'=>$competencysuggestionid,'ccpr.companygroupparamsuggrefid'=>$companygroupparamsuggrefid),null,true);

			$companycompetencyplanrefid = $companycompetencyplanref['companycompetencyplanrefid'];

			$usersessionsuggestionitemid = $this->input->get('competencyparameterid');

			// set
			if($userrs['type']=='sales-person'){
				$arr['userdevid'] = $form['userid'];
			}
			if($userrs['type']=='sales-manager'){
				$arr['userdevid'] = $teamuserid;
			}
			$arr['dueon'] = $form['dueon'];
			$arr['companycompetencyplanrefid'] = $companycompetencyplanrefid;
			
			// save
			$this->usersessionsuggestionitem_model->save($arr, $usersessionsuggestionitemid);

			/* create variable */
			$content['competencyparameterid'] = $form['competencyparameterid'];
			$content['title'] = $form['notes'];
			$content['filename'] = (isset($form['filename'])) ? $form['filename'] : $timestamps;
			$content['islocked'] = 0;
			$content['createdcompanyid'] = $userrs['companyid'];
			$content['type'] = 'content';
			/* save content */
			$saved_contentid = $this->content_model->save($content);

			$notefile = $form['notefile'];

			if(isset($notefile) && !empty($notefile)){

				$contents_split = explode(',', $notefile);
				$encoded = $contents_split[count($contents_split)-1];
				$decoded = "";
				for ($i=0; $i < ceil(strlen($encoded)/256); $i++) {
				    $decoded = $decoded . base64_decode(substr($encoded,$i*256,256)); 
				}
				
				$fp = fopen('uploads/_content/'.$timestamps, 'w');
				fwrite($fp, $decoded);
				fclose($fp);

				$content_att_form['contentid'] = $saved_contentid;
				$content_att_form['FileLocation'] = $timestamps;

				$this->contentattachment_model->save($content_att_form);
			}

		}

		$retarr['msg'] = 'The development note was successfully saved.';

		if($userrs['type']=='sales-person'){
			$retarr['redirect'] = 'sp-devplan-list.html';
		} elseif($userrs['type']=='sales-manager'){
			$retarr['companyuserteamid'] = $companyuserteamid;
			$retarr['userid'] = $managerid;
			$retarr['teamuserid'] = $teamuserid;
			$retarr['redirect'] = 'sm-devplan-list.html';
		}

		echo_json($retarr); exit;
	}

	public function savedevplanattechment()
	{
		$userid = $this->input->get('userid');
		$form = $this->input->get('form');

		if($form)
		{
			$contentids = $form['contentid'];
			if($contentids){
				foreach ($contentids as $key => $value) {
					$this->content_model->save(array('islocked'=>1),$value);
				}
			}
		}

		$retarr['msg'] = 'The development attechment was successfully saved.';

		echo_json($retarr); exit;
	}

	/* return modal html */
	function getdevplanattechmentmodal()
	{
		$userid = $this->input->get('userid');
		$competencyparameter = $this->input->get('competencyparameter');

		// get
		$userrs = $this->user_model->get($userid);
		$type = $userrs['type'];

		if($type=='sales-manager')
		{
			if($competencyparameter)
			{
				$content_in_company = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameter,'c.islocked'=>'1'),array('where-str'=>'c.createdcompanyid='.$userrs['companyid']));
				$content_from_admin = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameter,'c.islocked'=>'1'),array('where-str'=>'c.createdcompanyid IS NULL'));
				$data['contentrs'] = array_merge($content_in_company, $content_from_admin);
				$data['competencyparameterrs'] = $this->competencyparameter_model->get($competencyparameter);
			}
		}

		$data['loadview'] = 'mobile/coachingsessions/adddevelopmentplan-modal-sm';

		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		echo_json($retarr);exit;
	}

	public function documentlist()
	{
		$competencysuggestionid = $this->input->get('competencysuggestionid');

		if($competencysuggestionid){
			$competencysuggestion = $this->competencysuggestion_model->get($competencysuggestionid);
			$competencyparameterid = $competencysuggestion['competencyparameterid'];
			$data['contentrs'] = $this->content_model->get(null,array('c.competencyparameterid'=>$competencyparameterid,'c.islocked'=>'1'));
			$data['competencyparameterrs'] = $this->competencyparameter_model->get($competencyparameterid);

		}

		// set
		//echo '<pre>'; print_r($form); echo '</pre>'; exit;

		// set
		$data['loadview'] = 'mobile/coachingsessions/documentlist-sp';

		// load view
		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);
		//echo $retarr['view']; exit;

		// echo
		echo_json($retarr);
	}

	public function getdevplannotemodal()
	{
		$userid = $this->input->get('userid');
		$competencysuggestionid = $this->input->get('competencysuggestionid');
		$userrs = $this->user_model->get($userid);

		if(isset($userrs) && $userrs['type'] == 'sales-person'){
			$data['userid'] = $userrs['userid'];
			$data['salesmanageruserid'] = $userrs['smuserid'];
			$data['salespersonaluserid'] = $userrs['userid'];
			$data['competencysuggestionid'] = $competencysuggestionid;

			$data['loadview'] = 'mobile/coachingsessions/notedevelopmentplan-modal-sp';
		}elseif (isset($userrs) && $userrs['type'] == 'sales-manager') {
			$data['userid'] = $userrs['userid'];
			$data['salesmanageruserid'] = $userrs['smuserid'];
			$data['salespersonaluserid'] = $userrs['userid'];
			$data['competencysuggestionid'] = $competencysuggestionid;

			$data['loadview'] = 'mobile/coachingsessions/notedevelopmentplan-modal-sm';
		}

		

		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);

		echo_json($retarr);exit;
	}

	/* development plan add note on plus button */
	public function savedevplannotemodal()
	{	
		try {
			
			$retarr = array();

			$userid = $this->input->post('userid');
			$form = $this->input->post('form');
			$usernoteid = $this->input->post('usernoteid');
			$competencysuggestionid = $this->input->post('competencysuggestionid');

			$retarr['notetype'] = 'development';
			$retarr['competencysuggestionid'] = $competencysuggestionid;
			
			if($form){
				$notefile = $form['notefile'];

				if($usernoteid!=null && $form['notes']==null){
					// $notedata = $this->usernote_model->get($usernoteid);
					// if($notedata['filelocation']==null){
					// 	// save
					// } else{
					// 	$note_form['notes'] = "Voice note";
					// 	$noteid = $this->usernote_model->save($note_form, $usernoteid);
					// 	$retarr['msg'] = 'The note was successfully update.';
					// 	echo_json($retarr); exit;
					// }
						$noteid = $this->usernote_model->delete($usernoteid);
						// msg
						$retarr['msg'] = 'The note was successfully deleted.';

						echo_json($retarr); exit;
				} elseif($form['notes']!=null){
					$form = array_map('trim', $form);

					if($form['salesmanageruserid']!=null){
						$note_form['salesmanageruserid'] = $form['salesmanageruserid'];
					}
					if($form['salespersonaluserid']!=null){
						$note_form['salespersonaluserid'] = $form['salespersonaluserid'];
					}
					if(isset($form['competencysuggestionid']) && $form['competencysuggestionid']!=null){
						$note_form['competencysuggestionid'] = $form['competencysuggestionid'];
					}
					$note_form['notes'] = $form['notes'];
					$note_form['userid'] = $userid;
					$noteid = $this->usernote_model->save($note_form,$usernoteid);

					$notefile = $form['notefile'];
					if(!empty($notefile)){

						$contents_split = explode(',', $notefile);
						$encoded = $contents_split[count($contents_split)-1];
						$decoded = "";
						for ($i=0; $i < ceil(strlen($encoded)/256); $i++) {
						    $decoded = $decoded . base64_decode(substr($encoded,$i*256,256)); 
						}
						$timestamps = md5(time()).'.wav';
						$fp = fopen('uploads/_content/'.$timestamps, 'w');
						fwrite($fp, $decoded);
						fclose($fp);

						$content_form['usernoteid'] = $noteid;
						$content_form['type'] = 'content';
						$content_form['islocked'] = 1;

						$content_id = null;
						if($noteid){
							$content_info = $this->content_model->get(null,array('c.usernoteid'=>$noteid),null,true);
							$content_id = $content_info['contentid'];
						}

						$contentid = $this->content_model->save($content_form,$content_id);

						if($contentid){
							$attechment_lists = $this->contentattachment_model->get(null,array('c.contentid'=>$contentid));
							if(count($attechment_lists)!=0){
								foreach ($attechment_lists as $attechment) {
									$this->contentattachment_model->delete($attechment['contentattachmentid'],true);
								}
							}
						}

						$content_att_form['contentid'] = $contentid;
						$content_att_form['FileLocation'] = $timestamps;

						$this->contentattachment_model->save($content_att_form);

					}

					$retarr['msg'] = 'The development plan note was successfully saved.';
					$retarr['notetype'] = 'development';
					$retarr['competencysuggestionid'] = $competencysuggestionid;
				} elseif($form['notes']==null && !empty($notefile)){
					$note_form['notes'] = "Voice note";
					$form = array_map('trim', $form);

					if($form['salesmanageruserid']!=null){
						$note_form['salesmanageruserid'] = $form['salesmanageruserid'];
					}
					if($form['salespersonaluserid']!=null){
						$note_form['salespersonaluserid'] = $form['salespersonaluserid'];
					}
					if(isset($form['competencysuggestionid']) && $form['competencysuggestionid']!=null){
						$note_form['competencysuggestionid'] = $form['competencysuggestionid'];
					}
					$note_form['userid'] = $userid;
					$noteid = $this->usernote_model->save($note_form,$usernoteid);

					$notefile = $form['notefile'];
					if(!empty($notefile)){

						$contents_split = explode(',', $notefile);
						$encoded = $contents_split[count($contents_split)-1];
						$decoded = "";
						for ($i=0; $i < ceil(strlen($encoded)/256); $i++) {
						    $decoded = $decoded . base64_decode(substr($encoded,$i*256,256)); 
						}
						$timestamps = md5(time()).'.wav';
						$fp = fopen('uploads/_content/'.$timestamps, 'w');
						fwrite($fp, $decoded);
						fclose($fp);

						$content_form['usernoteid'] = $noteid;
						$content_form['type'] = 'content';
						$content_form['islocked'] = 1;

						$content_id = null;
						if($noteid){
							$content_info = $this->content_model->get(null,array('c.usernoteid'=>$noteid),null,true);
							$content_id = $content_info['contentid'];
						}

						$contentid = $this->content_model->save($content_form,$content_id);

						if($contentid){
							$attechment_lists = $this->contentattachment_model->get(null,array('c.contentid'=>$contentid));
							if(count($attechment_lists)!=0){
								foreach ($attechment_lists as $attechment) {
									$this->contentattachment_model->delete($attechment['contentattachmentid'],true);
								}
							}
						}

						$content_att_form['contentid'] = $contentid;
						$content_att_form['FileLocation'] = $timestamps;

						$this->contentattachment_model->save($content_att_form);

					}

					$retarr['msg'] = 'The development plan note was successfully saved.';
					$retarr['notetype'] = 'development';
					$retarr['competencysuggestionid'] = $competencysuggestionid;
				} else{

					$retarr['notetype'] = 'development';
					$retarr['competencysuggestionid'] = $competencysuggestionid;
				}
			}

			echo_json($retarr);exit;
		} catch (Exception $e) {
			$fp = fopen('data.txt','a');//opens file in append mode  
			fwrite($fp,json_encode($e));
			fclose($fp);

		}
	}

	public function getdevnoteslist()
	{
		// set
		$data = array();
		$retarr = array();

		$userid = $this->input->get('userid');
		$currentuserid = $this->input->get('currentuserid');
		$competencysuggestionid = $this->input->get('competencysuggestionid');
		$competencysuggestion = $this->competencysuggestion_model->get($competencysuggestionid);
		$competencyparameterid = $competencysuggestion['competencyparameterid'];
		$data['userrs'] = $this->user_model->get($userid);
		$fp = fopen('data.txt','a');//opens file in append mode  
			fwrite($fp,json_encode($currentuserid));
			fclose($fp);
		$data['currentuserrs'] = $this->user_model->get($currentuserid);

		// where
		$where = array('un.userid'=>$userid,'un.competencysuggestionid'=>$competencysuggestionid);
		//,'cn.competencyparameterid'=>$competencyparameterid
		$other = array();

		$data['userid'] = $userid;
		$data['usernoters'] = $this->usernote_model->get_usernote(null, $where, $other);

		$data['competencysuggestionid'] = $competencysuggestionid;
		$data['loadview'] = 'mobile/coachingsessions/devnoteslist-sp';

		$retarr['view'] = $this->load->view('mobile/_master-template', array('data' => $data), true);

		echo_json($retarr);exit;
	}
}

/* End of file coachingsessions.php */
/* Location: ./application/modules/admin/controllers/coachingsessions.php */
