

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Coaching Evaluations</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?=ons_select('nummonths', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'), $nummonths, 'onchange="onReportChange(this, \'reports/evaluations\');"');?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <ons-list-item>
                <?
                    // check
                    $rs = (isset($rs)) ? $rs : array();

                    // set
                    $descarr = $rs['ratingdescarr'];

                    // loop
                    foreach($rs['salespersons'][USERID]['rating-rows'] as $ratingid => $row){
                        $row['rating'] = (isset($descarr[$ratingid])) ? $descarr[$ratingid] : '<i>Unknown</i>';
                        $row['team-avg'] = (isset($rs['rating-rows'][$ratingid]['num-avg'])) ? $rs['rating-rows'][$ratingid]['num-avg'] : -1;
                        $class = (($row['num-avg']<$row['team-avg']) ? 'chart-val-bar bar-red' : 'chart-val-bar bar-blue');
                ?>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20 pad-b-10">
                        <div class="chart-title pad-l-10"><?=$row['rating'];?></div>
                        <div class="bar-container">
                            <div class="<?=$class;?>" style="width: <?=round($row['num-avg']);?>%"></div>
                            <div class="chart-val"><?=round($row['num-avg']);?>%</div>
                        </div>
                    </ons-col>
                </ons-row>
                <?}?>
                <?if(!count($rs['salespersons'][USERID]['rating-rows'])){?>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20 pad-b-10">No data available.</ons-col>
                </ons-row>
                <?}?>
            </ons-list-item>
        </ons-list>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue m-b-10" onclick="myNavigator.resetToPage('menu.html', {animation: 'none'});">Home</ons-button>
        </div>
    </div>
