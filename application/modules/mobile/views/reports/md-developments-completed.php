

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Developments Completed</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?=ons_select('monthrange', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'));?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <ons-list-item>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: 50%"></div>
                            <div class="chart-title">Team Member name</div>
                            <div class="chart-val">6</div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: 16%"></div>
                            <div class="chart-title">Team Member name</div>
                            <div class="chart-val">2</div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: 100%"></div>
                            <div class="chart-title">Team Member name</div>
                            <div class="chart-val">12</div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: 66%"></div>
                            <div class="chart-title">Team Member name</div>
                            <div class="chart-val">8</div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: 50%"></div>
                            <div class="chart-title">Team Member name</div>
                            <div class="chart-val">6</div>
                        </div>
                    </ons-col>
                </ons-row>
            </ons-list-item>
        </ons-list>
    </div>
