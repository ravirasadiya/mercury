

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <?if($people_coached){?>
            <ons-list class="pad-b-10">
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('numcoached-report.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">People Coached</ons-col>
                        <ons-col class="col-75px pad-r-25 ta-right"><?=$people_coached;?></ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('report-evaluations.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Rating By Team Members</ons-col>
                        <ons-col class="col-75px pad-r-25 ta-right"><?=$ratings_num_avg;?>%</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('list-competency-reports.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Ratings Per Competency</ons-col>
                        <ons-col class="col-75px pad-r-25 ta-right"><?=$ratings_per_competency;?>%</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('report-development-trends.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Development Trends Per Team Member</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('development-progress-report.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Development Action Progress </ons-col>
                        <ons-col class="col-75px pad-r-25 ta-right"><?=$dev_action_progress;?></ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('list-parameter-reports.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Per Parameter / Competency: No of times assessed as % of total visits</ons-col>
                        <ons-col class="col-65px pad-r-25 ta-right"><?=$eval_times_per_session;?>%</ons-col>
                    </ons-row>
                </ons-list-item>
            </ons-list>
        <?}else{?>
            <ons-list-item>
                <ons-row>
                    <ons-col class="pad-l-20">No data available.</ons-col>
                </ons-row>
            </ons-list-item>
        <?}?>
    </div>
