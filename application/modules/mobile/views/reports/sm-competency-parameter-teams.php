

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Ratings Per Competency</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20"><?=$competencygrouprs['group_index'].'. '.$competencygrouprs['title'];?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?=ons_select('nummonths', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'), $nummonths, 'onchange="onReportChange(this, \'reports/competencyparameterteams\', { competencygroupid: '.$competencygrouprs['competencygroupid'].' });"');?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <ons-list-item>
                <?
                    // check
                    $rs = (isset($rs)) ? $rs : array();

                    // loop
                    foreach($rs as $userid => $row){
                        $class = (($row['avg']<$row['group-avg'])? 'chart-val-bar bar-red' : 'chart-val-bar bar-blue');
                ?>
                <ons-row>
                    <ons-col class="pad-l-20">
                        <div class="bar-container" onclick="myNavigator.pushPage('competency-parameter-report.html', { data: { competencygroupid: <?=$competencygrouprs['competencygroupid'];?>, salespersonaluserid: <?=$userid;?>}, animation: 'none'});">
                            <div class="<?=$class;?>" style="width: <?=round($row['avg']);?>%"></div>
                            <div class="chart-title"><?=$row['name'];?></div>
                            <div class="chart-val"><?=round($row['avg']);?>%</div>
                            <em class="avg" style="left: <?=round($row['group-avg']);?>%;"></em>
                        </div>
                    </ons-col>
                    <ons-col class="col-40px pad-r-20 ta-right"><img class="chevron-img m-t-12" src="images/chevron-right.png" /></ons-col>
                </ons-row>
                <?}?>
                <?if(!count($rs)){?>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20 pad-b-10">No data available.</ons-col>
                </ons-row>
                <?}?>
            </ons-list-item>
        </ons-list>
    </div>
