

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Development Trends</ons-col>
            </ons-row>
            <ons-row>
                <ons-col class="legend"><div class="legend-title">Team</div><div class="legend-img"><img src="images/legend-blue.png" /></div></ons-col>
                <ons-col class="legend"><div class="legend-title">Average</div><div class="legend-img"><img src="images/legend-grey.png" /></div></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?//=ons_select('numratings', array(4 => 'The Last 4 Ratings', 6 => 'The Last 6 Ratings', 12 => 'The Last 12 Ratings'), $numratings, 'onchange="onReportChange(this, \'reports/developmenttrends\');"');?>
                    <?=ons_select('nummonths', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'), $nummonths, 'onchange="onReportChange(this, \'reports/developmenttrends\');"');?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?
                // check
                $rs = (isset($rs)) ? $rs : array();

                // loop
                foreach($rs as $teamid => $row){
            ?>
                <ons-list-item>
					<ons-row>
                        <ons-col class="pad-l-20 pad-r-5 has-nxt" onclick="myNavigator.pushPage('report-development-trends-per-team.html', { data: { teamid: <?=$teamid;?> }, animation: 'none'});"><div class="line-chart-title"><?=$row['name'];?></div>
                            <canvas id="<?=$row['chartid'];?>" class="my-chart" style="float: right;" data-chartdata='<?=$row['chartjson'];?>'></canvas>
						</ons-col>
					</ons-row>
				</ons-list-item>
            <?}?>
            <?if(!count($rs)){?>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20 pad-b-10">No data available.</ons-col>
            </ons-row>
            <?}?>
        </ons-list>
    </div>
