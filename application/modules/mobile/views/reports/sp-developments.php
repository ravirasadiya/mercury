

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Developments</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?=ons_select('nummonths', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'), $nummonths, 'onchange="onReportChange(this, \'reports/developments\');"');?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <ons-list-item>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-red" style="width: <?=($rs['num-suggs-perc-missed']);?>%"></div>
                            <div class="chart-title">Missed</div>
                            <div class="chart-val"><?=$rs['num-suggs-missed'];?></div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: 100%"></div>
                            <div class="chart-title">Total</div>
                            <div class="chart-val"><?=$rs['num-suggs-total'];?></div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: <?=($rs['num-suggs-perc-completed']);?>%"></div>
                            <div class="chart-title">Completed</div>
                            <div class="chart-val"><?=$rs['num-suggs-completed'];?></div>
                        </div>
                    </ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="chart-val-bar bar-blue" style="width: <?=$rs['num-suggs-perc-remaining'];?>%"></div>
                            <div class="chart-title">Remaining</div>
                            <div class="chart-val">(<?=$rs['num-suggs-planned'];?>) <?=$rs['num-suggs-perc-remaining'];?>%</div>
                        </div>
                    </ons-col>
                </ons-row>
            </ons-list-item>
        </ons-list>
    </div>
