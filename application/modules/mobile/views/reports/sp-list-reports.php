

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <?if($people_coached){?>
            <ons-list class="pad-b-10">
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('report-evaluations.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Coaching Evaluations</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('report-development-trends.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Development Trends</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('list-competency-reports.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Ratings Per Competency</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('report-developments.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Developments</ons-col>
                    </ons-row>
                </ons-list-item>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('list-parameter-reports.html', { animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20">Per Parameter / Competency: No of times assessed as % of total visits</ons-col>
                    </ons-row>
                </ons-list-item>
            </ons-list>
        <?}else{?>
            <ons-list-item>
                <ons-row>
                    <ons-col class="pad-l-20">No sessions to generate reports with.</ons-col>
                </ons-row>
            </ons-list-item>
        <?}?>
    </div>
