

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Per Parameter / Competency: No of times assessed as % of total visits</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?=ons_select('nummonths', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'), $nummonths, 'onchange="onReportChange(this, \'reports/listparameterreports\');"');?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?
                // check
                $rs = (isset($rs)) ? $rs : array();

                // loop
                foreach($rs as $competencygroupid => $row){
            ?>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('parameters-assessed-team-report.html', { data: { competencygroupid: <?=$competencygroupid;?> }, animation: 'none'});">
					<ons-row>
						<ons-col class="pad-l-20"><?=$row['title'];?></ons-col>
						<ons-col class="col-55px pad-r-25 ta-right"><?=round($row['avg']);?>%</ons-col>
					</ons-row>
				</ons-list-item>
            <?}?>
            <?if(!count($rs)){?>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20 pad-b-10">No data available.</ons-col>
            </ons-row>
            <?}?>
        </ons-list>
    </div>
