

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Reports</div>
        <div class="right"><img src="images/share.png" class="toolbar-img-right" data-openmodal="#reports-modal"/></div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Per Parameter / Competency: No of times assessed as % of total visits</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20"><?=$competencygrouprs['group_index'].'. '.$competencygrouprs['title'];?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20"><?=$spuserrs['fullname'];?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20 pad-r-20">
                    <?=ons_select('nummonths', array(3 => 'The Last 3 Months', 6 => 'The Last 6 Months', 12 => 'The Last 12 Months'), $nummonths, 'onchange="onReportChange(this, \'reports/parametersassessed\', { competencygroupid: '.$competencygrouprs['competencygroupid'].', salespersonaluserid: '.$spuserrs['userid'].' });"');?>
                </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <ons-list-item>
                <?
                    // check
                    $rs = (isset($rs)) ? $rs : array();

                    // loop
                    foreach($rs as $userid => $row){
                        $class = (($row['avg']<$row['team-avg']) ? 'chart-val-bar bar-red' : 'chart-val-bar bar-blue');
                ?>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20">
                        <div class="bar-container">
                            <div class="<?=$class;?>" style="width: <?=round($row['avg']);?>%"></div>
                            <div class="chart-title"><?=$row['title'];?></div>
                            <div class="chart-val"><?=round($row['avg']);?>%</div>
                            <em class="avg" style="left: <?=round($row['team-avg']);?>%;"></em>
                        </div>
                    </ons-col>
                </ons-row>
                <?}?>
                <?if(!count($rs)){?>
                <ons-row>
                    <ons-col class="pad-l-20 pad-r-20 pad-b-10">No data available.</ons-col>
                </ons-row>
                <?}?>
            </ons-list-item>
        </ons-list>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue m-b-10" onclick="myNavigator.resetToPage('menu.html', {animation: 'none'});">Home</ons-button>
        </div>
    </div>
