<ons-toolbar>

<div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
<div class="center">Add to Development Plan</div>
</ons-toolbar>
<form name="adddevelopmentform">
<div class="content">
    <ons-list-header>
      <ons-row>
          <ons-col class="col-70 pad-l-20"><?=$user['fullname'];?></ons-col>
          <ons-col class="col-30 pad-r-25 ta-right"><?=date("j M y");?></ons-col>
      </ons-row>
    </ons-list-header>
            <input type="hidden" name="coachingsessionid" value="" />
            <ons-list class="list-70 pad-b-10 expand-list-cont">
            <?
                $i = 0;
                $mdlarr = array();
                usort($group_companycompetencyplanrefrs, function($a, $b) {
                    return $a['sortorder'] - $b['sortorder'];
                });
                foreach($group_companycompetencyplanrefrs as $grow){
                    $competencygroupid = $grow['competencygroupid'];

                    $gsortorder = $grow['sortorder'];
                    $incnum = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;

                    $i++;
            ?>
                <ons-list-item>
                    <ons-row class="blue">
                        <ons-col class="pad-l-20 bold"><?=$gsortorder;?>. <?=$grow['group'];?></ons-col>
                    </ons-row>
                    <div class="detail-panel is-hidden">
                        <?
                            $ii = 0;
                            usort($parameter_companycompetencyplanrefrs, function($a, $b) {
                                return $a['sortorder'] - $b['sortorder'];
                            });
                            foreach($parameter_companycompetencyplanrefrs as $prow){
                                $competencyparameterid = $prow['competencyparameterid'];
                                $companycompetencyplanrefid = $prow['companycompetencyplanrefid'];

                                // check
                                if($prow['competencygroupid'] != $competencygroupid){
                                    continue;
                                }

                                $psortorder = $prow['sortorder'];
                                $p_incnum = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;

                                // set
                                $id = 'infomodal-'.$i.'-'.$ii.'-'.$competencyparameterid;

                                // add
                                $mdlarr[$id] = $prow;

                                $ii++;
                        ?>
                            <ons-row>
                                <ons-col class="pad-l-20">
                                    <div class="bar-container">
                                        <div class="chart-title1">
                                          <?=$gsortorder.'.'.$psortorder;?><?=htmlspecialchars($prow['parameter']);?></div>
                                    </div>
                                </ons-col>
                            </ons-row>

                            <div class="detail-panel is-hidden pad-b-10">
                            <ons-row>
                                <ons-col class="pad-l-20 pad-r-20">
                                    <div class="info-link" data-openmodal="#<?=$id;?>"><img src="images/info.png" />Information</div>
                                </ons-col>
                            </ons-row>
                            <?
                                usort($suggestion_companycompetencyplanrefrs, function($a, $b) {
                                    return $a['sortorder'] - $b['sortorder'];
                                });
                                foreach($suggestion_companycompetencyplanrefrs as $srow){
                                    $competencysuggestionid = $srow['competencysuggestionid'];
                                    $hasdate = (isset($usersessionsuggestionitemarr[$srow['companycompetencyplanrefid']])) ? $usersessionsuggestionitemarr[$srow['companycompetencyplanrefid']] : null;

                                    // check
                                    if($srow['competencygroupid'] != $competencygroupid || $srow['competencyparameterid'] != $competencyparameterid){
                                        continue;
                                    }

                                    $id = 'cb-'.$i.'-'.$ii.'-'.$competencysuggestionid;
                                    $date = ($hasdate) ? $hasdate : date("Y-m-d", strtotime("+7 days"));
                                    $checked = ($hasdate) ? 'checked="checked"' : '';
                                    $date_class = ($hasdate) ? 'sel-sugg-date is-selected' : 'sel-sugg-date';

                                    $ssortorder = $srow['sortorder'];
                                    $s_incnum = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : 0;
                            ?>
                            <ons-row>
                                <ons-col class="col-80 pad-l-20">
                                    <ons-checkbox name="form[<?=$competencygroupid;?>][<?=$competencyparameterid;?>][<?=$competencysuggestionid;?>]" value="<?=$competencysuggestionid;?>" input-id="<?=$id;?>" <?=$checked;?>></ons-checkbox>
                                    <label for="<?=$id;?>"><?=$gsortorder.'.'.$psortorder.'.'.$ssortorder;?> <?=htmlspecialchars($srow['suggestion']);?></label>
                                </ons-col>
                                <ons-col class="col-20 aln-c">
                                    <span class="<?=$date_class;?>"><ons-icon icon="fa-calendar"></ons-icon></span>
                                    <input type="date" name="dueform[<?=$competencygroupid;?>][<?=$competencyparameterid;?>][<?=$competencysuggestionid;?>]" class="cal-inp-hide" value="<?=$date;?>" data-rootdate="<?=date("Y-m-d");?>" />
                                </ons-col>
                            </ons-row>
                            <?}?>
                        </div>

                        <?}?>
                    </div>
                </ons-list-item>
            <?}?>
            </ons-list>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue is-submit-btn" 
            data-opendevelopmentmodel>Add to Plan</ons-button>
        </div>
</div>
</form>