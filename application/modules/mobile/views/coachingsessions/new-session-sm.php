

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">
            <?if($movesession){?>
                Move Coaching Session
            <?}else{?>
                New Coaching Session
            <?}?>
        </div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content" style="text-align: center">
        <div class="form">
            <form name="savesession">
                <input type="hidden" name="coachingsessionid" value="<?=$coachingsessionid;?>" />
                <?if($movesession){?>
                    <?=ons_select('companyuserteamid', $companyuserteamarr, $companyuserteamid, 'class="is-req sel-team" modifier="underbar" disabled="disabled"');?>
                    <?=ons_select('form[salespersonaluserid]', $companyuserteamrefrs, $coachingsessionrs['salespersonaluserid'], 'class="is-req sel-sp-user" modifier="underbar" disabled="disabled"');?>
                    <ons-input name="form[customertitle]" class="is-req" type="text" modifier="underbar" value="<?=$coachingsessionrs['customertitle'];?>" placeholder="Customer Name" disabled="disabled"></ons-input>
                    <ons-input name="form[customerno]" type="text" modifier="underbar" value="<?=$coachingsessionrs['customerno'];?>" placeholder="Customer Number (optional)" disabled="disabled"></ons-input>

                    <input type="hidden" name="companyuserteamids" value="<?=$companyuserteamid;?>" />
                    <input type="hidden" name="form[salespersonaluserid]" value="<?=$coachingsessionrs['salespersonaluserid'];?>" />
                    <input type="hidden" name="form[customertitle]" value="<?=$coachingsessionrs['customertitle'];;?>" />
                    <input type="hidden" name="form[customerno]" value="<?=$coachingsessionrs['customerno'];;?>" />
                <?}else{?>
                    <?=ons_select('companyuserteamid', $companyuserteamarr, $companyuserteamid, 'class="is-req sel-team" modifier="underbar"');?>
                    <?=ons_select('form[salespersonaluserid]', $companyuserteamrefrs, $coachingsessionrs['salespersonaluserid'], 'class="is-req sel-sp-user" modifier="underbar"');?>
                    <ons-input name="form[customertitle]" class="is-req" type="text" modifier="underbar" value="<?=$coachingsessionrs['customertitle'];?>" placeholder="Customer Name"></ons-input>
                    <ons-input name="form[customerno]" type="text" modifier="underbar" value="<?=$coachingsessionrs['customerno'];?>" placeholder="Customer Number (optional)"></ons-input>
                <?}?>
                <ons-input name="form[date]" type="date" class="is-date is-req" modifier="underbar" value="<?//=$coachingsessionrs['date'];?>" placeholder="<?=($coachingsessionrs['date'] ? $coachingsessionrs['date'] : 'Date');?>" data-rootdate="<?=date("Y-m-d");?>"></ons-input>
                <ons-input name="form[time]" type="time" class="is-time is-req" modifier="underbar" value="<?//=$coachingsessionrs['time'];?>" placeholder="<?=($coachingsessionrs['time'] ? $coachingsessionrs['time'] : 'Time');?>" data-roottime="<?=date("H:i");?>"></ons-input>
                <ons-button class="btn-lrg bg-blue is-submit-btn"><?=($coachingsessionid? 'Update Session' : 'Book Session');?></ons-button>
                <ons-button class="btn-thn bg-grey" onclick="myNavigator.popPage();">Cancel</ons-button>
            </form>
        </div>
    </div>
