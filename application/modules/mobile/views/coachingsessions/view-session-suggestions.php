

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.pushPage('menu.html', {animation: 'none'});" /></div>

        <div class="center">Development Areas</div>
        <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="showDevPlan('sessionsuggestions');"/></div>
        <!-- data-openmodal="#devplanModal" -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-70 pad-l-20"><?=$coachingsessionrs['salesperson'];?></ons-col>
                <ons-col class="col-30 pad-r-25 ta-right"><?=date("j M y", strtotime($coachingsessionrs['date']));?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list>
            <form name="sessionsuggestions">
            <input type="hidden" name="coachingsessionid" value="<?=$coachingsessionid;?>" />
            <ons-list class="list-70 pad-b-10 expand-list-cont">
                <?
                    $mdlarr = array();
                    $i = 0;
                    usort($group_companycompetencyplanrefrs, function($a, $b) {
                        return $a['sortorder'] - $b['sortorder'];
                    });
                    foreach($group_companycompetencyplanrefrs as $grow){
                        $competencygroupid = $grow['competencygroupid'];

                        // check
                        if(!isset($usersessionparameteritemarr[$competencygroupid])){
                            continue;
                        }

                        // check
                        $limitval = (isset($sp_usercompetencylimitarr[$competencygroupid]['value'])) ? $sp_usercompetencylimitarr[$competencygroupid]['value'] : 0;

                        $grouparr = $usersessionparameteritemarr[$competencygroupid];
                        $group_avg = ($grouparr['avg']) ? $grouparr['avg'] : 0;

                        $gsortorder = $grow['sortorder'];
                        $incnum = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;

                        $i++;

                        // check
                        if(!round($group_avg)){
                            continue;
                        }
                ?>
                <ons-list-item>
                    <ons-row class="blue">
                        <ons-col class="col-80 pad-l-20 bold"><?=$gsortorder;?>. <?=htmlspecialchars($grow['group']);?></ons-col>
                        <ons-col class="col-20 pad-r-20 bold ta-right"><?=round($group_avg);?>%</ons-col>
                    </ons-row>
                    <div class="detail-panel is-hidden">
                        <?
                            $ii = 0;
                            usort($parameter_companycompetencyplanrefrs, function($a, $b) {
                                return $a['sortorder'] - $b['sortorder'];
                            });
                            foreach($parameter_companycompetencyplanrefrs as $prow){
                                $competencyparameterid = $prow['competencyparameterid'];
                                $companycompetencyplanrefid = $prow['companycompetencyplanrefid'];

                                // check
                                $limitval = (isset($sp_usercompetencylimitarr[$competencygroupid][$competencyparameterid])) ? $sp_usercompetencylimitarr[$competencygroupid][$competencyparameterid]['value'] : 0;

                                // check
                                if($prow['competencygroupid'] != $competencygroupid){
                                    continue;
                                }

                                // check
                                $val = $grouparr['pararr'][$competencyparameterid]['value'];
                                $val = ($val) ? $val : 0;

                                // set
                                $id = 'infomodal-'.$i.'-'.$ii.'-'.$competencyparameterid;

                                // add
                                $mdlarr[$id] = $prow;

                                // check
                                $bar_class = ($limitval > $val) ? 'bar-red' : 'bar-blue';

                                $psortorder = $prow['sortorder'];
                                $p_incnum = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;

                                $ii++;

                                // check
                                if(round($val) == 0){
                                    continue;
                                }
                        ?>
                        <ons-row>
                            <ons-col class="pad-l-20 pad-r-20">
                                <div class="bar-container">
                                    <div class="chart-val-bar <?=$bar_class;?>" style="width: <?=$val;?>%;"></div>
                                    <div class="chart-title"><?=$gsortorder.'.'.$psortorder;?> <?=htmlspecialchars($prow['parameter']);?></div>
                                    <div class="chart-val"><?=$val;?>%</div>
                                </div>
                            </ons-col>
                        </ons-row>
                        <div class="detail-panel is-hidden pad-b-10">
                            <ons-row class="col-100">
                                <ons-col class="col-50 pad-l-20 pad-r-20">
                                    <div class="info-link" data-openmodal="#<?=$id;?>"><img src="images/info.png" />Information</div>
                                </ons-col>
                                <ons-col class="col-50 pad-l-20 pad-r-20 ta-right">
                                    <div class="info-link" data-openattachment data-competencyparameter="<?= $competencyparameterid ?>"><img src="images/attachment.png" />Attachments</div>
                                </ons-col>
                            </ons-row>
                            <?
                                usort($suggestion_companycompetencyplanrefrs, function($a, $b) {
                                    return $a['sortorder'] - $b['sortorder'];
                                });
                                foreach($suggestion_companycompetencyplanrefrs as $srow){
                                    $competencysuggestionid = $srow['competencysuggestionid'];
                                    $hasdate = (isset($usersessionsuggestionitemarr[$srow['companycompetencyplanrefid']])) ? $usersessionsuggestionitemarr[$srow['companycompetencyplanrefid']] : null;

                                    // check
                                    if($srow['competencygroupid'] != $competencygroupid || $srow['competencyparameterid'] != $competencyparameterid){
                                        continue;
                                    }

                                    $id = 'cb-'.$i.'-'.$ii.'-'.$competencysuggestionid;
                                    $date = ($hasdate) ? $hasdate : date("Y-m-d", strtotime("+7 days"));
                                    $checked = ($hasdate) ? 'checked="checked"' : '';
                                    $date_class = ($hasdate) ? 'sel-sugg-date is-selected' : 'sel-sugg-date';

                                    $ssortorder = $srow['sortorder'];
                                    $s_incnum = (isset($sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid])) ? $sugg_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid][$competencysuggestionid] : 0;
                            ?>
                            <ons-row>
                                <ons-col class="col-80 pad-l-20">
                                    <ons-checkbox name="form[<?=$competencygroupid;?>][<?=$competencyparameterid;?>][<?=$competencysuggestionid;?>]" value="<?=$competencysuggestionid;?>" input-id="<?=$id;?>" <?=$checked;?>></ons-checkbox>
                                    <label for="<?=$id;?>"><?=$gsortorder.'.'.$psortorder.'.'.$ssortorder;?> <?=htmlspecialchars($srow['suggestion']);?></label>
                                </ons-col>
                                <ons-col class="col-20 aln-c">
                                    <span class="<?=$date_class;?>"><ons-icon icon="fa-calendar"></ons-icon></span>
                                    <input type="date" name="dueform[<?=$competencygroupid;?>][<?=$competencyparameterid;?>][<?=$competencysuggestionid;?>]" class="cal-inp-hide" value="<?=$date;?>" data-rootdate="<?=date("Y-m-d");?>" />
                                </ons-col>
                            </ons-row>
                            <?}?>
                        </div>
                        <?}?>
                    </div>
                </ons-list-item>
                <?}?>
            </ons-list>
            </form>
        </ons-list>

        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue btn-notes no-refresh" data-opennotesmodel="<?=$coachingsessionrs['coachingsessionid'];?>" data-usernoteid="">Notes</ons-button>
        </div>
    </div>

    <?foreach($mdlarr as $id => $row){?>

    	<ons-modal id="<?=$id;?>" class="msg-modal">
    		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">
    			<h4 class="grey ta-left"><?=$row['parameter'];?>:</h4>
    			<p class="blue ta-left"><?=nl2br($row['parameterdescription']);?></p>
    			<ons-button class="btn-thn bg-grey">close</ons-button>
    		</div>
    	</ons-modal>

    <?}?>
