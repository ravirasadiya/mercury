

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Sessions To Evaluate</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-80 pad-l-20">Customer</ons-col>
                <ons-col class="col-20 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?
                foreach($coachingsessionrs as $row){
                    $row['customertitle'] .= ($row['ratingbeganon']) ? ' <span class="red">(TBC)</span>' : '';
            ?>
            <ons-list-item tappable onclick="myNavigator.pushPage('sp-rate-session.html', { data: { coachingsessionid: <?=$row['coachingsessionid'];?> }, animation: 'none'});">
                <ons-row>
                    <ons-col class="col-80 pad-l-20"><?=$row['customertitle'];?></ons-col>
                    <ons-col class="col-20 pad-r-20 ta-right"><?=date("j M y", strtotime($row['date']));?></ons-col>
                </ons-row>
            </ons-list-item>
            <?}?>
            <?if(!$coachingsessionrs){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No sessions currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>
