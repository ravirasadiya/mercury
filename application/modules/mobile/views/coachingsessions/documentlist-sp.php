<ons-toolbar>
   <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
   <div class="center">Documents</div>
</ons-toolbar>
<div class="content">
   <div class="form">
      <form name="profile">
         <ons-list-item>
            <ons-row class="blue">
               <ons-col class="bold pad-l-20 pad-r-20"><?= $competencyparameterrs['title'] ?></ons-col>
            </ons-row>
         </ons-list-item>
         <ons-list-item>
         	<?php if(isset($contentrs) && count($contentrs)!=0): ?>

                <?php foreach ($contentrs as $content): ?>
                  <ons-list-item>
                    <ons-row class="blue pad-l-20 pad-r-20 pad-t-20" style="text-align: left;">
                        	<ons-col class="col-70 note" style="vertical-align: center;"><?= $content['title'] ?></ons-col>
			                <ons-col class="col-30 ta-right">
			                  <div class="ta-right" style="margin-top: 4px;">
			                  	<?php $ext = pathinfo($content['filelocation'], PATHINFO_EXTENSION); ?>
			                  	<?php if($ext=='wav' || $ext=='jpg' || $ext=='jpeg' || $ext=='png'): ?>
                            <a onclick="downloadFile('<?= $content['filelocation'] ?>')">
  			                    	<img class="" data-filepath="<?= $content['filelocation'] ?>" src="images/play.png" style="width: 25px; height: 25px; margin-right: 20px;"/>
                            </a>
			                  	<?php endif; ?>
			                  	<a href="<?= $content['filelocation'] ?>" download>
			                    	<img src="images/download.png" class="edit-note-btn" style="width: 25px; height: 25px;"  />
                            <!-- onclick="downloadFile('<?= $content['filelocation'] ?>')" -->
			                    </a>
			                  </div>
			                </ons-col>
                    </ons-row>
                  </ons-list-item>
                <?php endforeach; ?>

            <?php endif; ?>
            <!-- <ons-row>
               <ons-col class="col-60 note" style="vertical-align: center;">DocumentTitle.pdf</ons-col>
               <ons-col class="col-40 ta-right">
                  <div class="ta-right" style="margin-top: 4px;">
                     <img src="images/download.png" class="edit-note-btn" style="width: 25px; height: 25px;"/>
                  </div>
               </ons-col>
            </ons-row> -->
         </ons-list-item>
        <!--  <ons-list-item>
            <ons-row class="mar-t-10">
               <ons-col class="col-60 note" style="vertical-align: center;">DocumentTitle.mov</ons-col>
               <ons-col class="col-40 ta-right">
                  <div class="ta-right" style="margin-top: 4px;">
                    <img class="media_buttons" src="images/play.png" style="width: 25px; height: 25px; margin-right: 20px;"/>
                    <img src="images/download.png" class="edit-note-btn" style="width: 25px; height: 25px;"/>
                  </div>
               </ons-col>
            </ons-row>
         </ons-list-item> -->
      </form>
   </div>
</div>