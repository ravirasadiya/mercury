<div class="form">

	<div class="content" style="text-align: center">
        <div class="form">
			<div class="blue bold fnt-s-20 pad-20 heading">Add A Note</div>
            <form name="addnotesdevelopmentmodal">
				<input type="hidden" name="ismodal" value="1" />
				<input type="hidden" name="userid" value="<?= $userid ?>" />
                <input type="hidden" name="form[competencysuggestionid]" value="<?= $competencysuggestionid ?>" />
                <input type="hidden" name="form[salespersonaluserid]" value="<?= $salespersonaluserid ?>" />
                <input type="hidden" name="form[salesmanageruserid]" value="<?= $salesmanageruserid  ?>" />
                <input type="hidden" name="norefresh" value="" />
                <input id="setbase64Note" type="hidden" name="form[notefile]" value="">
                <!-- <input type="hidden" id="audioSource" value="0" /> -->

				<div id="infoMessage" style="color: black;"></div>
                <div id="infoTimer" style="color: black;"></div>

                <textarea name="form[notes]" id="plannoteareaid" class="add-note textarea textarea--transparent mar-b-20" placeholder="Add note..."></textarea>

              <!--   <div id="wait" style="display:block;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='images/info.png' width="64" height="64" /><br>Loading..</div> -->
              	<ons-progress-bar id="progress" style="display: none; vertical-align: middle;align-items: center;" indeterminate></ons-progress-bar>

				<ons-row>
					<ons-button id="addnote1" class="col-75 btn-lrg bg-blue is-submit-btn" style="margin-right:5%;">Add Note</ons-button>
					
         		<ons-button id="start_capture_note_btn" class="col-20 btn-lrg bg-blue startCaptureNote"><img src="images/mic.png" style="vertical-align: middle; width: 30px; height: 30px; " ></ons-button>

         		<ons-button id="stop_capture_note_btn" style="display: none;" class="col-20 btn-lrg bg-blue stopCaptureNote"><img src="images/stop_white.png" id="stop_capture_img" style="vertical-align: middle; width: 30px; height: 30px; " ></ons-button>

         		<!-- <ons-button class="col-20 btn-lrg bg-blue stopCapture " style="margin-left:5%;"><img src="images/mic.png" style="vertical-align: middle; width: 30px; height: 30px; "></ons-button> -->

    		</ons-row>
                <ons-button id="addnoteback1" class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
                 <div id="recording-list" class="w3-section w3-border margin-5 w3-small"
                 style="word-break: break-all; color: black;">...Recordings will show up here...
            </div>
            </form>
        </div>
    </div>

</div>