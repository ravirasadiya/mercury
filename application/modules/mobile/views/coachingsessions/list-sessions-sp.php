

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.pushPage('menu.html', { animation: 'none'});" /></div>
        <div class="center">Scheduled Sessions</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-80 pad-l-20">Customer</ons-col>
                <ons-col class="col-20 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="list-70 pad-b-10">
            <?
                $today = date("Y-m-d");
                foreach($coachingsessionrs as $row){
                    $istoday = (date("Y-m-d", strtotime($row['date'])) == $today) ? 1 : 0;
                    
                    $class = ($istoday) ? 'is-today' : '';
                    $style = ($istoday) ? 'background-color: rgba(100,255,100,0.5);' : '';
            ?>
                <ons-list-item class="<?=$class;?>" style="<?=$style;?>" tappable onclick="myNavigator.pushPage('view-session.html', { data: { coachingsessionid: <?=$row['coachingsessionid'];?>}, animation: 'none'});">
                    <ons-row>
                        <ons-col class="col-80 pad-l-20"><?=$row['customertitle'];?></ons-col>
                        <ons-col class="col-20 pad-r-20 ta-right">
                            <?=date("j M y", strtotime($row['date']));?><br />
                            <small><?=date("H:i", strtotime($row['time']));?></small>
                        </ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            <?if(!$coachingsessionrs){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No sessions currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            <?if($cancelledcoachingsessionrs){?>
                <ons-list-header>
                    <ons-row class="red">
                        <ons-col class="aln-c">Cancelled Sessions</ons-col>
                    </ons-row>
                </ons-list-header>
                <ons-list-header>
                    <ons-row>
                        <ons-col class="col-33 pad-l-20">Sales Manager</ons-col>
                        <ons-col class="col-34 pad-l-10">Customer</ons-col>
                        <ons-col class="col-33 pad-l-10">Cancelled By</ons-col>
                    </ons-row>
                </ons-list-header>
                <?
                    $sessioncancelledbyarr = config_item('session-cancelled-by-arr');
                    foreach($cancelledcoachingsessionrs as $row){
                        $cancelledby = $sessioncancelledbyarr[$row['cancelledby']];
                ?>
                    <ons-list-item>
                        <ons-row>
                            <ons-col class="col-33 pad-l-20"><?=$row['salesmanager'];?></ons-col>
                            <ons-col class="col-34 pad-l-10"><?=$row['customertitle'];?></ons-col>
                            <ons-col class="col-33 pad-l-10"><?=$cancelledby;?></ons-col>
                        </ons-row>
                        <ons-row class="fnt-i">
                            <ons-col class="pad-l-20">Reason: <?=$row['cancelledreason'];?></ons-col>
                        </ons-row>
                    </ons-list-item>
                <?}?>
            <?}?>
        </ons-list>
    </div>
