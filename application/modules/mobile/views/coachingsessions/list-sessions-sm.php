

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.pushPage('menu.html', { animation: 'none'});" /></div>
        <div class="center">Scheduled Sessions</div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-40 pad-l-20">Team Member</ons-col>
                <ons-col class="col-40 pad-l-10">Customer</ons-col>
                <ons-col class="col-20 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="list-70 pad-b-10">
            <?
                $yesterday = date("Y-m-d", strtotime("-1 day"));
                $today = date("Y-m-d");
                foreach($coachingsessionrs as $row){
                    $row['salesperson'] .= ($row['beganon']) ? ' <span class="red">(TBC)</span>' : '';

                    $isyesterday = (date("Y-m-d", strtotime($row['date'])) == $yesterday) ? 1 : 0;
                    $istoday = (date("Y-m-d", strtotime($row['date'])) == $today) ? 1 : 0;

                    $class = ($isyesterday) ? 'is-yesterday' : '';
                    $class = ($istoday) ? 'is-today' : $class;
                    $style = ($isyesterday) ? 'background-color: rgba(255,100,100,0.5);' : '';
                    $style = ($istoday) ? 'background-color: rgba(100,255,100,0.5);' : $style;
            ?>
                <?if(date("Y-m-d", strtotime($row['date'])) > date("Y-m-d")){?>
                    <ons-list-item class="<?=$class;?>" style="<?=$style;?>" tappable onclick="confirmPushPage('view-session.html', { data: { coachingsessionid: <?=$row['coachingsessionid'];?>}, animation: 'none'}, 'This is a future dated session, do you want to continue?');">
                        <ons-row>
                            <ons-col class="col-40 pad-l-20"><?=$row['salesperson'];?></ons-col>
                            <ons-col class="col-40 pad-l-10"><?=$row['customertitle'];?></ons-col>
                            <ons-col class="col-20 pad-r-20 ta-right">
                                <?=date("j M y", strtotime($row['date']));?><br />
                                <small><?=date("H:i", strtotime($row['time']));?></small>
                            </ons-col>
                        </ons-row>
                    </ons-list-item>
                <?}else{?>
                    <ons-list-item class="<?=$class;?>" style="<?=$style;?>" tappable onclick="myNavigator.pushPage('view-session.html', { data: { coachingsessionid: <?=$row['coachingsessionid'];?>}, animation: 'none'});">
                        <ons-row>
                            <ons-col class="col-40 pad-l-20"><?=$row['salesperson'];?></ons-col>
                            <ons-col class="col-40 pad-l-10"><?=$row['customertitle'];?></ons-col>
                            <ons-col class="col-20 pad-r-20 ta-right">
                                <?=date("j M y", strtotime($row['date']));?><br />
                                <small><?=date("H:i", strtotime($row['time']));?></small>
                            </ons-col>
                        </ons-row>
                    </ons-list-item>
                <?}?>
            <?}?>
            <?if(!$coachingsessionrs){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No sessions currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            <?if($cancelledcoachingsessionrs){?>
                <ons-list-header>
                    <ons-row class="red">
                        <ons-col class="aln-c">Cancelled Sessions</ons-col>
                    </ons-row>
                </ons-list-header>
                <ons-list-header>
                    <ons-row>
                        <ons-col class="col-33 pad-l-20">Team Member</ons-col>
                        <ons-col class="col-34 pad-l-10">Customer</ons-col>
                        <ons-col class="col-33 pad-l-10">Cancelled By</ons-col>
                    </ons-row>
                </ons-list-header>
                <?
                    $sessioncancelledbyarr = config_item('session-cancelled-by-arr');
                    foreach($cancelledcoachingsessionrs as $row){
                        $cancelledby = $sessioncancelledbyarr[$row['cancelledby']];
                ?>
                    <ons-list-item>
                        <ons-row>
                            <ons-col class="col-33 pad-l-20"><?=$row['salesperson'];?></ons-col>
                            <ons-col class="col-34 pad-l-10"><?=$row['customertitle'];?></ons-col>
                            <ons-col class="col-33 pad-l-10"><?=$cancelledby; //$row['cancelledbyname'];?></ons-col>
                        </ons-row>
                        <ons-row class="fnt-i">
                            <ons-col class="pad-l-20">Reason: <?=$row['cancelledreason'];?></ons-col>
                        </ons-row>
                    </ons-list-item>
                <?}?>
            <?}?>
        </ons-list>
    </div>
