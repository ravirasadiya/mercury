

    <h3 class="blue ta-left">Please Add A Note</h3>
        
        <form name="savedevelopmentnoteform">


            <?if(count($form)){?>
                <?
                    foreach($form as $grow){
                        $rs = $grow['rs'];
                        $g_id = $rs['incnum'];
                        $gsortorder = $rs['sortorder'];
                ?>
                
<input type="hidden" name="form[competencygroupid]" value="<?= $rs['competencygroupid'] ?>">
                    <h4 class="grey ta-left"><?=$gsortorder.'. '.$rs['title'];?>:</h4>
                    <?
                        foreach($grow['params'] as $prow_key => $prow){
                            $rs = $prow['rs'];
                            $p_id = $rs['incnum'];
                            $psortorder = $rs['sortorder'];
                    ?>
                    <input type="hidden" name="form[competencyparameterid]" value="<?= $prow_key ?>">
                    <ul class="blue">
                        <lh><?=$gsortorder.'.'.$psortorder.'. '.htmlspecialchars($rs['title']);?></lh>
                        <?
                            foreach($prow['suggestions'] as $srow){
                                $rs = $srow['rs'];
                                $s_id = $rs['incnum'];
                                $ssortorder = $rs['sortorder'];
                        ?>
                            <input type="hidden" name="form[competencysuggestionid]" value="<?= $rs['competencysuggestionid'] ?>">
                            <li><?=$gsortorder.'.'.$psortorder.'.'.$ssortorder.'. '.htmlspecialchars($rs['title']);?> (Due: <?=$rs['dueon'];?>)</li>
                             <input type="hidden" name="form[dueon]" value="<?=$rs['dueon'];?>">
                        <?}?>
                    </ul>
                <?}?>
            <?}?>
            
            <!-- <input type="hidden" name="ismodal" value="1" /> -->
            <input type="hidden" name="form[userid]" value="<?php echo $userid; ?>" />
            
            <input type="hidden" name="form[companyuserteamid]" value="<?php echo $teamuserid; ?>" />
            <input id="setbase64Plan" type="hidden" name="form[notefile]" value="">

            <div id="infoMessage" style="color: black;"></div>
            <div id="infoTimerPlan" style="color: black;"></div>

            <ons-progress-bar id="progress" style="display: none; vertical-align: middle;align-items: center;" indeterminate></ons-progress-bar>
        
            <textarea name="form[notes]" id="devnotesareaid" class="add-note textarea textarea--transparent mar-b-20" placeholder="Add note..."></textarea>

        <!-- 
        <ons-button class="btn-lrg bg-blue" onclick="confirmDraftSave('sessionsuggestions');">Add To Plan</ons-button> -->

        <ons-row>
            <ons-button id="addnote2" class="col-75 btn-lrg bg-blue is-submit-btn" style="margin-right:5%;">Add Note</ons-button>
                            
            <ons-button id="start_capture_btn_plan" class="col-20 btn-lrg bg-blue startCapturePlan"><img src="images/mic.png" style="vertical-align: middle; width: 30px; height: 30px; " ></ons-button>

            <ons-button id="stop_capture_btn_plan" style="display: none;" class="col-20 btn-lrg bg-blue stopCapture1Plan"><img src="images/stop_white.png" id="stop_capture_img" style="vertical-align: middle; width: 30px; height: 30px; " ></ons-button>
        </ons-row>

        <ons-button id="addnoteback2"  class="btn-thn bg-grey close-mdl-btn">Back</ons-button>

        <div id="recording-list" class="w3-section w3-border margin-5 w3-small"
             style="word-break: break-all; color: black;">...Recordings will show up here...
        </div>

        </form>
    <?}?>



    <?if(!count($form)){?>
        <p class="fnt-b">You have selected no development suggestions. Do you want to continue?</p>
        <ons-button class="btn-lrg bg-blue" onclick="confirmDraftSave('sessionsuggestions');">Continue</ons-button>
        <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
    <?}?>
