

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Session Rating</div>
        <div class="right">
            <?if($companysessionratingrefrs){?>
                <img src="images/submit.png" class="toolbar-img-right" onclick="confirmDraftSave('savesessionratings');" />
            <?}?>
        </div>
    </ons-toolbar>
    <div class="content no-pad">
        <?if($companysessionratingrefrs){?>
        <?
            $total = 0;
            $total_i = 0;
            foreach($companysessionratingrefrs as $row){
                $val = (isset($usersessionratingitemarr[$row['companysessionratingrefid']])) ? $usersessionratingitemarr[$row['companysessionratingrefid']] : 0;

                $total += $val;
                $total_i++;
            }
            $totalavg = round($total/$total_i, 2);
        ?>
        <ons-list-header>
            <ons-row>
                <ons-col class="col-60 pad-l-20"><?=$coachingsessionrs['customertitle'];?></ons-col>
                <ons-col class="col-20 pad-r-25 ta-right"><?=date("j M y", strtotime($coachingsessionrs['date']));?></ons-col>
                <ons-col class="col-20 pad-r-25 ta-right is-range-avg"><?=$totalavg;?>%</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list>
            <form name="savesessionratings">
            <input type="hidden" name="coachingsessionid" value="<?=$coachingsessionid;?>" />
            <ons-list>
            <?
                foreach($companysessionratingrefrs as $row){
                    $val = (isset($usersessionratingitemarr[$row['companysessionratingrefid']])) ? $usersessionratingitemarr[$row['companysessionratingrefid']] : 0;
                    $val = ($val) ? $val : 0;
            ?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="col-20px"></ons-col>
                        <ons-col><?=$row['rating'];?></ons-col>
                        <ons-col class="col-20px"></ons-col>
                    </ons-row>
                    <ons-row class="m-t-10">
                        <ons-col class="col-80 pad-l-20"><ons-range name="form[<?=$row['companysessionratingrefid'];?>]" value="<?=$val;?>" step="10" class="sr-update-range" style="width: 100%;" value="0"></ons-range></ons-col>
                        <ons-col class="col-20 pad-r-20 ta-right bold is-range-target"><?=$val;?>%</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            </ons-list>
            </form>
        <ons-list>
        <?}else{?>
        <ons-list>
            <ons-list-item>
                <ons-row>
                    <ons-col class="pad-l-20">No ratings to rate by at the moment.</ons-col>
                </ons-row>
            </ons-list-item>
        </ons-list>
        <?}?>
    </div>
