

    <h3 class="blue ta-left">Development Suggestions:</h3>
    <?if(count($form)){?>
        <?
            foreach($form as $grow){
                $rs = $grow['rs'];
                $g_id = $rs['incnum'];
        ?>
            <h4 class="grey ta-left"><?=$g_id.'. '.$rs['title'];?>:</h4>
            <?
                foreach($grow['params'] as $prow){
                    $rs = $prow['rs'];
                    $p_id = $rs['incnum'];
            ?>
                <ul class="blue">
                    <lh><?=$g_id.'.'.$p_id.'. '.htmlspecialchars($rs['title']);?></lh>
                    <?
                        foreach($prow['suggestions'] as $srow){
                            $rs = $srow['rs'];
                            $s_id = $rs['incnum'];
                    ?>
                        <li><?=$g_id.'.'.$p_id.'.'.$s_id.'. '.htmlspecialchars($rs['title']);?> (Due: <?=$rs['dueon'];?>)</li>
                    <?}?>
                </ul>
            <?}?>
        <?}?>
        <ons-button class="btn-lrg bg-blue" onclick="confirmDraftSave('adddevelopmentform');">Add To Plan</ons-button>
        <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
    <?}?>
    <?if(!count($form)){?>
        <p class="fnt-b">You have selected no development suggestions. Do you want to continue?</p>
        <ons-button class="btn-lrg bg-blue" onclick="confirmDraftSave('adddevelopmentform');">Continue</ons-button>
        <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
    <?}?>
