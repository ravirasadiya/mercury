
    <!-- <style>
        ons-toolbar ~ .page__content {
       top: 50px !important;
        }
    </style> -->
    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Missed Deadlines</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    
    <div class="content no-pad" id="opendeadlinemodals">
        <ons-list-header>
            <ons-row>
                <input type="hidden" id="spuserrsId" value="<?=$spuserrs['userid'];?>">
                <input type="hidden" id="companyuserteamid" value="<?=$companyuserteamrs['companyuserteamid'];?>">
                <input type="hidden" id="userid" value="<?= USERID ?>">
                <ons-col class="col-90 pad-l-20"><?=$spuserrs['fullname'];?></ons-col>
                  <ons-col class="col-10 pad-r-10 right"><img src="images/setting.png" class="icon-img" data-opendeadlinemodals="" data-userid="<?= USERID ?>" data-companyuserteamid="<?=$companyuserteamrs['companyuserteamid'];?>"
                    data-deadlineid="">
            </ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="col-35 pad-l-20">Team Member</ons-col>
                <ons-col class="col-35 pad-l-10">Missed</ons-col>
                <ons-col class="col-30 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <!-- <div class="is-scroll-list"> -->
        <ons-list class="pad-b-10 expand-list-cont">
            <?foreach($rsarr as $row){?>
            <ons-list-item class="note-item">
                <ons-row>
                    <ons-col class="col-35 pad-l-20"><?=$row['salesperson'];?></ons-col>
                    <ons-col class="col-35 pad-l-10"><?=$row['type'];?></ons-col>
                    <ons-col class="col-30 pad-r-20 ta-right"><?=date("j M y", strtotime($row['date']));?></ons-col>
                </ons-row>
                <div class="detail-panel is-hidden">
                    <ons-row>
                        <ons-col class="pad-l-20"><?=implode('<br />', $row['data']);?></ons-col>
                    </ons-row>
                </div>
            </ons-list-item>
            <?}?>
            <?if(!count($rsarr)){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No missed deadlines currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
        <!-- </div> -->
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue m-b-10" onclick="myNavigator.pushPage('menu.html', { animation: 'none'});">Home</ons-button>
        </div>
    </div>
