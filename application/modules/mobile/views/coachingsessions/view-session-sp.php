

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Coaching Session</div>
    </ons-toolbar>
    <div class="content">
        <ons-list-header class="no-shadow">
            <ons-row>
                <ons-col class="pad-l-20"><?=$coachingsessionrs['customertitle'];?></ons-col>
                <ons-col class="pad-r-25 ta-right"><?=date("j M y", strtotime($coachingsessionrs['date']));?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row class="m-t-17n">
                <ons-col class="pad-l-20"><ons-button class="btn-thn bg-lgrey ta-center" onclick="showCancelSession(<?=$coachingsessionid;?>);">Cancel Session</ons-button></ons-col>
                <ons-col class="col-10px"></ons-col>
                <ons-col class="pad-r-20"><ons-button class="btn-thn bg-lgrey ta-center" onclick="myNavigator.pushPage('new-session.html', { data: { coachingsessionid: <?=$coachingsessionid;?>, movesession: 1 }, animation: 'none'});">Move Session</ons-button></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header class="clean">
            <ons-row>
                <ons-col class="pad-l-20 ta-center no-shadow">See Competences Below</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="list-70 pad-b-10 expand-list-cont">
            <?
                $i = 0;
                $mdlarr = array();
                usort($group_companycompetencyplanrefrs, function($a, $b) {
                    return $a['sortorder'] - $b['sortorder'];
                });
                foreach($group_companycompetencyplanrefrs as $grow){
                    $competencygroupid = $grow['competencygroupid'];

                    $gsortorder = $grow['sortorder'];
                    $incnum = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;

                    $i++;
            ?>
                <ons-list-item>
                    <ons-row class="blue">
                        <ons-col class="pad-l-20 bold"><?=$gsortorder;?>. <?=$grow['group'];?></ons-col>
                    </ons-row>
                    <div class="detail-panel is-hidden">
                        <?
                            $ii = 0;
                            usort($parameter_companycompetencyplanrefrs, function($a, $b) {
                                return $a['sortorder'] - $b['sortorder'];
                            });
                            foreach($parameter_companycompetencyplanrefrs as $prow){
                                $competencyparameterid = $prow['competencyparameterid'];
                                $companycompetencyplanrefid = $prow['companycompetencyplanrefid'];

                                // check
                                if($prow['competencygroupid'] != $competencygroupid){
                                    continue;
                                }

                                $psortorder = $prow['sortorder'];
                                $p_incnum = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;

                                // set
                                $id = 'infomodal-'.$i.'-'.$ii.'-'.$competencyparameterid;

                                // add
                                $mdlarr[$id] = $prow;

                                $ii++;
                        ?>
                            <ons-row>
                                <ons-col class="col-80 pad-l-20 pad-r-20">
                                    <?=$gsortorder.'.'.$psortorder;?> <?=$prow['parameter'];?>
                                </ons-col>
                                <ons-col class="col-20 pad-l-20 pad-r-20 aln-r">
                                    <div class="info-link" data-openmodal="#<?=$id;?>"><img src="images/info.png" style="width: 20px; height: auto;" /></div>
                                </ons-col>
                            </ons-row>
                        <?}?>
                    </div>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>

    <?foreach($mdlarr as $id => $row){?>

    	<ons-modal id="<?=$id;?>" class="msg-modal">
    		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">
    			<h4 class="grey ta-left"><?=$row['parameter'];?>:</h4>
    			<p class="blue ta-left"><?=nl2br($row['parameterdescription']);?></p>
    			<ons-button class="btn-thn bg-grey">close</ons-button>
    		</div>
    	</ons-modal>

    <?}?>
