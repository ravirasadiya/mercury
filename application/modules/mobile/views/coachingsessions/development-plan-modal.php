

    <h3 class="blue ta-left">Development Suggestions:</h3>
    <?if(count($form)){?>
        <?
            foreach($form as $grow){
                $rs = $grow['rs'];
                $g_id = $rs['incnum'];
                $gsortorder = $rs['sortorder'];
        ?>
            <h4 class="grey ta-left"><?=$gsortorder.'. '.$rs['title'];?>:</h4>
            <?
                foreach($grow['params'] as $prow){
                    $rs = $prow['rs'];
                    $p_id = $rs['incnum'];
                    $psortorder = $rs['sortorder'];
            ?>
                <ul class="blue">
                    <lh><?=$gsortorder.'.'.$psortorder.'. '.htmlspecialchars($rs['title']);?></lh>
                    <?
                        foreach($prow['suggestions'] as $srow){
                            $rs = $srow['rs'];
                            $s_id = $rs['incnum'];
                            $ssortorder = $rs['sortorder'];
                    ?>
                        <li><?=$gsortorder.'.'.$psortorder.'.'.$ssortorder.'. '.htmlspecialchars($rs['title']);?> (Due: <?=$rs['dueon'];?>)</li>
                    <?}?>
                </ul>
            <?}?>
        <?}?>
        <ons-button class="btn-lrg bg-blue" onclick="confirmDraftSave('sessionsuggestions');">Add To Plan</ons-button>
        <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
    <?}?>
    <?if(!count($form)){?>
        <p class="fnt-b">You have selected no development suggestions. Do you want to continue?</p>
        <ons-button class="btn-lrg bg-blue" onclick="confirmDraftSave('sessionsuggestions');">Continue</ons-button>
        <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
    <?}?>
