

    <div class="content" style="text-align: center">
        <div class="form">
            <form name="cancelsession">
                <input type="hidden" name="coachingsessionid" value="<?=$coachingsessionid;?>" />

                    <p>&nbsp;</p>

                <?=ons_select('form[cancelledby]', config_item('session-cancelled-by-arr'), null, 'modifier="underbar" class="is-req"');?>
                <!--<ons-input name="form[customertitle]" class="input-text is-req" type="text" modifier="underbar" placeholder="Customer..." value="<?=$coachingsessionrs['customertitle'];?>"></ons-input>-->
                <ons-input name="form[cancelledreason]" class="input-text is-req" type="text" modifier="underbar" placeholder="Reason..." ></ons-input>

                    <p>&nbsp;</p>

                <ons-button class="btn-lrg bg-blue is-submit-btn">Cancel Session</ons-button>
                <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
            </form>
        </div>
    </div>
