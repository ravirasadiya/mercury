

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Missed Deadlines</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-35 pad-l-20">Customer</ons-col>
                <ons-col class="col-35 pad-l-10">Missed</ons-col>
                <ons-col class="col-30 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10 expand-list-cont">
            <?foreach($rsarr as $row){?>
            <ons-list-item>
                <ons-row>
                    <ons-col class="col-35 pad-l-20"><?=$row['customertitle'];?></ons-col>
                    <ons-col class="col-35 pad-l-10"><?=$row['type'];?></ons-col>
                    <ons-col class="col-30 pad-r-20 ta-right"><?=date("j M y", strtotime($row['date']));?></ons-col>
                </ons-row>
                <div class="detail-panel is-hidden">
                    <ons-row>
                        <ons-col class="pad-l-20"><?=implode('<br />', $row['data']);?></ons-col>
                    </ons-row>
                </div>
            </ons-list-item>
            <?}?>
            <?if(!count($rsarr)){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No missed deadlines currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>
