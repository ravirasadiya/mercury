

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Missed Deadlines</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20"><?=$companyusergrouprs['title'];?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Sales Managers</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?foreach($smanagerarr as $salesmanageruserid => $name){?>
            <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('deadlines.html', { data: { listtype : 'teams', companyusergroupid: <?=$companyusergrouprs['companyusergroupid']?>, salesmanageruserid: <?=$salesmanageruserid;?> }, animation: 'none'});">
                <ons-row>
                    <ons-col class="pad-l-20"><?=$name;?></ons-col>
                </ons-row>
            </ons-list-item>
            <?}?>
            <?if(!count($smanagerarr)){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No missed deadlines currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>
