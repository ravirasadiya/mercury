

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Coaching Session</div>
        <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="confirmDraftSave('savesessionparams');" /></div>
    </ons-toolbar>
    <div class="content no-pad-btm">
        <ons-list-header class="no-shadow">
            <ons-row>
                <ons-col class="pad-l-20"><?=$coachingsessionrs['customertitle'];?></ons-col>
                <ons-col class="pad-r-25 ta-right"><?=date("j M y", strtotime($coachingsessionrs['date']));?></ons-col>
            </ons-row>
        </ons-list-header>
        <?if(!$coachingsessionrs['isyesterday']){?>
        <ons-list-header>
            <ons-row class="m-t-17n">
                <ons-col class="pad-l-20"><ons-button class="btn-thn bg-lgrey ta-center" onclick="showCancelSession(<?=$coachingsessionid;?>);">Cancel Session</ons-button></ons-col>
                <ons-col class="col-10px"></ons-col>
                <ons-col class="pad-r-20"><ons-button class="btn-thn bg-lgrey ta-center" onclick="myNavigator.pushPage('new-session.html', { data: { coachingsessionid: <?=$coachingsessionid;?>, movesession: 1 }, animation: 'none'});">Move Session</ons-button></ons-col>
            </ons-row>
        </ons-list-header>
        <?}?>
        <ons-list-header class="clean">
            <ons-row>
                <ons-col class="pad-l-20 ta-center no-shadow">Do Ratings Below</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list>
            <form name="savesessionparams">
            <input type="hidden" name="coachingsessionid" value="<?=$coachingsessionid;?>" />
            <ons-list class="list-70 pad-b-10 expand-list-cont">
                <?
                    $i = 0;
                    usort($group_companycompetencyplanrefrs, function($a, $b) {
                        return $a['sortorder'] - $b['sortorder'];
                    });
                    foreach($group_companycompetencyplanrefrs as $grow){
                        $competencygroupid = $grow['competencygroupid'];

                        // average
                        $total = 0;
                        $total_i = 0;
                        usort($parameter_companycompetencyplanrefrs, function($a, $b) {
                            return $a['sortorder'] - $b['sortorder'];
                        });
                        foreach($parameter_companycompetencyplanrefrs as $prow){
                            $companycompetencyplanrefid = $prow['companycompetencyplanrefid'];

                            // check
                            if($prow['competencygroupid'] != $competencygroupid){
                                continue;
                            }

                            $val = (isset($usersessionparameteritemarr[$companycompetencyplanrefid])) ? $usersessionparameteritemarr[$companycompetencyplanrefid] : 0;
                            $total += ($val) ? $val : 0;
                            $total_i = ($val) ? $total_i+1 : $total_i;
                        }
                        $total = ($total) ? round($total/$total_i) : 0;

                        $gsortorder = $grow['sortorder'];
                        $incnum = (isset($group_companygroupparamsuggrefarr[$competencygroupid])) ? $group_companygroupparamsuggrefarr[$competencygroupid] : 0;

                        $i++;
                ?>
                    <ons-list-item>
                        <ons-row class="blue">
                            <ons-col class="col-80 pad-l-20 bold"><?=$gsortorder;?>. <?=$grow['group'];?></ons-col>
                            <ons-col class="col-20 pad-r-20 bold ta-right is-range-avg"><?=($total == 0 ? '-' : $total.'%');?></ons-col>
                        </ons-row>
                        <div class="detail-panel is-hidden">
                            <?
                                $ii = 0;
                                usort($parameter_companycompetencyplanrefrs, function($a, $b) {
                                    return $a['sortorder'] - $b['sortorder'];
                                });
                                foreach($parameter_companycompetencyplanrefrs as $prow){
                                    $competencyparameterid = $prow['competencyparameterid'];
                                    $companycompetencyplanrefid = $prow['companycompetencyplanrefid'];

                                    // check
                                    if($prow['competencygroupid'] != $competencygroupid){
                                        continue;
                                    }

                                    $val = (isset($usersessionparameteritemarr[$companycompetencyplanrefid])) ? $usersessionparameteritemarr[$companycompetencyplanrefid] : 0;
                                    $val = ($val) ? $val : 0;

                                    $psortorder = $prow['sortorder'];
                                    $p_incnum = (isset($param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid])) ? $param_companygroupparamsuggrefarr[$competencygroupid][$competencyparameterid] : 0;

                                    $ii++;
                            ?>
                                <p class="pad-l-20"><?=$gsortorder.'.'.$psortorder;?> <?=$prow['parameter'];?></p>
                                <ons-row>
                                    <ons-col class="col-80 pad-l-20"><ons-range name="form[<?=$companycompetencyplanrefid;?>]" step="5" class="update-range" style="width: 100%;" value="<?=$val;?>"></ons-range></ons-col>
                                    <ons-col class="col-20 pad-r-20 ta-right is-range-target"><?=$val;?>%</ons-col>
                                </ons-row>
                            <?}?>
                        </div>
                    </ons-list-item>
                <?}?>
            </ons-list>
            </form>
        </ons-list>
    </div>
