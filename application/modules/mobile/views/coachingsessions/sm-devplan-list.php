

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Development Plan</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20" id="temp_name"><?=$userrs['fullname'];?></ons-col>
                 <!-- <ons-col class="col-0" id="teamuserid"><?=$data['userrs']['userid'];?></ons-col>
                 <input type="hidden" id="teamuserid" value="<?=$data['userrs']['userid'];?>"></input> -->
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="col-20 pad-l-20">Date</ons-col>
                <ons-col class="col-50 pad-l-10">Area</ons-col>
                <ons-col class="col-20 pad-l-20">Due</ons-col>
                 <ons-col class="col-10 pad-r-25"><img src="images/add.png" class="icon-img" onclick="myNavigator.pushPage('add-development-plan.html', { data : { teamuserid:<?= $teamuserid ?> }, animation: 'none'});"></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?foreach($usersessionsuggestionitemrs as $row){?>
                <?php
                    $grp_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid']), array('where-str' => '(cgpsr.competencyparameterid IS NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
                    $gsortorder = $grp_companygroupparamsuggref['sortorder'];

                    $para_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid'],'cgpsr.competencyparameterid'=>$row['competencyparameterid']), array('where-str' => '(cgpsr.competencyparameterid IS NOT NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
                    $psortorder = $para_companygroupparamsuggref['sortorder'];

                    $sugg_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid'],'cgpsr.competencyparameterid'=>$row['competencyparameterid'],'cgpsr.competencysuggestionid'=>$row['competencysuggestionid']), array('where-str' => '(cgpsr.competencysuggestionid IS NOT NULL)'),true);
                    $ssortorder = $sugg_companygroupparamsuggref['sortorder'];
                ?>
            <ons-list-item>
                <ons-row>
                    <ons-col class="col-20 pad-l-20 ta-center"><?=date("j M y", strtotime($row['sessiondate']?$row['sessiondate']:$row['createdon']));?></ons-col>
                    <ons-col class="col-60 pad-l-10"><?=$gsortorder.'.'.$psortorder.'. '.$row['parameter'];?></ons-col>
                    <ons-col class="col-20 pad-r-20 ta-center"><?=date("j M y", strtotime($row['dueon']));?></ons-col>
                     <ons-col class="col-15 pad-l-10" data-completesuggestion="<?=$row['usersessionsuggestionitemid'];?>"><img class="complete-icon" src="images/submit.png"/></ons-col>
                </ons-row>
                <ons-row>
                    <ons-col class="pad-l-10"><span class="fnt-b">Action:</span> <?=$gsortorder.'.'.$psortorder.'.'.$ssortorder.'. '.$row['suggestion'];?></ons-col>
                </ons-row>
                <ons-row class="col-100" style="margin-top: 10px;">
                        
                        <ons-col class="col-10 pad-l-10"><img src="images/attachment.png" class="icon-img" data-opendevattechment="<?= $row['competencysuggestionid'] ?>" onclick="myNavigator.pushPage('documentlist.html', { data: { competencysuggestionid: <?=$row['competencysuggestionid'];?>}, animation: 'none'});" ></ons-col>
                        
                        <ons-col class="col-10 pad-l-10"><img src="images/notes.png" class="icon-img" data-opendevplannotes="<?=$row['competencysuggestionid'];?>" data-userid="<?= $userrs['userid'] ?>"></ons-col>
                        
                        <ons-col class="col-10 pad-l-10"><img src="images/add-note-plan.png" class="icon-img" data-opennotesmodelplan="" data-userid="<?= USERID ?>" data-competencysuggestionid="<?=$row['competencysuggestionid'];?>"></ons-col>
                        
                    </ons-row>
            </ons-list-item>
            <?}?>
            <?if(!$usersessionsuggestionitemrs){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No development plans currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue btn-notes" onclick="myNavigator.pushPage('user-notes.html', { animation: 'none'});">Notes</ons-button>
        </div>
    </div>
