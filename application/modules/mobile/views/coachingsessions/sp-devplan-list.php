

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Development Plan</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-20 pad-l-20">Date</ons-col>
                <ons-col class="col-50 pad-l-10">Area</ons-col>
                <ons-col class="col-20 pad-r-20">Due</ons-col>
                  <ons-col class="col-10 pad-r-25"><img src="images/add.png" class="icon-img" onclick="myNavigator.pushPage('add-development-plan.html', { animation: 'none'});"></ons-col>
             
            </ons-row>
        </ons-list-header>
        <ons-list class="list-70 pad-b-10">
            
            <?
            foreach($usersessionsuggestionitemrs as $row){?>
                <?php
                    $grp_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid']), array('where-str' => '(cgpsr.competencyparameterid IS NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
                    $gsortorder = $grp_companygroupparamsuggref['sortorder'];

                    $para_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid'],'cgpsr.competencyparameterid'=>$row['competencyparameterid']), array('where-str' => '(cgpsr.competencyparameterid IS NOT NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
                    $psortorder = $para_companygroupparamsuggref['sortorder'];

                    $sugg_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid'],'cgpsr.competencyparameterid'=>$row['competencyparameterid'],'cgpsr.competencysuggestionid'=>$row['competencysuggestionid']), array('where-str' => '(cgpsr.competencysuggestionid IS NOT NULL)'),true);
                    $ssortorder = $sugg_companygroupparamsuggref['sortorder'];
                ?>
                <ons-list-item>
                    <ons-row class="<?=($row['completedon'] ? 'action-complete' : '');?>">
                        <ons-col class="col-20 pad-l-20 ta-center"><?=date("j M y", strtotime($row['sessiondate']?$row['sessiondate']:$row['createdon']));?></ons-col>
                        <ons-col class="col-55 pad-l-10"><?=$gsortorder.'.'.$psortorder.'. '.$row['parameter'];?></ons-col>
                        <ons-col class="col-20 pad-r-20 ta-center"><?=date("j M y", strtotime($row['dueon']));?></ons-col>
                        <ons-col class="col-15 pad-l-10" data-completesuggestion="<?=$row['usersessionsuggestionitemid'];?>"><img class="complete-icon" src="images/submit.png"/></ons-col>
                    </ons-row>
                    <ons-row class="<?=($row['completedon'] ? 'action-complete' : '');?>">
                        <ons-col class="col-75 pad-l-10"><span class="fnt-b">Action:</span> <?=$gsortorder.'.'.$psortorder.'.'.$ssortorder.'. '.$row['suggestion'];?></ons-col>
                        <ons-col class="col-10"></ons-col>
                        <!-- <ons-col class="col-15 pad-l-10"><img class="complete-icon" src="images/attachment.png"/></ons-col> -->
                    </ons-row>
                    <ons-row class="col-100" style="margin-top: 10px;">
                        <ons-col class="col-10 pad-l-10"><img src="images/attachment.png" class="icon-img" data-opendevattechment="<?= $row['competencysuggestionid'] ?>" onclick="myNavigator.pushPage('documentlist.html', { data: { competencysuggestionid: <?=$row['competencysuggestionid'];?>}, animation: 'none'});" ></ons-col>
                        <ons-col class="col-10 pad-l-10"><img src="images/notes.png" class="icon-img" data-opendevplannotes="<?=$row['competencysuggestionid'];?>" ></ons-col>
                          <!-- onclick="myNavigator.pushPage('user-notes.html', { animation: 'none'});" -->
                        <ons-col class="col-10 pad-l-10"><img src="images/add-note-plan.png" class="icon-img" data-opennotesmodelplan="" data-userid="<?= USERID ?>" data-competencysuggestionid="<?=$row['competencysuggestionid'];?>"></ons-col>
                           <ons-col class="col-70"></ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            <?if(!$usersessionsuggestionitemrs){?>
                <ons-list-item>
                    
                    <ons-row>
                        <ons-col class="pad-l-20">No development plans currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>

            <ons-list-header>
                <ons-row class="blue">
                    <ons-col class="aln-c">Completed Tasks</ons-col>
                </ons-row>
            </ons-list-header>
            <ons-list-header>
                <ons-row>
                    <ons-col class="col-20 pad-l-20">Date</ons-col>
                    <ons-col class="col-55 pad-l-10">Area</ons-col>
                    <ons-col class="col-20 pad-r-20">Completed</ons-col>
                    <ons-col class="col-15 pad-l-10"></ons-col>
                </ons-row>
            </ons-list-header>
            <?foreach($usersessionsuggestionitemrs_completed as $row){?>
                <?php
                    $grp_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid']), array('where-str' => '(cgpsr.competencyparameterid IS NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
                    $gsortorder = $grp_companygroupparamsuggref['sortorder'];

                    $para_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid'],'cgpsr.competencyparameterid'=>$row['competencyparameterid']), array('where-str' => '(cgpsr.competencyparameterid IS NOT NULL AND cgpsr.competencysuggestionid IS NULL)'),true);
                    $psortorder = $para_companygroupparamsuggref['sortorder'];

                    $sugg_companygroupparamsuggref = $this->companygroupparamsuggref_model->get_new_groups_list(null, array('cgpsr.companyid' => COMPANYID,'cgpsr.competencygroupid'=>$row['competencygroupid'],'cgpsr.competencyparameterid'=>$row['competencyparameterid'],'cgpsr.competencysuggestionid'=>$row['competencysuggestionid']), array('where-str' => '(cgpsr.competencysuggestionid IS NOT NULL)'),true);
                    $ssortorder = $sugg_companygroupparamsuggref['sortorder'];
                ?>
                <ons-list-item class="action-complete">
                    <ons-row>
                        <ons-col class="col-20 pad-l-20 ta-center"><?=date("j M y", strtotime($row['sessiondate']));?></ons-col>
                        <ons-col class="col-55 pad-l-10"><?=$gsortorder.'.'.$psortorder.'. '.$row['parameter'];?></ons-col>
                        <ons-col class="col-20 pad-r-20 ta-center"><?=date("j M y", strtotime($row['dueon']));?></ons-col>
                        <ons-col class="col-15 pad-l-10"><img class="complete-icon" src="images/done.png"/></ons-col>
                    </ons-row>
                    <ons-row class="">
                        <ons-col class="pad-l-10"><span class="fnt-b">Action:</span> <?=$gsortorder.'.'.$psortorder.'.'.$ssortorder.'. '.$row['suggestion'];?></ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            <?if(!$usersessionsuggestionitemrs_completed){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No tasks completed yet.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue btn-notes" onclick="myNavigator.pushPage('user-notes.html', { animation: 'none'});">Notes</ons-button>
        </div>
    </div>
