    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Notes</div>
        <!-- <div class="right"><img src="images/add-note.png" class="toolbar-img-right mar-r-10" data-opennotesmodelplan="" data-userid="<?= USERID ?>" data-competencysuggestionid="<?=$competencysuggestionid;?>"/></div> -->
    </ons-toolbar>
    <div class="content no-pad">
		<ons-list-header>
	        <ons-row>
	        	<?php if(USERTYPE=="sales-person"): ?>
	        		<?php $currentuserid = $userrs['userid']; ?>
	        		<ons-col class="col-70 pad-l-20"><?= @$userrs['fullname'] ?></ons-col>
	            <?php else: ?>
	            	<?php $currentuserid = $currentuserrs['userid']; ?>
	            	<ons-col class="col-70 pad-l-20"><?= @$currentuserrs['fullname'] ?></ons-col>
	        	<?php endif; ?>
	            <ons-col class="col-30 pad-r-25 ta-right"><?=date("j M y");?></ons-col>
	        </ons-row>
	    </ons-list-header>
	    <ons-list-header>
	        <ons-row>
	            <ons-col class="col-70 pad-l-20">Note</ons-col>
	            <ons-col class="col-30 pad-r-25 ta-right">Date</ons-col>
	        </ons-row>
	    </ons-list-header>

	    <ons-list class="list-70 pad-b-10">
	        <?
	            foreach($usernoters as $row){
	                $li_class = ($row['userid'] == USERID) ? 'note-item is-owner' : 'note-item';
	                $isowner = ($row['userid'] == USERID) ? 1 : 0;
	        ?>
	        <ons-list-item class="<?=$li_class;?>">
	            <ons-row>
	                <ons-col class="col-70 pad-l-20 note"><?=nl2br($row['notes']);?></ons-col>
	                <ons-col class="col-30 pad-r-20 ta-right">
	                    <?=date("j M y", strtotime($row['createdon']));?>
	                    <?if($isowner){?>
	                       <div class="ta-right" style="margin-top: 4px;">
                                <?php if($row['filelocation']!=null): ?>
                                	<a onclick="downloadFile('<?= $row['filelocation'] ?>')">
	                                    <img class="" src="images/play.png" style="width: 25px; height: 25px;" data-filepath="<?= $row['filelocation'] ?>"/>
	                                </a>
                                <?php endif ?>
                                <img src="images/edit-note.png" class="edit-note-btn" style="width: 25px; height: 25px;" data-usernoteid="<?=$row['usernoteid'];?>" data-opennotesmodel="<?=$row['coachingsessionid'];?>" data-notetype="development" data-competencysuggestionid="<?=$competencysuggestionid;?>" data-currentuserid="<?= $currentuserid ?>"/>
                            </div>
	                    <?}?>
	                </ons-col>
	            </ons-row>
	        </ons-list-item>
	        <?}?>
	    </ons-list>
    </div>