<div class="form">

	<div class="content" style="text-align: left;">
        <div class="form">
			<div class="blue bold fnt-s-20 headingattach" style="margin-top: 20px;">Select Attachments</div>
            <form name="addattachmentmodal">
				<input type="hidden" name="ismodal" value="1" />
				<input type="hidden" name="userid" value="<?= USERID ?>" />
                <input type="hidden" name="form[salesmanageruserid]" value="" />
                <input type="hidden" name="form[salespersonaluserid]" value="" />
                <input type="hidden" name="norefresh" value="" />
                <ons-list-item>
                    <ons-row class="blue pad-t-20">
                        <ons-col class="bold"><?= $competencyparameterrs['title'] ?></ons-col>
                    </ons-row>

                     <ons-row class="mar-b-20">
                        <ons-col></ons-col>
                    </ons-row>

                    <?php if(isset($contentrs) && count($contentrs)!=0): ?>

                        <?php foreach ($contentrs as $content): ?>

                            <ons-row class="blue" style="text-align: left;">
                                <ons-col>
                                    <ons-checkbox name="form[contentid][]" input-id="<?= $content['contentid'] ?>" value="<?= $content['contentid'] ?>"></ons-checkbox>
                                    <label style="vertical-align: middle;" for="AttachmentTitle"><?= $content['title'] ?></label>
                                </ons-col>
                            </ons-row>

                        <?php endforeach; ?>

                    <?php endif; ?>

                </ons-list-item>


				<ons-row style="text-align: center;">
					<ons-button id="addplannote" class="btn-thn bg-blue is-submit-btn_attach is-submit-btn">Submit</ons-button>
                </ons-row>

        		<ons-row style="text-align: center;">
                    <ons-button id="addattachback" class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
                </ons-row>
            </form>
        </div>
    </div>

</div>
