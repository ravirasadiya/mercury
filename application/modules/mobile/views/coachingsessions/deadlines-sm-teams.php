

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Missed Deadlines</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20">Team</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?foreach($spteamarr as $companyuserteamid => $spteam){?>
            <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('deadlines.html', { data: { companyuserteamid: <?=$companyuserteamid;?> }, animation: 'none'});">
                <ons-row>
                    <ons-col class="pad-l-20"><?=$spteam;?></ons-col>
                </ons-row>
            </ons-list-item>
            <?}?>
            <?if(!count($spteamarr)){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No missed deadlines currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>
