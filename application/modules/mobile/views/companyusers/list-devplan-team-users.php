

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Development Plan</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20"><?=$companyuserteamrs['title'];?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?foreach($companyuserteamrefrs as $row){?>
            <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('sm-devplan-list.html', { data: { companyuserteamid: <?=$row['companyuserteamid'];?>, teamuserid: <?=$row['userid'];?> }, animation: 'none'});">
                <div class="pad-l-20"><?=$row['user'];?></div>
            </ons-list-item>
            <?}?>
        </ons-list>
    </div>
