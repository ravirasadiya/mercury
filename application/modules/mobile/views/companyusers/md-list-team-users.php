

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Team Members</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <?
                $avg = (isset($statsarr[$companyuserteamrs['companyusergroupid']]['managers'][$companyuserteamrs['userid']]['avg'])) ? $statsarr[$companyuserteamrs['companyusergroupid']]['managers'][$companyuserteamrs['userid']]['avg'] : 0;
            ?>
            <ons-row>
                <ons-col class="pad-l-20"><?=$companyuserteamrs['manager'];?></ons-col>
                <ons-col class="col-20 ta-right pad-r-20"><?=$avg;?>%</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <?
                $avg = (isset($statsarr[$companyuserteamrs['companyusergroupid']]['managers'][$companyuserteamrs['userid']]['teams'][$companyuserteamrs['companyuserteamid']]['avg'])) ? $statsarr[$companyuserteamrs['companyusergroupid']]['managers'][$companyuserteamrs['userid']]['teams'][$companyuserteamrs['companyuserteamid']]['avg'] : 0;
            ?>
            <ons-row>
                <ons-col class="pad-l-20"><?=$companyuserteamrs['title'];?></ons-col>
                <ons-col class="col-20 ta-right pad-r-20"><?=$avg;?>%</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="pad-b-10">
            <?
                foreach($rs as $row){
                    $avg = (isset($statsarr[$companyuserteamrs['companyusergroupid']]['managers'][$companyuserteamrs['userid']]['teams'][$companyuserteamrs['companyuserteamid']]['users'][$row['userid']]['avg'])) ? $statsarr[$companyuserteamrs['companyusergroupid']]['managers'][$companyuserteamrs['userid']]['teams'][$companyuserteamrs['companyuserteamid']]['users'][$row['userid']]['avg'] : 0;
            ?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="col-80 pad-l-20 note">
                            <div class="pad-l-20"><?=$row['user'];?></div>
                        </ons-col>
                        <ons-col class="col-20 ta-right pad-r-20"><?=$avg;?>%</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue m-b-10" onclick="myNavigator.resetToPage('menu.html', { animation: 'none'});">Home</ons-button>
        </div>
    </div>
