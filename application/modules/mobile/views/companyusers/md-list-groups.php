

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Sales Managers</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list class="pad-b-10">
            <?
                $idarr = array();

                foreach($rs as $row){
                    // set
                    $group_uid = $row['companyusergroupid'].'-'.$row['userid'];

                    // check
                    if(in_array($group_uid, $idarr)){
                        continue;
                    }
                    $idarr[] = $group_uid;

                    $avg = (isset($statsarr[$row['companyusergroupid']]['managers'][$row['userid']]['avg'])) ? $statsarr[$row['companyusergroupid']]['managers'][$row['userid']]['avg'] : 0;
            ?>
                <ons-list-item tappable modifier="chevron"	onclick="myNavigator.pushPage('list-teams.html', { data: { salesmanageruserid: <?=$row['userid'];?> }, animation: 'none'});">
                    <ons-row>
                        <ons-col class="col-80 pad-l-20 note">
                            <div class="pad-l-20"><?=$row['group'];?></div>
                            <div class="pad-l-20 tm-det"><?=$row['manager'];?></div>
                        </ons-col>
                        <ons-col class="col-20 ta-right" style="padding: 8px 25px 0 0;"><?=$avg;?>%</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>
