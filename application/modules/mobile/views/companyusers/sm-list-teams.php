

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Teams</div>
    </ons-toolbar>
    <div class="content">
        <ons-list class="pad-b-10">
            <?
                foreach($rs as $row){
                    $avg = (isset($statsarr[$row['userid']]['teams'][$row['companyuserteamid']]['avg'])) ? $statsarr[$row['userid']]['teams'][$row['companyuserteamid']]['avg'] : 0;
            ?>
            <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('list-team-users.html', { data: { companyuserteamid: <?=$row['companyuserteamid'];?> }, animation: 'none'});">
                <ons-row>
                    <ons-col class="col-80 pad-l-20 note">
                        <div class="pad-l-20"><?=$row['title'];?></div>
                    </ons-col>
                    <ons-col class="col-20 ta-right pad-r-20"><?=$avg;?>%</ons-col>
                </ons-row>
            </ons-list-item>
            <?}?>
        </ons-list>
    </dv>
