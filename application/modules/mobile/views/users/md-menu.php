    <?php 
        $userrs = $this->user_model->get(null, array('u.email' =>$useremail, 'u.type!=' => 'admin'), null);
    ?>

    <ons-row class="v-50">
        <ons-col class="grid-btn">
                <div class="canvas-wrap">
                    <canvas id="md-canvas-home-<?=rand(1000, 100000);?>" class="my-arc" data-chartdata='<?=$chartdata;?>'></canvas>
                    <span id="sp-procent"></span>
                    <h2>Overall Rating</h2>
                </div>
        </ons-col>
    </ons-row>
    <ons-row class="v-25">
        <ons-col class="grid-btn bg-g-2 white">
            <div class="grid-btn-el" <?php if(count($userrs)>1): ?> onclick="myNavigator.resetToPage('home.html', { data:{ email:'<?= $useremail ?>' }, animation: 'none'});" <?php endif; ?>>
                <img src="images/icons/homemenu.png" /><br/>Home
            </div>
        </ons-col>
       <ons-col class="grid-btn bg-b-1 white" onclick="myNavigator.pushPage('activityfeedback.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/target.png" /><br/>Activities</div></ons-col>
        <ons-col class="grid-btn bg-g-2 white" onclick="myNavigator.pushPage('deadlines.html', { data: { listtype : 'groups'}, animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/deadlines.png" /><br/>Missed Deadlines</div><?=$missed_deadlines;?></ons-col>
    </ons-row>
    <ons-row class="v-25">
        <ons-col class="grid-btn bg-b-4 white" onclick="myNavigator.pushPage('list-reports.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/reports.png" /><br/>Reports</div></ons-col>
        <ons-col class="grid-btn bg-b-3 white" onclick="myNavigator.pushPage('profile.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/profile.png" /><br/>My Profile</div></ons-col>
        <ons-col class="grid-btn bg-g-3 white" onclick="myNavigator.pushPage('list-groups.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/team.png" /><br/>Teams</div></ons-col>
    </ons-row>
