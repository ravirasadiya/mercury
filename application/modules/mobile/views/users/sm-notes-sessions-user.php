

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Notes</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content">
        <ons-list-header>
            <ons-row>
                <ons-col class="pad-l-20"><?=$spuserrs['fullname'];?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="col-70 pad-l-20">Customer</ons-col>
                <ons-col class="col-30 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="list-70 pad-b-10">
            <?foreach($coachingsessionrs as $row){?>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('user-notes.html', { data: { salespersonaluserid: <?=$spuserrs['userid'];?>, coachingsessionid: <?=$row['coachingsessionid'];?>}, animation: 'none'});">
                    <ons-row>
                        <ons-col class="col-70 pad-l-20"><?=$row['customertitle'];?></ons-col>
                        <ons-col class="col-30 pad-r-20 ta-right"><?=date("j M y", strtotime($row['date']));?></ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
            <?if(!$coachingsessionrs){?>
                <ons-list-item>
                    <ons-row>
                        <ons-col class="pad-l-20">No sessions currently listed.</ons-col>
                    </ons-row>
                </ons-list-item>
            <?}?>
        </ons-list>
    </div>
