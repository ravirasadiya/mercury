<ons-toolbar>
     
           <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
            <div class="center">Activity Feedback</div>
         
    </ons-toolbar>

<ons-list class="list-70 pad-b-10 expand-list-cont">

    <ons-list-item>

        <ons-row>
            <ons-col class="pad-l-20 pad-r-20">
                <div class="bar-container">
                    <div class="chart-val-bar"></div>
                    <div class="chart-title">Activity Group 1 title</div>
                </div>
            </ons-col>
        </ons-row>

        <div class="detail-panel is-hidden">
             <ons-row class="col-100">
                <ons-col class="col-50 pad-l-20">
                    <div class="info-link">Period: 10 Aug19 17 Aug19</div>
                </ons-col>
                <ons-col class="col-50 pad-r-20 ta-right" style="color: RED; display: inherit">
                    <div class="info-link">Deadline: 24 Aug19</div>
                </ons-col>
            </ons-row>
            <ons-row class="pad-l-20 pad-r-20">
                <div>Activity Description Description Description Description...</div>
            </ons-row>
            <ons-row class="pad-l-20 pad-r-20">
                <div style="width: 100%;display: block;">
                	<ons-col style="width: 35%;display: inline-block; padding-right: 5%;">Actual<br>
                        <input style="width: 100%;" type="number" name="actual" value="">
                    </ons-col>
                	<ons-col style="width: 35%;display: inline-block; padding-right: 5%;">Target<br>
                        <input style="width: 100%;" type="number" name="target" value="">
                    </ons-col>
                	<ons-col style="color: RED; text-align: right; width: 15%;display: inline-block;"><br>50%</ons-col>
                </div>
            </ons-row>
        </div>

    </ons-list-item>

     <ons-row class="pad-l-20 pad-r-20" style="text-align: center;">
					<ons-button id="submitfeedback" class="btn-thn bg-blue is-submit-btn_feedback is-submit-btn">Submit</ons-button>
                </ons-row>
</ons-list>
