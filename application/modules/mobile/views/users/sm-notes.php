

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Notes</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content no-pad">
        <ons-list>
            <ons-list-header>
                <ons-row>
                    <ons-col class="pad-l-20"><?=$companyuserteamrs['title'];?></ons-col>
                </ons-row>
            </ons-list-header>
            <ons-list-header>
                <ons-row>
                    <ons-col class="pad-l-20">Team Member</ons-col>
                </ons-row>
            </ons-list-header>
            <ons-list>
                <?foreach($coachingsessionrs as $salespersonaluserid => $salesperson){?>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('user-notes.html', { data: { salespersonaluserid: <?=$salespersonaluserid;?>}, animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20 note"><?=$salesperson;?></ons-col>
                    </ons-row>
                </ons-list-item>
                <?}?>
            </ons-list>
        </ons-list>
    </div>
