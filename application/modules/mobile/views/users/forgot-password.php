

    <div class="content" style="text-align: center">
        <div class="logo-container">
            <img src="images/logo.png" />
        </div>
        <div class="form">
            <form name="forgotpassword">
                <ons-input name="form[email]" class="input-email" type="text" modifier="underbar" placeholder="email" ></ons-input>
                <ons-button class="btn-lrg bg-blue is-submit-btn">Send</ons-button>
            </form>
            <ons-button class="btn-thn bg-grey" onclick="myNavigator.popPage();">Login</ons-button>
            <div class="text-link blue" data-openmodal="#tandc-modal">Terms and Conditions</div>
            <div class="text-link blue" data-openmodal="#privpol-modal">Privacy Policy</div>
        </div>
    </div>
