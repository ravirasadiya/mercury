    <?php 
        $md_id = null;$md_enabled = null;$sm_id = null;$sm_enabled = null;$sp_id = null;$sp_enabled = null;
    ?>
    <?php foreach($multiusers as $users): ?>

        <?php if($users['type']=='managing-director'): ?>
            <?php $md_id = $users['userid']; $md_enabled = $users['isenabled']; ?>
        <?php endif; ?>

        <?php if($users['type']=='sales-manager'): ?>
            <?php $sm_id = $users['userid']; $sm_enabled = $users['isenabled']; ?>
        <?php endif; ?>

        <?php if($users['type']=='sales-person'): ?>
            <?php $sp_id = $users['userid']; $sp_enabled = $users['isenabled']; ?>
        <?php endif; ?>

    <?php endforeach; ?>
    <style type="text/css">
        .h-100{
            height: 40%;
        }
        .h-40{
            height: 40%;
        }
        .h-20{
            height: 20%;
            margin-bottom: 5%;
        }
        .content{
            height: 100%;
        }
    </style>
    <ons-toolbar>
        <!-- <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div> -->
        <div class="center">Home</div>
    </ons-toolbar>
    <div class="content">
        <one-row class="h-40">
            <div class="" style="width: 60%;margin-left: 20%;text-align: center; margin-bottom: 3%;padding-top: 3%;">
                <img src="images/logo.png" style="max-width: 75%;max-height: 200px;" />
            </div>
        </one-row>

        <one-row class="h-20">
            <a class="set-current-user <?php if($md_enabled==null): ?> user-disabled <?php endif; ?>" data-type="managing-director" data-userid="<?= $md_id ?>">
                <ons-row class="h-20 col-100 bg-h-1 home-button-back " style="width: 95%;<?php if($md_enabled==null): ?> opacity: 0.5 !important; <?php endif; ?>">
                        <ons-col class="grid-btn col-30">
                            <div class="grid-btn-el">
                                <img style="<?php if($md_enabled==null): ?> opacity: 0.4 !important; <?php endif; ?>" src="images/icons/director.png"/>
                            </div>
                        </ons-col>
                        
                        <ons-col class="grid-btn col-70 white">
                            <div class="grid-btn-el" style="text-align: left; font-size: 18px;<?php if($md_enabled==null): ?> opacity: 0.4 !important; <?php endif; ?>"><b>Director</b></div>
                        </ons-col>
                </ons-row>
            </a>
        </one-row>

        <one-row class="h-20">
            <a class="set-current-user <?php if($sm_enabled==null): ?> user-disabled <?php endif; ?>" data-type="sales-manager" data-userid="<?= $sm_id ?>">
                <ons-row class="h-20 col-100 bg-h-2 home-button-back " style="width: 95%;<?php if($sm_enabled==null): ?> opacity: 0.5 !important; <?php endif; ?>">
                        <ons-col class="grid-btn col-30">
                            <div class="grid-btn-el"><img style="<?php if($sm_enabled==null): ?> opacity: 0.4 !important; <?php endif; ?>" src="images/icons/manager.png"/></div></ons-col>
                        <ons-col class="grid-btn col-70 white"><div class="grid-btn-el" style="text-align: left; font-size: 18px;<?php if($sm_enabled==null): ?> opacity: 0.4 !important; <?php endif; ?>"><b>Manager</b></div></ons-col>
                </ons-row>
            </a>
        </one-row>
        
        <one-row class="h-20">
            <a class="set-current-user <?php if($sp_enabled==null): ?> user-disabled <?php endif; ?>" data-type="sales-person" data-userid="<?= $sp_id ?>">
                <ons-row class="h-20 col-100 bg-h-3 home-button-back" style="width: 95%;<?php if($sp_enabled==null): ?> opacity: 0.5 !important; <?php endif; ?>">
                        <ons-col class="grid-btn col-30"><div class="grid-btn-el"><img style="<?php if($sp_enabled==null): ?> opacity: 0.4 !important; <?php endif; ?>" src="images/icons/sales.png"/></div>
                        </ons-col>
                        <ons-col class="grid-btn col-70 white"><div class="grid-btn-el" style="text-align: left; font-size: 18px;<?php if($sp_enabled==null): ?> opacity: 0.4 !important; <?php endif; ?>"><b>Sales Person</b></div>
                        </ons-col>
                </ons-row>
            </a>
        </one-row>
        
    </div>