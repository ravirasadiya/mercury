

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Notes</div>
        <div class="right"><img src="images/add-note.png" class="toolbar-img-right mar-r-10" data-opennotesmodel="<?=$coachingsessionrs['coachingsessionid'];?>" data-usernoteid=""/></div>
    </ons-toolbar>
    <div class="content no-pad">
        <ons-list-header>
            <ons-row>
                <ons-col class="col-70 pad-l-20"><?=$coachingsessionrs['customertitle'];?></ons-col>
                <ons-col class="col-30 pad-r-25 ta-right"><?=date("j M y", strtotime($coachingsessionrs['date']));?></ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list-header>
            <ons-row>
                <ons-col class="col-70 pad-l-20">Note</ons-col>
                <ons-col class="col-30 pad-r-25 ta-right">Date</ons-col>
            </ons-row>
        </ons-list-header>
        <ons-list class="list-70 pad-b-10">
            <?
                foreach($usernoters as $row){
                    $li_class = ($row['userid'] == USERID) ? 'note-item is-owner' : 'note-item';
                    $isowner = ($row['userid'] == USERID) ? 1 : 0;
            ?>
            <ons-list-item class="<?=$li_class;?>">
                <ons-row>
                    <ons-col class="col-70 pad-l-20 note"><?=nl2br($row['notes']);?></ons-col>
                    <ons-col class="col-30 pad-r-20 ta-right">
                        <?=date("j M y", strtotime($row['createdon']));?>
                           <div class="ta-right" style="margin-top: 4px;">
                                    <?php if($row['filelocation']!=null): ?>
                                        <a onclick="downloadFile('<?= $row['filelocation'] ?>')">
                                        <img class="" src="images/play.png" style="width: 25px; height: 25px;" data-filepath="<?= $row['filelocation'] ?>" />
                                        </a>
                                    <?php endif ?>
                                    <?if($isowner){?>
                                    <img src="images/edit-note.png" class="edit-note-btn" style="width: 25px; height: 25px;" data-opennotesmodel="<?=$coachingsessionrs['coachingsessionid'];?>" data-usernoteid="<?=$row['usernoteid'];?>"/>
                                <?}?>
                                </div>
                    </ons-col>
                </ons-row>
            </ons-list-item>
            <?}?>
        </ons-list>
        <?if(false){?>
        <div class="addnotes-cont">
            <form name="addnotes">
                <input type="hidden" name="usernoteid" value="" />
                <input type="hidden" name="salespersonaluserid" value="<?=$coachingsessionrs['salespersonaluserid'];?>" />
                <input type="hidden" name="coachingsessionid" value="<?=$coachingsessionrs['coachingsessionid'];?>" />
                <input type="hidden" name="form[coachingsessionid]" value="<?=$coachingsessionrs['coachingsessionid'];?>" />
                <input type="hidden" name="form[salesmanageruserid]" value="<?=$coachingsessionrs['salesmanageruserid'];?>" />
                <input type="hidden" name="form[salespersonaluserid]" value="<?=$coachingsessionrs['salespersonaluserid'];?>" />
                <ons-row class="note-tabbar">
                    <ons-col class="col-20px"></ons-col>
                    <!-- <ons-col><input name="form[notes]" class="add-note" type="text" placeholder="Add note" ></input></ons-col> -->
                    <ons-col><textarea name="form[notes]" class="add-note textarea textarea--transparent" placeholder="Add note..."></textarea></ons-col>
                    <ons-col class="col-10px"></ons-col>
                    <ons-col class="col-20"><div class="add-note ta-center is-submit-btn"><img src="images/add.png" /></div></ons-col>
                    <ons-col class="col-20px"></ons-col>
                </ons-row>
            </form>
        </div>
        <?}?>
        <div class="pad-l-20 pad-r-20" style="text-align: center">
            <ons-button class="btn-lrg bg-blue m-b-10" onclick="myNavigator.pushPage('menu.html', {animation: 'none'});">Home</ons-button>
        </div>
    </div>
