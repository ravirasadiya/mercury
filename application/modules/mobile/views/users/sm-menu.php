    <?php 
        $userrs = $this->user_model->get(null, array('u.email' =>$useremail, 'u.type!=' => 'admin'), null);
    ?>

    <ons-row class="v-50">
        <ons-col class="grid-btn">
                <div class="canvas-wrap">
                    <canvas id="sm-canvas-home-<?=rand(1000, 100000);?>" class="my-arc" data-chartdata='<?=$chartdata;?>'></canvas>
                    <span id="sm-procent"></span>
                    <h2>Overall Rating</h2>
                </div>
        </ons-col>
        <ons-col class="grid-btn">
            <ons-row style="height: 16%;">
                <ons-col class="grid-btn bg-g-3 white">
                    <div class="grid-btn-el" <?php if(count($userrs)>1): ?> onclick="myNavigator.resetToPage('home.html', { data:{ email:'<?= $useremail ?>' }, animation: 'none'});" <?php endif; ?>>Home
                    </div>
                    <!-- { data: {email:data.email}, animation: 'none' }); -->
                    <!-- onclick="myNavigator.resetToPage('home.html',{ data: {email:<?= $useremail ?>}, animation: 'none' });" -->
                </ons-col>
            </ons-row>
            <ons-row style="height: 28%;">
                <ons-col class="grid-btn bg-b-5 white"><div class="grid-btn-el" onclick="myNavigator.pushPage('activityfeedback.html', { animation: 'none'});"><img src="images/icons/target.png" /><br/>Activities</div></ons-col>
            </ons-row>
             <ons-row style="height: 28%;">
                 <ons-col class="grid-btn bg-b-1 white"><div class="grid-btn-el" onclick="myNavigator.pushPage('user-notes.html', { animation: 'none'});"><img src="images/icons/notes.png" /><br/>Notes</div></ons-col>
            </ons-row>
             <ons-row style="height: 28%;">
                <ons-col class="grid-btn bg-g-1 white" onclick="myNavigator.pushPage('list-teams.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/team.png" /><br/>Team</div></ons-col>
            </ons-row>
        </ons-col>
    </ons-row>
    <ons-row class="v-25">
        <ons-col class="grid-btn bg-g-2 white" onclick="myNavigator.pushPage('deadlines.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/deadlines.png" /><br/>Missed Deadlines</div><?=$missed_deadlines;?></ons-col>
        <ons-col class="grid-btn bg-b-3 white" onclick="myNavigator.pushPage('list-reports.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/reports.png" /><br/>Reports</div></ons-col>
        <ons-col class="grid-btn bg-b-2 white" onclick="myNavigator.pushPage('new-session.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/new.png" /><br/>Create New Coaching Session</div></ons-col>
    </ons-row>
    <ons-row class="v-25">
        <ons-col class="grid-btn bg-b-4 white" onclick="myNavigator.pushPage('profile.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/profile.png" /><br/>My Profile</div></ons-col>
        <ons-col class="grid-btn bg-g-3 white" onclick="myNavigator.pushPage('list-devplan-teams.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/development.png" /><br/>Development Plan</div></ons-col>
        <ons-col class="grid-btn bg-b-5 white" onclick="myNavigator.pushPage('sessions.html', { animation: 'none'});"><div class="grid-btn-el"><img src="images/icons/sessions.png" /><br/>Scheduled Sessions</div></ons-col>
    </ons-row>
