

    <div class="content" style="text-align: center">
        <div class="logo-container">
            <img src="images/logo.png" />
        </div>
        <div class="form">
            <form name="login">
                <ons-input name="form[email]" class="input-email" type="text" modifier="underbar" placeholder="email" ></ons-input>
                <ons-input name="form[password]" class="input-pass" type="password" modifier="underbar" placeholder="password" ></ons-input>
                <ons-button class="btn-lrg bg-blue is-submit-btn">Sign In</ons-button>
            </form>
            <ons-button class="btn-thn bg-grey" onclick="myNavigator.pushPage('forgot-password.html', { animation: 'none'});">Forgot Password</ons-button>
            <div class="text-link blue" data-openmodal="#tandc-modal">Terms and Conditions</div>
            <div class="text-link blue" data-openmodal="#privpol-modal">Privacy Policy</div>
        </div>
    </div>
