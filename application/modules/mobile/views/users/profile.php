

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">My Profile</div>
    </ons-toolbar>
    <div class="content" style="text-align: center">
        <div class="form">
            <form name="profile">
                <ons-input name="form[title]" type="text" modifier="underbar" value="<?=$rs['title'];?>" placeholder="Title" ></ons-input>
                <ons-input name="form[firstname]" type="text" modifier="underbar" value="<?=$rs['firstname'];?>" placeholder="Name" ></ons-input>
                <ons-input name="form[lastname]" type="text" modifier="underbar" value="<?=$rs['lastname'];?>" placeholder="Surname" ></ons-input>
                <ons-input name="form[email]" type="text" modifier="underbar" value="<?=$rs['email'];?>" placeholder="Email" ></ons-input>
                <ons-input name="form[mobile]" type="text" modifier="underbar" value="<?=$rs['mobile'];?>" placeholder="Mobile" ></ons-input>
                <ons-input name="form[officeno]" type="text" modifier="underbar" value="<?=$rs['officeno'];?>" placeholder="Office No" ></ons-input>
                <ons-input name="form[password]" type="password" modifier="underbar" value="" placeholder="Password" ></ons-input>
                <small>* leave the password blank to keep it unchanged</small>
                <ons-button class="btn-lrg bg-blue is-submit-btn">Save</ons-button>
                <ons-button class="btn-thn bg-grey" onclick="myNavigator.popPage();">Cancel</ons-button>
                <ons-button class="btn-thn bg-lgrey" onclick="logoutUser();">Logout</ons-button>
            </form>
        </div>
    </div>