

    <ons-toolbar>
        <div class="left"><img src="images/back.png" class="back-img" onclick="myNavigator.popPage();" /></div>
        <div class="center">Notes</div>
        <!-- <div class="right"><img src="images/submit.png" class="toolbar-img-right" onclick="myNavigator.popPage();" /></div> -->
    </ons-toolbar>
    <div class="content no-pad">
        <ons-list>
            <ons-list-header>
                <ons-row>
                    <ons-col class="pad-l-20">Team</ons-col>
                </ons-row>
            </ons-list-header>
            <ons-list>
                <?foreach($smteamarr as $companyuserteamid => $team){?>
                <ons-list-item tappable modifier="chevron" onclick="myNavigator.pushPage('user-notes.html', { data: { companyuserteamid: <?=$companyuserteamid;?>}, animation: 'none'});">
                    <ons-row>
                        <ons-col class="pad-l-20 note"><?=$team;?></ons-col>
                    </ons-row>
                </ons-list-item>
                <?}?>
            </ons-list>
        </ons-list>
    </div>
