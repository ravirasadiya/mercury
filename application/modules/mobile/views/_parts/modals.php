

	<ons-modal id="devplanModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">
			
		</div>
	</ons-modal>

		<ons-modal id="devplanaddModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">
			
		</div>
	</ons-modal>


	<ons-modal id="cancelSessionModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

		</div>
	</ons-modal>

	<ons-modal id="notesModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

			<div class="content" style="text-align: center">
		        <div class="form">
					<div class="blue bold fnt-s-20 pad-20 heading">Add A Note</div>
		            <form name="addnotesmodal">
						<input type="hidden" name="notetype" value="" />
						<input type="hidden" name="competencysuggestionid" value="" />
						<input type="hidden" name="currentuserid" value="" />
						<input type="hidden" name="ismodal" value="1" />
						<input type="hidden" name="usernoteid" value="" />
		                <input type="hidden" name="salespersonaluserid" value="" />
		                <input type="hidden" name="coachingsessionid" value="" />
		                <input id="coachingsessionid" type="hidden" name="form[coachingsessionid]" value="" />
		                <input id="salesmanageruserid" type="hidden" name="form[salesmanageruserid]" value="" />
		                <input id="salespersonaluserid" type="hidden" name="form[salespersonaluserid]" value="" />
		                <input type="hidden" name="norefresh" value="" />
		                <input id="setbase64" type="hidden" name="form[notefile]" value="">
		                <!-- <input type="hidden" id="audioSource" value="0" /> -->

						<div id="infoMessage" style="color: black;"></div>
		                <div id="infoTimer" style="color: black;"></div>

		                <textarea name="form[notes]" id="notesareaid" class="add-note textarea textarea--transparent mar-b-20" placeholder="Add note..."></textarea>

		              <!--   <div id="wait" style="display:block;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='images/info.png' width="64" height="64" /><br>Loading..</div> -->
		              <ons-progress-bar id="progress" style="display: none; vertical-align: middle;align-items: center;" indeterminate></ons-progress-bar>

 					<ons-row>
 						<ons-button id="addnote" class="col-75 btn-lrg bg-blue is-submit-btn" style="margin-right:5%;">Add Note</ons-button>
 						
                 		<ons-button id="start_capture_btn" class="col-20 btn-lrg bg-blue startCapture"><img src="images/mic.png" style="vertical-align: middle; width: 30px; height: 30px; " ></ons-button>

                 		<ons-button id="stop_capture_btn" style="display: none;" class="col-20 btn-lrg bg-blue stopCapture1"><img src="images/stop_white.png" id="stop_capture_img" style="vertical-align: middle; width: 30px; height: 30px; " ></ons-button>

                 		<!-- <ons-button class="col-20 btn-lrg bg-blue stopCapture " style="margin-left:5%;"><img src="images/mic.png" style="vertical-align: middle; width: 30px; height: 30px; "></ons-button> -->

            		</ons-row>
		                <ons-button id="addnoteback" class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
		                 <div id="recording-list" class="w3-section w3-border margin-5 w3-small"
                         style="word-break: break-all; color: black;">...Recordings will show up here...
                    </div>
		            </form>
		        </div>
		    </div>

		</div>
	</ons-modal>

	<ons-modal id="notesdeveleopmentModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

		</div>
	</ons-modal>

	<ons-modal id="adddevelopmentModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

		</div>
	</ons-modal>

	<ons-modal id="deadlineModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

			<div class="content">
		        <div class="form">
					<div class="blue bold fnt-s-20 pad-20 heading">Deadline Settings</div>
		            <form name="adddeadlinemodal">

						<input type="hidden" name="ismodal" value="1" />
						<input type="hidden" name="deadlineid" value="" />
		                <input type="hidden" name="norefresh" value="" />
		                <input type="hidden" name="form[companyuserteamid]" value="" />

				        <ons-row>
				        	<ons-col class="col-55">
					            <label class="container-radio" style="vertical-align: middle;">
					            	Missed Deadlines
			  						<input type="checkbox" name="check" onclick="onlyOne(this)" value="missed" class="check-single check_missed">
			  							<span class="checkmark"></span>
								</label>
							</ons-col>

							<ons-col class="col-45">
								<label class="container-radio">
									All Deadlines
			  						<input type="checkbox" name="check" onclick="onlyOne(this)" value="all" class="check-single check_all">
			  							<span class="checkmark"></span>
								</label>
							</ons-col>
						</ons-row>

						<!-- <div style="padding: 0px !important; margin: 0px !important; width: 100%; display: block;">
							<div class="col-50">
			  					<label class="container-radio label-black">
			  						<input type="checkbox" name="check" checked="checked" onclick="onlyOne(this)">
			  							<span class="checkmark"></span>
			  							Missed Deadlines
								</label>
							</div>

							<div class="col-50">
								<label class="container-radio label-black">
			  						<input type="checkbox" name="check" onclick="onlyOne(this)">
			  							<span class="checkmark"></span>
			  							All Deadline
								</label>
							</div>
						</div> -->

						<div class="blue bold fnt-s-20 pad-20">Deadline Notification Settings</div>

						<ons-input name="form[noofdeadline]" class="is-req" type="number" modifier="underbar" placeholder="Number of deadlines to display"></ons-input>

						<ons-input name="form[notificationdate]" type="date" class="is-date is-req" modifier="underbar" placeholder="Notifcation Reset Date" data-rootdate="<?=date("Y-m-d");?>"></ons-input>
		               	
		               	<!--  <textarea name="form[notes]" class="deadline textarea textarea--transparent mar-b-20" placeholder="Add note..."></textarea> -->
 						<ons-row class="mar-b-20"></ons-row>
		               	<ons-button class="btn-lrg bg-blue is-submit-btn">Save</ons-button>
		                <ons-button class="btn-thn bg-grey close-mdl-btn">Back</ons-button>
		            </form>
		        </div>
		    </div>

		</div>
	</ons-modal>

	<ons-modal id="attachmentModal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

		</div>
	</ons-modal>

	<ons-modal id="tandc-modal" class="msg-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10 scroll">
			<h4 class="grey ta-left"><?=$termsrs['title']?></h4>
			<div class="blue ta-left is-scroll"><?=$termsrs['text']?></div>
			<ons-button class="btn-thn bg-grey">Close</ons-button>
		</div>
	</ons-modal>

	<ons-modal id="privpol-modal" class="msg-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10 scroll">
			<h4 class="grey ta-left"><?=$pripolicyrs['title']?></h4>
			<div class="blue ta-left is-scroll"><?=$pripolicyrs['text']?></div>
			<ons-button class="btn-thn bg-grey">Close</ons-button>
		</div>
	</ons-modal>

	<ons-modal id="save-draft-mdl">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

			<div class="content no-pad-btm">
                <ons-button class="btn-lrg bg-blue confirm-submit">Submit</ons-button>
					<br /><br />
                <ons-button class="btn-lrg bg-lightblue confirm-draft">Save As Draft</ons-button>
					<br /><br />
                <ons-button class="btn-thn bg-lightgrey close-mdl-btn">Cancel</ons-button>
		    </div>

		</div>
	</ons-modal>

	<ons-modal id="session-overlap-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

		</div>
	</ons-modal>

	<ons-modal id="sugg-date-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

			<div class="content" style="text-align: center">
		        <div class="form">
					<div class="blue bold fnt-s-20 pad-20 heading">Select A Date</div>
		            <form name="seldatemodal">
		                <ons-input name="form[date]" type="date" class="is-date is-req" modifier="underbar" value="<?=date("Y-m-d", strtotime("+7 days"));?>" placeholder="Date"></ons-input>

		                <ons-button class="btn-lrg bg-blue is-submit-btn">Set Date</ons-button>
		                <ons-button class="btn-thn bg-lightgrey close-mdl-btn">Cancel</ons-button>
		            </form>
		        </div>
		    </div>

		</div>
	</ons-modal>

	<ons-modal id="reports-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

			<div class="content" style="text-align: center">
		        <p>
					<ons-button class="bg-blue" data-showreport="assessed-vs-coaching-sessions">Assessed vs Coaching Sessions</ons-button>
					<ons-button class="bg-blue" data-showreport="coaching-session-adherence">Coaching Sessions Adherence</ons-button>
					<ons-button class="bg-blue" data-showreport="developments">Developments</ons-button>
					<ons-button class="bg-blue" data-showreport="people-coached">People Coached</ons-button>
					<ons-button class="bg-blue" data-showreport="ratings-per-competency">Ratings Per Competency</ons-button>
					<ons-button class="bg-blue" data-showreport="team-member-evaluations">Team Member Evaluations</ons-button>
				</p>
		        <p><ons-button class="btn-thn bg-lightgrey close-mdl-btn">Back</ons-button></p>
		    </div>

		</div>
	</ons-modal>

	<ons-modal id="report-filter-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10 scroll">

		</div>
	</ons-modal>

	<ons-modal id="confirm-pushpage-modal">
		<div class="modal-content pad-l-10 pad-r-10 pad-b-10">

			<div class="content" style="text-align: center">
				<div class="blue bold fnt-s-20 pad-20 heading">Coaching Session</div>

				<p class="fnt-s-20 msg"></p>

				<ons-button class="btn-lrg bg-blue confirm-btn" onclick="">Yes</ons-button>
				<ons-button class="btn-thn bg-lightgrey close-mdl-btn">Cancel</ons-button>
		    </div>

		</div>
	</ons-modal>
