
<p class="fnt-b">
    You have overlapping sessions. Please choose another time or date. The overlapping sessions are:
</p>

<ons-list-header>
    <ons-row>
        <ons-col class="col-40 pad-l-20">Team Member</ons-col>
        <ons-col class="col-40 pad-l-10">Customer</ons-col>
        <ons-col class="col-20 pad-r-25 ta-right">Date</ons-col>
    </ons-row>
</ons-list-header>
<ons-list class="list-70 pad-b-10">
<?
    foreach($coachingsessionrs as $row){
        $row['salesperson'] .= ($row['beganon']) ? ' (Draft)' : '';
?>
    <ons-list-item>
        <ons-row>
            <ons-col class="col-40 pad-l-20"><?=$row['salesperson'];?></ons-col>
            <ons-col class="col-40 pad-l-10"><?=$row['customertitle'];?></ons-col>
            <ons-col class="col-20 pad-r-20 ta-right">
                <?=date("j M y", strtotime($row['date']));?><br />
                <small><?=date("H:i", strtotime($row['time']));?></small>
            </ons-col>
        </ons-row>
    </ons-list-item>
<?}?>
</ons-list>

    <br />

<ons-button class="btn-lrg bg-blue close-mdl-btn">OK</ons-button>
