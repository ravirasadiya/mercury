
    <div class="form">
        <form name="createreport">
            <div class="blue bold fnt-s-20 pad-20 heading"><?=$title;?></div>
            <input type="hidden" name="userid" value="<?=USERID;?>" />
            <input type="hidden" name="form[companyarr][]" value="<?=COMPANYID;?>" />
            <input type="hidden" name="form[type]" value="<?=$type;?>" />
            <input type="hidden" name="form[mobile]" value="1" />

            <?
                $li_class = 'disable-uncheck';
                $data = '<ul class="report-filter">';
                // companies
                foreach($statsarr as $companyid => $comprow){
                    // directors
                    foreach($comprow['directors'] as $mduserid => $dirrow){
                        // check
                        if(MDUSERID && $mduserid != MDUSERID){
                            continue;
                        }

                        $data .= '<li class="'.$li_class.'">';
                            $data .= '<label>';
                                $data .= '<input type="checkbox" name="form[mdarr][]" value="'.$mduserid.'" checked="checked" />';
                                $data .= $dirrow['name'];
                            $data .= '</label>';

                        $data .= '<ul>';

                        // groups
                        foreach($dirrow['groups'] as $companyusergroupid => $grouprow){
                            // check
                            if(GROUPID && $companyusergroupid != GROUPID){
                                continue;
                            }
                            if(USERTYPE == 'managing-director'){
                                $li_class = '';
                            }

                            $data .= '<li class="'.$li_class.'">';
                                $data .= '<label>';
                                    $data .= '<input type="checkbox" name="form[grouparr][]" value="'.$companyusergroupid.'" checked="checked" />';
                                    $data .= 'Group: '.$grouprow['title'];
                                $data .= '</label>';

                            $data .= '<ul>';

                            // managers
                            foreach($grouprow['managers'] as $smuserid => $managerrow){
                                // check
                                if(SMUSERID && $smuserid != SMUSERID){
                                    continue;
                                }

                                $data .= '<li class="'.$li_class.'">';
                                    $data .= '<label>';
                                        $data .= '<input type="checkbox" name="form[smarr]" value="'.$smuserid.'" checked="checked" />';
                                        $data .= $managerrow['name'];
                                    $data .= '</label>';

                                $data .= '<ul>';

                                // teams
                                foreach($managerrow['teams'] as $teamid => $teamrow){
                                    // check
                                    if(TEAMID && $teamid != TEAMID){
                                        continue;
                                    }
                                    if(USERTYPE == 'sales-manager'){
                                        $li_class = '';
                                    }

                                    $data .= '<li class="'.$li_class.'">';
                                        $data .= '<label>';
                                            $data .= '<input type="checkbox" name="form[teamarr][]" value="'.$teamid.'" checked="checked" />';
                                            $data .= 'Team: '.$teamrow['title'];
                                        $data .= '</label>';

                                    $data .= '<ul>';

                                    // sales personal
                                    foreach($teamrow['salespersonal'] as $spuserid => $sprow){
                                        // check
                                        if(USERTYPE == 'sales-person' && $spuserid != USERID){
                                            continue;
                                        }

                                        $data .= '<li class="'.$li_class.'">';
                                            $data .= '<label>';
                                                $data .= '<input type="checkbox" name="form[sparr][]" value="'.$spuserid.'" checked="checked" />';
                                                $data .= $sprow['name'];
                                            $data .= '</label>';
                                        $data .= '</li>';
                                    } // end-salespersonal

                                    $data .= '</ul>';

                                    $data .= '</li>';
                                } // end-teams

                                $data .= '</ul>';

                                $data .= '</li>';
                            } // end-managers

                            $data .= '</ul>';

                            $data .= '</li>';
                        } // end-groups

                        $data .= '</ul>';

                        $data .= '</li>';
                    } // end-directors
                } // end companies
                $data .= '</ul>';
                echo $data;
            ?>

                <br /><hr /><br />

            <?=ons_select('form[action]', config_item('report-actiontype-arr'), 'email-report', 'class="is-req" modifier="underbar"');?>
            <ons-input name="form[email]" class="is-req" type="text" modifier="underbar" value="<?=$userrs['email'];?>" placeholder="E-Mail Address"></ons-input>

            <ons-button class="btn-lrg bg-blue is-submit-btn">Create Report</ons-button>
            <ons-button class="btn-thn bg-grey close-mdl-btn">Cancel</ons-button>
        </form>
    </div>
