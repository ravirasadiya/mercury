<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Sitewide Config
|--------------------------------------------------------------------------
*/

// $config['themes_dir'] = 'http://localhost/themes/';
$config['themes_dir'] = 'http://localhost/Mercuri-Web/themes/';
$config['admin_themes_dir'] = $config['themes_dir'].'admin/';
$config['uploads_dir'] = '/uploads/';
$config['content_dir'] = $config['uploads_dir'].'_content/';
$config['other_dir'] = $config['uploads_dir'].'_other/';
$config['reports_dir'] = $config['uploads_dir'].'_reports/';
$config['imports_dir'] = $config['uploads_dir'].'_imports/';


/*
|--------------------------------------------------------------------------
| Constants
|--------------------------------------------------------------------------
*/

define('THEMES_DIR', $config['themes_dir']);
define('ADMIN_THEMES_DIR', $config['admin_themes_dir']);
define('UPLOADS_DIR', $config['uploads_dir']);
define('CONTENT_DIR', $config['content_dir']);
define('OTHER_DIR', $config['other_dir']);
define('REPORTS_DIR', $config['reports_dir']);
define('IMPORTS_DIR', $config['imports_dir']);


// files
define('IMG_FILETYPES', 'jpg|jpeg|png|bmp|gif');
//define('IMG_FILETYPES', '*');
///define('FILE_FILETYPES', 'doc|docx|xls|xlsx|csv|pdf|txt|jpg|jpeg|png|bmp|gif');
define('FILE_FILETYPES', '*');
define('MAX_FILESIZE', 4000);
define('MAX_IMG_WIDTH', 1200);
define('MAX_IMG_HEIGHT', 1200);
define('IMG_SMALL_WIDTH', 250);
define('IMG_SMALL_HEIGHT', 250);


// dates
define('FROM_LIMIT_DATE', date("Y-m-01", strtotime("-12 months")));

// decimals
define('ROUND_DECIMAL', 4);

/*
|--------------------------------------------------------------------------
| Branding
|--------------------------------------------------------------------------
*/
define('ADMIN_TITLE', 'Mercuri International');

$email_admin_sig = '<p>';
	$email_admin_sig .= 'Regards.<br />';
	$email_admin_sig .= ADMIN_TITLE.'<br />';
	$email_admin_sig .= '<a href="'.config_item('base_url').'" target="_blank">'.config_item('base_url').'</a><br />';
$email_admin_sig .= '</p>';

$config['email-admin-sig'] = $email_admin_sig;



/*
|--------------------------------------------------------------------------
| Dropdowns
|--------------------------------------------------------------------------
*/

$config['yesno-arr'] = array(0 => 'No', 1 => 'Yes');
$config['user-title-arr'] = array(
		'Mr.' => 'Mr.',
		'Ms.' => 'Ms.',
		'Mrs.' => 'Mrs.',
		'Me.' => 'Me.',
	);
$config['months'] = array(
		""=>"Select Month",
		"01"=>"January",
		"02"=>"February",
		"03"=>"March",
		"04"=>"April",
		"05"=>"May",
		"06"=>"June",
		"07"=>"July",
		"08"=>"August",
		"09"=>"September",
		"10"=>"October",
		"11"=>"November",
		"12"=>"December"
	);
$config['frequency'] = array(
		''=>'Select Frequency',
		'monthly'=>'Monthly',
		'every_2_months' => 'Every 2 Months',
		'every_3_months' => 'Every 3 Months',
		'every_4_months' => 'Every 4 Months',
		'every_6_months' => 'Every 6 Months',
		'once_per_year' => 'Once Per Year'
	);
$config['status'] = array(
		'' => 'Select Status',
		'active' => 'Active',
		'inactive' => 'Inactive',
	);
$config['content-type-arr'] = array(
		'terms-and-conditions' => 'Terms & Conditions',
		'privacy-policy' => 'Privacy Policy',
	);
$config['session-cancelled-by-arr'] = array(
		'' => 'Cancelled by...',
		'sales-manager' => 'Sales Manager',
		'sales-person' => 'Team Member',
		'customer' => 'Customer',
	);
$config['session-freq-arr'] = array(
		'1-week' => '1 Week',
		'2-weeks' => '2 Weeks',
		'1-month' => '1 Month',
		'2-months' => '2 Months',
		'3-months' => '3 Months',
		'4-months' => '4 Months',
		'6-months' => '6 Months',
		'12-months' => '12 Months',
	);
$config['reports-freq-arr'] = array(
		'1-month' => '1 Month',
		'2-months' => '2 Months',
		'3-months' => '3 Months',
		'4-months' => '4 Months',
		'6-months' => '6 Months',
		'12-months' => '12 Months',
	);
$config['report-actiontype-arr'] = array(
		'' => 'Select Delivery Type...',
		'email-report' => 'E-Mail',
		'download-report' => 'Download',
	);
$config['num-select-arr'] = array_combine(range(1, 20), range(1, 20));
$config['notification-interval-arr'] = array('days' => 'Days', 'weeks' => 'Weeks');
$config['notification-position-arr'] = array('before' => 'Before', 'after' => 'After');
$config['notification-recipient-arr'] = array(
		'sm' => 'Sales Manager',
		'sp' => 'Team-Member'
	);
$config['notification-type-arr'] = array(
		'coaching-session' => 'Coaching Session',
		'coaching-session-rating' => 'Coaching Session Rating',
		'dev-plan-deadline' => 'Development Plan Deadline'
	);

/*
|--------------------------------------------------------------------------
| E-Mail
|--------------------------------------------------------------------------
*/

define('TO_MOBILE_FEEDBACK_EMAIL', 'support@.net');

$config['default-email-arr'] = array(
		 'from-email' => 'no-reply@isolvemobile2.co.za',
		 'from-name' => ADMIN_TITLE,
		 'to-email' => 'riaan.dreyer@isolve.co.za',
		 'subject' => '[no subject was set]',
		 'msg' => '[no message was set]',
	);
$defaultsig = '<p>';
	$defaultsig .= 'Kind regards.<br />';
	$defaultsig .= 'Mercuri International.';
	//$defaultsig .= '<a href="mailto:support@.net">support@.net</a>';
$defaultsig .= '</p>';
$defaultsig .= '<img src="'.rtrim(config_item('base_url'), '/').OTHER_DIR.'email-signature.png'.'" width="300" />';
$config['default-signature'] = $defaultsig;



/*
|--------------------------------------------------------------------------
| Portable PHP password hashing framework
|--------------------------------------------------------------------------
|
| http://www.openwall.com/phpass/
|
*/
define('PHPASS_HASH_STRENGTH', 8);
define('PHPASS_HASH_PORTABLE', FALSE);



/*
|--------------------------------------------------------------------------
| ONESIGNAL
|--------------------------------------------------------------------------s
*/
define('ONESIGNAL_APPADMIN', 'info@mercuri.co.za');
define('ONESIGNAL_APPID', '799eb26f-4b6c-4b82-85be-aad357963d98');
define('ONESIGNAL_RESTAPIKEY', 'Mjk5NjA3MjYtMjhhZi00NjgxLTgyNmMtYjBiYzBkNmRjNzJi');
define('ONESIGNAL_DEFAULT_TEMPLATEID', '4896d723-df61-4f58-a258-0398e26e79d6');



/*
|--------------------------------------------------------------------------
| Default Values
|--------------------------------------------------------------------------
*/
// multiselect
$config['multi-sel-default-settings'] = 'data-plugin-multiselect data-plugin-options=\'{ "buttonWidth": "100%", "nonSelectedText": "Select Option...", "includeSelectAllOption": true, "selectAllText": "All", "selectAllValue": "", "maxHeight": 300 }\'';
$config['report-title-arr'] = array(
		'assessed-vs-coaching-sessions' => 'Assessed vs Coaching Sessions',
		'coaching-session-adherence' => 'Coaching Sessions Adherence',
		'developments' => 'Developments',
		'people-coached' => 'People Coached',
		'ratings-per-competency' => 'Ratings Per Competency',
		'team-member-evaluations' => 'Team Member Evaluations',
	);

/*
|--------------------------------------------------------------------------
| E-Mail
|--------------------------------------------------------------------------
*/
define('ERROR_EMAILTO', 'riaan.dreyer@isolve.co.za');



/*
|--------------------------------------------------------------------------
| Menus
|--------------------------------------------------------------------------
*/
$config['admin-main-menu-config'] = array(
		'menu-container' => array('before' => '<ul class="nav nav-main">', 'after' => '</ul>'),
		'links-link-container' => array('before' => '<li>', 'after' => '</li>'),
		'links-sublink-container' => array('before' => '<li class="nav-parent">', 'after' => '</li>'),
		'links-sublink-container-active' => array('before' => '<li class="nav-parent nav-expanded nav-active">', 'after' => '</li>'),
		'sub-menu-container' => array('before' => '', 'after' => ''),
		'sub-menu-links-container' => array('before' => '<ul class="nav nav-children">', 'after' => '</ul>'),
		'sub-menu-links-link-container' => array('before' => '<li>', 'after' => '</li>'),
		'sub-menu-links-link-container-active' => array('before' => '<li class="nav-active">', 'after' => '</li>')
	);
	$config['admin-main-menu'] = array(
			array('uri' => '/admin/dashboard', 'prepend-title' => '<i class="fa fa-dashboard"></i>', 'title' => '<span>Dashboard</span>'),
			array(
				'uri' => '/admin/companies', 'title' => '<span>Manage Companies</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-building"></i>',
				'sublinks' => array(
					array('uri' => '/admin/companies', 'title' => 'Manage Companies'),
					//array('uri' => '/admin/companies/save', 'title' => 'Add A New Company'),
				),
				'session' => array('!ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/companies/save/profile', 'title' => '<span>Manage Company Profile</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-desktop"></i>',
				'session' => array('ADMIN_COMPANYID')
			),
			/*array(
				'uri' => '/admin/companies', 'title' => '<span>Manage Company</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-plus-square"></i>',
				'sublinks' => array(
					array('uri' => '/admin/companies/save/profile', 'title' => 'Manage Company Profile'),
					array('uri' => '/admin/companycompetencyplans/save/plan', 'title' => 'Manage Competency Plan'),
					array('uri' => '/admin/companysessionratings/save/plan', 'title' => 'Manage Session Ratings'),
				),
				'session' => array('ADMIN_COMPANYID')
			),*/
			array(
				'uri' => '/admin/companycompetencyplans', 'title' => '<span>Manage Competency Plans</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-file-o"></i>',
				'sublinks' => array(
					array('uri' => '/admin/companycompetencyplans', 'title' => 'Manage Competency Plan'),
					array('uri' => '/admin/companycompetencyplans/content', 'title' => 'Manage Competency Plan Content'),
					//array('uri' => '/admin/companycompetencyplans/save', 'title' => 'Add A New Competency Plan'),
				),
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/customeractivitygroups', 'title' => '<span>Manage Activity Groups</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-users"></i>',
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/companyactivities', 'title' => '<span>Manage Activities</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-line-chart"></i>',
				'sublinks' => array(
					array('uri' => '/admin/customeractivity/list', 'title' => 'Activity List'),
					array('uri' => '/admin/customeractivity/feedback', 'title' => 'Activity Feedback'),
					//array('uri' => '/admin/companycompetencyplans/save', 'title' => 'Add A New Competency Plan'),
				),
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/companysessionratings/save/plan', 'title' => '<span>Manage Session Ratings</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-tasks"></i>',
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/companyusergroups', 'title' => '<span>Manage Company Groups/Teams</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-comments"></i>',
				'sublinks' => array(
					array('uri' => '/admin/companyusergroups', 'title' => 'Manage Groups'),
					//array('uri' => '/admin/companyusergroups/save', 'title' => 'Add A New Group'),
					array('uri' => '/admin/companyuserteams', 'title' => 'Manage Teams'),
					//array('uri' => '/admin/companyuserteams/save', 'title' => 'Add A New Team'),
				),
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/users', 'title' => '<span>Manage Company Users</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-users"></i>',
				'sublinks' => array(
					array('uri' => '/admin/users/directors', 'title' => 'Manage Directors'),
					//array('uri' => '/admin/users/save/directors', 'title' => 'Add A New Director'),
					array('uri' => '/admin/users/managers', 'title' => 'Manage Managers'),
					//array('uri' => '/admin/users/save/managers', 'title' => 'Add A New Manager'),
					array('uri' => '/admin/users/teammembers', 'title' => 'Manage Team Members'),
					//array('uri' => '/admin/users/save/teammembers', 'title' => 'Add A New Team Member'),
				),
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/notifications', 'title' => '<span>Manage Team Notifications</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-bell-o"></i>',
				'sublinks' => array(
					array('uri' => '/admin/notifications', 'title' => 'Manage Team Notifications'),
				),
				'session' => array('ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/reports', 'title' => '<span>Manage Reports</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-calendar"></i>',
				'sublinks' => array(
					array('uri' => '/admin/reports/assessedvscoachingsessions', 'title' => 'Assessed vs Coaching Sessions'),
					array('uri' => '/admin/reports/coachingsessionadherence', 'title' => 'Coaching Sessions Adherence'),
					array('uri' => '/admin/reports/developments', 'title' => 'Developments'),
					array('uri' => '/admin/reports/peoplecoached', 'title' => 'People Coached'),
					array('uri' => '/admin/reports/ratingspercompetency', 'title' => 'Ratings Per Competency'),
					array('uri' => '/admin/reports/teammemberevaluations', 'title' => 'Team Member Evaluations'),
				),
				//'session' => array('ADMIN_COMPANYID')
			),

			array(
				'uri' => '/admin/competencies', 'title' => '<span>Manage Competencies</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-check-circle-o"></i>',
				'sublinks' => array(
					array('uri' => '/admin/competencies/groups', 'title' => 'Manage Competency Groups'),
					//array('uri' => '/admin/competencies/save/groups', 'title' => 'Add A New Competency Group'),
					array('uri' => '/admin/competencies/parameters', 'title' => 'Manage Competency Parameter'),
					//array('uri' => '/admin/competencies/save/parameters', 'title' => 'Add A New Competency Parameter'),
					array('uri' => '/admin/competencies/suggestions', 'title' => 'Manage Development Suggestions'),
					//array('uri' => '/admin/competencies/save/suggestions', 'title' => 'Add A New Development Suggestion'),
				),
				'session' => array('!ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/sessionratings', 'title' => '<span>Manage Session Ratings</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-tasks"></i>',
				'sublinks' => array(
					array('uri' => '/admin/sessionratings', 'title' => 'Manage Session Ratings'),
					//array('uri' => '/admin/sessionratings/save', 'title' => 'Add A New Session Rating'),
				),
				'session' => array('!ADMIN_COMPANYID')
			),

			array(
				'uri' => '/admin/users/content', 'title' => '<span>Manage Content</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-file-o"></i>',
				'sublinks' => array(
					array('uri' => '/admin/content', 'title' => 'Manage Content Pages'),
					//array('uri' => '/admin/content/save', 'title' => ' - Add A New Content Page'),
				),
				'session' => array('!ADMIN_COMPANYID')
			),
			array(
				'uri' => '/admin/users/admin', 'title' => '<span>Manage Admin Users</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-users"></i>',
				'sublinks' => array(
					array('uri' => '/admin/users/admin', 'title' => 'Manage Users'),
					//array('uri' => '/admin/users/save/admin', 'title' => ' - Add A New User'),
				),
				'session' => array('!ADMIN_COMPANYID')
			),
			/*array(
				'uri' => '/admin/lookups', 'title' => '<span>Lookups</span>', 'class' => 'dropdown-toggle',
				'prepend-title' => '<i class="fa fa-list"></i>',
				'sublinks' => array(
					array('uri' => '/admin/lookups/levelid', 'title' => 'Lesson Levels'),
				),
				'session' => 'ISADMIN'
			),*/

			array('uri' => '/admin/companies/select', 'prepend-title' => '<i class="fa fa-plug"></i>', 'title' => '<span>Deselect Company</span>', 'session' => 'ADMIN_COMPANYID'),
		);



/*
|--------------------------------------------------------------------------
| Mobile
|--------------------------------------------------------------------------
*/
$config['default-mobile-response'] = array(
		'success' => 0,
		'msg' => 'Oops, something went wrong.'
	);


/*
|--------------------------------------------------------------------------
| Debug
|--------------------------------------------------------------------------
*/
if(stristr(config_item('base_url'), 'isolve.')){
	define('DEBUG_EMAIL', true);
	define('DEBUG_SMS', true);
	define('TEST_EMAIL', '');
	define('TEST_SMS', '');
}else{
	define('DEBUG_EMAIL', false);
	define('DEBUG_SMS', false);
	define('TEST_EMAIL', '');
	define('TEST_SMS', '');
}


/* End of file config-default.php */
/* Location: ./application/config/config-default.php */
?>
