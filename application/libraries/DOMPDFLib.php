<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DOMPDFLib {

	// create
	function create($html, $filename, $stream=false, $attachment=0) {
		// require
		require_once("DOMPDF/dompdf_config.inc.php");
		spl_autoload_register('DOMPDF_autoload');

		// clean
        $html = preg_replace('/>\s+</', '><', $html);

		// init
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);

		$filename = ltrim($filename, '/');

		$dompdf->render(); // render
		//$output = $dompdf->output();
		//return $output;

		if($stream) {
			$dompdf->stream($filename, array('attachment' => $attachment));
		} else {
			// get ci
			$ci = & get_instance();
			// load
			$ci->load->helper('file');
			// write
			write_file($filename, $dompdf->output());
		}
	}

}

/* End of file dompdflib.php */
?>
