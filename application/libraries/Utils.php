<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Utils
 */
class Utils {

	/**
	 * @var CI_Controller
     */
	public $ci;
	/**
	 * @var null
     */
	public $chartarr = null;

	/**
	 *
     */
	function __construct()
	{
		// get ci
		$this->ci =& get_instance();
	}


	// create empty field array
	/**
	 * @param $tablename
	 * @return mixed
     */
	public function create_empty_field_array($tablename)
	{
		$fields = $this->ci->db->list_fields($tablename);

		foreach($fields as $fieldname){
			$array[$fieldname] = '';
		}

		return $array;
	}


	// datetime
	/**
	 * @return bool|string
     */
	public function datetime()
	{
		return date("Y-m-d H:i:s");
	}


	// get date
	/**
	 * @param null $datetime
	 * @return bool|string
     */
	public function get_date($datetime=null)
	{
		$date = ($datetime) ? date("Y-m-d", strtotime($datetime)) : date("Y-m-d");

		return $date;
	}


	// form - create db dropdown array
	/**
	 * @param $table
	 * @param $valuefield
	 * @param $displayfield
	 * @param null $where
	 * @param null $extraline
	 * @param null $pushoption
	 * @return array
     */
	public function get_table_dropdown_array($table, $valuefield, $displayfield, $where=null, $extraline=null, $pushoption=null)
	{
		// check
		if(!is_array($displayfield)){
			$displayfield = array($displayfield);
		}

		// query db
		$this->ci->db->select('*');
		$this->ci->db->from($table);
			if($where){
				foreach($where as $wherefield => $wherevalue){
					// check
					if(stristr($wherevalue, 'NULL')){
						$this->ci->db->where("$wherefield $wherevalue");
					}else{
						$this->ci->db->where($wherefield, $wherevalue);
					}
				}
			}
		// loop
		foreach($displayfield as $fieldname){
			$this->ci->db->order_by($fieldname);
		}
		$rs = $this->ci->db->get()->result_array();

		$array = array();

		if($extraline != null){
			$array[''] = $extraline;
		}

		foreach($rs as $row){
			$display = '';
			foreach($displayfield as $fieldname){
				$display .= $row[$fieldname].' ';
			}

			$array[$row[$valuefield]] = trim($display);
		}

		// check push option
		$array = ($pushoption) ? $this->push_option($pushoption, $array) : $array;

		//echo $this->ci->db->last_query();
		//echo $this->debug_array($array); exit;

		return $array;
	}


	// create unique folder
	/**
	 * @param null $dir
	 * @param string $root
	 * @param int $len
	 * @return mixed|null|string
     */
	public function create_udir($dir=null, $root=UPLOADS_DIR, $len=10)
	{
		// check
		if($dir){
			$dir = explode('/', '/'.trim($dir, '/'));
			$dir = end($dir);

			if(!file_exists(ltrim($root.$dir, '/'))){
				// create
				mkdir('.'.$root.$dir, 0777, true);
			}
			return $dir;
		}

		// loop
		$direxists = true;
		$inc = 1;
		while($direxists == true){
			$dirname = md5(microtime().rand(100, 200));

			$dirname = substr(preg_replace('/\W/i', '', $dirname), 0, $len);

			//$dirname = 'test';

			if(!file_exists(ltrim($root.$dirname, '/'))){
				// create
				mkdir('.'.$root.$dirname);

				$direxists = false;
			}

			// safety
			if($inc++ > 25){
				echo 'Loop!'; exit;
			}
		}

		return $dirname;
	}



	/*
	 * EXCEL
	 */
	/**
	 * @param $sheetarr
	 * @param null $filename
	 * @param array $optionsarr
     */
	public function save_excel($sheetarr, $filename=null, $optionsarr=array(), $savetopath=null)
	{
		// load
		$this->ci->load->library('PHPExcel');

		// set
		$objphpexcel = new PHPExcel();

		// loop sheets
		$sheetindex = 0;
		foreach($sheetarr as $sheettitle => $rowarr){
			// create new sheet
			if($sheetindex > 0){
				$objworksheet = $objphpexcel->createSheet($sheetindex);
				//$objphpexcel->setWorksheet($objworksheet);
			}
			// set active sheet
			$objphpexcel->setActiveSheetIndex($sheetindex++);

			// sheet title
			$objphpexcel->getActiveSheet()->setTitle($sheettitle);

			// set indexes
			$clmindex = 'A';
			$rowindex = 0;

			// loop rows
			foreach($rowarr as $datarr){
				// reset indexes
				$clmindex = 'A';
				$rowindex++;

				foreach($datarr as $celldata){
					$cellindex = $clmindex.$rowindex;

					// set formatting
					if(!is_array($celldata)){
						$celldata = array('data' => $celldata);
					}

					// format
					$objphpexcel->getActiveSheet()->getColumnDimension($clmindex)->setAutoSize(true);
					// font
					if(isset($celldata['fnt-b'])){
						$objphpexcel->getActiveSheet()->getStyle($cellindex)->applyFromArray(array('font' => array('bold' => true)));
					}
					// align
					if(isset($celldata['aln-c'])){
						$objphpexcel->getActiveSheet()->getStyle($cellindex)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
					}
					// data
					$objphpexcel->getActiveSheet()->SetCellValue($cellindex, $celldata['data']);

					// merge
					if(isset($celldata['merge-clms'])){
						$marr = explode(':', $celldata['merge-clms']);
						$startclm = reset($marr).$rowindex;
						$endclm = end($marr).$rowindex;
						$objphpexcel->getActiveSheet()->mergeCells("$startclm:$endclm");
					}

					$clmindex++; // inc
				}
			} // end-rows

		} // end-row

		// set options
		if(isset($optionsarr['orientation']) && $optionsarr['orientation'] == 'L'){
			$objphpexcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		}

		// exit;

		// set
		$writer = new PHPExcel_Writer_Excel5($objphpexcel);

		// check
		if($savetopath){
			$savetopath = './'.ltrim($savetopath, '/');

			// save
			$writer->save($savetopath.$filename);

			//$this->ci->phpexcel->disconnectWorksheets();
			//unset($writer);

			return $filename;
		}

		// filename
		if($filename){
			header('Content-type: application/vnd.ms-excel');
			header("Content-Disposition: attachment; filename=\"$filename\"");

			$writer->save('php://output');
		}
	}

	// read excel
	/**
	 * @param $filepath
	 * @param array $retarr
	 * @return array
     */
	public function read_excel($filepath, $retarr=array())
	{
		// clean
		$filepath = ltrim($filepath, './');
		$filextarr = explode('.', '.'.$filepath);
		$filext = end($filextarr);

		// check
		if(!file_exists($filepath) || !in_array($filext, array('xls', 'xlsx'))){
			return array();
		}

		// load library
		$this->ci->load->library('PHPExcel');

		// load file
		$objphpexcel = PHPExcel_IOFactory::load($filepath);

		// read
		$retarr = $objphpexcel->getActiveSheet()->toArray(null,true,true,true);

		// remove empty rows
		if(is_array($retarr)){
			// loop
			foreach($retarr as $ind => $row){
				$row = array_map('trim', $row);

				if( is_array($row) && !array_filter($row) ){
					unset($retarr[$ind]);
				}else{
					$retarr[$ind] = $row;
				}
			}
		}

		//echo $this->debug_array($retarr); exit;

		return $retarr;
	}


	// convert xls to csv
	public function convert_excel_to_csv($filepath, $filepathsave, $delimiter=';', $enclosure='"')
	{
		// load library
		$this->ci->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$inputFileType = PHPExcel_IOFactory::identify($filepath);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($filepath);

		// Export to CSV file.
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->setSheetIndex(0);   // Select which sheet.
		$objWriter->setDelimiter($delimiter);  // Define delimiter
		$objWriter->setEnclosure($enclosure);  // Define enclosure
		$objWriter->save($filepathsave);
	}


	// read csv
	/**
	 * @param $filepath
	 * @return array
     */
	function read_csv($filepath)
	{
		//$datarr = array_map('str_getcsv', file($filepath));

		// read
		$datarr = $this->read_excel($filepath);

		// flatten
		foreach($datarr as $key => $val){
			$datarr[$key] = implode(' ', $val);
		}

		//echo $this->debug_array($datarr); exit;

		return $datarr;
	}



	// parse import array
	/**
	 * @param $filepath
	 * @param string $char
	 * @param array $retarr
	 * @return array
     */
	function parse_import_array($filepath, $char=DELIMCHAR, $retarr=array())
	{
		// load
		$this->ci->load->helper('file');

		// set
		$filepath = './'.ltrim($filepath, '/');

		// load
		$this->ci->load->helper('file');

		// read
		$data = read_file($filepath);
		//echo $data;

		// type
		$type = (stristr($data, '[Content_Types].xml')) ? 'xls' : 'txt';
		//echo $type;

		// xls
		if($type == 'xls'){
			// read
			$datarr = $this->read_excel($filepath);

			//echo $this->debug_array($datarr); exit;

			// check
			$fkey = end( array_reverse( array_keys($datarr) ) );
			$implodechar = (isset($datarr[$fkey]['A']) && stristr($datarr[$fkey]['A'], $char)) ? ' ' : DELIMCHAR;

			// loop
			$keyarr = null;
			foreach($datarr as $row){
				$str = (is_array($row)) ? implode($implodechar, $row) : $row;

				if($keyarr){
					$rowarr = explode($char, $str);
					$rowarr = array_map('trim', $rowarr);

					$rowarr = array_pad($rowarr, count($keyarr), '');

					$retarr[] = array_combine($keyarr, $rowarr);
				}else{
					$keyarr = explode($char, $str);
					$keyarr = array_map('make_uri', $keyarr);
				}
			}
		}

		// txt
		if($type == 'txt'){
			// set
			$keyarr = null;

			// loop
			//foreach(explode(PHP_EOL, $data) as $str){
			foreach(explode("\n", $data) as $str){
				// check
				if(!trim($str)){
					continue;
				}

				// replace
				$str = str_replace(array('[n]', '[r]'), array("\n", "\r"), $str);

				if($keyarr){
					$rowarr = explode($char, $str);
					$rowarr = array_map('trim', $rowarr);

					$rowarr = array_pad($rowarr, count($keyarr), '');

					$retarr[] = array_combine($keyarr, $rowarr);
				}else{
					$keyarr = explode($char, $str);
					$keyarr = array_map('trim', $keyarr);
					$keyarr = array_map('make_uri', $keyarr);
				}
			}

		}

		//echo $this->debug_array($retarr); exit;

		return $retarr;
	}


	// create mneu
	/**
	 * @param $menuarr
	 * @param array $config
	 * @param int $lvl
	 * @param string $data
	 * @return string
     */
	function create_menu($menuarr, $config=array(), $lvl=1, $data='')
	{
		// wrap
		$data .= ($lvl == 1 && isset($config['menu-container']['before'])) ? $config['menu-container']['before'] : '';
		$data .= ($lvl > 1 && isset($config['sub-menu-links-container']['before'])) ? $config['sub-menu-links-container']['before'] : '';

		// open
		if($lvl)
		// loop
		foreach($menuarr as $key => $row){
			$uri = (isset($row['uri'])) ? $row['uri'] : '';
			$class = (isset($row['class'])) ? $row['class'] : '';
			$title = (isset($row['title'])) ? $row['title'] : '';
			$prependtitle = (isset($row['prepend-title'])) ? $row['prepend-title'] : '';
			$appendtitle = (isset($row['append-title'])) ? $row['append-title'] : '';
			$type = (isset($row['type'])) ? $row['type'] : 'a';
			$props = (isset($row['props'])) ? $row['props'] : '';
			$sublinks = (isset($row['sublinks']) && is_array($row['sublinks'])) ? $row['sublinks'] : null;
			$loadview = (isset($row['load-view'])) ? $row['load-view'] : null;
			$session = (isset($row['session'])) ? $row['session'] : null;

			// check elements
			if(stristr($key, 'elem-')){
				$data .= $row;
				continue;
			}

			// set
			$sublinkuriarr = array();
			if($sublinks){
				// loop
				foreach($sublinks as $subrow){
					$sublinkuriarr[] = trim(trim($subrow['uri'], '/'));
				}

				/*
				$currenturi = preg_replace('/\/\d/', '', uri_string());
				if(stristr($uri, 'lookups')){
					echo $currenturi;
					echo '<pre>'; print_r($sublinkuriarr); echo '</pre>';
					echo array_search($currenturi, $sublinkuriarr); exit;
				}
				/**/
			}

			// check active
			$currenturi = preg_replace('/\/\d/', '', uri_string());
			$urineedle = ($this->ci->uri->segment(3)) ? MODULE.'/'.CONTROLLER.'/'.$this->ci->uri->segment(3) : MODULE.'/'.CONTROLLER;
			$iscurrentcontaineruri = (stristr($uri, $urineedle)) ? true : false;
			$iscurrentcontaineruri = (!$iscurrentcontaineruri && array_search($currenturi, $sublinkuriarr) !== false) ? true : $iscurrentcontaineruri;
			$iscurrenturi = (trim($uri, '/') == trim($currenturi, '/')) ? true : false;

			//$title .= ($iscurrentcontaineruri) ? ' - iscuri' : '';

			// clear top uri
			$uri = ($sublinks) ? '#' : $uri;

			// check
			if($session){
				$sessionarr = (is_array($session)) ? $session : array($session);
				$continue = false;

				// loop
				foreach($sessionarr as $sessionval){
					$not = (stristr($sessionval, '!')) ? true : false;
					$sessionval = trim($sessionval, '!');

					if($not && $this->ci->session->userdata($sessionval)){
						$continue = true;
						break;
					}
					if(!$not && !$this->ci->session->userdata($sessionval)){
						$continue = true;
						break;
					}
				}

				if($continue){
					continue;
				}
			}

			// wrap
			$data .= ((!$sublinks && !$loadview) && $lvl == 1 && isset($config['links-link-container']['before'])) ? $config['links-link-container']['before'] : '';
			$data .= (($sublinks || $loadview) && $lvl == 1 && !$iscurrentcontaineruri && isset($config['links-sublink-container']['before'])) ? $config['links-sublink-container']['before'] : '';
			$data .= (($sublinks || $loadview) && $lvl == 1 && $iscurrentcontaineruri && isset($config['links-sublink-container-active']['before'])) ? $config['links-sublink-container-active']['before'] : '';
			$data .= (($sublinks || $loadview) && isset($config['sub-menu-container']['before'])) ? $config['sub-menu-container']['before'] : '';
			$data .= ($lvl > 1 && !$iscurrenturi && isset($config['sub-menu-links-link-container']['before'])) ? $config['sub-menu-links-link-container']['before'] : '';
			$data .= ($lvl > 1 && $iscurrenturi && isset($config['sub-menu-links-link-container-active']['before'])) ? $config['sub-menu-links-link-container-active']['before'] : '';

			// switch
			switch($type){
				case 'button':
					$data .= '<button type="button" class="'.$class.'" '.$props.'>'.$prependtitle.$title.$appendtitle.'</button>';
				break;
				default:
					$data .= ($sublinks) ? '<a class="'.$class.'" '.$props.'>'.$prependtitle.$title.$appendtitle.'</a>' : '';
					$data .= (!$sublinks) ? '<a href="'.$uri.'" class="'.$class.'" '.$props.'>'.$prependtitle.$title.$appendtitle.'</a>' : '';
				break;
			}

			// recursive
			$data .= ($sublinks) ? $this->create_menu($sublinks, $config, $lvl+1) : '';

			// load view
			$data .= ($loadview) ? $this->ci->load->view($loadview, array(), true) : '';

			// wrap
			$data .= ($lvl > 1 && $iscurrenturi && isset($config['sub-menu-links-link-container-active']['after'])) ? $config['sub-menu-links-link-container-active']['after'] : '';
			$data .= ($lvl > 1 && isset($config['sub-menu-links-link-container']['after'])) ? $config['sub-menu-links-link-container']['after'] : '';
			$data .= (($sublinks || $loadview) && isset($config['sub-menu-container']['after'])) ? $config['sub-menu-container']['after'] : '';
			$data .= (($sublinks || $loadview) && $lvl == 1 && isset($config['links-sublink-container']['after'])) ? $config['links-sublink-container']['after'] : '';
			$data .= ((!$sublinks && !$loadview) && $lvl == 1 && isset($config['links-link-container']['after'])) ? $config['links-link-container']['after'] : '';
		}

		// wrap
		$data .= ($lvl > 1 && isset($config['sub-menu-links-container']['after'])) ? $config['sub-menu-links-container']['after'] : '';
		$data .= ($lvl == 1 && isset($config['menu-container']['after'])) ? $config['menu-container']['after'] : '';

		return $data;
	}


	/*
	 *
	 * DEBUG FUNCTIONS
	 *
	 * */

	// debug array
	/**
	 * @param $array
	 * @param string $spacer
	 * @return string
     */
	public function debug_array($array, $spacer='')
	{
		$data = '';

		foreach($array as $key => $value)
		{
			if(is_array($value)){
				$data .= "$spacer $key => <br>";
				$data .= $this->debug_array($value, $spacer.' &nbsp; - &nbsp; ');
			}else{
				$data .= "$spacer $key => $value <br>";
			}
		}

		return $data;
	}


}

/* End of file Utils.php */
?>
