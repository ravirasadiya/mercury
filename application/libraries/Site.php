<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site {

	public $ci;
	public $consarr = array();
	public $stickynotearr = array();
	private $logarr = array();

	// vars
	public $breadcrumbarr = array(
			'/admin/dashboard' => '<i class="fa fa-home"></i>',
			'/admin/dashboard/' => '<span>Dashboard</span>',
		);

	function __construct()
	{
		// get ci
		$this->ci =& get_instance();
	}


	// display breadcrumbs
	public function display_breadcrumbs($data='')
	{
		// load view
		//$data .= $this->ci->load->view('/admin/_parts/breadcrumbs', array('breadcrumbarr' => $this->breadcrumbarr), true);

		return $data;
	}


	// check company dir
	function check_company_dir($companyid, $save=true, $path=UPLOADS_DIR)
	{
		// get
		$rs = ($companyid) ? $this->ci->company_model->get($companyid) : null;

		// set
		$uploadsdir = ($rs) ? $rs['uploadsdir'] : null;

		// create unique dir
		$uploadsdir = $this->ci->utils->create_udir($uploadsdir, $path);

		// check
		if($save){
			// save
			$this->ci->company_model->save(array('uploadsdir' => $uploadsdir), $rs['companyid']);
		}

		return $uploadsdir;
	}
}

/* End of file Site.php */
?>
