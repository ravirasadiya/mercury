<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ActionQueueLib {

	public $ci;
	public $debug_view = false;

	function __construct()
	{
		// get ci
		$this->ci =& get_instance();

		// check
		if($this->ci->uri->segment(1) == 'test' && !defined('ADMIN_USERID')){
			define('ADMIN_USERID', 1);
		}

		// set limits
		set_time_limit(0);
		ini_set('max_execution_time', 6000);
		ini_set('memory_limit', '2000M');
	}

	// add
	public function add($arr)
	{
		// update
		$arr['createdbyuserid'] = (defined('USERID')) ? USERID : ADMIN_USERID;

		// save
		$actionqueueid = $this->ci->actionqueue_model->save($arr);

		return $actionqueueid;
	}


	// process
	public function process($actionqueueid=null, $resparr=array())
	{
		// load
		$this->ci->load->helper('jpgraph_helper');

		// set
		$where = ($actionqueueid) ? array('aq.actionqueueid' => $actionqueueid) : null;
		$other = array('where-str' => '(aq.processedon IS NULL AND COALESCE(aq.numattempts, 0)<3)');

		// get
		$rs = $this->ci->actionqueue_model->get(null, $where, $other);
		//echo '<pre>'; print_r($rs); echo '</pre>'; exit;

		// loop
		foreach($rs as $row){
			$action = $row['action'];
			$response = null;

			// switch
			switch($action){
				case 'email-report':
					// e-mail report
					$response = $this->email_report($row);
				break;
				case 'download-report':
					// download report
					$response = $this->download_report($row);
				break;
				case 'push-notification':
					//
				break;
			}

			//continue;

			// update
			if($response){
				// set
				$arr = array('processedon' => date("Y-m-d H:i:s"), 'response' => $response);

				// check
				if(isset($response['filename'])){
					$arr['filename'] = $response['filename'];
				}
				if(is_array($arr['response'])){
					$arr['response'] = json_encode($arr['response']);
				}

				// save
				$this->ci->actionqueue_model->save($arr, $row['actionqueueid']);

				$resparr[] = $response;
			}
			// inc attempts
			$this->ci->actionqueue_model->inc_attempts($row['actionqueueid']);
		}

		return $resparr;
	}


	// e-mail report
	private function email_report($rs)
	{
		// set
		$response = null;
		$data = array();
		$emarr = null;
		$attarr = null;

		// switch
		switch($rs['type']){
			case 'coaching-session-result':
				// create
				$response = $this->create_coaching_session_result_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
			case 'assessed-vs-coaching-sessions':
				// create
				$response = $this->create_coachingsessionadherence_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
			case 'people-coached':
				$response = $this->create_peoplecoached_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
			case 'coaching-session-adherence':
				// create
				$response = $this->create_coachingsessionadherence_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
			case 'developments':
				// create
				$response = $this->create_developments_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
			case 'ratings-per-competency':
				// create
				$response = $this->create_ratingspercompetency_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
			case 'team-member-evaluations':
				// create
				$response = $this->create_teammemberevaluations_report($rs);

				// set
				$emarr = $response['emarr'];
				$attarr = $response['attarr'];
			break;
		}

		// check
		if(isset($response['filepath'])){
			$response['fileurl'] = site_url($response['filepath']);
		}

		//echo '<pre>'; print_r($emarr); echo '</pre>'; exit;
		send_email($emarr, $attarr);

		// return
		return $response;
	}


	// download report
	private function download_report($rs)
	{
		// load
		$this->ci->load->helper('download');

		// set
		$response = null;
		$data = array();

		// switch
		switch($rs['type']){
			case 'coaching-session-result':
				// create
				$response = $this->create_coaching_session_result_report($rs);
			break;
			case 'assessed-vs-coaching-sessions':
				$response = $this->create_assessed_vs_coaching_sessions_report($rs);
			break;
			case 'people-coached':
				$response = $this->create_peoplecoached_report($rs);
			break;
			case 'coaching-session-adherence':
				$response = $this->create_coachingsessionadherence_report($rs);
			break;
			case 'developments':
				$response = $this->create_developments_report($rs);
			break;
			case 'ratings-per-competency':
				$response = $this->create_ratingspercompetency_report($rs);
			break;
			case 'team-member-evaluations':
				$response = $this->create_teammemberevaluations_report($rs);
			break;
		}

		// check
		if(isset($response['filepath']) && !$response['mobile']){
			// set
			$arr = array('processedon' => date("Y-m-d H:i:s"), 'response' => $response);

			// check
			if(isset($response['filename'])){
				$arr['filename'] = $response['filename'];
			}
			if(is_array($arr['response'])){
				$arr['response'] = json_encode($arr['response']);
			}

			// save
			$this->ci->actionqueue_model->save($arr, $rs['actionqueueid']);

			// force
			force_download('.'.$response['filepath'], NULL);
		}
		if(isset($response['filepath'])){
			$response['fileurl'] = site_url($response['filepath']);
		}
		//echo '<pre>'; print_r($response); echo '</pre>'; exit;

		// return
		return $response;
	}


	// create coaching session result report
	private function create_coaching_session_result_report($rs)
	{
		// set
		$retarr = array();

		// set
		$coachingsessionid = $rs['coachingsessionid'];

		// get
		$data['coachingsessionrs'] = $this->ci->coachingsession_model->get($rs['coachingsessionid']);
		$data['usernoters'] = $this->ci->usernote_model->get(null, array('un.coachingsessionid' => $rs['coachingsessionid']), array('order' => array('un.usernoteid' => 'ASC')));
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$companyrs = $this->ci->company_model->get($data['coachingsessionrs']['companyid']);
		$sm_userrs = $this->ci->user_model->get($data['coachingsessionrs']['salesmanageruserid']);
		$sp_userrs = $this->ci->user_model->get($data['coachingsessionrs']['salespersonaluserid']);
		//echo '<pre>'; print_r($data['coachingsessionrs']); echo '</pre>'; exit;

		// check dir
		$uploadsdir = $this->ci->site->check_company_dir($companyrs['companyid']);
		$uploadsdir = UPLOADS_DIR.$uploadsdir.'/';

		// get
		$data['statsarr'] = $this->ci->report_model->single_session_stats($data['coachingsessionrs']['coachingsessionid']);
		$data['statsarr'] = $data['statsarr'][$rs['coachingsessionid']];
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// set
		$filename = 'Coaching-Session-Results-'.date("Y-m-d").'--('.$coachingsessionid.').pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/coaching-session-result-report', $data, true);
		//echo $html;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);

		// set e-mail
		$retarr['emarr'] = array(
				'to-email' => array($sm_userrs['email'], $sp_userrs['email']),
				'subject' => $companyrs['title'].': Coaching Session Result Report',
				'msg' => $this->ci->load->view('admin/_email/coaching-session-result-report', $data, true),
			);
		$retarr['attarr'] = array($filepath);

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		return $retarr;
	}


	// create assessed vs coaching sessions report
	private function create_assessed_vs_coaching_sessions_report($rs)
	{
		// set
		$retarr = array();

		// set
		$params = ($rs['params']) ? json_decode($rs['params'], true) : array();

		// check dir
		$uploadsdir = REPORTS_DIR;

		// get
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$data['statsarr'] = $this->ci->report_model->session_competency_stats($params);
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// set
		$filename = 'Assessed-VS-Coaching-Sessions-Report-'.date("Y-m-d-His").'.pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/assessed-vs-coaching-sessions-report', $data, true);
		//echo $html; exit;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);

		// check
		if(isset($params['email'])){
			// set e-mail
			$retarr['emarr'] = array(
					'to-email' => array($params['email']),
					'subject' => 'Assessed VS Coaching Sessions Report',
					'msg' => $this->ci->load->view('admin/_email/assessed-vs-coaching-sessions-report', $data, true),
				);
			$retarr['attarr'] = array($filepath);
		}

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		return $retarr;
	}


	// people coached
	private function create_peoplecoached_report($rs)
	{
		// set
		$retarr = array();

		// set
		$params = ($rs['params']) ? json_decode($rs['params'], true) : array();

		// check dir
		$uploadsdir = REPORTS_DIR;

		// get
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$data['statsarr'] = $this->ci->report_model->session_stats($params);
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;
		//$arr = $data['statsarr'][5]['directors'][45]['groups'][5]['managers'][47]['teams'][17];
		//echo '<pre>'; print_r($arr); echo '</pre>'; exit;

		// add charts
		foreach($data['statsarr'] as $companyid => $row){
			$totalsummary_chart = chart_overview($row, true);
			$chart_director_percentages = chart_director_percentages($row, true);
			$chart_director_sessions = chart_director_sessions($row, true);
			$chart_manager_percentages = chart_manager_percentages($row, true);
			$chart_manager_sessions = chart_manager_sessions($row, true);
			$chart_teammember_percentages = chart_teammember_percentages($row, true);
			$chart_teammember_sessions = chart_teammember_sessions($row, true);

			$data['statsarr'][$companyid]['chartimgpatharr'] = array(
					'totalsummary-chart' => create_jpg_pie_chart($totalsummary_chart),
					'chart-director-percentages' => create_jpg_bar_chart($chart_director_percentages, array('#F9D76F')),
					'chart-director-sessions' => create_jpg_grouped_bar_chart($chart_director_sessions),
					'chart-manager-percentages' => create_jpg_bar_chart($chart_manager_percentages, array('#F9D76F')),
					'chart-manager-sessions' => create_jpg_grouped_bar_chart($chart_manager_sessions),
					'chart-teammember-percentages' => create_jpg_bar_chart($chart_teammember_percentages, array('#F9D76F')),
					'chart-teammember-sessions' => create_jpg_grouped_bar_chart($chart_teammember_sessions),
				);
		}

		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// set
		$filename = 'People-Coached-Report-'.date("Y-m-d-His").'.pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/people-coached-report', $data, true);
		//echo $html; exit;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);
		//exit;

		// cleanup
		foreach($data['statsarr'] as $companyid => $row){
			// loop
			foreach($row['chartimgpatharr'] as $path){
				unlink($path);
			}
		}

		//exit;

		// check
		if(isset($params['email'])){
			// set e-mail
			$retarr['emarr'] = array(
					'to-email' => array($params['email']),
					'subject' => 'People Coached Report',
					'msg' => $this->ci->load->view('admin/_email/people-coached-report', $data, true),
				);
			$retarr['attarr'] = array($filepath);
		}

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		return $retarr;
	}


	// coaching session adherence
	private function create_coachingsessionadherence_report($rs)
	{
		// set
		$retarr = array();

		// set
		$params = ($rs['params']) ? json_decode($rs['params'], true) : array();

		// check dir
		$uploadsdir = REPORTS_DIR;

		// get
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$data['statsarr'] = $this->ci->report_model->session_stats($params);
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// add charts
		foreach($data['statsarr'] as $companyid => $row){
			$totalsummary_chart = chart_adherence_overview($row, true);
			$chart_director_percentages = chart_adherence_director_percentages($row, true);
			$chart_director_sessions = chart_adherence_director_sessions($row, true);
			$chart_manager_percentages = chart_adherence_manager_percentages($row, true);
			$chart_manager_sessions = chart_adherence_manager_sessions($row, true);
			$chart_teammember_percentages = chart_adherence_teammember_percentages($row, true);
			$chart_teammember_sessions = chart_adherence_teammember_sessions($row, true);

			$data['statsarr'][$companyid]['chartimgpatharr'] = array(
					'totalsummary-chart' => create_jpg_pie_chart($totalsummary_chart),
					'chart-director-percentages' => create_jpg_bar_chart($chart_director_percentages),
					'chart-director-sessions' => create_jpg_grouped_bar_chart($chart_director_sessions),
					'chart-manager-percentages' => create_jpg_bar_chart($chart_manager_percentages),
					'chart-manager-sessions' => create_jpg_grouped_bar_chart($chart_manager_sessions),
					'chart-teammember-percentages' => create_jpg_bar_chart($chart_teammember_percentages),
					'chart-teammember-sessions' => create_jpg_grouped_bar_chart($chart_teammember_sessions),
				);
		}

		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// set
		$filename = 'Coaching-Session-Adherence-Report-'.date("Y-m-d-His").'.pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/coaching-session-adherence-report', $data, true);
		//echo $html; exit;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);
		//exit;

		// cleanup
		foreach($data['statsarr'] as $companyid => $row){
			// loop
			foreach($row['chartimgpatharr'] as $path){
				unlink($path);
			}
		}

		// check
		if(isset($params['email'])){
			// set e-mail
			$retarr['emarr'] = array(
					'to-email' => array($params['email']),
					'subject' => 'Coaching Session Adherence Report',
					'msg' => $this->ci->load->view('admin/_email/coaching-session-adherence-report', $data, true),
				);
			$retarr['attarr'] = array($filepath);
		}

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		return $retarr;
	}


	// developments
	private function create_developments_report($rs)
	{
		// set
		$retarr = array();

		// set
		$params = ($rs['params']) ? json_decode($rs['params'], true) : array();
		$chartimgpatharr = array();

		// check dir
		$uploadsdir = REPORTS_DIR;

		// get
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$data['statsarr'] = $this->ci->report_model->session_competency_stats($params);
		//$arr = $data['statsarr'][5]['directors'][45]['groups'][5]['managers'][47]['teams'][17];
		//unset($arr['eval-months']);
		//unset($arr['comparr']);
		//echo '<pre>'; print_r($arr); echo '</pre>'; exit;

		// add charts
		// companies
		foreach($data['statsarr'] as $companyid => $comparr){
			$compdescarr = $comparr['compdescarr'];

			// directors
			foreach($comparr['directors'] as $mduserid => $dirrow){
				// check
				if(!is_numeric($mduserid)){
					continue;
				}

				// groups
				foreach($dirrow['groups'] as $companyusergroupid => $grouprow){
					// check
					if(!is_numeric($companyusergroupid)){
						continue;
					}

					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
						// check
						if(!is_numeric($smuserid)){
							continue;
						}

						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
							// check
							if(!is_numeric($companyuserteamid)){
								continue;
							}

							// sessions
							foreach($teamrow['users'] as $userid => $userrow){
								$totalsummary_chart = chart_developments_overview($userrow, true);

								$chartimgpatharr[$companyid][] = $data['statsarr'][$companyid]['directors'][$mduserid]['groups'][$companyusergroupid]['managers'][$smuserid]['teams'][$companyuserteamid]['users'][$userid]['chartimgpatharr'] = array(
										'totalsummary-chart' => create_jpg_pie_chart($totalsummary_chart)
									);
							} // end-users
						} // end-teams
					} // end-managers
				} // end-groups
			} // end-directors
		} // end companies

		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;
		//echo '<pre>'; print_r($chartimgpatharr); echo '</pre>'; exit;

		// set
		$filename = 'Developments-Report-'.date("Y-m-d-His").'.pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/developments-report', $data, true);
		//echo $html; exit;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);
		//exit;

		// cleanup
		foreach($chartimgpatharr as $companyid => $chartrow){
			// loop
			foreach($chartrow as $row){
				// loop
				foreach($row as $path){
					unlink($path);
				}
			}
		}

		// check
		if(isset($params['email'])){
			// set e-mail
			$retarr['emarr'] = array(
					'to-email' => array($params['email']),
					'subject' => 'Developments Report',
					'msg' => $this->ci->load->view('admin/_email/developments-report', $data, true),
				);
			$retarr['attarr'] = array($filepath);
		}

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		return $retarr;
	}


	// ratings per competency
	private function create_ratingspercompetency_report($rs)
	{
		// set
		$retarr = array();

		// set
		$params = ($rs['params']) ? json_decode($rs['params'], true) : array();

		// check dir
		$uploadsdir = REPORTS_DIR;

		// get
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$data['statsarr'] = $this->ci->report_model->session_competency_stats($params);
		//echo '<pre>'; print_r($data['statsarr']); echo '</pre>'; exit;

		// create charts
		$charts_grouparr = array();
		$charts_paramarr = array();

		// companies
		foreach($data['statsarr'] as $companyid => $comprow){
			$compdescarr = $comprow['compdescarr'];
			$sessionmontharr = $comprow['sessionmonths'];

			// directors
			foreach($comprow['directors'] as $mduserid => $grouprow){
				// check
				if(!is_numeric($mduserid)){
					continue;
				}

				// groups
				foreach($grouprow['groups'] as $companyusergroupid => $grouprow){
					// check
					if(!is_numeric($companyusergroupid)){
						continue;
					}

					// managers
					foreach($grouprow['managers'] as $smuserid => $managerrow){
						// check
						if(!is_numeric($smuserid)){
							continue;
						}

						// teams
						foreach($managerrow['teams'] as $companyuserteamid => $teamrow){
							// check
							if(!is_numeric($companyuserteamid)){
								continue;
							}

							// set
							$team_evalmontharr = $teamrow['eval-months'];

							// sessions
							foreach($teamrow['users'] as $userid => $userrow){
								// set
								$user_evalmontharr = $userrow['eval-months'];

								// groups
								foreach($userrow['comparr']['groups'] as $groupid => $paramrow){
									$comparr = $compdescarr[$groupid];
									$g_i = $comparr['row']['index'];
									$user_grouparr = array();

									// loop months
									foreach($sessionmontharr as $date => $datestr){
										// months
										$date_team_evalmontharr = (isset($team_evalmontharr[$date])) ? $team_evalmontharr[$date] : null;
										$date_user_evalmontharr = (isset($user_evalmontharr[$date])) ? $user_evalmontharr[$date] : null;

										// avgs
										$team_avg = ($date_team_evalmontharr) ? $date_team_evalmontharr[$groupid] : null;
										$user_avg = ($date_user_evalmontharr) ? $date_user_evalmontharr[$groupid] : null;

										// chedck
										if(!$team_avg && !$user_avg){
											//continue;
										}

										// append
										$user_grouparr['title'] = $g_i.'. '.$comparr['row']['title'];
										$user_grouparr['dates'][$date] = array(
												'team-avg' => ($team_avg['evalavg']) ? $team_avg['evalavg'] : '-',
												'user-avg' => ($user_avg['evalavg']) ? $user_avg['evalavg'] : '-',
											);
									} // end-evalmonths

									// append
									$charts_grouparr[$userid][$groupid] = $user_grouparr;

									//echo '<pre>'; print_r($user_grouparr); echo '</pre>'; exit;

									// params
									foreach($paramrow['params'] as $paramid => $sessrow){
										$comparr = $compdescarr[$groupid][$paramid];
										$team_comparr = $teamrow['comparr']['groups'][$groupid]['params'][$paramid];
										$p_i = $comparr['row']['index'];

										$user_paramarr = array();

										// loop months
										foreach($sessionmontharr as $date => $datestr){
											// months
											$date_team_evalmontharr = (isset($team_evalmontharr[$date])) ? $team_evalmontharr[$date] : null;
											$date_user_evalmontharr = (isset($user_evalmontharr[$date])) ? $user_evalmontharr[$date] : null;

											// avgs
											$team_avg = ($date_team_evalmontharr) ? $date_team_evalmontharr[$groupid]['params'][$paramid] : null;
											$user_avg = ($date_user_evalmontharr) ? $date_user_evalmontharr[$groupid]['params'][$paramid] : null;

											// chedck
											if(!$team_avg && !$user_avg){
												//continue;
											}

											// append
											$user_paramarr['title'] = $g_i.'.'.$p_i.'. '.$comparr['row']['title'];
											$user_paramarr['dates'][$date] = array(
													'team-avg' => ($team_avg['evalavg']) ? $team_avg['evalavg'] : '-',
													'user-avg' => ($user_avg['evalavg']) ? $user_avg['evalavg'] : '-',
												);
										} // end-evalmonths

										// append
										$charts_paramarr[$userid][$paramid] = $user_paramarr;
									} // end-params
								} // end-groups

							} // end-users
							//echo '<pre>'; print_r($charts_grouparr); echo '</pre>';
							//echo '<pre>'; print_r($charts_paramarr); echo '</pre>';
						} // end-teams
					} // end-managers
				} // end-groups
			} // end-directors
		} // end-company

		//echo '<pre>'; print_r($charts_grouparr); echo '</pre>'; exit;
		//echo '<pre>'; print_r($charts_paramarr); echo '</pre>'; exit;

		// loop chart data
		foreach($charts_grouparr as $userid => $grouprow){
			// set
			$data['usergroups_chartimgpatharr'][$userid] = array();

			// loop groups
			foreach($grouprow as $groupid => $row){
				// extract data
				$chartarr = chart_competency_groups($row, true);
				//echo '<pre>'; print_r($row); echo '</pre>';

				// generate chart image
				$chartimg = create_jpg_line_chart($chartarr, true);

				// append
				$data['usergroups_chartimgpatharr'][$userid][] = $chartimg;
			}
		}
		foreach($charts_paramarr as $userid => $paramrow){
			// set
			$data['userparams_chartimgpatharr'][$userid] = array();

			// loop params
			foreach($paramrow as $paramid => $row){
				// extract data
				$chartarr = chart_competency_params($row, true);

				// generate chart image
				$chartimg = create_jpg_line_chart($chartarr, true);

				// append
				$data['userparams_chartimgpatharr'][$userid][] = $chartimg;
			}
		}

		// set
		$filename = 'Ratings-Per-Competency-Report-'.date("Y-m-d-His").'.pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/ratings-per-competency-report', $data, true);
		//echo $html; exit;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);
		//exit;

		// cleanup
		foreach($data['usergroups_chartimgpatharr'] as $userid => $imgrow){
			// loop
			foreach($imgrow as $path){
				unlink($path);
			}
		}
		foreach($data['userparams_chartimgpatharr'] as $userid => $imgrow){
			// loop
			foreach($imgrow as $path){
				unlink($path);
			}
		}

		// check
		if(isset($params['email'])){
			// set e-mail
			$retarr['emarr'] = array(
					'to-email' => array($params['email']),
					'subject' => 'Ratings Per Competency Report',
					'msg' => $this->ci->load->view('admin/_email/ratings-per-competency-report', $data, true),
				);
			$retarr['attarr'] = array($filepath);
		}

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		return $retarr;
	}


	// team-member evaluations
	private function create_teammemberevaluations_report($rs)
	{
		// set
		$retarr = array();

		// set
		$params = ($rs['params']) ? json_decode($rs['params'], true) : array();

		// check dir
		$uploadsdir = REPORTS_DIR;

		// get
		$data['createdbyuserrs'] = $this->ci->user_model->get($rs['createdbyuserid']);
		$data['statsarr'] = $this->ci->report_model->session_evaluations_stats($params);
		//$arr = $data['statsarr'][5]['directors'][46]['groups'][8]['managers'][54];
		//$arr = $data['statsarr'];
		//unset($arr['eval-months']);
		//unset($arr['comparr']);
		//echo '<pre>'; print_r($arr); echo '</pre>'; exit;

		// add charts
		foreach($data['statsarr'] as $companyid => $row){
			// check
			if(!$row['ratings']['num-rated']){
				continue;
			}
			$totalsummary_chart = chart_evaluation_ratings($row, true);
			//echo '<pre>'; print_r($totalsummary_chart); echo '</pre>'; exit;

			// check
			if(count($totalsummary_chart['labels']) <= 1){
				continue;
			}

			$data['statsarr'][$companyid]['chartimgpatharr'] = array(
					'totalsummary-chart' => create_jpg_line_chart($totalsummary_chart),
				);
		}

		// set
		$filename = 'Team-Member-Evaluations-Report-'.date("Y-m-d-His").'.pdf';
		$filepath = $uploadsdir.$filename;
		$html = $this->ci->load->view('admin/_pdf/team-member-evaluations-report', $data, true);
		//echo $html; exit;
		//echo $filepath; exit;

		// check
		if($this->debug_view){
			echo $html; exit;
		}

		// create pdf
		create_pdf($html, $filepath);
		//exit;

		// cleanup
		foreach($data['statsarr'] as $companyid => $row){
			// check
			if(!isset($row['chartimgpatharr'])){
				continue;
			}
			// loop
			foreach($row['chartimgpatharr'] as $path){
				unlink($path);
			}
		}

		// check
		if(isset($params['email'])){
			// set e-mail
			$retarr['emarr'] = array(
					'to-email' => array($params['email']),
					'subject' => 'Team Member Evaluations Report',
					'msg' => $this->ci->load->view('admin/_email/team-member-evaluations-report', $data, true),
				);
			$retarr['attarr'] = array($filepath);
		}

		// update
		$retarr['filename'] = $filename;
		$retarr['filepath'] = $filepath;
		$retarr['mobile'] = (isset($params['mobile'])) ? 1 : 0;

		//echo '<pre>'; print_r($retarr); echo '</pre>'; exit;

		return $retarr;
	}
}

/* End of file ActionQueueLib.php */
?>
