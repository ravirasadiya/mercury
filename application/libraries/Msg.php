<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Msg
 */
class Msg {

	/**
	 * @var CI_Controller
     */
	public $ci;
	/**
	 * @var array|mixed
     */
	private $msgarr = array();
	/**
	 * @var bool
     */
	public $disable = false;
	/**
	 * @var bool
     */
	public $flash_disable = false;

	/**
	 *
     */
	function __construct()
	{
		// get ci
		$this->ci =& get_instance();
		
		// set flash session messages
		$msgarr = $this->ci->session->flashdata('PUBLIC_MSGARR');
		$this->msgarr = ($msgarr) ? unserialize($msgarr) : array();
		
		//echo $this->ci->utils->debug_array($this->msgarr);
	}
	
	
	// add message
	/**
	 * @param $msg
	 * @param string $type
	 * @param bool|false $onceoff
     */
	public function add($msg, $type='info', $onceoff=false)
	{
		// flash disable
		if(!$onceoff && ($this->flash_disable || $this->disable)){
			$this->flash_disable = false;
			
			return;
		}
		
		$msg = array('type' => $type, 'msg' => $msg);
		
		if(!in_array($msg, $this->msgarr)){
			$this->msgarr[] = $msg;
		
			if(!$onceoff){
				$this->ci->session->set_flashdata('PUBLIC_MSGARR', serialize($this->msgarr));
			}
		}
	}

	
	// clear
	/**
	 *
     */
	public function clear()
	{
		$this->msgarr = array();
		$this->ci->session->set_flashdata('PUBLIC_MSGARR', serialize($this->msgarr));
	}
	
	
	// display messages
	/**
	 * @return string
     */
	public function display_all()
	{
		$data = '';
		
		foreach($this->msgarr as $msgarr){
			$type = $msgarr['type'];
			$msg = $msgarr['msg'];
			
			$class = 'alert';
			switch($type){
				case 'success':
					$class .= ' alert-success';
				break;
				case 'info':
					$class .= ' alert-info';
				break;
				case 'warning':
					$class .= ' alert-warning';
				break;
				case 'danger':
					$class .= ' alert-danger';
				break;
			}
			
			$data .= '<div class="'.$class.'">';
				$data .= $msg;
				//$data .= '<a href="" class="close">&times;</a>';
			$data .= '</div>';
		}
		
		return $data;
	}
	
	
	// check messages
	public function hasmsg()
	{
		return (count($this->msgarr) ? true: false);
	}
}

/* End of file msg.php */
?>
